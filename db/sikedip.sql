-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 05, 2019 at 05:04 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sikedip`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_kompi`
--

CREATE TABLE `admin_kompi` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `kompi` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_kompi`
--

INSERT INTO `admin_kompi` (`id`, `user`, `kompi`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, '2019-05-03', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `asal_sekolah`
--

CREATE TABLE `asal_sekolah` (
  `id` int(11) NOT NULL,
  `sekolah` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asal_sekolah`
--

INSERT INTO `asal_sekolah` (`id`, `sekolah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'SMKN 2 BLITAR', '2019-05-02', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `asrama`
--

CREATE TABLE `asrama` (
  `id` int(11) NOT NULL,
  `nama_asrama` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asrama`
--

INSERT INTO `asrama` (`id`, `nama_asrama`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'ASRAMA DUA', '2019-05-02', 11, NULL, NULL, 0),
(2, 'ASRAMA PUTRA', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bidang_penghargaan`
--

CREATE TABLE `bidang_penghargaan` (
  `id` int(11) NOT NULL,
  `bidang_penghargaan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bidang_penghargaan`
--

INSERT INTO `bidang_penghargaan` (`id`, `bidang_penghargaan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Bidang Akademik', '2019-04-11', 1, NULL, NULL, 0),
(2, 'Bidang Olahraga dan Seni', '2019-04-11', 1, NULL, NULL, 0),
(3, 'Bidang Organisasi', '2019-04-11', 1, NULL, NULL, 0),
(4, 'Bidang Kerohanian', '2019-04-11', 1, NULL, NULL, 0),
(5, 'Bidang Pengabdian Masyarakat', '2019-04-11', 1, '2019-04-11 05:48:43', 1, 0),
(6, 'Bidang Ekonomi', '2019-04-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chat_admin`
--

CREATE TABLE `chat_admin` (
  `id` int(11) NOT NULL,
  `from` varchar(45) DEFAULT NULL,
  `from_label` varchar(150) NOT NULL,
  `to` int(11) DEFAULT NULL,
  `to_label` varchar(150) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `tgl_kirim` datetime NOT NULL,
  `kompi` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat_manajemen`
--

CREATE TABLE `chat_manajemen` (
  `id` int(11) NOT NULL,
  `from` int(11) DEFAULT NULL,
  `from_label` varchar(150) NOT NULL,
  `to` int(11) DEFAULT NULL,
  `to_label` varchar(150) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `tgl_kirim` datetime NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hubungan_wali`
--

CREATE TABLE `hubungan_wali` (
  `id` int(11) NOT NULL,
  `hubungan` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `creatdeby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hubungan_wali`
--

INSERT INTO `hubungan_wali` (`id`, `hubungan`, `createddate`, `creatdeby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Ayah', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(2, 'Ibu', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(3, 'Adik', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(4, 'Kakak', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(5, 'Teman', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(6, 'Suami', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(7, 'Istri', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(8, 'Saudara', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hukuman`
--

CREATE TABLE `hukuman` (
  `id` int(11) NOT NULL,
  `angka` int(11) DEFAULT NULL,
  `hukuman` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hukuman`
--

INSERT INTO `hukuman` (`id`, `angka`, `hukuman`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 30, 'Teguran Lisan', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(2, 50, 'Teguran Tilis Pertama', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(3, 70, 'Teguran Tulis Kedua', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(4, 90, 'Teguran Tulis Ketiga', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(5, 100, 'Skorsing/Pemberhentian', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(6, 300, 'Tes', '2019-04-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_luar`
--

CREATE TABLE `jadwal_luar` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `jenis_jadwal_luar` int(11) NOT NULL,
  `jam_awal` varchar(50) NOT NULL,
  `jam_akhir` varchar(50) NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `semester` int(11) NOT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `lokasi` varchar(150) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_luar`
--

INSERT INTO `jadwal_luar` (`id`, `taruna`, `jenis_jadwal_luar`, `jam_awal`, `jam_akhir`, `tanggal_awal`, `semester`, `tanggal_akhir`, `lokasi`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 4, 8, '20:30', '', '2019-05-01', 1, '2019-05-01', NULL, 'Keluar', '2019-05-01', 1, '2019-05-01 09:57:31', 1, 1),
(2, 4, 4, '12:30', '03:30', '2019-05-05', 1, '2019-05-05', NULL, 'Cuti Mendadak', '2019-05-05', 1, NULL, NULL, 0),
(3, 4, 4, '12:05', '11:05', '2019-05-17', 1, '2019-05-17', NULL, 'sas', '2019-05-17', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_luar_has_taruna`
--

CREATE TABLE `jadwal_luar_has_taruna` (
  `id` int(11) NOT NULL,
  `jadwal_luar_lain` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `prodi` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_luar_has_taruna`
--

INSERT INTO `jadwal_luar_has_taruna` (`id`, `jadwal_luar_lain`, `taruna`, `prodi`, `semester`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 4, 3, 1, '2019-05-01', 1, '2019-05-04 16:26:24', 1, 0),
(2, 3, 4, 3, 3, '2019-05-09', 1, NULL, NULL, 0),
(3, 4, 4, 3, 3, '2019-05-17', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_luar_lain`
--

CREATE TABLE `jadwal_luar_lain` (
  `id` int(11) NOT NULL,
  `jam_awal` varchar(45) DEFAULT NULL,
  `jam_akhir` varchar(50) NOT NULL,
  `jenis_jadwal_luar` int(11) NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `lokasi` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `prodi` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_luar_lain`
--

INSERT INTO `jadwal_luar_lain` (`id`, `jam_awal`, `jam_akhir`, `jenis_jadwal_luar`, `tanggal_awal`, `lokasi`, `keterangan`, `tanggal_akhir`, `prodi`, `semester`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, '12:05', '', 5, '2019-05-01', NULL, 'Dinas Luar', '2019-05-01', 3, 1, '2019-05-01', 1, '2019-05-04 16:26:24', 1, 0),
(3, '11:15', '20:30', 3, '2019-05-09', NULL, '-', '2019-05-09', 3, 3, '2019-05-09', 1, NULL, NULL, 0),
(4, '01:05', '10:15', 3, '2019-05-17', NULL, 'ssss', '2019-05-17', 3, 3, '2019-05-17', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_rutin`
--

CREATE TABLE `jadwal_rutin` (
  `id` int(11) NOT NULL,
  `hari` varchar(45) DEFAULT NULL,
  `pukul` varchar(45) DEFAULT NULL,
  `kegiatan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_rutin`
--

INSERT INTO `jadwal_rutin` (`id`, `hari`, `pukul`, `kegiatan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Senin s/d Kamis', ' - 05:15', 'Bangun pagi dan sholat subuh', '2019-04-11', 1, '2019-04-11 09:50:20', 1, 1),
(2, 'Senin s/d Kamis', ' - 05:15', 'Bangun pagi dan sholat subuh', '2019-04-11', 1, '2019-04-11 09:50:17', 1, 1),
(3, 'Senin s/d Kamis', '04:30 - 05:15', 'Bangun pagi dan sholat subuh', '2019-04-11', 1, NULL, NULL, 0),
(4, 'Senin s/d Kamis', '05:15 - 06:00', 'Olahraga Pagi (Strecthing/Pemanasan) dan Pembersihan/penataan lingkungan kamar/asrama', '2019-04-24', 1, NULL, NULL, 0),
(5, 'Senin s/d Kamis', '06:00 - 06:30', 'Mandi, dan, Apel Pengibaran Bendera Merah Putih oleh divisi jaga', '2019-04-24', 1, NULL, NULL, 0),
(6, 'Senin s/d Kamis', '06:30 - 07:00', 'Makan Pagi', '2019-04-24', 1, NULL, NULL, 0),
(7, 'Senin s/d Kamis', '07:00 - 07:30', 'Apel pagi', '2019-04-24', 1, NULL, NULL, 0),
(8, 'Senin s/d Kamis', '07:30 - 12:29', 'Kegiatan perkuliahan', '2019-04-24', 1, NULL, NULL, 0),
(9, 'Senin s/d Kamis', '12:29 - 13:30', 'Ishoma', '2019-04-24', 1, NULL, NULL, 0),
(10, 'Senin s/d Kamis', '13:30 - 15:00', 'KegiatanPerkuliahan', '2019-04-24', 1, NULL, NULL, 0),
(11, 'Senin s/d Kamis', '15:00 - 15:30', 'Sholat ashar dan ekstra puding', '2019-04-24', 1, NULL, NULL, 0),
(12, 'Senin s/d Kamis', '15:30 - 16:30', 'KegiatanPerkuliahan', '2019-04-24', 1, NULL, NULL, 0),
(13, 'Senin s/d Kamis', '16:30 - 18:30', 'Kegiatan Ekstrakurikuler, Mandi, solat magrib dan Apel dan Penurunan Bendera Merah Putih oleh divisi jaga', '2019-04-24', 1, NULL, NULL, 0),
(14, 'Senin s/d Kamis', '18:30 - 19:00', 'Makan malam', '2019-04-24', 1, NULL, NULL, 0),
(15, 'Senin s/d Kamis', '19:00 - 19:30', 'Sholat isya', '2019-04-24', 1, NULL, NULL, 0),
(16, 'Senin s/d Kamis', '19:30 - 21:00', 'Wajib belajar', '2019-04-24', 1, NULL, NULL, 0),
(17, 'Senin s/d Kamis', '21:00 - 21:30', 'Apel malam', '2019-04-24', 1, NULL, NULL, 0),
(18, 'Senin s/d Kamis', '21:30 - 22:00', 'Persiapan Istirahat malam', '2019-04-24', 1, NULL, NULL, 0),
(19, 'Senin s/d Kamis', '22:00 - 04:30', 'Istirahat malam', '2019-04-24', 1, NULL, NULL, 0),
(20, 'Jumat', '04:30 - 05:00', 'Bangun pagi dan sholat subuh', '2019-04-24', 1, NULL, NULL, 0),
(21, 'Jumat', '05:00 - 06:00', 'Olahraga Pagi (Strecthing/Pemanasan), Pembersihan/penataan lingkungan kamar/asrama', '2019-04-24', 1, NULL, NULL, 0),
(22, 'Jumat', '06:01 - 07:00', 'Mandi, makan pagi dan Apel Pengibaran Bendera Merah Putih oleh divisi jaga', '2019-04-24', 1, NULL, NULL, 0),
(23, 'Jumat', '07:00 - 07:30', 'Apel pagi', '2019-04-24', 1, NULL, NULL, 0),
(24, 'Jumat', '07:30 - 11:00', 'Kegiatan perkuliahan', '2019-04-24', 1, NULL, NULL, 0),
(25, 'Jumat', '11:00 - 13:30', 'Shalat Jum’at dan makan siang', '2019-04-24', 1, NULL, NULL, 0),
(26, 'Jumat', '13:30 - 15:00', 'Kegiatan perkuliahan', '2019-04-24', 1, NULL, NULL, 0),
(27, 'Jumat', '15:00 - 15:30', 'Sholat ashar dan ekstra puding', '2019-04-24', 1, NULL, NULL, 0),
(28, 'Jumat', '15:30 - 16:30', 'Kegiatan perkuliahan', '2019-04-24', 1, NULL, NULL, 0),
(29, 'Jumat', '16:30 - 18:30', 'Mandi dan sholat Maghrib dan Apel Penurunan Bendera Merah Putih oleh divisi jaga', '2019-04-24', 1, NULL, NULL, 0),
(30, 'Jumat', '18:30 - 19:00', 'Makan malam', '2019-04-24', 1, NULL, NULL, 0),
(31, 'Jumat', '19:00 - 19:30', 'Sholat isya', '2019-04-24', 1, NULL, NULL, 0),
(32, 'Jumat', '19:30 - 21:30', 'Wajib belajar', '2019-04-24', 1, NULL, NULL, 0),
(33, 'Jumat', '21:00 - 21:30', 'Apel malam', '2019-04-24', 1, NULL, NULL, 0),
(34, 'Jumat', '21:30 - 22:00', 'Persiapan Istirahat malam', '2019-04-24', 1, NULL, NULL, 0),
(35, 'Jumat', '22:00 - 04:30', 'Istirahat malam', '2019-04-24', 1, NULL, NULL, 0),
(36, 'Sabtu & Minggu', '04:30 - 05:00', 'Bangun pagi dan sholat subuh', '2019-04-24', 1, '2019-04-24 05:40:49', 1, 0),
(37, 'Sabtu & Minggu', '05:00 - 06:00', 'Olahraga Pagi (Strecthing/Pemanasan), Pembersihan/penataan lingkungan kamar/asrama', '2019-04-24', 1, '2019-04-24 05:40:38', 1, 0),
(38, 'Sabtu & Minggu', '06:00 - 07:00', 'Mandi, makan pagi dan Apel Pengibaran Bendera Merah Putih oleh divisi jaga', '2019-04-24', 1, '2019-04-24 05:41:02', 1, 0),
(39, 'Sabtu & Minggu', '07:00 - 12:00', 'Kegiatan Ekstrakurikuler', '2019-04-24', 1, '2019-04-24 05:41:13', 1, 0),
(40, 'Sabtu & Minggu', '12:00 - 13:00', 'Ishoma', '2019-04-24', 1, '2019-04-24 05:41:23', 1, 0),
(41, 'Sabtu & Minggu', '20:00 - 21:30', 'Apel malam', '2019-04-24', 1, NULL, NULL, 0),
(42, 'Sabtu & Minggu', '21:30 - 22:00', 'Persiapan Istirahat malam', '2019-04-24', 1, NULL, NULL, 0),
(43, 'Sabtu & Minggu', '22:00 - 04:30', 'Istirahat malam', '2019-04-24', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_jadwal_luar`
--

CREATE TABLE `jenis_jadwal_luar` (
  `id` int(11) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_jadwal_luar`
--

INSERT INTO `jenis_jadwal_luar` (`id`, `jenis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Waktu Pesiar', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(2, 'Ijin Bermalam', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(3, 'Libur', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(4, 'Cuti', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(5, 'Dinas Luar', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(6, 'Studi Banding', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(7, 'Praktek Kerja Lapangan', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(8, 'Ijin Khusus', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(9, 'Kegiatan Luar Kampus', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(10, 'Tes', '2019-04-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kelamin`
--

CREATE TABLE `jenis_kelamin` (
  `id` int(11) NOT NULL,
  `kelamin` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_kelamin`
--

INSERT INTO `jenis_kelamin` (`id`, `kelamin`) VALUES
(1, 'Laki - Laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `id` int(11) NOT NULL,
  `kamar` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id`, `kamar`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Kamar 12', '2019-05-02', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_pelanggaran`
--

CREATE TABLE `kategori_pelanggaran` (
  `id` int(11) NOT NULL,
  `kategori` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_pelanggaran`
--

INSERT INTO `kategori_pelanggaran` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Pelanggaran Ringan', NULL, NULL, NULL, NULL, 0),
(2, 'Peelanggaran Berat', NULL, NULL, NULL, NULL, 0),
(3, 'Merokok', '2019-04-10', 1, '2019-04-10 10:29:03', 1, 1),
(4, 'Pelanggaran Khusus', '2019-04-20', 1, '2019-04-20 14:25:20', 1, 0),
(5, 'Pelanggaran Sedang', '2019-04-20', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kesalahan`
--

CREATE TABLE `kesalahan` (
  `id` int(11) NOT NULL,
  `pelanggaran` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `taruna` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesalahan`
--

INSERT INTO `kesalahan` (`id`, `pelanggaran`, `tanggal`, `taruna`, `semester`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 1, '2019-04-12', 4, 1, '-', '2019-04-10', 1, NULL, NULL, 0),
(3, 1, '2019-04-16', 5, 1, '-', '2019-04-11', 1, NULL, NULL, 0),
(4, 215, '2019-05-10', 4, 1, '-', '2019-05-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kesalahan_status`
--

CREATE TABLE `kesalahan_status` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `kesalahan` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'APPROVED\nREJECTED\n',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` varchar(45) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesalahan_status`
--

INSERT INTO `kesalahan_status` (`id`, `user`, `kesalahan`, `status`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(6, 748, 2, 'APPROVED', NULL, '2019-05-06', 748, NULL, NULL, '0'),
(7, 1, 4, 'DRAFT', NULL, '2019-05-10', 1, NULL, NULL, '0'),
(8, 748, 3, 'APPROVED', NULL, '2019-05-10', 748, NULL, NULL, '0'),
(18, 748, 4, 'APPROVED', NULL, '2019-05-10', 748, NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `kompi`
--

CREATE TABLE `kompi` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kompi`
--

INSERT INTO `kompi` (`id`, `nama`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'KOMPI BRIMOB', '2019-05-02', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `propinsi` int(11) NOT NULL,
  `nama_kota` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `propinsi`, `nama_kota`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Surabaya', '2019-05-02', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `log_histori`
--

CREATE TABLE `log_histori` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` varchar(15) DEFAULT NULL,
  `aktifitas` varchar(150) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` varchar(150) NOT NULL,
  `nama_module` varchar(150) DEFAULT NULL,
  `parent` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `nama_module`, `parent`) VALUES
('bidang_penghargaan', 'Bidang Penghargaan', 'Penghargaan'),
('dashboard', 'Dashboard', 'Dashboard'),
('hukuman', 'Hukuman', 'Pelanggaran'),
('izin', 'Daftar Izin', 'Jadwal'),
('jadwal_luar', 'Jadwal Luar', 'Jadwal'),
('jadwal_luar_lain', 'Jadwal Luar Lain', 'Jadwal'),
('jadwal_rutin', 'Jadwal Rutin', 'Jadwal'),
('kategori_pelanggaran', 'Kategori Pelanggaran', 'Pelanggaran'),
('kesalahan', 'Kesalahan', 'Pelanggaran'),
('laporan_izin', 'Laporan Izin', 'Laporan'),
('laporan_kesalahan', 'Laporan Kesalahan', 'Laporan'),
('laporan_penghargaan', 'Laporan Penghargaan', 'Laporan'),
('pelanggaran', 'Pelanggaran', 'Pelanggaran'),
('penghargaan', 'Penghargaan', 'Penghargaan'),
('penghasilan', 'Penghasilan', 'Taruna'),
('point_kesalahan', 'Point Kesalahan', 'Pelanggaran'),
('point_penghargaan', 'Point Penghargaan', 'Penghargaan'),
('prestasi', 'Prestasi', 'Penghargaan'),
('prodi', 'Program Studi', 'Taruna'),
('semester', 'Semester', 'Taruna'),
('taruna', 'Data Taruna', 'Taruna');

-- --------------------------------------------------------

--
-- Table structure for table `module_web`
--

CREATE TABLE `module_web` (
  `id` varchar(150) NOT NULL,
  `nama_module` varchar(150) DEFAULT NULL,
  `parent` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_web`
--

INSERT INTO `module_web` (`id`, `nama_module`, `parent`) VALUES
('bidang_penghargaan', 'Bidang Penghargaan', 'Penghargaan'),
('dashboard', 'Dashboard', 'Dashboard'),
('hukuman', 'Hukuman', 'Pelanggaran'),
('izin', 'Daftar Izin', 'Jadwal'),
('jadwal_luar', 'Jadwal Luar', 'Jadwal'),
('jadwal_luar_lain', 'Jadwal Luar Lain', 'Jadwal'),
('jadwal_rutin', 'Jadwal Rutin', 'Jadwal'),
('kategori_pelanggaran', 'Kategori Pelanggaran', 'Pelanggaran'),
('kesalahan', 'Kesalahan', 'Pelanggaran'),
('laporan_izin', 'Laporan Izin', 'Laporan'),
('laporan_kesalahan', 'Laporan Kesalahan', 'Laporan'),
('laporan_penghargaan', 'Laporan Penghargaan', 'Laporan'),
('pelanggaran', 'Pelanggaran', 'Pelanggaran'),
('penghargaan', 'Penghargaan', 'Penghargaan'),
('penghasilan', 'Penghasilan', 'Taruna'),
('point_kesalahan', 'Point Kesalahan', 'Pelanggaran'),
('point_penghargaan', 'Point Penghargaan', 'Penghargaan'),
('prestasi', 'Prestasi', 'Penghargaan'),
('prodi', 'Program Studi', 'Taruna'),
('semester', 'Semester', 'Taruna'),
('taruna', 'Data Taruna', 'Taruna');

-- --------------------------------------------------------

--
-- Table structure for table `naik_semester`
--

CREATE TABLE `naik_semester` (
  `id` int(11) NOT NULL,
  `prodi` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `semester_sekarang` int(11) DEFAULT NULL,
  `semester_lanjut` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `naik_semester`
--

INSERT INTO `naik_semester` (`id`, `prodi`, `tanggal`, `semester_sekarang`, `semester_lanjut`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 3, '2019-05-14', 1, 3, '2019-05-04', 1, '2019-05-04 16:13:12', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggaran`
--

CREATE TABLE `pelanggaran` (
  `id` int(11) NOT NULL,
  `jenis` text,
  `point` int(11) DEFAULT NULL,
  `kategori_pelanggaran` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggaran`
--

INSERT INTO `pelanggaran` (`id`, `jenis`, `point`, `kategori_pelanggaran`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Merokok', 30, 1, '2019-04-10', 1, '2019-04-20 14:32:55', 1, 1),
(2, 'Kamar tidur, ruang belajar, kamar mandi dan koridor asrama tidak rapi atau kotor', 5, 1, '2019-04-20', 1, '2019-04-20 14:32:53', 1, 1),
(3, 'Kamar tidur, ruang belajar, kamar mandi dan koridor asrama tidak rapi atau kotor', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(4, 'Tata letak perlengkapan tidak sesuai ketentuan peraturan urusan dinas dalam', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(5, 'Menjemur pakaian di tempat yang tidak pada tempatnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(6, 'Memakai gadget elektronik selama tingkat 1', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(7, 'Mencoret atau menempel gambar di kamar tidur/tempat belajar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(8, 'Membuat keributan di dalam asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(9, 'Membawa peralatan olahraga di dalam kamar/lingkungan asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(10, 'Membuang sampah sembarangan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(11, 'Membuang kotoran/sampah di lingkungan Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(12, 'Tidak berada di dalam asrama pada waktu istirahat malam', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(13, 'Berada di asrama pada jam-jam pelajaran', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(14, 'Memakai pakaian dalam atau tidak semestinya di lorong asrama', 5, 1, '2019-04-20', 1, '2019-04-20 15:01:52', 1, 1),
(15, 'Menjemur pakaian di tempat yang tidak pada tempatnya', 5, 1, '2019-04-20', 1, '2019-04-20 15:01:45', 1, 1),
(16, 'Memakai gadget elektronik selama tingkat 1', 5, 1, '2019-04-20', 1, '2019-04-20 15:01:38', 1, 1),
(17, 'Mencoret atau menempel gambar di kamar tidur/tempat belajar', 5, 1, '2019-04-20', 1, '2019-04-20 15:01:07', 1, 1),
(18, 'Membuat keributan di dalam asrama', 5, 1, '2019-04-20', 1, '2019-04-20 15:01:02', 1, 1),
(19, 'Membawa peralatan olahraga di dalam kamar/lingkungan asrama', 5, 1, '2019-04-20', 1, '2019-04-20 15:00:55', 1, 1),
(20, 'Membuang sampah sembarangan', 5, 1, '2019-04-20', 1, '2019-04-20 15:00:47', 1, 1),
(21, 'Memakai pakaian dalam atau tidak semestinya di lorong asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(22, 'Meninggalkan lemari dalam keadaan tidak terkunci', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(23, 'Membawa uang ke asrama yang berlebihan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(24, 'Terlambat membayar uang pendidikan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(25, 'Mengambil uang di bank atau ATM di luar jam yang telah ditentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(26, 'Terlambat membayar uang asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(27, 'Tidak mematikan kran air, lampu dan peralatan listrik lainnya yang merupakan tanggung jawabnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(28, 'Mengubah instalasi/jaringan listrik atau air tanpa izin dari Pengasuh/Petugas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(29, 'Membawa atau menyimpan makanan ke dalam kamar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(30, 'Memakai pakaian di dalam asrama selain peraturan yang telah ditetapkan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(31, 'Membawa/menggunakan/menyimpan peralatan memasak di dalam asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(32, 'Membawa binatang di lingkungan asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(33, 'Terlambat mengikuti kegiatan pendidikan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(34, 'Membawa, menyimpan, bendera / panji–panji tidak pada tempat semestinya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(35, 'Membawa, menyimpan kendaraan pribadi di lingkungan Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(36, 'Menyewa/ Meminjam sepeda motor dan menggunakannya tidak sesuai peraturan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(37, 'Tidak mematuhi Peraturan Lalu Lintas saat berkendara di luar Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(38, 'Mengendarai kendaraan milik dosen/pegawai/instruktur/pengasuh pada hari kuliah', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(39, 'Memelihara kumis, jenggot, jambang, kuku, dan rambut melebihi ketentuan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(40, 'Menggunakan atribut tidak sesuai dengan peraturan yang berlaku', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(41, 'Memakai perhiasan atau asesoris (cincin, kalung, gelang, anting)', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(42, 'Memakai behel tanpa surat keterangan dokter klinik di Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(43, 'Berpacaran yang melanggar norma-norma dan kaidah sosial', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(44, 'Memperlakukan tamu dengan tidak hormat', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(45, 'Menerima tamu di luar waktu yang telah ditentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(46, 'Membunyikan alat-alat musik pada tempat dan waktu yang dapat mengganggu ketenangan orang lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(47, 'Lalai membuat laporan setelah tugas selesai', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(48, 'Lalai mengembalikan buku-buku atau alat-alat pada tempat semula', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(49, 'Lalai menjalankan tugas-tugas yang bersifat non akademik/Ekstra kurikuler', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(50, 'Duduk di tempat yang tidak semestinya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(51, 'Meninggalkan kelas tanpa ijin Dosen/Instruktur/Pengasuh', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(52, 'Membuat keributan di dalam kelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(53, 'Membuang sampah di ruang kelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(54, 'Membuat laporan palsu mengenai jumlah anggota kelasnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(55, 'Meninggalkan kelas tidak teratur atau ribut', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(56, 'Membuat keributan di kelas pada waktu Dosen/Instruktur belum datang / tidak ada di kelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(57, 'Bercanda/bersenda gurau pada waktu dosen/Instruktur memberikan perkuliahan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(58, 'Meninggalkan kelas dengan keadaan tidak rapih dan kotor', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(59, 'Terlambat masuk kelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(60, 'Meninggalkan wajib belajar malam tanpa ijin Pengasuh', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(61, 'Meninggalkan kuliah tanpa ijin Dosen/Instruktur', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(62, 'Tidak menyiapkan sarana pembelajaran saat Dosen/Instruktur akan mengajar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(63, 'Tidur di kelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(64, 'Tidur di asrama pada waktu kegiatan akademik dan non akademik berlangsung tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(65, 'Tidur pada tempat yang tidak pantas / semestinya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(66, 'Tidur di luar waktu yang telah ditentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(67, 'Mencoret-coret meja, kursi atau dinding di Lembaga Diklat Transpotasi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(68, 'Terlambat mengembalikan buku dan peralatan milik Lembaga Diklat Transpotasi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(69, 'Tidak melakukan tugas jaga sesuai dengan jadwal yang telah ditetapkan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(70, 'Terlambat melaksanakan/ mengikuti tugas jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(71, 'Meninggalkan tugas jaga sebelum tugas jaga selesai', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(72, 'Mengganti jaga tanpa sepengetahuan Perwira Jaga (PAGA)', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(73, 'Mengabaikan pengisian buku jurnal jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(74, 'Tidak memakai baju jaga dan atribut sesuai dengan peraturan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(75, 'Tidur pada saat jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(76, 'Tidak menjaga kebersihan pos jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(77, 'Terlambat/mendahului kegiatan makan di ruang makan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(78, 'Membuang makanan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(79, 'Membawa makanan ke luar/ ke dalam ruang makan tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(80, 'Makan, minum sambil berjalan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(81, 'Membuat kegaduhan/keributan di ruang makan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(82, 'Menukar/mengambil makan Taruna/i lain tanpa kesepakatan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(83, 'Makan / minum ditempat yang tidak semestinya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(84, 'Tidak mengikuti makan di ruang makan sesuai jadwal/waktu yang telah di tentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(85, 'Mengambil makanan di ruang makan diluar jam yang telah ditentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(86, 'Berada di kantin pada jam kuliah', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(87, 'Membuat kegaduhan di kantin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(88, 'Tidak tertib dalam melaksanakan pergerakan/ bersikap dalam lingkungan sekolah', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(89, 'Keluar/ Meninggalkan dari barisan tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(90, 'Meninggalkan apel tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(91, 'Meninggalkan apel sebelum apel berakhir', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(92, 'Terlambat apel/ Upacara tanpa alasan yang jelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(93, 'Tidak mengikuti apel dengan sungguh sungguh', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(94, 'Membuat kegaduhan pada waktu apel /Inspeksi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(95, 'Tidak tertib dalam mengikuti pelatihan baris berbaris (PBB)', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(96, 'Berbaris tidak rapi pada saat apel', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(97, 'Tidak melakukan penghormatan saat penaikan dan penurunan bendera merah putih', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(98, 'Membuat laporan palsu mengenai jumlah anggota kamarnya pada saat apel malam', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(99, 'Membubarkan diri dari barisan secara tidak tertib', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(100, 'Mengabaikan perintah / instruksi dari instruktur pengawas atau pembina', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(101, 'Terlambat mengikuti olahraga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(102, 'Tidak mengggunakan pakaian olahraga sebagaimana mestinya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(103, 'Tidak mematuhi instruksi pelatih/instruktur selama mengikuti kegiatan olahraga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(104, 'Tidak mengikuti olah raga pada jam kegiatan olah raga tanpa alasan jelas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(105, 'Olah raga di dalam kamar tidur', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(106, 'Memakai / menyimpan alat-alat olahraga bukan pada tempatnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(107, 'Mengabaikan keselamatan saat melakukan kegiatan olahraga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(108, 'Tidak memakai perlengkapan atribut yang telah di tentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(109, 'Pesiar tanpa ijin/ tidak melapor diri saat keluar dan kembali pesiar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(110, 'Menuliskan keterangan palsu di buku pesiar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(111, 'Tidak mencatat nama pada buku pesiar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(112, 'Pesiar memakai pakaian bebas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(113, 'Melakukan kegiatan ketarunaan di luar Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang  tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(114, 'Menggunakan alat/perlengkapan inventaris sekolah tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(115, 'Menyimpan, membawa, memiliki dan atau menggunakan alat rekam video dan atau gambar dalam bentuk apapun', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(116, 'Tidak menitipkan handphone dan uang ke loker pengasuh taruna setiap pulang dari pesiar atau bermalam dan liburan.', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(117, 'Mengubah arah /menutupi kamera CCTV asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(118, 'Membawa inventaris dapur / ruang makan ke asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(119, 'Menyalahgunakan pemakaian tenaga listrik tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(120, 'Penggunaan laptop untuk bermain game, sosial media, merekam audio video, animasi dan foto tanpa izin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(121, 'Menyimpan, memiliki radio/TV/Video/CD/Modem dan alat musik lainnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(122, 'Memindahkan inventaris kamar tidak sesuai dengan ketentuan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(123, 'Menggunakan sound system (PA) tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(124, 'Menelepon diwaktu kuliah / jam pelajaran', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(125, 'Menggunakan modem di luar jam yang telah ditentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(126, 'Menggunakan peralatan elektronik yang menggunakan listrik asrama', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(127, 'Menggunakan laptop/komputer di luar jam yang telah ditentukan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(128, 'Membawa speaker aktif, stick playstation, aksesoris komputer atau perangkat elektronik berlebihan serta fasilitas lainnya yang dapat mengganggu hak privasi Taruna lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(129, 'Menyalahgunakan dispensasi/surat keterangan dokter', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(130, 'Memakai pakaian dinas tidak sesuai dengan peraturan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(131, 'Memakai pakaian tidak sesuai dengan waktu dan situasi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(132, 'Menempatkan pakaian dinas tidak pada tempatnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(133, 'Tidak memakai pakaian dinas harian (PDH) lengkap pada saat mengikuti aktivitas sehari-hari', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(134, 'Tidak memakai pakaian dinas pesiar (PDP) lengkap pada saat pesiar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(135, 'Tidak memakai pakaian dinas lapangan (PDL) lengkap saat berdinas jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(136, 'Tidak memakai pakaian dinas upacara I (PDU I) lengkap saat ada acara/ upacara kebesaran', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(137, 'Tidak memakai pakaian dinas upacara II (PDU II) lengkap saat ada acara/ upacara kebesaran', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(138, 'Tidak memakai pakaian dinas upacara III (PDU III) lengkap saat ada acara / upacara kebesaran', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(139, 'Memakai pakaian yang tidak sopan di lingkungan Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(140, 'Tidak memakai pakaian dinas olahraga (PDO) pada waktu olahraga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(141, 'Tidak memakai pakaian dinas kerja (PDK) waktu praktek belajar, kerja bakti dan tugas-tugas rutin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(142, 'Tidak memakai pakaian dinas drum band (PDD) pada waktu latihan drumband', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(143, 'Menggantungkan tas di bahu pada saat berpakaian dinas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(144, 'Berpakaian dinas tidak lengkap', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(145, 'Menjual pakaian / perlengkapan dinas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(146, 'Menggantung pakaian tidak pada tempatnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(147, 'Memakai sepatu tanpa di semir dan perlengkapan pakaian dinas yang tidak di braso', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(148, 'Memakai pakaian dinas dengan kancing terbuka', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(149, 'Berpakaian tidak rapi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(150, 'Mengubah bentuk pakaian dinas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(151, 'Memakai pakaian dinas dalam keadaan kotor / tidak rapi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(152, 'Tidak memberikan hormat kepada pengasuh/yang lebih tinggi tingkatannya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(153, 'Tidak bersikap sopan atau tidak menghargai terhadap pengasuh/ yang lebih tinggi tingkatannya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(154, 'Melakukan kegiatan di luar jadwal tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(155, 'Meninggalkan tempat pada waktu inspeksi belum selesai', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(156, 'Bersikap tidak sopan dan melanggar peraturan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(157, 'Tiidak bersikap sempurna pada waktu menghadap taruna yang tingkatnya lebih tinggi, instruktur dan pembina saat berpapasan', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(158, 'Bersikap tidak siap pada waktu inspeksi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(159, 'Menghina / meremehkan orang lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(160, 'Bertindak tidak peduli dengan kehidupan sosial', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(161, 'Tidak memberi contoh / teladan yang baik', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(162, 'Menggunakan nama Korps secara salah atau untuk kepentingan pribadi', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(163, 'Bersikap memihak kepada yang salah', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(164, 'Pura-pura/mengaku sakit', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(165, 'Tidak mematuhi aturan atau tata tertib yang berlaku di poliklinik', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(166, 'Berobat atas nama orang lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(167, 'Meninggalkan poliklinik dalam keadaan sakit tanpa seijin dokter jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(168, 'Keluar masuk poliklinik tanpa seijin petugas atau dokter jaga', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(169, 'Memberikan identitas dan informasi palsu', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(170, 'Menyalahgunakan ijin kegiatan untuk kegiatan lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(171, 'Melakukan kegiatan ekstra kurikuler di luar jadwal yang ditetapkan tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(172, 'Membuat dan menginformasikan berita atau percakapan yang membuat suasana tidak kondusif di media sosial', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(173, 'Tidak memiliki Buku Saku dan Pertibtar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(174, 'Tidak memiliki Kartu identitas Taruna', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(175, 'Memakai sarana dan prasarana Diklat tanpa ijin', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(176, 'Memerintahkan Taruna lain untuk mengisi buku pesiar', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(177, 'Memaksakan kehendak kepada orang lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(178, 'Memberikan perintah di luar wewenangnya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(179, 'Bersikap dan bertindak sewenang-wenang terhadap orang lain', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(180, 'Mengadakan rapat atau briefing tanpa ijin perwira yang bertugas', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(181, 'Tidak melaksanakan atau meneruskan perintah sebagaimana mestinya', 5, 1, '2019-04-20', 1, NULL, NULL, 0),
(182, 'Tidak mengikuti pelajaran yang diwajibkan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(183, 'Merusak dengan sengaja sarana prasarana Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(184, 'Tidak mematuhi ketentuan sebagai pemegang ijin kegiatan akademik dan non akademik', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(185, 'Mengerjakan tugas dan ujian dengan menyontek/menjiplak tugas (kertas kerja) orang lain', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(186, 'Melakukan tindakan perjokian saat ujian', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(187, 'Tidak mengikuti apel/upacara tanpa keterangan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(188, 'Keluar Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang  tanpa ijin', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(189, 'Membuat kegaduhan pada waktu pesiar', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(190, 'Terlambat kembali ke Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang  setelah pesiar / Kegiatan di luar Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang  lainnya tanpa ada alasan yang jelas dan benar', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(191, 'Pesiar dalam keadaan pembinaan Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(192, 'Tidak masuk asrama tanpa alasan yang jelas dan benar', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(193, 'Membawa barang dari luar yang tidak sesuai dengan pertibtar', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(194, 'Membuat kegaduhan / keributan di ruang makan, ruang kelas, ruang tidur dan tempat-tempat lainnya', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(195, 'Mengadakan pesta tanpa ijin di lingkungan Politeknik Transportasi Sungai, Danau dan Penyeberangan Palembang ', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(196, 'Melanggar kode etik taruna/i', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(197, 'Memaksa taruna/i yang lebih rendah tingkatannya membawa uang/makanan/ barang lainnya untuk kepentingan taruna/i yang lebih tinggi tingkatannya', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(198, 'Memanggiltaruna/i yang lebih rendah tingkatannya ke kamar taruna/i yang lebih tinggitingkatannya', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(199, 'Memaksa taruna/i yang lebih rendah tingkatannya untuk melakukan atau berbuat sesuatu untuk kepentingan taruna/i yang lebih tinggi tingkatannya', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(200, 'Memberikan hukuman kepada taruna yang tingkatnya lebih rendah dengan berlebihan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(201, 'Tidak mematuhi perintah Pengasuh/ Instruktur/ Pelatih', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(202, 'Menyimpan/membawa rokok/rokok elektrik di lingkungan Lembaga Diklat Transpotasi', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(203, 'Bersikap dan berkelakuan tidak senonoh', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(204, 'Bertemu Taruna dan taruni tidak pada tempat yang semestinya', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(205, 'Hidup berkelompok sesama suku/daerah di asrama', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(206, 'Mengutamakan dan menonjolkan fanatisme kedaerahan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(207, 'Tidak berani bertanggungjawab terhadap perbuatan yang dilakukan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(208, 'Tidak mau terlibat dalam acara keagamaan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(209, 'Memakai anting untuk Taruna', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(210, 'Memakai cat rambut', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(211, 'Membuat Tatto selama pendidikan', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(212, 'Membawa tamu ke dalam kamar/asrama tanpa ijin', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(213, 'Membawa handphone ke dalam asrama / barak', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(214, 'Membawa uang lebih dari Rp. 500.000,00 ke dalam asrama / barak', 20, 5, '2019-04-20', 1, NULL, NULL, 0),
(215, 'Tidak menghormati agama lain (Mencemooh atau menghina agama orang lain)', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(216, 'Melakukan penghinaan yang menjurus SARA', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(217, 'Tidak memeluk salah satu agama /Atheis', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(218, 'Memaksakan suatu agama kepada orang lain yang telah memeluk agama lain', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(219, 'Mengganggu pelaksanaan ibadah, baik agama sendiri maupun agama orang lain', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(220, 'Membuat keributan/tidak menjaga ketertiban di tempat-tempat ibadah', 20, 2, '2019-04-20', 1, NULL, NULL, 0),
(221, 'Mencemarkan nama baik Lembaga Diklat Transportasi di lingkungan BPSDMP', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(222, 'Menjiplak tugas akhir/skripsi (plagiat)', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(223, 'Terbukti merencanakan dan melakukan tindakan kejahatan (kriminal)', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(224, 'Mengijinkan / membiarkan pelanggaran terjadi dibawah tanggung jawabnya', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(225, 'Menghasut sehingga terjadinya kekacauan/kerusuhan (provokasi)', 50, 5, '2019-04-20', 1, NULL, NULL, 0),
(226, 'Melawan perintah yang bersifat pembentukan karakter', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(227, 'Menghasut yang sifatnya bertentangan dengan fungsi pembentukan karakter', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(228, 'Merusak dengan sengaja sarana dan prasarana ibadah', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(229, 'Merusak dengan sengaja sarana dan prasarana diklat', 50, 1, '2019-04-20', 1, NULL, NULL, 0),
(230, 'Membiarkan kerusuhan / kegaduhan', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(231, 'Berlaku tidak jujur / curang dalam pembayaran', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(232, 'Mengajukan protes tanpa mengindahkan kode etik dan tata cara yang benar', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(233, 'Membawa, memiliki, menyimpan, mengedarkan serta mengkonsumsi media visual berupa gambar/foto/video yang di dalamnya mengandung unsur pornografi', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(234, 'Melakukan perbuatan asusila', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(235, 'Menyembunyikan terjadinya tindak kekerasan terhadap dirinya atau orang lain', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(236, 'Melakukan perkelahian, pemukulan dan segala bentuk penganiayaan serta tindak kekerasan lainnya di dalam maupun di luar Lembaga Diklat Transportasi', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(237, 'Melakukan ancaman/intimidasi terhadap orang lain atau antar sesama taruna', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(238, 'Melakukan diskriminasi/bully terhadap orang lain atau antar sesama taruna', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(239, 'Menyelenggarakan dan mengikuti kegiatan yang melanggar norma agama dan norma sosial', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(240, 'Dengan sengaja merusak inventaris Lembaga Diklat Transportasi', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(241, 'Meninggalkan sekolah lebih dari 3 hari tanpa ijin dari pihak yang berwenang', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(242, 'Membawa lawan jenis ke asrama', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(243, 'Menjadi anggota organisasi terlarang', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(244, 'Memalsukan tanda tangan pejabat struktural/fungsional yang berkaitan dengan pendidikan', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(245, 'Memalsukan tanda tangan orang lain untuk kepentingan pribadi', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(246, 'Memeras/ melakukan segala bentuk pemerasan', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(247, 'Menipu / melakukan segala jenis penipuan', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(248, 'Mencuri / melakukan segala bentuk pencurian', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(249, 'Melanggar peraturan lalu lintas yang mengakibatkan kerugian bagi orang lain', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(250, 'Membuat surat keterangan sakit (surat keterangan lain) palsu', 50, 2, '2019-04-20', 1, NULL, NULL, 0),
(251, 'Melakukan tindak kejahatan yang sudah dalam penanganan pihak berwajib', 100, 4, '2019-04-20', 1, NULL, NULL, 0),
(252, 'Menghilangkan nyawa orang lain', 100, 4, '2019-04-20', 1, NULL, NULL, 0),
(253, 'Pemerkosaan terhadap lawan jenis dan sesama jenis', 100, 4, '2019-04-20', 1, NULL, NULL, 0),
(254, 'Membawa, memiliki, menyimpan, mengedarkan dan mempergunakan obat terlarang (NARKOBA), minuman keras (beralkohol), ataupun senjata api/senjata tajam', 100, 4, '2019-04-20', 1, NULL, NULL, 0),
(255, 'Menikah selama pendidikan atau Berhubungan badan (bersetubuh) dengan lawan jenis maupun sesama jenis', 100, 4, '2019-04-20', 1, NULL, NULL, 0),
(256, 'Melakukan perjudian secara langsung maupun perjudian online', 100, 4, '2019-04-20', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penghargaan`
--

CREATE TABLE `penghargaan` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `prestasi` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penghargaan`
--

INSERT INTO `penghargaan` (`id`, `taruna`, `tanggal`, `prestasi`, `semester`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 4, '2019-04-11', 2, 1, '-', '2019-04-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `penghasilan`
--

CREATE TABLE `penghasilan` (
  `id` int(11) NOT NULL,
  `penghasilan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penghasilan`
--

INSERT INTO `penghasilan` (`id`, `penghasilan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '3.000.000 - 5.000.000', '2019-04-30', 1, NULL, NULL, 0),
(2, '< 1.000.000', '2019-04-30', 1, NULL, NULL, 0),
(3, 'Rp.  2.500.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(4, '-', '2019-04-30', 11, NULL, NULL, 0),
(5, 'Rp.2000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(6, 'Rp.1.800.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(7, '\"5', '2019-04-30', 11, NULL, NULL, 0),
(8, '6/15/1964', '2019-04-30', 11, NULL, NULL, 0),
(9, '7/16/1984', '2019-04-30', 11, NULL, NULL, 0),
(10, '2/9/1979', '2019-04-30', 11, NULL, NULL, 0),
(11, 'Rp2.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(12, 'Rp1.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(13, 'Rp.2.500.000', '2019-04-30', 11, NULL, NULL, 0),
(14, '2.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(15, 'Rp. 3.000.000', '2019-04-30', 11, NULL, NULL, 0),
(16, 'Rp. 2.500.000', '2019-04-30', 11, NULL, NULL, 0),
(17, '10.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(18, '5.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(19, 'Wiraswasta', '2019-04-30', 11, NULL, NULL, 0),
(20, 'Ibu Rumah Tangga', '2019-04-30', 11, NULL, NULL, 0),
(21, '0', '2019-04-30', 11, NULL, NULL, 0),
(22, '\"2', '2019-04-30', 11, NULL, NULL, 0),
(23, '7/7/1969', '2019-04-30', 11, NULL, NULL, 0),
(24, 'Rp. 2.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(25, 'Rp. 0/ bulan', '2019-04-30', 11, NULL, NULL, 0),
(26, 'Rp.5.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(27, '6/23/1968', '2019-04-30', 11, NULL, NULL, 0),
(28, '7/7/1977', '2019-04-30', 11, NULL, NULL, 0),
(29, '\"Rp2', '2019-04-30', 11, NULL, NULL, 0),
(30, '8/20/1972', '2019-04-30', 11, NULL, NULL, 0),
(31, '\"Rp5', '2019-04-30', 11, NULL, NULL, 0),
(32, '10/12/1972', '2019-04-30', 11, NULL, NULL, 0),
(33, '7/5/1968', '2019-04-30', 11, NULL, NULL, 0),
(34, 'Rp.1000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(35, 'Rp.2.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(36, 'anjasarya6@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(37, '1000000', '2019-04-30', 11, NULL, NULL, 0),
(38, 'Rp.3.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(39, 'Rp. 1.800.000', '2019-04-30', 11, NULL, NULL, 0),
(40, 'Guru', '2019-04-30', 11, NULL, NULL, 0),
(41, '\"4', '2019-04-30', 11, NULL, NULL, 0),
(42, '6/1/2019', '2019-04-30', 11, NULL, NULL, 0),
(43, 'diniadeandani28@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(44, '2000000', '2019-04-30', 11, NULL, NULL, 0),
(45, 'Rp.1.500.000', '2019-04-30', 11, NULL, NULL, 0),
(46, '2/26/2019', '2019-04-30', 11, NULL, NULL, 0),
(47, '10/13/2019', '2019-04-30', 11, NULL, NULL, 0),
(48, 'M Hutagalung', '2019-04-30', 11, NULL, NULL, 0),
(49, 'L.Hutasoit', '2019-04-30', 11, NULL, NULL, 0),
(50, '\"3', '2019-04-30', 11, NULL, NULL, 0),
(51, '9/28/1967', '2019-04-30', 11, NULL, NULL, 0),
(52, 'Rp. 1.500.000', '2019-04-30', 11, NULL, NULL, 0),
(53, 'Rp. 2.000.000', '2019-04-30', 11, NULL, NULL, 0),
(54, 'Rp.3.000.000', '2019-04-30', 11, NULL, NULL, 0),
(55, 'Harnadi', '2019-04-30', 11, NULL, NULL, 0),
(56, 'Tri Nurita', '2019-04-30', 11, NULL, NULL, 0),
(57, 'Alm', '2019-04-30', 11, NULL, NULL, 0),
(58, '1500000', '2019-04-30', 11, NULL, NULL, 0),
(59, 'PNS (Pensiun)', '2019-04-30', 11, NULL, NULL, 0),
(60, 'IRT', '2019-04-30', 11, NULL, NULL, 0),
(61, 'Rp1.500. 000', '2019-04-30', 11, NULL, NULL, 0),
(62, 'Rp.2.000.000', '2019-04-30', 11, NULL, NULL, 0),
(63, '5/25/1963', '2019-04-30', 11, NULL, NULL, 0),
(64, '6/4/1964', '2019-04-30', 11, NULL, NULL, 0),
(65, '\"1', '2019-04-30', 11, NULL, NULL, 0),
(66, '8/6/1971', '2019-04-30', 11, NULL, NULL, 0),
(67, '081377680899', '2019-04-30', 11, NULL, NULL, 0),
(68, 'kecamatan ilir barat I', '2019-04-30', 11, NULL, NULL, 0),
(69, 'radinalihsan9@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(70, '1/10/1967', '2019-04-30', 11, NULL, NULL, 0),
(71, '10/3/1970', '2019-04-30', 11, NULL, NULL, 0),
(72, 'Imam Habinajud', '2019-04-30', 11, NULL, NULL, 0),
(73, 'Dewi Soraya', '2019-04-30', 11, NULL, NULL, 0),
(74, 'kode pos 31121\"', '2019-04-30', 11, NULL, NULL, 0),
(75, '4/3/1969', '2019-04-30', 11, NULL, NULL, 0),
(76, 'pancamustikas@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(77, 'Rp. 2.700.000', '2019-04-30', 11, NULL, NULL, 0),
(78, '6/7/1968', '2019-04-30', 11, NULL, NULL, 0),
(79, '11/28/1970', '2019-04-30', 11, NULL, NULL, 0),
(80, '8/17/1967', '2019-04-30', 11, NULL, NULL, 0),
(81, 'Tidak ada', '2019-04-30', 11, NULL, NULL, 0),
(82, 'Rp.2.400.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(83, 'Rp.5.439.000', '2019-04-30', 11, NULL, NULL, 0),
(84, 'Rp. 4.391.500', '2019-04-30', 11, NULL, NULL, 0),
(85, 'Rp. 1.500.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(86, '4/23/1969', '2019-04-30', 11, NULL, NULL, 0),
(87, '5/20/1978', '2019-04-30', 11, NULL, NULL, 0),
(88, '7000000', '2019-04-30', 11, NULL, NULL, 0),
(89, '2.000.000/ bln', '2019-04-30', 11, NULL, NULL, 0),
(90, '', '2019-04-30', 11, NULL, NULL, 0),
(91, '+- Rp 2.000.000', '2019-04-30', 11, NULL, NULL, 0),
(92, 'Rp. 800.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(93, 'Rp 2.500.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(94, 'Rp.8.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(95, 'ratihayuff@gmail.com.com', '2019-04-30', 11, NULL, NULL, 0),
(96, 'adeliasyahira2@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(97, 'PNS', '2019-04-30', 11, NULL, NULL, 0),
(98, 'Rp. 4.576.300/bulan', '2019-04-30', 11, NULL, NULL, 0),
(99, 'Rp. 3.565.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(100, 'Rp.  350.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(101, 'Rp 2.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(102, 'Rp 0/ bulan', '2019-04-30', 11, NULL, NULL, 0),
(103, 'Petani', '2019-04-30', 11, NULL, NULL, 0),
(104, '\"500', '2019-04-30', 11, NULL, NULL, 0),
(105, 'Rp. 4.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(106, '1.500.000-2.000.000', '2019-04-30', 11, NULL, NULL, 0),
(107, 'Rp. 3.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(108, 'Rp. 2.500.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(109, 'Pegawai swasta', '2019-04-30', 11, NULL, NULL, 0),
(110, 'Ibu rumahtangga', '2019-04-30', 11, NULL, NULL, 0),
(111, 'razmijuliapaknawan@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(112, 'TNI AD', '2019-04-30', 11, NULL, NULL, 0),
(113, 'Buruh', '2019-04-30', 11, NULL, NULL, 0),
(114, 'Rp.1.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(115, 'ecfqmrpsdz@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(116, 'panjiasking.rtf@gmail.com', '2019-04-30', 11, NULL, NULL, 0),
(117, 'hasdar.jr07@gmail.con', '2019-04-30', 11, NULL, NULL, 0),
(118, 'Dagang', '2019-04-30', 11, NULL, NULL, 0),
(119, 'Rp. 3.000.0000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(120, 'KODE POS 68416\"', '2019-04-30', 11, NULL, NULL, 0),
(121, 'SUKA WISNU', '2019-04-30', 11, NULL, NULL, 0),
(122, 'Mohamad syamsuddin', '2019-04-30', 11, NULL, NULL, 0),
(123, 'Sandra Turisia', '2019-04-30', 11, NULL, NULL, 0),
(124, 'Rp. 5.000.000/ bulan', '2019-04-30', 11, NULL, NULL, 0),
(125, 'Rp. 5.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(126, '9/28/1972', '2019-04-30', 11, NULL, NULL, 0),
(127, '10/14/1975', '2019-04-30', 11, NULL, NULL, 0),
(128, 'Rp. 1.600.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(129, 'Rp.3.200.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(130, 'Rp . 500.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(131, 'Rp.4.000.000', '2019-04-30', 11, NULL, NULL, 0),
(132, 'Rp 3.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(133, '9/19/1969', '2019-04-30', 11, NULL, NULL, 0),
(134, '9/5/1976', '2019-04-30', 11, NULL, NULL, 0),
(135, 'Awaludin', '2019-04-30', 11, NULL, NULL, 0),
(136, 'Ida miryani', '2019-04-30', 11, NULL, NULL, 0),
(137, 'Rp. 2.100.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(138, 'RP. 0 /bulan', '2019-04-30', 11, NULL, NULL, 0),
(139, 'Wirausaha', '2019-04-30', 11, NULL, NULL, 0),
(140, 'Rp.4.000.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(141, 'Rp.500.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(142, '2/22/1956', '2019-04-30', 11, NULL, NULL, 0),
(143, '6/6/1965', '2019-04-30', 11, NULL, NULL, 0),
(144, 'Rp.7.000.000', '2019-04-30', 11, NULL, NULL, 0),
(145, 'Rp.5.000.000', '2019-04-30', 11, NULL, NULL, 0),
(146, 'Rp.3.000.000 /bulan', '2019-04-30', 11, NULL, NULL, 0),
(147, '500', '2019-04-30', 11, NULL, NULL, 0),
(148, '200', '2019-04-30', 11, NULL, NULL, 0),
(149, 'Rp.0/bulan', '2019-04-30', 11, NULL, NULL, 0),
(150, 'Rp 900.000/bulan', '2019-04-30', 11, NULL, NULL, 0),
(151, 'Imran aditya warman', '2019-04-30', 11, NULL, NULL, 0),
(152, 'Alifa tahulending', '2019-04-30', 11, NULL, NULL, 0),
(153, 'Rp.200.000/bulan', '2019-04-30', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text,
  `ditujukan` varchar(150) DEFAULT NULL COMMENT 'TARUNA\nWALI\nSEMUA',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `judul`, `isi`, `ditujukan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 'Pengumuman A', 'Ada Disnatalis', 'SEMUA', '2019-08-05', 11, '2019-08-05 15:13:06', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `point_kesalahan`
--

CREATE TABLE `point_kesalahan` (
  `id` int(11) NOT NULL,
  `angka` varchar(50) DEFAULT NULL,
  `kondite` varchar(50) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `point_kesalahan`
--

INSERT INTO `point_kesalahan` (`id`, `angka`, `kondite`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '0', 'Teladan (A)', NULL, NULL, NULL, NULL, 0),
(2, '10 - 20', 'Baik Sekali (B)', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(3, '21 - 30', 'Baik (C)', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(4, '31 - 40', 'Kurang (D)', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(5, '41 - 60', 'Sedang (E)', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(6, '> 60', 'Sangat Buruk (F)', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(7, '80', 'Tidak Batas', '2019-04-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `point_penghargaan`
--

CREATE TABLE `point_penghargaan` (
  `id` int(11) NOT NULL,
  `point` int(11) DEFAULT NULL,
  `kondite` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `point_penghargaan`
--

INSERT INTO `point_penghargaan` (`id`, `point`, `kondite`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 100, 'Teladan', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(2, 90, 'Baik Sekali', NULL, NULL, NULL, NULL, 0),
(3, 80, 'Baik', NULL, NULL, NULL, NULL, 0),
(4, 70, 'Cukup Baik', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(5, 50, 'Cukup', '2019-04-07', 1, '2019-04-07 10:00:00', 1, 0),
(6, 1400, 'Tes', '2019-04-11', 1, '2019-04-11 05:37:02', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(11) NOT NULL,
  `bidang_penghargaan` int(11) NOT NULL,
  `prestasi` text,
  `skor` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id`, `bidang_penghargaan`, `prestasi`, `skor`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Peringkat kelas no 1 s/d 5', 25, '2019-04-11', 1, NULL, NULL, 0),
(2, 1, 'Peringkat kelas no 6 s/d 10', 15, '2019-04-11', 1, NULL, NULL, 0),
(3, 1, 'Mengikuti kegiatan akademik yang mewakili sekolah', 15, '2019-04-20', 1, NULL, NULL, 0),
(4, 1, 'Juara I/II/III dalam lomba tingkat Kabupaten', 40, '2019-04-20', 1, NULL, NULL, 0),
(5, 1, 'Juara I/II/III dalam lomba tingkat Propinsi', 60, '2019-04-20', 1, NULL, NULL, 0),
(6, 1, 'Juara I/II/III dalam lomba tingkat Nasional', 80, '2019-04-20', 1, NULL, NULL, 0),
(7, 1, 'Mengadakan/terlibat dalam penelitian bidang IPTEK', 20, '2019-04-20', 1, NULL, NULL, 0),
(8, 2, 'Menjadi anggota tim Drum Band', 10, '2019-04-20', 1, NULL, NULL, 0),
(9, 2, 'Menjadi anggota tim olah raga sekolah', 10, '2019-04-20', 1, NULL, NULL, 0),
(10, 2, 'Menjadi anggota Paskibra tingkat Lembaga Pendidikan danPelatihan/Kabupaten/Provinsi/Nasional', 40, '2019-04-20', 1, NULL, NULL, 0),
(11, 2, 'Panitia dan pengisi pagelaran karya seni/kegiatan olahraga', 5, '2019-04-20', 1, NULL, NULL, 0),
(12, 2, 'Juara I/II/III lomba olah raga/seni tingkat Kabupaten', 40, '2019-04-20', 1, NULL, NULL, 0),
(13, 2, 'Juara I/II/III lomba olah raga/seni tingkat Propinsi', 60, '2019-04-20', 1, NULL, NULL, 0),
(14, 2, 'Juara I/II/III lomba olah raga/seni tingkat Nasional', 80, '2019-04-20', 1, NULL, NULL, 0),
(15, 2, 'Mewakili sekolah dalam kejuaraan olah raga/seni', 15, '2019-04-20', 1, NULL, NULL, 0),
(16, 4, 'Menjadi panitia dalam kegiatan kerohanian', 5, '2019-04-20', 1, NULL, NULL, 0),
(17, 4, 'Sebagai penceramah kegiatan kerohanian', 15, '2019-04-20', 1, NULL, NULL, 0),
(18, 4, 'Juara I/II/III lomba bidang kerohanian tingkat Kabupaten', 40, '2019-04-20', 1, NULL, NULL, 0),
(19, 4, 'Juara I/II/III lomba bidang kerohanian tingkat Propinsi', 60, '2019-04-20', 1, NULL, NULL, 0),
(20, 4, 'Juara I/II/III lomba bidang kerohanian tingkat nasional', 80, '2019-04-20', 1, NULL, NULL, 0),
(21, 4, 'Mewakili sekolah dalam lomba bidang Rohani', 15, '2019-04-20', 1, NULL, NULL, 0),
(22, 5, 'Sebagai penyuluh kegiatan IPTEK', 20, '2019-04-20', 1, NULL, NULL, 0),
(23, 5, 'Kegiatan pembangunan desa', 20, '2019-04-20', 1, NULL, NULL, 0),
(24, 5, 'Kegiatan alih teknologi pada masyarakat', 25, '2019-04-20', 1, NULL, NULL, 0),
(25, 5, 'Membantu pelaksanaan pendidikan dan pelatihan untuk masyarakat', 20, '2019-04-20', 1, NULL, NULL, 0),
(26, 5, 'Menjadi donor darah', 15, '2019-04-20', 1, NULL, NULL, 0),
(27, 5, 'Bakti sosial', 15, '2019-04-20', 1, NULL, NULL, 0),
(28, 5, 'Menggalang bantuan untuk bencana alam/sosial', 20, '2019-04-20', 1, NULL, NULL, 0),
(29, 5, 'Menyelenggarakan kegiatan yang menghasilkan nilai tambah ekonomi (keuntungan) untuk Korps Taruna', 10, '2019-04-20', 1, NULL, NULL, 0),
(30, 5, 'Menyelenggarakan pelelangan', 10, '2019-04-20', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'Admin'),
(2, 'Wali Murid'),
(3, 'Taruna'),
(4, 'Superadmin'),
(5, 'Manajemen'),
(6, 'Supervisor');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `id` int(11) NOT NULL,
  `prodi` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id`, `prodi`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'TEKNIK', NULL, NULL, NULL, NULL, 1),
(2, 'GENCATAN SENJATA', '2019-04-10', 1, '2019-04-10 09:47:35', 1, 1),
(3, 'DIII STUDI NAUTIKA', '2019-04-24', 1, NULL, NULL, 0),
(4, 'DIII TEKNOLOGI NAUTIKA', '2019-04-24', 1, NULL, NULL, 0),
(5, 'DIII PENGELOLAAN PELABUHAN', '2019-04-24', 1, NULL, NULL, 0),
(6, 'D III  LLASDP', '2019-05-01', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--

CREATE TABLE `propinsi` (
  `id` int(11) NOT NULL,
  `propinsi` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`id`, `propinsi`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Jawa Timur', '2019-05-02', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `psikologi`
--

CREATE TABLE `psikologi` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `taruna` int(11) NOT NULL,
  `jam` varchar(45) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `psikologi`
--

INSERT INTO `psikologi` (`id`, `tanggal`, `taruna`, `jam`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '2019-05-02', 4, '12:35', '- Hasil Pemerisakaan Bagus', '2019-05-02', 1, NULL, NULL, 0),
(2, '2019-05-23', 4, '01:10', 'Psikologi', '2019-05-05', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_admin`
--

CREATE TABLE `role_admin` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `save` int(11) DEFAULT '1',
  `update` int(11) DEFAULT '1',
  `delete` int(11) DEFAULT '1',
  `detail` int(11) DEFAULT '1',
  `link` text,
  `module` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_admin`
--

INSERT INTO `role_admin` (`id`, `user`, `status`, `save`, `update`, `delete`, `detail`, `link`, `module`, `deleted`, `updateddate`, `updatedby`, `createddate`, `createdby`) VALUES
(1, 12, 1, 1, 1, 1, 1, NULL, 'dashboard', 0, NULL, NULL, '2019-05-06', 11),
(2, 12, 1, 1, 1, 1, 1, NULL, 'jadwal_luar', 0, NULL, NULL, '2019-05-06', 11),
(3, 12, 1, 1, 1, 1, 1, NULL, 'jadwal_luar_lain', 0, NULL, NULL, '2019-05-06', 11),
(4, 12, 1, 1, 1, 1, 1, NULL, 'jadwal_rutin', 0, NULL, NULL, '2019-05-06', 11),
(5, 12, 1, 1, 1, 1, 1, NULL, 'laporan_izin', 0, NULL, NULL, '2019-05-06', 11),
(6, 12, 1, 1, 1, 1, 1, NULL, 'laporan_kesalahan', 0, NULL, NULL, '2019-05-06', 11),
(7, 12, 1, 1, 1, 1, 1, NULL, 'kesalahan', 0, NULL, NULL, '2019-05-06', 11),
(8, 12, 1, 1, 1, 1, 1, NULL, 'penghargaan', 0, NULL, NULL, '2019-05-06', 11),
(9, 12, 1, 1, 1, 1, 1, NULL, 'semester', 0, NULL, NULL, '2019-05-06', 11);

-- --------------------------------------------------------

--
-- Table structure for table `role_priveledge`
--

CREATE TABLE `role_priveledge` (
  `id` int(11) NOT NULL,
  `priveledge` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `save` int(11) DEFAULT '1',
  `update` int(11) DEFAULT '1',
  `delete` int(11) DEFAULT '1',
  `detail` int(11) DEFAULT '1',
  `link` text,
  `module` varchar(255) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_priveledge`
--

INSERT INTO `role_priveledge` (`id`, `priveledge`, `status`, `save`, `update`, `delete`, `detail`, `link`, `module`, `updateddate`, `updatedby`, `createddate`, `createdby`) VALUES
(1, 1, 1, 1, 1, 1, 1, '', 'dashboard', '2019-04-30 14:49:12', 11, NULL, NULL),
(2, 1, 1, 1, 1, 1, 1, NULL, 'taruna', '2019-04-30 14:49:12', 11, NULL, NULL),
(3, 1, 0, 1, 1, 1, 1, NULL, 'prodi', '2019-04-30 14:49:12', 11, NULL, NULL),
(4, 1, 0, 1, 1, 1, 1, '', 'semester', '2019-04-30 14:49:12', 11, NULL, NULL),
(5, 1, 0, 1, 1, 1, 1, '', 'hukuman', '2019-04-30 14:49:12', 11, NULL, NULL),
(6, 1, 0, 1, 1, 1, 1, '', 'kategori_pelanggaran', '2019-04-30 14:49:12', 11, NULL, NULL),
(7, 1, 0, 1, 1, 1, 1, '', 'pelanggaran', '2019-04-30 14:49:12', 11, NULL, NULL),
(8, 1, 0, 1, 1, 1, 1, '', 'point_kesalahan', '2019-04-30 14:49:12', 11, NULL, NULL),
(10, 1, 1, 1, 1, 1, 1, '', 'kesalahan', '2019-04-30 14:49:12', 11, NULL, NULL),
(11, 1, 0, 1, 1, 1, 1, '', 'point_penghargaan', '2019-04-30 14:49:12', 11, NULL, NULL),
(12, 1, 0, 1, 1, 1, 1, '', 'bidang_penghargaan', '2019-04-30 14:49:12', 11, NULL, NULL),
(13, 1, 0, 1, 1, 1, 1, '', 'prestasi', '2019-04-30 14:49:12', 11, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, '', 'penghargaan', '2019-04-30 14:49:12', 11, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, NULL, 'jadwal_luar', '2019-04-30 14:49:12', 11, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, '', 'jadwal_rutin', '2019-04-30 14:49:12', 11, NULL, NULL),
(17, 1, 0, 1, 1, 1, 1, '', 'izin', '2019-04-30 14:49:12', 11, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, '', 'laporan_kesalahan', '2019-04-30 14:49:12', 11, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, '', 'laporan_penghargaan', '2019-04-30 14:49:12', 11, NULL, NULL),
(20, 1, 1, 1, 1, 1, 1, '', 'laporan_izin', '2019-04-30 14:49:12', 11, NULL, NULL),
(21, 2, 0, 1, 1, 1, 1, NULL, 'bidang_penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(22, 2, 1, 1, 1, 1, 1, NULL, 'dashboard', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(23, 2, 0, 1, 1, 1, 1, NULL, 'hukuman', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(24, 2, 0, 1, 1, 1, 1, NULL, 'izin', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(25, 2, 0, 1, 1, 1, 1, NULL, 'jadwal_luar', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(26, 2, 0, 1, 1, 1, 1, NULL, 'jadwal_rutin', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(27, 2, 0, 1, 1, 1, 1, NULL, 'kategori_pelanggaran', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(28, 2, 0, 1, 1, 1, 1, NULL, 'kesalahan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(29, 2, 1, 1, 1, 1, 1, NULL, 'laporan_izin', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(30, 2, 1, 1, 1, 1, 1, NULL, 'laporan_kesalahan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(31, 2, 1, 1, 1, 1, 1, NULL, 'laporan_penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(32, 2, 0, 1, 1, 1, 1, NULL, 'pelanggaran', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(33, 2, 0, 1, 1, 1, 1, NULL, 'penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(34, 2, 0, 1, 1, 1, 1, NULL, 'point_kesalahan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(35, 2, 0, 1, 1, 1, 1, NULL, 'point_penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(36, 2, 0, 1, 1, 1, 1, NULL, 'prestasi', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(37, 2, 0, 1, 1, 1, 1, NULL, 'prodi', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(38, 2, 0, 1, 1, 1, 1, NULL, 'semester', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(39, 2, 0, 1, 1, 1, 1, NULL, 'taruna', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(40, 3, 0, 1, 1, 1, 1, NULL, 'bidang_penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(41, 3, 1, 1, 1, 1, 1, NULL, 'dashboard', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(42, 3, 0, 1, 1, 1, 1, NULL, 'hukuman', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(43, 3, 0, 1, 1, 1, 1, NULL, 'izin', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(44, 3, 0, 1, 1, 1, 1, NULL, 'jadwal_luar', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(45, 3, 0, 1, 1, 1, 1, NULL, 'jadwal_rutin', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(46, 3, 0, 1, 1, 1, 1, NULL, 'kategori_pelanggaran', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(47, 3, 0, 1, 1, 1, 1, NULL, 'kesalahan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(48, 3, 1, 1, 1, 1, 1, NULL, 'laporan_izin', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(49, 3, 1, 1, 1, 1, 1, NULL, 'laporan_kesalahan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(50, 3, 1, 1, 1, 1, 1, NULL, 'laporan_penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(51, 3, 0, 1, 1, 1, 1, NULL, 'pelanggaran', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(52, 3, 0, 1, 1, 1, 1, NULL, 'penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(53, 3, 0, 1, 1, 1, 1, NULL, 'point_kesalahan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(54, 3, 0, 1, 1, 1, 1, NULL, 'point_penghargaan', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(55, 3, 0, 1, 1, 1, 1, NULL, 'prestasi', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(56, 3, 0, 1, 1, 1, 1, NULL, 'prodi', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(57, 3, 0, 1, 1, 1, 1, NULL, 'semester', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(58, 3, 0, 1, 1, 1, 1, NULL, 'taruna', '2019-04-30 14:49:12', 11, '2019-04-15', 1),
(62, 1, 0, 1, 1, 1, 1, NULL, 'penghasilan', '2019-04-30 14:49:12', 11, '2019-04-30', 11),
(63, 2, 0, 1, 1, 1, 1, NULL, 'penghasilan', '2019-04-30 14:49:12', 11, '2019-04-30', 11),
(64, 3, 0, 1, 1, 1, 1, NULL, 'penghasilan', '2019-04-30 14:49:12', 11, '2019-04-30', 11),
(65, 1, 1, 1, 1, 1, 1, NULL, 'jadwal_luar_lain', '2019-04-30 14:49:12', 11, '2019-04-30', 11),
(66, 2, 0, 1, 1, 1, 1, NULL, 'jadwal_luar_lain', '2019-04-30 14:49:12', 11, '2019-04-30', 11),
(67, 3, 0, 1, 1, 1, 1, NULL, 'jadwal_luar_lain', '2019-04-30 14:49:12', 11, '2019-04-30', 11);

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `semester` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `semester`, `deleted`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 0, NULL, NULL, NULL, NULL),
(2, 2, 0, NULL, NULL, NULL, NULL),
(3, 3, 0, NULL, NULL, NULL, NULL),
(4, 4, 0, NULL, NULL, NULL, NULL),
(5, 5, 0, NULL, NULL, NULL, NULL),
(6, 6, 0, NULL, NULL, NULL, NULL),
(7, 7, 0, NULL, NULL, NULL, NULL),
(8, 8, 0, NULL, NULL, NULL, NULL),
(9, 9, 0, NULL, NULL, NULL, NULL),
(10, 10, 0, NULL, NULL, NULL, NULL),
(11, NULL, 1, '2019-04-10', 1, '2019-04-10 10:07:15', 1),
(12, NULL, 1, '2019-04-10', 1, '2019-04-10 10:07:07', 1),
(13, 11, 0, '2019-04-10', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seperasuhan`
--

CREATE TABLE `seperasuhan` (
  `id` int(11) NOT NULL,
  `seperasuhan` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seperasuhan`
--

INSERT INTO `seperasuhan` (`id`, `seperasuhan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'A', '2019-05-02', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna`
--

CREATE TABLE `taruna` (
  `id` int(11) NOT NULL,
  `no_taruna` varchar(150) DEFAULT NULL,
  `foto` text,
  `user` int(11) NOT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `jenis_kelamin` int(11) NOT NULL,
  `tempat_lahir` varchar(150) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `npwp` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `email` text NOT NULL,
  `sim` varchar(255) DEFAULT NULL,
  `gol_darah` varchar(45) DEFAULT NULL,
  `propinsi` int(11) NOT NULL,
  `kota` int(11) NOT NULL,
  `asal_sekolah` int(11) NOT NULL,
  `kompi` int(11) NOT NULL,
  `seperasuhan` int(11) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna`
--

INSERT INTO `taruna` (`id`, `no_taruna`, `foto`, `user`, `no_ktp`, `jenis_kelamin`, `tempat_lahir`, `nama`, `npwp`, `alamat`, `no_hp`, `email`, `sim`, `gol_darah`, `propinsi`, `kota`, `asal_sekolah`, `kompi`, `seperasuhan`, `tanggal_lahir`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(4, '1318033', 'images.png', 7, '131313', 1, 'Blitar', 'Dodik Rismawan Affrudin', '', 'Blitar', '+6285748233712', 'dodikitn@gmail.com', '', 'O', 1, 1, 1, 1, 1, '2019-04-22', '2019-04-10', 1, '2019-05-05 10:12:55', 1, 0),
(5, '1318031', 'Screenshot from 2019-04-09 13-42-30.png', 9, '113', 1, 'Malang', 'Abel Halomoan K. Tambunan', '', '-', '+62', '', '', 'A', 1, 1, 1, 1, 1, '1994-12-10', '2019-04-11', 1, '2019-05-14 07:43:28', 1, 0),
(6, '160418', NULL, 13, '7471050110960002', 1, 'Kendari', 'Esa sakti al qadri amanu', '-', 'Jln.  Bunga duri 1 no 23a. Kel. Lahundape.  Kec. Kendari barat.  Kota Kendari provinsi sulawesi tenggara', '085210294901', 'esasaktialqadri@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1996-10-01', '2019-05-01', 11, NULL, NULL, 0),
(7, '1604025', NULL, 16, '1671076502990010', 2, 'Palembang', 'Gita Farera Monica', '-', 'Jl.Cempaka RT.09 RW.02 NO.049 Purwodadi Kecamatan.Sukarami Kelurahan.Sukodadi Palembang 30154', '081770460156', 'gitafmhr@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-02-25', '2019-05-01', 11, NULL, NULL, 0),
(8, '160434', NULL, 19, '1671084805990002', 2, 'Palembang', 'Melya Yolanda', '', 'Jl. Bakung X No. 33 RT. 39 RW. 16 Kelurahan Sialang Kecamatan Sako Perumnas Palembang. Kode pos : 30163', '081278512585', 'melya160434@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-05-08', '2019-05-01', 11, NULL, NULL, 0),
(9, '1604041', NULL, 22, '1171010903960001', 1, 'Banda aceh', 'Musrinal liyanda', '', '\"Jl.mesjid taqwa no 29', 'desa seutui', 'kec.baiturrahman kota banda aceh kode pos 23243\"', NULL, NULL, 0, 0, 0, 0, 0, '1996-03-09', '2019-05-01', 11, NULL, NULL, 0),
(10, '1604028', NULL, 25, '1601144310980005', 2, 'Kotabumi', 'Intan Permata Sari', '', 'Jl. Mayor toyib lr.nangke no 15 rt1 rw5', '082372546738', 'intanpsari03@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-03', '2019-05-01', 11, NULL, NULL, 0),
(11, '160422', NULL, 28, '1671075302990007', 2, 'Palembang', 'Febydina Arinda', '', 'Jalan Sukabangun 2 Jalan S.parman Lorong Prima RT 10 RW 02 kelurahan sukajaya kecamatan sukarami 30151 Kota Palembang', '082175539707', 'febydina13@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-02-13', '2019-05-01', 11, NULL, NULL, 0),
(12, '1604020', NULL, 31, '1671096704990002', 2, 'Palembang', 'Farranisa Rayhanna', '', 'Jalan.balayudha no 310-1107 rt.012 rw.004 kel. Ario kemuning kec.kemuning kode pos 30128', '0895353402143', 'farranisarayhanna94@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-27', '2019-05-01', 11, NULL, NULL, 0),
(13, '1604027', NULL, 34, '3207091003950002', 1, 'Ciamis', 'Herisyana Nurahman', '', 'Dusun Singandaru RT 01/08 Desa Kawalimukti Kec. Kawali Kab. Ciamis. Jawa Barat. 46253', '081323414193', 'herisyana160427@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1995-03-10', '2019-05-01', 11, NULL, NULL, 0),
(14, '1604020', NULL, 37, '1671096704990002', 2, 'Palembang', 'Farranisa Rayhanna', '', 'Jalan.balayudha no 310-1107 rt.012 rw.004 kel. Ario kemuning kec.kemuning kode pos 30128', '0895353402143', 'farranisarayhanna94@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-27', '2019-05-01', 11, NULL, NULL, 0),
(15, '160424', NULL, 40, '1671075201980006', 2, 'Palembang', 'Fitria Cahyanti', '', 'Jalan Sukabangun II komp PLN blok G-4 RT/RW 003/001 kel. Sukajaya kec. Sukarami Palembang 30151', '0711-416773', 'fitria.chynti@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-01-12', '2019-05-01', 11, NULL, NULL, 0),
(16, '1604048', NULL, 43, '1610026207982001', 2, 'Palembang', 'SELLA ISLAMIAH', '', '\"Jl. Sayid Makdum RT003/RW002 Kel. Tanjung Batu Kec. Tanjung Batu Kab. Ogan Ilir', '30664\"', '085768162536', NULL, NULL, 0, 0, 0, 0, 0, '1998-07-22', '2019-05-01', 11, NULL, NULL, 0),
(17, '160429', NULL, 46, '1671134803000003', 2, 'Palembang', 'Islamiati Rianto', '', 'Jalan kimerogan No. 540A Rt. 09 Rw. 02 Kemas Rindo Kertapati Palembanv', '081271306841', 'riantoislamiati@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-08', '2019-05-01', 11, NULL, NULL, 0),
(18, '1604032', NULL, 49, '1471072511970002', 1, 'Pekanbaru', 'M. Aviv Alieffandra', '', 'Jl. T.Bey Gg.Taxi Sei mintan No:1', '081279732561', 'apipalieffandra@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-11-25', '2019-05-01', 11, NULL, NULL, 0),
(19, '160455', NULL, 52, '1673015603980001', 2, 'LUBUKLINGGAU', 'Trissa Dhea Marlita', '', 'Jalan Pinangsia no. 68 rt. 06 kelurahan watervang kecamatan lubuklinggau timur 1 kota lubuklinggau kode pos 31628', '085368195779', 'trissadhea03@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-03-16', '2019-05-01', 11, NULL, NULL, 0),
(20, '160437', NULL, 55, '1671102008980005', 1, 'Palembang', 'M.Rivaldo Al-Hafidz', '', 'Jl.Sambung Rasa Blok B.3 Komplek Taman Pondok Indah Kenten Permai 1 Kecamatan Kalidoni Kelurahan Bukit Sangkal Palembang', '082281263149', 'alhafidzrivaldo@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-20', '2019-05-01', 11, NULL, NULL, 0),
(21, '1604023', NULL, 58, '1671092901990001', 1, 'Palembang', 'Fikri miftahurrahman', '', '\"Jln.pelita no.574 rt/rw:006/002', 'kelurahan ario kemuning', 'kecamatan kemuninh\"', NULL, NULL, 0, 0, 0, 0, 0, '1999-01-29', '2019-05-01', 11, NULL, NULL, 0),
(22, '1604001', NULL, 61, '1671091204970009', 1, 'Mangun Jaya', 'Achmad Jayadi Harjo', '', 'Jl. Let. Simanjuntak lr. Lebak Mulyo No. 1390 RT.021 RW.008 Kel. Pahlawan Kec. Kemuning Palembang (30126)', '+62082185948664', 'achmadjharjo@gmail.com', '', 'A', 1, 1, 1, 1, 1, '1997-04-12', '2019-05-01', 11, '2019-05-13 02:51:56', 1, 0),
(23, '1604052', NULL, 64, '1671046211980005', 2, 'Palembang', 'Syadza Yasminra', '', 'Jalan Kapten A Rivai Lrg Masjid II No 32 RT 01 RW 02', '082178282581 / 082182683216', 'syadzayasminra52@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-11-22', '2019-05-01', 11, NULL, NULL, 0),
(24, '160445', NULL, 67, '1606012005980003', 1, 'SEKAYU', 'Rizky putra wijaya', '', 'Griya randik blok b1 no 11 kelurahan kayuara kecamatan sekayu 30711', '081366688988', 'rizkipw220@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-05-20', '2019-05-01', 11, NULL, NULL, 0),
(25, '160430', NULL, 70, '1601142107980004', 1, 'Baturaja', 'Jogie Prasetya', '', 'Jl. Kol. Wahab Sarobu no.0294 lr. Kamboja RT.2 RW.1 kel. Sekar Jaya Kec. Baturaja Timur 32111', '081279063575', 'jogie.prasetya@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-07-21', '2019-05-01', 11, NULL, NULL, 0),
(26, '1604038', NULL, 73, '1408011402980001', 1, 'Duri', 'M.Suhendra Alfitra', '', 'Jl.lingkungan RT.016 RW.05 Kelurahan.Kampung Dalam Kecamatan.Siak', '082373555588', 'msuhendraalfitra@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-02-14', '2019-05-01', 11, NULL, NULL, 0),
(27, '160405', NULL, 76, '3311062307980004', 1, 'Sukoharjo', 'Anjas Arya Bagaskara', '', '\"Toriyo RT 02 RW 03', 'Toriyo', 'Bendosari', NULL, NULL, 0, 0, 0, 0, 0, '1998-07-23', '2019-05-01', 11, NULL, NULL, 0),
(28, '160417', NULL, 79, '1671106904970003', 2, 'palembang', 'eprilia eka putri', '', 'jalan brigjen hasan kasim rt 44 rw 9 no 40 kel bukit sangkal kec kalidoni', '082175261770', 'epriliaekaputri@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-04-29', '2019-05-01', 11, NULL, NULL, 0),
(29, '1604056', NULL, 82, '1671044701980004', 2, 'Palembang', 'Zazkia ramadhani', '', 'Jalan demang lebar daun no.54 RT/RW 047/014 kelurahan lorok pakjo kecamatan ilir barat I kode pos:30137', '085100490762', 'zazkiaramadhani160456@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-01-07', '2019-05-01', 11, NULL, NULL, 0),
(30, '1604046', NULL, 85, '1671146412980003', 2, 'Palembang', 'Rizqi Nabilah', '', 'Jl. kopral Urip lr. Banyu biru 2 no 44 RT 02 RW 01 kelurahan talang putri kecamatan plaju', '0821-7857-4946', 'rizqinabilah26@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-12-24', '2019-05-01', 11, NULL, NULL, 0),
(31, '1604012', NULL, 88, '1607104110980001', 2, 'Palembang', 'Devi Octa Saputri', '', 'Komplek taman sari 2 blok M nomor 2 RT.012 RW.003 Kecamatan Talang Kelapa', '082177056982', 'devioctasaputri@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-01', '2019-05-01', 11, NULL, NULL, 0),
(32, '1604011', NULL, 91, '1671035812980005', 2, 'palembang', 'deska uliyani', '', '\"jalan megamendung no 315', 'rt.  25 rw.007 kelurahan sentosa kecamatan seberan', '0895384892091', NULL, NULL, 0, 0, 0, 0, 0, '1998-12-19', '2019-05-01', 11, NULL, NULL, 0),
(33, '1604035', NULL, 94, '1671071406980016', 1, 'Palembang', 'M. NOOR RIZKI HIDAYAT', '', 'JL. H. Sanusi Lr. Mekar I RT. 33 RW.05 Kel. Suka bangun Kec. Sukarami Kota Palembang ( 30151 )', '081278969425', 'mnoorrizkih@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-06-14', '2019-05-01', 11, NULL, NULL, 0),
(34, '160413', NULL, 97, '1673076809980008', 2, 'LUBUKLINGGAU', 'DINI ADE ANDANI', '', '\"JL. ARJUNA', 'RT. 007', 'KEL. MARGA MULYA', NULL, NULL, 0, 0, 0, 0, 0, '1998-09-28', '2019-05-01', 11, NULL, NULL, 0),
(35, '160414', NULL, 100, '1674020512980009', 1, 'Prabumulih', 'Dio Deski Putra Maros', '', 'Jl. bukit sulap No 68 kel. Muara dua Kec. Prabumulih timur', '085268836800', 'diodeski.pm@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-12-05', '2019-05-01', 11, NULL, NULL, 0),
(36, '1604053', NULL, 103, '5204050907980002', 1, 'Mataram', 'Teguh Imansyah', '', 'Rt 02 Rw 01 Dusun Dalam Desa Dalam Kecamatan Alas Kabupaten Sumbawa-NTB', '083114003460', 'tegar.maulana03@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-07-09', '2019-05-01', 11, NULL, NULL, 0),
(37, '1604007', NULL, 106, '1208095506990002', 2, 'Sarimatondang', 'Astry Cicilia Damanik', '', '\"Jl.Mesjid Sarimatondang', 'Sidamanik', '21171\"', NULL, NULL, 0, 0, 0, 0, 0, '2019-06-15', '2019-05-01', 11, NULL, NULL, 0),
(38, '1604009', NULL, 109, '1671086912980004', 2, 'Palembang', 'Christin Natalia Hutagalung', '', '\"Jln tulang bawang 6 rambutan 10 no 2386 RT.35', 'RW.06', 'Kel.lebung gajah', NULL, NULL, 0, 0, 0, 0, 0, '1998-12-29', '2019-05-01', 11, NULL, NULL, 0),
(39, '160404', NULL, 112, '1671151310980001', 1, 'Palembang', 'Anggit satria wibawa', '', 'Jl. Sultan hasanudin No.2525 A Rt.38 Rw.11 Kel.Karya Baru Kec.Alang-Alang Lebar Kode Pos: 30151', '089679420950', 'anggitwibawa1@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-13', '2019-05-01', 11, NULL, NULL, 0),
(40, '160419', NULL, 115, '1607090503990002', 1, 'Upang', 'Fani afrizal', '', 'Komp. Bumi mas indah blok. CC 03 RT. 34 RW. 03 kel. Tanah mas kec. Talang kelapa 30961', '081343291833', 'faniafrizal160419@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-03-05', '2019-05-01', 11, NULL, NULL, 0),
(41, '1604015', NULL, 118, '3275051303970005', 1, 'BEKASI', 'Dodi lando sihombing', '', 'Perum mutiara gading timur 2 blok n26/36', '082175908776', 'dlando48@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-03-13', '2019-05-01', 11, NULL, NULL, 0),
(42, '1604002', NULL, 121, '1607011003980003', 1, 'Palembang', 'Akbar Maulana', '-', '\"Jalan Petaling RT/RW 006/01', 'Kelurahan Mariana', 'Kecamatan Banyuasin 1', NULL, NULL, 0, 0, 0, 0, 0, '1998-03-10', '2019-05-01', 11, NULL, NULL, 0),
(43, '1604039', NULL, 124, '1671140607980004', 1, 'Palembang', 'Muhammad juliansyah', '', '\"Jl kapten robbani kadir rt 13 rw 04 kelurahan talang putri kecamatan plaju darat', '30268\"', '081238296151', NULL, NULL, 0, 0, 0, 0, 0, '1998-07-06', '2019-05-01', 11, NULL, NULL, 0),
(44, '1604031', NULL, 127, '1601080503960004', 1, 'GUNUNG KURIPAN', 'MARTIN ALAN BUANA', '', 'JL.LINTAS SUMATERA KM.45 DS.GUNUNG KURIPAN KEC.PENGANDONAN KAB.OKU 32155', 'O82306321214', 'martinalanbuana505@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1996-03-05', '2019-05-01', 11, NULL, NULL, 0),
(45, '160450', NULL, 130, '1607104908980002', 2, 'Palembang', 'SHINTA AULIA TRIANY', '', '\"Komplek Alam Sako Baru Blok. E No. 12. Kelurahan Sako Baru', 'jln Sako Baru. Palembang\"', '081369123569', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-09', '2019-05-01', 11, NULL, NULL, 0),
(46, '1604021', NULL, 133, '1607107011980004', 2, 'Palembang', 'Fathia Rahmania', '', 'Komp megah asri 2 blok e6 no 11 rt 35 rw 12', '089637294140', 'fathiarahmania2@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-11-30', '2019-05-01', 11, NULL, NULL, 0),
(47, '160433', NULL, 136, '1601146505980007', 2, 'Baturaja', 'Meila Permata Sari', '', 'JL.KAPT.M.NUR No 257A RT 004 RW 002 Kelurahan Sukaraya Kecamatan Baturaja Timur. Kode Pos 32112', '081367356713', 'meilapermata12@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-05-25', '2019-05-01', 11, NULL, NULL, 0),
(48, '1604036', NULL, 139, '1671052210980003', 1, 'palembang', 'M.rama farhansyah', '', 'jl mayor santoso no 1467 rt.23 rw.009 kelurahan. 20 ilir D III kecamatan ilir timur 1', '082186739743', 'farhansyah2281@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-22', '2019-05-01', 11, NULL, NULL, 0),
(49, '160408', NULL, 142, '1671024209980003', 2, 'PALEMBANG', 'BELLA BIDASWARA', '', '\"PERUM OPI JL. TEMBESU BLOK N NO.45 RT.46/RW.14 KELURAHAN 15 ULU', 'KECAMATAN SEBERANG ULU I', 'KODE POS: 30257\"', NULL, NULL, 0, 0, 0, 0, 0, '1998-09-02', '2019-05-01', 11, NULL, NULL, 0),
(50, '1604054', NULL, 145, '1671025205980007', 2, 'Palembang', 'Tri Meyla Damayanti', '', 'Jalan kh wahid hasyim lr. Kedukan no915 rt/rw:025:005 kelurahan 5 ulu darat kecamatan seberang ulu 1 kode pos: 30254', '083173610596', 'meyladamayanti@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-05-28', '2019-05-01', 11, NULL, NULL, 0),
(51, '160416', NULL, 148, '1671075610980005', 2, 'Bireuen Aceh', 'Ella Sofani', '', '\"Jalan Perindustrian 2 Komplek Tirta Pesona Indah D4 km9', 'RT 053', 'RW 001', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-17', '2019-05-01', 11, NULL, NULL, 0),
(52, '1604044', NULL, 151, '1671040909980008', 1, 'PALEMBANG', 'RADINAL IHSAN', '', '\"Jl. Pdam', 'Lr. Mandi api', 'no 114', NULL, NULL, 0, 0, 0, 0, 0, '1998-09-09', '2019-05-01', 11, NULL, NULL, 0),
(53, '1604003', NULL, 154, '12721040708980002', 1, 'Pematangsiantar', 'Albert Bagas Siallagan', '', '\"Jl. Kisaran II No. 55 Kelurahan Martimbang', 'Kecamatan Siantar Selatan', 'Pematangsiantar 21125\"', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-07', '2019-05-01', 11, NULL, NULL, 0),
(54, '1604047', NULL, 157, '1106201004960001', 1, 'Banda aceh', 'Ryan Haviz', '', '\"Dusun teungoh', 'desa Lampineung', 'kecamatan Baitussalam', NULL, NULL, 0, 0, 0, 0, 0, '1996-04-01', '2019-05-01', 11, NULL, NULL, 0),
(55, '1604026', NULL, 160, '1674056208980001', 2, 'Prabumulih', 'Gustini Dwijaya', '', '\"Jalan Jendral Sudirman', 'Lorong Lematang No: 49', 'RT: 02', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-22', '2019-05-01', 11, NULL, NULL, 0),
(56, '160443', NULL, 163, '2172020106980005', 1, 'TANJUNGPUNANG', 'Panca Mustika Syahputra', '', '\"JLN. nusantara km 13 GG. murai 2 no 25', 'RW 011 /RT 002', 'Kel BATU IX', NULL, NULL, 0, 0, 0, 0, 0, '1998-06-01', '2019-05-01', 11, NULL, NULL, 0),
(57, '1604042', NULL, 166, '6404042002980001', 1, 'BONE', 'NUGRA PRATAMA', '', '\"PROV. KALIMANTAN UTARA', 'KAB. BULUNGAN', 'DESA TANAH KUNING RT 2 / RW 1\"', NULL, NULL, 0, 0, 0, 0, 0, '1998-02-20', '2019-05-01', 11, NULL, NULL, 0),
(58, '1604056', NULL, 169, '1671044701980004', 2, 'Palembang', 'Zazkia ramadhani', '', 'Jalan demang lebar daun no.54 RT/RW 047/014 kelurahan lorok pakjo kecamatan ilir barat I kode pos:30137', '085100490762', 'zazkiaramadhani160456@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-01-07', '2019-05-01', 11, NULL, NULL, 0),
(59, '160451', NULL, 172, '1671055101970002', 2, 'Salatiga', 'Suci Rizki Putri', '', 'komp.Garuda Putra 1 blok D no.21 RT009 RW003 kec.Ilir timur 1', '082175957423', 'sucirizkyputri@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-01-11', '2019-05-01', 11, NULL, NULL, 0),
(60, '160440', NULL, 175, '1671042903980004', 1, 'Palembang', 'Muhammad Putra Rayyan', '', 'JL.INSP.MARZUKI Lr. DAMAI IV/18 RT 001 RW 009 DESA SIRING AGUNG KECAMATAN ILIR BARAT 1 PALEMBANG SUMATERA SELATAN', '083173808197', 'muhammadputrarayyan@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-03-29', '2019-05-01', 11, NULL, NULL, 0),
(61, '1604049', NULL, 178, '1607104510980004', 2, 'Palembang', 'Sheila Trinanda S', '', 'Megahasri 2 blok h9 no.2', '08127820573', 'tnandasheila@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-05', '2019-05-01', 11, NULL, NULL, 0),
(62, '1704045', NULL, 181, '1671074106990010', 2, 'palembang', 'MAHARANI PRATIWI', '', 'prindustian 2 tirta mutiara indah block ae 16', '081377726117', 'pratiwimaharani11@icloud.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-06-01', '2019-05-01', 11, NULL, NULL, 0),
(63, '1704013', NULL, 184, '1471085101990001', 2, 'Sungai penuh', 'Dwi rahma qonita', 'Tidak ada', 'Jl. HR soebrantas gg ijen no 97 rt/rw 003/004 kel.sidomulyo barat kec.tampan', '085219001519', 'dwirahma18@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-11-01', '2019-05-01', 11, NULL, NULL, 0),
(64, '1704042', NULL, 187, '1671025010990028', 2, 'Palembang', 'Oktaria Putri', '', 'Perum OPI Lr jati II Blok M No70 RT45 RW 14 Kelurahan 15ulu Kecamatan Seberang ulu 1 jakabaring Palembang 30257', '082378082909', 'oktariaputri60@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-10', '2019-05-01', 11, NULL, NULL, 0),
(65, '1704056', NULL, 190, '1671076902000003', 2, 'Palembang', 'Shafira riza azahra', '', 'Jalan ade irma suryani blok L komplek rumah tumbuh RT. 03 RT. 08 Muara enim 31311', '087765976518', 'sfrarza.sra@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-29', '2019-05-01', 11, NULL, NULL, 0),
(66, '1704039', NULL, 193, '1603021204980004', 1, 'Tanjung Serian', 'Muhammad Reza Putrawan', '', 'Jl Raya Palembang Ds 2 Desa Tanjung Serian Kec Muara Enim Kode Pos 31351', '085378311898', 'rezaputrawan12@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-04-12', '2019-05-01', 11, NULL, NULL, 0),
(67, '1704057', NULL, 196, '1607106510990001', 2, 'Palembang', 'SHERIN AMALIA SAHANI', 'Tidak ada', '\"JL. Ahmad Yani', 'Desa Air Batu', 'Rt 07 Rw 03 kel. Air Batu kec. Talang kelapa. 30961\"', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-25', '2019-05-01', 11, NULL, NULL, 0),
(68, '1704016', NULL, 199, '1674021702980004', 1, 'Prabumulih', 'Febrian Dwi Samantha', '', 'Jl. Flores no.50 RT 01 RW 01 kel. Gunung Ibul barat Kec. Prabumulih Timur  kota prabumulih sumatera selatan Kode pos : 31117', '0821-8373-4851', 'ebi.samantha@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-02-17', '2019-05-01', 11, NULL, NULL, 0),
(69, '1704013', NULL, 202, '1471085101990001', 2, 'Sungai penuh', 'Dwi rahma qonita', 'Tidak ada', 'Jl. Hr soebrantas gg ijen no 97 rt 03 rw 04 kel. Sidomulyo barat kec. Tampan', '085219001519', 'dwirahma18@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-01-11', '2019-05-01', 11, NULL, NULL, 0),
(70, '1704064', NULL, 205, '1604106503000003', 2, 'Lahat', 'Wanda Mutiara Anom', 'Tidak ada', '\"Jalan lintas pagar alam', 'Prumnas Muara Siban Blok D 5  Dusun IV', 'kecamatan Gumay Talang', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-25', '2019-05-01', 11, NULL, NULL, 0),
(71, '1704001', NULL, 208, '1610021809990001', 1, 'Seri Tanjung', 'Abdi Permana', '', 'Jl. Merdeka Ds. II RT/RW 003/000 Desa Seri Tanjung Kec. Tanjung Batu Kab. Ogan Ilir', '082177404027', 'abdipermana57@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-09-18', '2019-05-01', 11, NULL, NULL, 0),
(72, '1704033', NULL, 211, '1671104711980005', 2, 'Banyuwangi', 'Monica vlavia fernanda', '', '\"Komplek PT. Pusri jalan bayam no.2', 'sei.selayur', 'kalidoni', NULL, NULL, 0, 0, 0, 0, 0, '1998-11-07', '2019-05-01', 11, NULL, NULL, 0),
(73, '1704049', NULL, 214, '9208015602000006', 2, 'Fakfak', 'Ratih Ayu Febrianty Fraim', '', '\"Jl Brawijaya 2/ jalan baru belakang', 'desa Kaimana kota', 'rt.028/rw.0', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-16', '2019-05-01', 11, NULL, NULL, 0),
(74, '1704002', NULL, 217, '1609060202000001', 2, 'OKU Timur', 'Adelia febriani nur syahira', '', '\"Dsn.Jelabat', 'Ds.Serbaguna', 'RT.001/RW.001', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-01', '2019-05-01', 11, NULL, NULL, 0),
(75, '1704047', NULL, 220, '1606016812990001', 2, 'Palembang', 'Rahmaniar Destianti', '', 'Komplek Perumnas Jalan Unglen Blok B No. 34 Rt/Rw 04/02 Kelurahan Balai Agung Kecamatan Sekayu Kabupaten Musi Banyuasin Provinsi Sumatera Selatan Kode Pos 30711', '081379111207', 'rahmaniar.d@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-12-28', '2019-05-01', 11, NULL, NULL, 0),
(76, '1704017', NULL, 223, '9203016601960001', 2, 'Fak fak', 'Hardani Sagas', '', 'Kampung Sekru RT. 04 Distrik Pariwari. Kode pos 0956', '08535382766543', 'marwa260196@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1996-01-26', '2019-05-01', 11, NULL, NULL, 0),
(77, '1704020', NULL, 226, '1671084505990004', 2, 'Palembang', 'Indriana', '', 'Komplek griya mutiara indah blok c.10 rt 33 rw 09 kecamatan sako kelurahan sukamaju kodepos 30164', '085273033518', 'iindrianagunawan@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-05-05', '2019-05-01', 11, NULL, NULL, 0),
(78, '1704046', NULL, 229, '1671070203980004', 1, 'Palembang', 'Rahmad Hidayat', '', 'Jalan Tanjung Raya RT 24 RW 05 Nomor 1577 Kelurahan Sukodadi Kecamatan Sukarami Palembang 30154', '082175566324', 'rahmadhidayat2398@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-03-02', '2019-05-01', 11, NULL, NULL, 0),
(79, '1704021', NULL, 232, '9202294512940001', 2, 'Waroser', 'Irma Sapari', '', '\"Desa Waroser', 'RT/RW 002/001\"', '6282182614821', NULL, NULL, 0, 0, 0, 0, 0, '1995-12-05', '2019-05-01', 11, NULL, NULL, 0),
(80, '1704021', NULL, 235, '9202294512940001', 2, 'Waroser', 'Irma Sapari', '', '\"Desa Waroser', 'RT/RW 002/001\"', '6282182614821', NULL, NULL, 0, 0, 0, 0, 0, '1995-12-05', '2019-05-01', 11, NULL, NULL, 0),
(81, '1704043', NULL, 238, '9203024110990002', 2, 'Fakfak', 'Orli Royani Woy', 'Tidak ada', 'Jl.Torea-werba Fakfak Papua Barat', '081366394329', 'royaniwoy@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-01', '2019-05-01', 11, NULL, NULL, 0),
(82, '1704038', NULL, 241, '1671042503000007', 1, 'Palembang', 'M Reza Pahlevi PB', '-', 'Jl Sei Betung no 57B RT 01 RW 06 kelurahan Siring agung kecamatan Ilir barat 1 Palembang 30138', '089627120321', 'Rezapahlevi2543@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-25', '2019-05-01', 11, NULL, NULL, 0),
(83, '1704006', NULL, 244, 'Kristen katholik', 2, '\"Bintuni', 'Arnolda Manibuy', '9206014107980020', '', '\"ARGOSIGEMERAI', 'RT 002/ RW 003', NULL, NULL, 0, 0, 0, 0, 0, '1970-01-01', '2019-05-01', 11, NULL, NULL, 0),
(84, '1704061', NULL, 247, '1605195609990003', 2, 'Sumber Harta', 'VHIOLETTA S ADRIANTI', '', 'jl. Raya Sumber Harta Rt 17 Kel. Sumber Harta Kec. Sumber Harta Kab. Musi Rawas', '082288546322', 'vhioletta.speed33@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-09-16', '2019-05-01', 11, NULL, NULL, 0),
(85, '1704044', NULL, 250, '1709061611990001', 1, 'Bengkulu', 'Pandu Wicaksono', '', '\"Jl. Pariwisata Bengkulu-Curup Kel. Taba Penanjung kec. Taba Penanjung Kab. Bengkulu Tengah', 'Bengkulu 38386\"', '082183026532', NULL, NULL, 0, 0, 0, 0, 0, '2019-11-16', '2019-05-01', 11, NULL, NULL, 0),
(86, '1704063', NULL, 253, '1704012008990002', 1, 'Palembang', 'Wahyu Hadinata', '', '\"Jl gelam', 'RT/RW 025/005', 'Kel. Mariana', NULL, NULL, 0, 0, 0, 0, 0, '1999-08-20', '2019-05-01', 11, NULL, NULL, 0),
(87, '1704050', NULL, 256, '9271055307980001', 2, 'Sorong(Papua Barat)', 'Razmi Julia Paknawan', '', '\"Asrama korem 171/PVT', 'klagete', '001/001', NULL, NULL, 0, 0, 0, 0, 0, '2019-07-13', '2019-05-01', 11, NULL, NULL, 0),
(88, '1704018', NULL, 259, '1671071706970010', 1, 'Palembang', 'Hariyanto', '', '\"JL. TPA sukawinatan LR. Perum.  Griya Mutiara 2 No. 125 Rt. 36 Rw. 10 kec. Sukarami kel. Sukajaya palembang', 'sumatera selatan 30151\"', '082280722223', NULL, NULL, 0, 0, 0, 0, 0, '1997-06-17', '2019-05-01', 11, NULL, NULL, 0),
(89, '1704022', NULL, 262, '1209210606990001', 1, 'BANDAR SELAMAT', 'JERI FAMILY LUBIS', '', '\"DUSUN I', '000/000', 'kel AEKSONGSONGAN', NULL, NULL, 0, 0, 0, 0, 0, '1999-06-06', '2019-05-01', 11, NULL, NULL, 0),
(90, '1704002', NULL, 265, '1609060202000001', 2, 'OKU Timur', 'Adelia febriani nur syahira', '', '\"Dsn.Jelabat', 'Ds.Serbaguna', 'RT.001/RW.001', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-01', '2019-05-01', 11, NULL, NULL, 0),
(91, '1704041', NULL, 268, '7301016307990001', 2, 'SELAYAR', 'NUR WAHDANIYA ARNAS', '', '\"Jln. SOEKARNO HATTA NO.1', 'BENTENG', '004/001', NULL, NULL, 0, 0, 0, 0, 0, '2019-06-23', '2019-05-01', 11, NULL, NULL, 0),
(92, '1704030', NULL, 271, '9201074912990001', 1, 'Sorong', 'Muhammad Ircham Ramadhan Nur Badewi', '', '\"Jl.Trikora', 'Aimas', '001/003', NULL, NULL, 0, 0, 0, 0, 0, '2019-12-09', '2019-05-01', 11, NULL, NULL, 0),
(93, '1704028', NULL, 274, '9208010509980002', 1, 'Kaimana', 'Muhammad Hasdar juddar', '', '\"Jl. Perindustrian', 'Kaimana kota', '009/000', NULL, NULL, 0, 0, 0, 0, 0, '1998-09-05', '2019-05-01', 11, NULL, NULL, 0),
(94, '1704065', NULL, 277, '1604165107000001', 2, 'Keban Agung', 'Yueyi  Andara', 'Tidak ada', '\"Jl. Gumai ulu km 1 desa muara siban', 'lahat\"', '081272168327', NULL, NULL, 0, 0, 0, 0, 0, '2019-07-11', '2019-05-01', 11, NULL, NULL, 0),
(95, '1704060', NULL, 280, '9270011805970003', 1, 'Sorong', 'Vernando bisay', '', '\"Jln.perikanan klademak 2 pantai', 'kota sorong papua barat', '001/005', NULL, NULL, 0, 0, 0, 0, 0, '1997-05-18', '2019-05-01', 11, NULL, NULL, 0),
(96, '1704035', NULL, 283, '1671060503000008', 1, 'Palembang', 'M. Rizky Rewa Gamara', '', 'Jl. Sultan agung lr pendidikan 2 rt 12 rw 03 no 514 kel 1 ilir kec it 2 palembang', '081274159521', 'rewagamara3@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-05', '2019-05-01', 11, NULL, NULL, 0),
(97, '1704031', NULL, 286, '3510160509980005', 1, 'BANYUWANGI', 'MOHAMMAD ALFAN FERDIANSAH', '', '\"JL. JAKSA AGUNG SUPRAPTO GG. YUDHISTIRA NO.10', 'DS. WELARAN', 'RT03/RW03', NULL, NULL, 0, 0, 0, 0, 0, '1998-09-05', '2019-05-01', 11, NULL, NULL, 0),
(98, '1704031', NULL, 289, '3510160509980005', 1, 'BANYUWANGI', 'MOHAMMAD ALFAN FERDIANSAH', '', '\"JL. JAKSA AGUNG SUPRAPTO GG. YUDHISTIRA NO.10', 'DS. WELARAN', 'RT03/RW03', NULL, NULL, 0, 0, 0, 0, 0, '1998-09-05', '2019-05-01', 11, NULL, NULL, 0),
(99, '1704032', NULL, 292, '1601142212980007', 1, 'Baturaja', 'Mohamad Syarif Ramadhan', '', '\"Jalan gotong royong lintas sumatera lorog keluarga no.1 kemalaraja', 'baturaja timur', 'rt21 rw5', NULL, NULL, 0, 0, 0, 0, 0, '1998-12-22', '2019-05-01', 11, NULL, NULL, 0),
(100, '1704034', NULL, 295, '1671042405990007', 1, 'Palembang', 'M. Ridho Alfaridzi', '', '\"Jl. Inspektur marzuki', 'griya permata pakjo blok d no.5 rt.5 rw.8', 'kelurahan siring agung', NULL, NULL, 0, 0, 0, 0, 0, '1999-05-24', '2019-05-01', 11, NULL, NULL, 0),
(101, '1704026', NULL, 298, '1671070808980016', 1, 'Palembang', 'M. Duwi Cahyo', '-', 'Dusun 3 Desa Sukarami kecamatan sekayu kode pos 30711', '082179224608', 'cduwi37@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-08', '2019-05-01', 11, NULL, NULL, 0),
(102, '1704027', NULL, 301, '1671030610990001', 1, 'Palembang', 'M FAJRI', '', 'Jl.Kh.azhari 12 ulu lr.pedatuan darat RT.09 RW.02 NO.250 kecamatan seberang ulu II KODE POS :30262', '081368973355', 'fajri8009@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-06', '2019-05-01', 11, NULL, NULL, 0),
(103, '1704040', NULL, 304, '1671075011990005', 2, 'Palembang', 'Noventa Wijaya Ayuningtia', '', 'RSS Srijaya Blok J7 RT 28 RW 09 Kelurahan Srijaya Kecamatan Alang-alang Lebar Palembang Kode Pos 30153', '0895637174264', 'noventawijaya@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-11-10', '2019-05-01', 11, NULL, NULL, 0),
(104, '1704008', NULL, 307, '1671030310980003', 1, 'Palembang', 'DEAFRIHATINO', '', 'jl jaya 4 no 1388 rt 25 rw 08 kelurahan 16 ulu kecamatan SU II 30265', '081280085665', 'deafrihatino03@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-10-03', '2019-05-01', 11, NULL, NULL, 0),
(105, '1704009', NULL, 310, '1607013112970001', 1, 'Plaju', 'Denis Afandi', '', '\"Jl.Sabar Jaya', 'Prumnas Raswari Rt.04 Rw.01 Mariana Ilir', 'Banyuasin 1\"', NULL, NULL, 0, 0, 0, 0, 0, '1997-12-31', '2019-05-01', 11, NULL, NULL, 0),
(106, '1704005', NULL, 313, '1671034709980001', 2, 'PALEMBANG', 'ANISA TIARA', '78. 196.872.2-306.000', 'Jl. A YANI JAYA II NO.1516 RT 27 RW 08 KEL 16 ULU KEC. SU II PLAJU-PALEMBANG 30265', '085758504300', 'anisatiara98@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-07-09', '2019-05-01', 11, NULL, NULL, 0),
(107, '1704019', NULL, 316, '9271032806990001', 1, 'Sorong Papua Barat', 'Hizkia Joshua Sambil', '', 'Jalan Danau Wam No : 34 KPR Pepabri RT : 003 RW: 004', '087797622957', 'hizkiajho19@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-06-28', '2019-05-01', 11, NULL, NULL, 0),
(108, '1704054', NULL, 319, '1607015303000003', 2, 'Banyuasin', 'Roissatul kamila', '', 'Jalan sabar jaya rt.18 rw.004 no 1100A', '081373448280', 'roissatulkamila43@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-13', '2019-05-01', 11, NULL, NULL, 0),
(109, '1704036', NULL, 322, '1671070305990005', 1, 'Palembang', 'Muhammad Rusedky', '', 'Taman sari 4 km 6 plg', '082176921361', 'ekyrusedky3@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-05-03', '2019-05-01', 11, NULL, NULL, 0),
(110, '1704026', NULL, 325, '1671070808980016', 1, 'Palembang', 'M. Duwi Cahyo', '-', 'Dusun 3 Desa Sukarami kecamatan sekayu kode pos 30711', '082179224608', 'cduwi37@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-08', '2019-05-01', 11, NULL, NULL, 0),
(111, '1704037', NULL, 328, '1603072407990002', 1, 'tanjung enim', 'Muhammad Daffa', '', 'jl. kemas sp waras no102A RT001 RW 010 kel. tanjung enim kec. lawang kidul kode pos:31712', '087788984300', 'daffah35@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-07-24', '2019-05-01', 11, NULL, NULL, 0),
(112, '1704029', NULL, 331, '1671131209950003', 1, 'Palembang', 'M ikhsan irsyandi', 'Tidak ada', '\"Jln Meranti RT.56 RW.10 Kelurahan Ogan Baru', 'Kecamatan Kertapati', 'Palenbang 30258\"', NULL, NULL, 0, 0, 0, 0, 0, '1995-09-12', '2019-05-01', 11, NULL, NULL, 0),
(113, '1704058', NULL, 334, '1671101504990008', 1, 'Palembang', 'SUTIONO WIDODO', '', 'Jl.mayzen lrg.margoyoso no.09 rw.03 no.09 kec.kalidoni kel.sei selayur kode pos: 30119', '085267064319', 'sutionowidodo15@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-15', '2019-05-01', 11, NULL, NULL, 0),
(114, '1704059', NULL, 337, '6101012809960004', 1, 'Sanggau Ledo', 'Tri Siwi Agung', '', 'Jl.damai kel. Sungai Garam hilir Rt/Rw 010/003 kec. Singkawang Utara 73400', '089694220078', 'siwiagung96@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1996-09-28', '2019-05-01', 11, NULL, NULL, 0),
(115, '1704012', NULL, 340, '1671071312990015', 1, 'PALEMBANG', 'DWIKY ARMI RAMADON', '', 'JALAN HAJI SANUSI LORONG KOPRAL SLAMET NO.2793A KELURAHAN SUKA BANGUN KECAMATAN SUKARAMI. KODE POS: 30151', '085840336913', 'dwijyarmi23@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-12-13', '2019-05-01', 11, NULL, NULL, 0),
(116, '1704007', NULL, 343, '1671141705990007', 1, 'Bandar Lampung', 'Budi kusuma', '', '\"Jl di panjaitan', 'lr sunia no 46', 'rt 35', NULL, NULL, 0, 0, 0, 0, 0, '1999-05-17', '2019-05-01', 11, NULL, NULL, 0),
(117, '1704053', NULL, 346, '1607010904990004', 1, 'MUSI BANYUASIN', 'RIZKY APRIAN', '', 'PERUMNAS GRIYA SEJAHTERA BLOK G.10 RT/RW:03/01 KELURAHAN MARIANA ILIR KECAMATAN BANYUASIN 1 KODEPOS:30763', '08970818853', 'rizky_aprian@ymail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-09', '2019-05-01', 11, NULL, NULL, 0),
(118, '1704015', NULL, 349, '1671065811990005', 2, 'Lombok', 'ERIKA DINTA SARI', '', '\"Jln Blabak No.55 Rt 28 Rw 15 kelurahan 3 ilir', 'kecamatan ilir timur II kota palembang kode pos 30', '085381708672', NULL, NULL, 0, 0, 0, 0, 0, '1999-11-18', '2019-05-01', 11, NULL, NULL, 0),
(119, '1704048', NULL, 352, '1701112609960004', 1, 'MANNA', 'RAKYAN KUSUMA', '', '\"JALAN SERSAN M TAHA', 'RT. 007/RW. 003', 'KEL. KETAPANG BESAR', NULL, NULL, 0, 0, 0, 0, 0, '1996-09-26', '2019-05-01', 11, NULL, NULL, 0),
(120, '1704022', NULL, 355, '1209210606990001', 1, 'BANDAR SELAMAT', 'JERI FAMILY LUBIS', '', '\"DUSUN I', '000/000', 'kel AEKSONGSONGAN', NULL, NULL, 0, 0, 0, 0, 0, '1999-06-06', '2019-05-01', 11, NULL, NULL, 0),
(121, '1704052', NULL, 358, '1602034311990003', 2, 'Pedamaran', 'Reza Wantri Ayu', '', 'Jl. Merdeka RT.06 RW.02 Desa Pedamaran 3 Kec.Pedamaran kode pos 30672', '082246737386', 'rezawanogizaka@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-11-03', '2019-05-01', 11, NULL, NULL, 0),
(122, '1704004', NULL, 361, '1802080512990009', 1, 'Rejo Asri', 'Ahzar Taufik Susilo', '', '\"Dusun 01 RT/RW 003/001 Rejo Asri', 'Kec. Seputih Raman', 'Lampung Tengah\"', NULL, NULL, 0, 0, 0, 0, 0, '1999-12-05', '2019-05-01', 11, NULL, NULL, 0),
(123, '1704011', NULL, 364, '1607035202000005', 2, 'Pangkalan balai', 'DINI ROSMALIZA', '', 'JALAN RIOSELI NO.15 RT.09 RW.04 KEL PANGKALAN BALAI KEC BANYUASIN III KAB BANYUASIN', '??+62 831 81983278??', 'drosmaliza@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-02', '2019-05-01', 11, NULL, NULL, 0),
(124, '1704023', NULL, 367, '9104017006990004', 2, 'Nabire', 'Juniar mauren gabriela waray', '', 'Jl.lembah hijau', '081284809503', 'juniarmauren40603@gmail.com.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-03-06', '2019-05-01', 11, NULL, NULL, 0),
(125, '1704062', NULL, 370, '9207014804980001', 2, 'Rado', 'Viladora delvia manaruri', '', 'Jln.Raya wasior/kampung Rado/Wasior kabupaten teluk wondama/98346', '081344575014', 'viladoradelviamanaruri@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-04-08', '2019-05-01', 11, NULL, NULL, 0),
(126, '1704046', NULL, 373, '1671070203980004', 1, 'Palembang', 'Rahmad Hidayat', '', 'Jalan Tanjung Raya RT 24 RW 05 Nomor 1577 Kelurahan Sukodadi Kecamatan Sukarami Palembang 30154', '082175566324', 'rahmadhidayat2398@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-03-02', '2019-05-01', 11, NULL, NULL, 0),
(127, '1704003', NULL, 376, '1603022410990002', 1, 'Palembang', 'Agung Hidayatullah', '', '\"Komp.Perumahan Patal Blok H No. 4', '009/003', 'Talang Aman', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-24', '2019-05-01', 11, NULL, NULL, 0),
(128, '1704024', NULL, 379, '1208091302990002', 1, 'Balimbingan', 'Kevin s meliala', '', '\"Emplasmen sidamanik', 'kecamatan sidamanik', 'kabupaten simalungun', NULL, NULL, 0, 0, 0, 0, 0, '1999-02-13', '2019-05-01', 11, NULL, NULL, 0),
(129, '1704055', NULL, 382, '9203016202990001', 2, 'Fakfak', 'Sausan fadilah usman', '', '\"Jl R.A kartini no 8', 'RT 11', 'kelurahan fakfak selatan', NULL, NULL, 0, 0, 0, 0, 0, '1999-02-22', '2019-05-01', 11, NULL, NULL, 0),
(130, '1704025', NULL, 385, '9202052009970001', 1, 'Manokwari', 'Laurensius mansumber', '', '98311', '081379915586', 'sivanmansumber650@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-09-20', '2019-05-01', 11, NULL, NULL, 0),
(131, '1704014', NULL, 388, '9271010106960007', 1, 'Sorong', 'EDISON GALIO RAMANDEY', '', '\"Jalan sawo kompleks Navigasi', 'RT 002/001', 'Kelurahan Malawei', NULL, NULL, 0, 0, 0, 0, 0, '1996-06-01', '2019-05-01', 11, NULL, NULL, 0),
(132, '1804040', NULL, 391, '1403011111992910', 1, 'Bengkalis, Riau', 'FIKRI AL RIFQI', '', 'Jl. Antara gg. Sidomulyo RT004/RW005 Desa Senggoro, Kec. Bengkalis, 28712', '082285190465', 'rfikrial@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-11-11', '2019-05-01', 11, NULL, NULL, 0),
(133, '1804038', NULL, 394, '1472031906000000', 1, 'Bengkalis', 'Fernanda Ikhsan', '', 'Jl. Agenda RT 005 kel. Bukit Nenas kec. Bukit Kapur kode pos 28882', '082177604648', 'fernandaikhsankls9.4@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-19', '2019-05-01', 11, NULL, NULL, 0),
(134, '1804093', NULL, 397, '1403016101010001', 2, 'Bengkalis', 'RAHMATUL SYAFITRI PRIBADI', '', 'Jl. Pramuka RT/RW 001/004 Desa Air Putih Kec. Bengkalis, Kab. Bengkalis, Riau', '082385758375', 'rahmaa.syafitri@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-01-21', '2019-05-01', 11, NULL, NULL, 0),
(135, '1804057', NULL, 400, '5201125203000001', 2, 'Mataram', 'Keni sri darma putri', '', 'Teragtag desa Batu Kumbung Lombok Barat NTB', '082359428236', 'kenyputri22@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-12', '2019-05-01', 11, NULL, NULL, 0),
(136, '1804054', NULL, 403, '3525167006000001', 2, 'Medan', 'Jihan Fajriya', '', 'Jl. Mayjend sungkono perumahan wilmar no.52 kelurahan kembangan kecamatan kebomas kabupaten gresik provinsu jawa timur kode pos 61124', '082110734200', 'jihanfajriya@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-30', '2019-05-01', 11, NULL, NULL, 0),
(137, '1804062', NULL, 406, '1602246707000002', 2, 'Kayulabu', 'Lewis kasari', '', 'Jl.raya desa kayulabu kec.pedamaran timur kab. Ogan komering ilir prov.sumatera selatan kode pos 30670', '082282883778', 'lewiskasari27@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-07-27', '2019-05-01', 11, NULL, NULL, 0),
(138, '1804087', NULL, 409, '6302022210990003', 1, 'KOTABARU', 'Oky Reza Ary Prawinanto', '', 'Jln raya Stagen km 10.5 desa Stagen rt 7 rw 2 kecamatan pulau laut Utara kode pos 72151', '082254513402', 'rezaoky845@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-22', '2019-05-01', 11, NULL, NULL, 0),
(139, '1804021', NULL, 412, '3372050910990006', 1, 'Surakarta', 'BAYRENO PATRIA NEGARA', '', 'Aspol Manahan RT/RW 03/12 Kel.Manahan Kec.Banjarsari Kota Surakarta', '082243909180', 'bayrenopatrianegara@yahoo.co.id', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-09', '2019-05-01', 11, NULL, NULL, 0),
(140, '1804106', NULL, 415, '1471094407990061', 2, 'Pekanbaru', 'Sri Maya Arifa', '', 'Jl. Wonosari gg. Kaktus no.5 RT 002/ RW 008. Kel. Tangkerang Tengah, Kec. Marpoyan Damai. Kota Pekanbaru, Prov. Riau. Kode pos 28282', '082381089998', 'srimayaarifa99@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-07-04', '2019-05-01', 11, NULL, NULL, 0),
(141, '1804048', NULL, 418, '1812031902000003', 1, 'Kartaraharja', 'Ilham Akbar', '', 'Jl ratu pengadilan no. 56 RT 01 desa kartaraharja kec. Tuba Udik kab. Tuba Barat lampung', '081279367154', 'akbarilham761@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-19', '2019-05-01', 11, NULL, NULL, 0),
(142, '1804010', NULL, 421, '3216070810970000', 1, 'Bekasi', 'Alwi Oktidiya Humaedi', '', 'Jl. Marina IV Perumahan Metland Cibitung Cluster Taman Marian Blok Q7 no 3 RT 004 RW 020 Ds. Telaga Murni Kec. Cikarang Barat Kab. Bekasi 17520', '087713308794', 'alwioktdy25@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-10-08', '2019-05-01', 11, NULL, NULL, 0),
(143, '1804103', NULL, 424, '1701115511990001', 2, 'Curup', 'Shafira Dwika Putri', '', 'Jl.sersan m.taha, RT/RW 007/003,kel.ketapang besar,kec.pasar manna,kab.bengkulu selatan,38516', '089626820169', 'shafiradwika11@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-11-15', '2019-05-01', 11, NULL, NULL, 0),
(144, '1804018', NULL, 427, '3671032103010003', 1, 'Bekasi', 'Athallah Yazid Zaidan', '', 'Jl.Sumatra 2 No. 1 Cimone Jaya, Karawaci, Tangerang, Banten', '08117873671', 'athallahyzaidan@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-03-21', '2019-05-01', 11, NULL, NULL, 0),
(145, '1804080', NULL, 430, '1671120911000006', 1, 'PALEMBANG', 'M. AGUNG WAHYUDI', '', 'Perum Griya Asri Blok O no 72, RT03/RW01, Kelurahan PuloKerto, Kecamatan Gandus, Palembang, 30149', '085267765603/089601240364', 'agungwhyd9@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-11-09', '2019-05-01', 11, NULL, NULL, 0),
(146, '1804011', NULL, 433, '9203010304990000', 1, 'Fakfak', 'Amrizal Rumasukun', '', 'Jl izak telussa rt.17, kelurahan. fakfak selatan, kec fakfak.  Kode pos: 98611', '081344698171', 'amrizalrumasukun@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-03', '2019-05-01', 11, NULL, NULL, 0),
(147, '1804039', NULL, 436, '5204225701000001', 2, 'Sumbawa, NTB', 'Fhina Soufana', '', 'Jl. Buin Batu No 1 Rt/Rw 001/005 Dusun Unter Gedong, Desa Uma Beringin, Kecamatan Unter Iwes, Kabupaten Sumbawa, Provinsi NTB.', '085237634049', 'fhinsof1710@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-17', '2019-05-01', 11, NULL, NULL, 0),
(148, '1804032', NULL, 439, '1607014502000004', 2, 'Perambahan', 'DWI TUTI WALIYANI', '', 'Jalan poros dusun II desa perambahan rt 01 kelurahan perambahan kecamatan banyuasin I 30763', '081273505902', 'tutiwdwi4@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-05', '2019-05-01', 11, NULL, NULL, 0),
(149, '1804013', NULL, 442, '1607015404000008', 2, 'Srinanti', 'Aprilia', '', 'Jl.raya pasar wempe rt 05 rw 03 srinanti sungai kecamatan banyuasin 1 kabupaten banyuasin sumatera selatan', '081367615614', 'Aprilia14400@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-04-14', '2019-05-01', 11, NULL, NULL, 0),
(150, '1804008', NULL, 445, '6471052808980005', 1, 'Balikpapan', 'Alberto Sinaga', '', 'Jl.Mayjend sutoyo Rt.61 No.42 kec.Balikpapan Kota, Kel.Klandasan Ilir', '081347919455', 'albertosinaga420@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-28', '2019-05-01', 11, NULL, NULL, 0),
(151, '1804117', NULL, 448, '1671026201010007', 2, 'Palembang', 'Yola Monica', '', 'Jalan Harapan Rt 42 No 10 Rw 06 Kelurahan Silaberanti Kecamatan Jakabaring Palembang', '082280006509', 'monicayola56@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-01-22', '2019-05-01', 11, NULL, NULL, 0),
(152, '1804061', NULL, 451, '1671070504990009', 1, 'Palembang', 'LEANDRO ANUGERAH PANJAITAN', '-', 'Jl Naskah asrama hubdam blok G no5 rt 011/004 km7 palembang', '082177728141', 'leandropanjaitan@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-05', '2019-05-01', 11, NULL, NULL, 0),
(153, '1804086', NULL, 454, '1204130311000003', 1, 'Hiligeo Afia', 'Novan Berkat Sozanolo Zendrato', '', 'Hiligeo Afia kec.Lotu kab. Nias Utara', '081362590877', 'novanzend@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-11', '2019-05-01', 11, NULL, NULL, 0),
(154, '1804068', NULL, 457, '1218045809950009', 2, 'Pekanbaru', 'MARIA HYGEIA FAPERINA MARPAUNG', '', 'Dusun XIV, Desa Firdaus, Kecamatan Sei Rampah, 20995', '082137984881', 'mfaperina@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1995-09-18', '2019-05-01', 11, NULL, NULL, 0),
(155, '1804047', NULL, 460, '5104021208000003', 1, 'Denpasar', 'I Gst Ngr Gede Mahesa Putra', '', 'Br. Kerti Yasa, Desa Bona, Kec Blahbatuh, Kab Gianyar, Prov Bali', '081339663697', 'mahesamahesa84@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-08', '2019-05-01', 11, NULL, NULL, 0),
(156, '1804091', NULL, 463, '1671106502010002', 2, 'Palembang', 'Putri Febry Andani', '', 'Desa Bogorejo RT/RW.01/02 kec.Gedong tataan Kab. Pesawaran Prov. Lampung, 35371', '082371191980', 'putriandani73@yahoo.co.id', NULL, NULL, 0, 0, 0, 0, 0, '2001-02-25', '2019-05-01', 11, NULL, NULL, 0),
(157, '1804113', NULL, 466, '1471011512000023', 1, 'PEKANBARU', 'WAHYU TOGHI RAMADHAN', '', 'JL.PEPAYA NO 21 RT 002 RW 002 KEL PULAU KAROMAH KEC SUKAJADI 28127', '081398463549', 'wahyutoghir@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-15', '2019-05-01', 11, NULL, NULL, 0),
(158, '1804025', NULL, 469, '1606014712000003', 2, 'Palembang', 'Daffa hanifah', '', 'Gang valencia blok bII no.18 GMP, sekayu musi banyuasin. Rt16 rw02 kel. Balai agung kec. Sekayu, 30711', '081282451939', 'daffahanifah07@gmail.con', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-07', '2019-05-01', 11, NULL, NULL, 0),
(159, '1804005', NULL, 472, '1601206201010003', 2, 'Kelumpang', 'Afra Salsabilla', '', 'Jalan Masjid Istiqomah Dusun III RT/RW.007/003 Desa Kelumpang Kec. Ulu Ogan Kab. OKU SUMSEL (32157)', '081367076945', 'Afrasalsabillahbta@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-01-22', '2019-05-01', 11, NULL, NULL, 0),
(160, '1804063', NULL, 475, '1671026005000006', 2, 'Palembang', 'Lina Astuti', '85.725.797.6-306.000', 'Jln kh Wahid Hasyim lrg Aa no 731 Rt 26 RW 06 kelurahan 2 ulu kecamatan SU1', '085268137635', 'linaastuti0500@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-05-20', '2019-05-01', 11, NULL, NULL, 0),
(161, '1804045', NULL, 478, '6301032704000006', 1, 'TANAH LAUT', 'GERI ODISTIYA PERMANA', '', 'JL. ATILAM, RT/RW 08/04, KELURAHAN PABAHANAN, KECAMATAN PELAIHARI, 70815', '082253685303', 'odistiyageri@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-04-27', '2019-05-01', 11, NULL, NULL, 0),
(162, '1804117', NULL, 481, '1671026201010007', 2, 'Palembang', 'Yola Monica', '', 'Jalan Harapan Rt 42 No 10 Rw 06 Kelurahan Silaberanti Kec Jakabaring Palembang', '082280006509', 'monicayola56@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-01-22', '2019-05-01', 11, NULL, NULL, 0),
(163, '1804028', NULL, 484, '9171014201000001', 2, 'Bojonegoro', 'Devika Wandha Rosadi', '-', 'Perumahan graha mentari a6/07 Rt.004 Rw.006 Mlajeh Bangkalan Madura Jawa Timur', '081344991010', 'devikawandharosadi@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-02', '2019-05-01', 11, NULL, NULL, 0),
(164, '1804015', NULL, 487, '1671141808010007', 1, 'Palembang', 'Arif hidayatullah', '', 'Jalan kopral urip no.08 rt.41 rw.13 plaju ilir palembang', '083801256156', 'arifafr1808@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-08-18', '2019-05-01', 11, NULL, NULL, 0),
(165, '1804023', NULL, 490, '3312071703000002', 1, 'Wonogiri', 'Besar Alviansyah', '', 'Jl Nuswantoro No. 35, Duwet Kidul, RT 01/RW 22, Baturetno, Wonogiri, 57673', '089503756433', 'besaralviansyah@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-17', '2019-05-01', 11, NULL, NULL, 0),
(166, '1804076', NULL, 493, '1671041412000001', 1, 'Palembang', 'M Ramadhan Setiawan', '', 'JL.Mayjen Yusuf Singadekane Lrg.Hj.Daisyah Rt.03 Rw.02 kec.kertapati kel.karya jaya', '087800032641', 'setiawanrn1412@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-14', '2019-05-01', 11, NULL, NULL, 0),
(167, '1804042', NULL, 496, '1671066403000003', 2, 'Palembang', 'FIRDA MARIA T.F', '', 'Jl. Slamet riady no.1927 rt.022 rw.01 kel. Lawang kidul kec. Ilir timur II Palembang 30115', '08127382443', 'frdmaria24@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-24', '2019-05-01', 11, NULL, NULL, 0),
(168, '1804066', NULL, 499, '3306093006000002', 1, 'JAKARTA', 'Luthfi fadhilah', '', 'Desa suren rt02/rw01 kec.kutoarjo kab.purworejo jawa tengah', '083177720689', 'luthfifadhilah19@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-30', '2019-05-01', 11, NULL, NULL, 0),
(169, '1804065', NULL, 502, '6371024502000006', 2, 'Wonosobo', 'Lulu Zakia Salsabila', '', 'Jl.veteran komp ayani 2 rt25 no15 kel.pengambangan kec.banjarmasin timur kota banjarmasin kalimantan selatan', '085810752553', 'lulu.zakia020@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-05', '2019-05-01', 11, NULL, NULL, 0),
(170, '1804002', NULL, 505, '1671074307000000', 2, 'Palembang', 'adira Balqis Saputri', '', 'Jalan rajawali 4 blok A 7 No 6  RT 09 RW 04 ,kel. Talang kelapa, kec. Alang-alang lebar,  km 10 , Maskarebet, palembang, sumsel .30154', '081367457924', 'Adirabs@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-07-03', '2019-05-01', 11, NULL, NULL, 0),
(171, '1804026', NULL, 508, '1607031402000001', 1, 'Pangkalan Balai', 'David Zuhardi', '', 'Jl. Sri Cinta, Kedondong Raye, Banyuasin III, Banyuasin 30753', '085380564077', 'zuhardidavid@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-14', '2019-05-01', 11, NULL, NULL, 0),
(172, '1804075', NULL, 511, '1671100110000007', 1, 'Palembang', 'Muhammad kevin al furqon', '', 'Jalan mayorzen lorong margoyoso no 90 rt 10 rw 03 kelurahan sei selayur kecamatan kalidoni palembang 30119', '089626958750', 'alfurqonm10@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-10-01', '2019-05-01', 11, NULL, NULL, 0),
(173, '1804109', NULL, 514, '1802054512970003', 2, 'Adipuro', 'Tamara', '', 'Lk. Adimulyo, RT/RW 048/016, KELUHAN/DESA: ADIPURO, KEC. TRIMURJO, KAB. LAMPUNG TENGAH, KODE POS 34172', '082176654141', 'tamaradesia7@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-12-05', '2019-05-01', 11, NULL, NULL, 0),
(174, '1804016', NULL, 517, '1306030412990001', 1, 'Jambak', 'Arman saputra', '', 'Tanjuang batuang, nagari duo koto, kec. Tanjung raya, 26471', '082210065671', 'armansaputra576@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-12-04', '2019-05-01', 11, NULL, NULL, 0),
(175, '1804095', NULL, 520, '7403055108980202', 2, 'Guali', 'Reka Linda Yani', 'Tidak ada', 'Jl. Ring road kota laworo, desa guali, RT 002 RW 001, Kec. Kusambi, Kode Pos 93655', '081340340365', 'wulan110898@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-11', '2019-05-01', 11, NULL, NULL, 0),
(176, '1804001', NULL, 523, '1671073012980010', 1, 'Palembang', 'Adani fairul zabadi', '-', 'Jln. Sukabngun 2 lr kaur blok e no 1 rt 48 rw 09 kec. Sukarami kel. Sukajaya 30151', '089505187049', 'adanifairull@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-12-30', '2019-05-01', 11, NULL, NULL, 0),
(177, '1804089', NULL, 526, '1671102803990009', 1, 'Palembang', 'Prasetia Aditya Nugraha', '', 'Jl. Mayorzen lr. Margoyoso rt:11 rw:03 no:96 kec.kalidoni kel. Sei selayur palembang', '083800715943', 'prasetiaadityanugraha28@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-03-28', '2019-05-01', 11, NULL, NULL, 0),
(178, '1804118', NULL, 529, '3314101708000002', 1, 'Sragen', 'Yonathan Hut Riawan', '', 'Jetak kalang rt 2 rw 2,jetak,sidoharjo,sragen,jawa tengah', '6283802592417', 'yonathanhutriawan@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-08-17', '2019-05-01', 11, NULL, NULL, 0),
(179, '1804077', NULL, 532, '1671122312000001', 1, 'PALEMBANG', 'MUHAMMAD RIORAMZI NAJWAN', '', 'Jl. P Sido Ing Lautan Lr.Manggis No.77 Rt.04 Rw.01 Kel.36 Ilir Kec.Gandus , 30147', '082183842394', 'rioramzi88@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-23', '2019-05-01', 11, NULL, NULL, 0),
(180, '1804098', NULL, 535, '1671075406000019', 2, 'Palembang', 'Rizki Rahma Yunita', '', 'Jl. RA Abusamah Lr. Damai No3488 RT4/06 Kec. Sukarame Kel. Sukabangun Lebong Siarang Palembang Kode Pos 30151', '0895323134037', 'rizkirahmayunita@yahoo.co.id', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-14', '2019-05-01', 11, NULL, NULL, 0),
(181, '1804058', NULL, 538, '3504183110990001', 1, 'TULUNGAGUNG', 'KUKUH OKTANUGRAH TRY WIYONO', '', 'DSN.MULYO RT/RW 003/002 DS.SAMBITAN KEC.PAKEL KAB.TULUNGAGUNG', '085645005692', 'kukuhoktanugrah92@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-31', '2019-05-01', 11, NULL, NULL, 0),
(182, '1804071', NULL, 541, '3307090111980003', 1, 'Wonosobo', 'Muhammad Afif', '', 'Sribit RT. 05 RW. 03 Wonolelo Wonosobo 56311', '082123091937', 'muhammadafifkr4@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-11-01', '2019-05-01', 11, NULL, NULL, 0),
(183, '1804073', NULL, 544, '1671081305010004', 1, 'Palembang', 'Muhammad Fadlil Hazmi', '', 'Jalan Bakung X nomor 33 Rt.39/Rw.16 Kelurahan Sialang Kecamatan Sako Kota Palembang 30163', '089626738648', 'epedelek@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-05-13', '2019-05-01', 11, NULL, NULL, 0),
(184, '1804097', NULL, 547, '1802060103000001', 1, 'Nunggalrejo', 'Riyan suganda', '', 'Dusun irian 1 rt 011 rw 005 totokaton kec. Punggur kab. Lampung tengah, Lampung', '081276497084', 'sugandariyan00@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-13', '2019-05-01', 11, NULL, NULL, 0);
INSERT INTO `taruna` (`id`, `no_taruna`, `foto`, `user`, `no_ktp`, `jenis_kelamin`, `tempat_lahir`, `nama`, `npwp`, `alamat`, `no_hp`, `email`, `sim`, `gol_darah`, `propinsi`, `kota`, `asal_sekolah`, `kompi`, `seperasuhan`, `tanggal_lahir`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(185, '1804024', NULL, 550, '3501045310990002', 2, 'Kebumen', 'CAROLIN FARAH DIBA', '', 'Rt 01 Rw 03, Desa Arjowinangun, Kab. Pacitan, Jawa Timur, 63516', '082234709219', 'carolinafarah13@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-10-13', '2019-05-01', 11, NULL, NULL, 0),
(186, '1804051', NULL, 553, '7472060305990003', 1, 'BAUBAU', 'Irwan h.z', '', 'Jl.gajah mada , Lamangga , Rt/Rw : 001/001 , Kec.Murhum', '082259379709', 'irwanhz044@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-03-05', '2019-05-01', 11, NULL, NULL, 0),
(187, '1804081', NULL, 556, '7271033003000006', 1, 'Palu', 'Nabil Akram', '-', 'JL.TANJUNG SENG NO.6 RT003/RW008 KEL.LOLU SELATAN KEC.PALU TIMUR PROV.SULAWESI TENGAH, 94125', '081297737772', 'nabilakram021@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-03-30', '2019-05-01', 11, NULL, NULL, 0),
(188, '1804060', NULL, 559, '5202042201000001', 1, 'Sengkol', 'LALU AS FORMADI', '', 'Sengkol kerok desa sengkol kecamatan pujut lombok tengah NTB', '085333674054', 'asforlalu@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-22', '2019-05-01', 11, NULL, NULL, 0),
(189, '1804053', NULL, 562, '6104200308960001', 1, 'Sukaraja', 'Jhodi', '', 'Dusun Sukajaya RT/RW : 008/004 desa sukaraja kec. singkup KAB. ketapang PROV. KALBAR', '081545515708', 'Jhodiindriansyah96@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1996-08-03', '2019-05-01', 11, NULL, NULL, 0),
(190, '1804069', NULL, 565, '1607105009000009', 2, 'EPIL', 'MetaHastuti', '', 'Komplek megah asri 1 blom M No 15 RT/RW 04/01 kelurahan sukajadi kecamatan talang kelapa', '0895636773577', 'metahastuti8@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-09-10', '2019-05-01', 11, NULL, NULL, 0),
(191, '1804037', NULL, 568, '3201084102000002', 2, 'Bogor', 'Fenny Febrianty Aulia Ansor', '', 'Jl brigdjen daharsono desa cariu rt 009 rw 03 kecamatan cariu kabupaten bogor 16840', '085884433779', 'fennyfebriantyaulia@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-01', '2019-05-01', 11, NULL, NULL, 0),
(192, '1804082', NULL, 571, '730705560300004', 2, 'Sinjai', 'Namira adha safira', '', 'Btn Gojeng permai, RT 01 RW 01 kelurahan Biringere kecamatan Sinjai utara, 92611', '082194045449', 'namiraadhasafira@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-03-16', '2019-05-01', 11, NULL, NULL, 0),
(193, '1804012', NULL, 574, '1671146109000005', 2, 'PALEMBANG', 'ANNISA VERANIKA', '0000027068117', 'JALAN TEGAL BINANGUN KOMPLEK PATRA ABADI LORONG ROSELA BLOK B NO 1 RT. 34 RW. 08 KELURAHAN PLAJU DARAT KECAMATAN PLAJU PALEMBANG, 30267', '081367132389', 'annisaveranika@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-09-21', '2019-05-01', 11, NULL, NULL, 0),
(194, '1804074', NULL, 577, '3325113105000001', 1, 'Batang', 'Muhammad Farhan Labib Hammam', '', 'Perumahan Pisma Griya Asri, Jalan Asri 1, B:6, Rt01/Rw05, Desa Denasri Kulon, Kecamatan Batang, Kabupaten Batang, 51216', '081542319670', 'muhammadfarhan.mf692@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-05-31', '2019-05-01', 11, NULL, NULL, 0),
(195, '1804099', NULL, 580, '1607104207000004', 2, 'Palembang', 'RIZKY AMALIA', '', 'Komp. Megahasri I Blok Y No. 6 Rt. 03 Rw. 01 Kelurahan Sukajadi Kecamatan Talang Kelapa Kabupaten Banyuasin Kode Pos. 30961', '089624973612', 'rizkyamalia020700@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-07-02', '2019-05-01', 11, NULL, NULL, 0),
(196, '1804036', NULL, 583, '7208121502990002', 1, 'Palu', 'Febrian partil alexsander karwur', '', 'Desa kotaraya, kec. Mepanga, kab. Parimo, sulteng', '082293165521', 'febriankarwur99@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-02-15', '2019-05-01', 11, NULL, NULL, 0),
(197, '1804017', NULL, 586, '1671100201010007', 1, 'Palembang', 'Arya Tulus Syahputra', '', 'Jalan Residen Abdul Rozak No 45 Rt.11 Rw.03 Depan Pombensin Sekojo Palembang Sumatera Selatan Kode Pos:30118', '089654916327', 'aryatulussyahputra123@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-01-02', '2019-05-01', 11, NULL, NULL, 0),
(198, '1804007', NULL, 589, '5271021000001', 1, 'MATARAM', 'Aji Maspanji Surya Mataram', '', 'JL. MERDEKA RAYA D. 46 PAGESANGAN BARU', '081337891397', 'ajimaspanji@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-10', '2019-05-01', 11, NULL, NULL, 0),
(199, '1804050', NULL, 592, '7271021309000001', 1, 'Boyaoge', 'I PUTU DICHA CANDRA PANGESTU', '', 'Jalan Cemangi Lorong II, RT3/RW1, Kelurahan Duyu, Kecamatan Tatanga, Kota Palu, Sulawesi Tengah, 94225', '085241046935/085240697981', 'iputudicha@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-09-13', '2019-05-01', 11, NULL, NULL, 0),
(200, '1804079', NULL, 595, '6404052908980002', 1, 'Tanjung Selor', 'MUHAMMAD YUSRIL SUDITOMO', '', 'Jl. Sabanar Lama RT 068 RW 025 Kel. Tanjung Selor hilir, Kec. Tanjung Selor, Kab. Bulungan, Prov. Kalimantan Utara, Kode Pos 77212', '082153269490', 'myusril111@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-08-29', '2019-05-01', 11, NULL, NULL, 0),
(201, '1804044', NULL, 598, '5102050302000002', 1, 'Tabanan', 'Gede Yogi Surya Narendra', '', 'Jl Yos Sudarso no 8, Desa Dajan peken, Tabanan 82114', '082237230212', 'deadcode1470@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-03', '2019-05-01', 11, NULL, NULL, 0),
(202, '1804100', NULL, 601, '3275110607990001', 1, 'Kebumen', 'RM Mayora Ruwi Perwira Putra', '', 'Jl Zamrud Utara 7 Perum DuKuh Zamrud Blok Q1 No.6S (Huruf S) RT:003/RW:013 Kel.Padurenan Kec.Mustika Jaya Kota Bekasi', '081213646122', 'radenmayora@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-07-06', '2019-05-01', 11, NULL, NULL, 0),
(203, '1804119', NULL, 604, '1607014906000001', 2, 'Palembang', 'Yuni Karlina', '', 'Jl.sabar jaya,perumahan griya sejahtera blok f198 rt 003/rw 001,desa mariana ilir,kecamatan banyuasin 1.kode pos (30763)', '085267106180', 'yuniikarlina96@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-09', '2019-05-01', 11, NULL, NULL, 0),
(204, '1804111', NULL, 607, '1607105707000012', 2, 'Lahat', 'Veronike', '', 'Komp azhar blok aw 2 no 12 rt 029 rw 007', '081368148502', 'veronikee97@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-07-17', '2019-05-01', 11, NULL, NULL, 0),
(205, '1804034', NULL, 610, '1671074201990009', 2, 'Palemban', 'ELLA JELITA SIRINGORINGO', '', 'Jln tanjung apiapo lrg bambu kuning no 32 rt/rw 63/11 kelurahan kebunbunga kecamatan sukarami 30152', '082177780633', 'ellasiringoringo18@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-01-02', '2019-05-01', 11, NULL, NULL, 0),
(206, '1804112', NULL, 613, '3502040205980001', 1, 'Ponorogo', 'Victor Aditya Azhar', '-', 'Jl. Raya Ponorogo-Trenggalek, RT/RW 002/002, Desa Bulu, Kecamatan Sambit, Kabupaten Ponorogo, Provinsi Jawa Timur, Kode Pos 63474', '085843577943', 'victoradityaaz@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-05-02', '2019-05-01', 11, NULL, NULL, 0),
(207, '1804009', NULL, 616, '1405026804000008', 2, 'Purworejo', 'ALMAS AVIDA BASYASYA', '', 'BTN BUMI LAGO PERMAI jln. Anggur 4 no.16, 001/012, kelurahan pangkalan kerinci kota, kecamatan pangkalan kerinci. 28381', '082377144522', 'almasavidab@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-04-28', '2019-05-01', 11, NULL, NULL, 0),
(208, '1804116', NULL, 619, '6107144605000001', 2, 'Serukam', 'Yohana Meilieana Sabty', '', 'Dusun Lamat Semalat desa Cipta Karya kec Sungai Betung kab Bengkayang', '089518425595', 'meilieanasabty@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-05-06', '2019-05-01', 11, NULL, NULL, 0),
(209, '1804041', NULL, 622, '1606075512000008', 2, 'PALEMBANG', 'Fina Hamidah', '', 'Jalan Palembang-Jambi km 121, RT 001 RW 002 Desa Linggosari Kec. Sungai lilin kab. Musi Banyuasin Sumatera Selatan, kode pos 30755', '082175798156', 'finahamidah15@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-15', '2019-05-01', 11, NULL, NULL, 0),
(210, '1804006', NULL, 625, '3172025107000009', 2, 'Jakarta', 'Aisyah Maudini Putri', '', 'Jl. Sungai Bambu iv no.28 RT. 05 RW. 08 Kelurahan Sungai Bambu Kecamatan Tg. Priok 14330', '087889512463', 'aisyah11.ap@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-07-11', '2019-05-01', 11, NULL, NULL, 0),
(211, '1804004', NULL, 628, '1306010511980002', 1, 'Kototinggi,suliki', 'Afdhal Aulia', '', 'Jalan mangga kelurahan nunang daya bangun rt 002 rw 005 kecamatan payakumbuh barat kota payakumbuh provinsi sumatera barat kode pos 26221', '082285069739', 'afdhalaulia5@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1998-11-05', '2019-05-01', 11, NULL, NULL, 0),
(212, '1804052', NULL, 631, '1606060606990001', 1, 'BABAT', 'JEMIZAR AMAZON', '', 'Jl.propinsi kel.Babat rt011 rw 002 kec.Babat Toman Kab.musi banyuasin prov.Sumatera Selatan,30752', '081311017807', 'jemizaramazon15@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-06-06', '2019-05-01', 11, NULL, NULL, 0),
(213, '1804020', NULL, 634, '6108071301000005', 1, 'Banying', 'Bartolonius Gadam', '', 'Rt1/Rw1 dusun banting desa banting kecamatan dengan temukan kan landak', '085368096104', 'gadam13012000@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-13', '2019-05-01', 11, NULL, NULL, 0),
(214, '1804107', NULL, 637, '1671076011970004', 2, 'Kepahiang', 'St Hayaty Elyusri', '', 'Jl. Batu Jajar Perumahan Puri Impian 3 Blok E7 RT/RW.021/007 Kel/Kec.Sukarami Kota Palembang 30152, Sumatera Selatan.', '0895335072708', 'elyarmas20@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-11-20', '2019-05-01', 11, NULL, NULL, 0),
(215, '1804029', NULL, 640, '1606012504000005', 1, 'Bailangu', 'DICKI ALMAPRI', '', 'Dusun IV Rt.10 No.46 Desa Bailangu Timur Kec. Sekayu 30711', '085357877693', 'dickialmapri25@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-04-25', '2019-05-01', 11, NULL, NULL, 0),
(216, '1804104', NULL, 643, '1671096011000011', 2, 'Palembang', 'Siti Sakinah Mawaddah Warohmah', '', 'Jalan Jenderal Sudirman Km 4,5 , RT/RW 22/01, Kelurahan Ario Kemuning, Kecamatan Kemuning, Kode Pos 30128', '08127381973', 'mwsakinah@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-11-20', '2019-05-01', 11, NULL, NULL, 0),
(217, '1804092', NULL, 646, '1671072806000008', 1, 'PALEMBANG', 'Raffi Muhamad', '', 'Jl. Putri Rambut Selako desa Kemang manis RT 09/RW03 Kelurahan kemang manis kecamatan Ilir Barat 2 kode pos 30144', '0895327777604', 'raffi.muhamad02@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-28', '2019-05-01', 11, NULL, NULL, 0),
(218, '1804056', NULL, 649, '5108044502000001', 2, 'Banjar Tegeha', 'Kadek Melinia Risma Dewi', '', 'Banjar Dinas Tengah, Desa Banjar Tegeha, Kecamatan Banjar, Kabupaten Buleleng, Bali 81151', '083117966611', 'melinia022000@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-05', '2019-05-01', 11, NULL, NULL, 0),
(219, '1804030', NULL, 652, '1208094211990002', 2, 'P. SIANTAR', 'DWI CHINDY ROCKY ANGELI SIAHAAN', '', 'Jln. Sukamulia Sarimatondang no. 5A kec. Sidamanik kab. Simalungun Sumatera Utara 21171', '082273834590', 'dwicindy02@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-11-02', '2019-05-01', 11, NULL, NULL, 0),
(220, '1804019', NULL, 655, '5171035302000003', 2, 'denpsar', 'ayu diah prema widiatmika dewi', '', 'jalan slamet riyadi I no d/12 kompleks perumahan tni-ad kodam IX udayana rt 8 lingkungan kirana desa dauh puri kecamatan denpasar barat', '081239605103', 'premadiahayu@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-13', '2019-05-01', 11, NULL, NULL, 0),
(221, '1804055', NULL, 658, '5371036107000002', 2, 'kupang', 'JULIANA AISYAH INA', '', 'Jalan Soverdi, Gang Kebun Sirih, RT/RW 030/007, Kelurahan/Desa Oebufu, Kecamatan Oebobo', '081338909803', 'julianaaisyah21@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-07-21', '2019-05-01', 11, NULL, NULL, 0),
(222, '1804084', NULL, 661, '5171014108000007', 2, 'Denpasar', 'Ni Komang Budiantini', '', 'Jalan Waturenggong Gang XXVII C no 5', '085858684193', 'Budiantini01@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-08-01', '2019-05-01', 11, NULL, NULL, 0),
(223, '1804046', NULL, 664, '3216063006000018', 1, 'Bekasi', 'Hafizh Naufal', '', 'Jl nusantara 3,sumber jaya,001/027,tambun selatan,17501', '082114013078', 'hafizh78097@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-30', '2019-05-01', 11, NULL, NULL, 0),
(224, '1804105', NULL, 667, '7271016609000006', 2, 'Palu', 'Siti zahwa aulia dewi', '', 'Jl.rusa No.78B, kec. Palu timur, kel. Talise, 94118', '082194060980', 'sitizahwaauliadewi@yahoo.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-09-26', '2019-05-01', 11, NULL, NULL, 0),
(225, '1804003', NULL, 670, '3578131806000001', 1, 'Surabaya', 'Ady Yanto Saputro', '', 'Asem jajar XII/29', '081234512770', 'adyyanto63@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-18', '2019-05-01', 11, NULL, NULL, 0),
(226, '1804108', NULL, 673, '5203095612000002', 2, 'Kembang kerang,16 desember 2000', 'Susi azhari anggraini', 'Tidak ada', 'Jl.segara anak km.03 kembang kerang daya kec.aikmel kab.lombok timur prov.NTB kode pos 83653', '081907088927/081949014781', 'susiazhari1612@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-16', '2019-05-01', 11, NULL, NULL, 0),
(227, '1804049', NULL, 676, '3327095801000014', 2, 'Pemalang', 'Ilmi inayatul fatikhah', '', 'Jl.reformasi RT/RW 01/03 Wanarejan Utara taman pemalang Jawa tengah 52361', '085924968817', 'ilmiinayatul.f@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-18', '2019-05-01', 11, NULL, NULL, 0),
(228, '1804067', NULL, 679, '3515082104990005', 1, 'Sidoarjo', 'M Yusuf Raka Surya Gemilang', '', 'Jl R A Kartini 2/10 RT.05 RW.02 Sidoklumpuk, Sidoarjo, Jawa Timur 61219', '083833356280', 'gilangputra1.krenz@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-04-21', '2019-05-01', 11, NULL, NULL, 0),
(229, '1804035', NULL, 682, '6106066810000002', 2, 'Nanga Semangut', 'Fany rahmasari', '', 'Jam. K. S. Tubuh No 131,Rt/rw :001/005,kelurahan putussibau kota, kecamatan putussibau utara, kode pos: 78711', '082159571603', 'fany.rahmasari2810@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-10-28', '2019-05-01', 11, NULL, NULL, 0),
(230, '1804064', NULL, 685, '3201010305000007', 1, 'JAKARTA', 'LOUIS FERNANDO', '', 'JLN.MAYOR OKING,PERUM CIRIUNG CEMERLANG,RT 7 RW 14,KEL.CIRIUNG,KEC.CIBINONG,16918', '081311844811', 'Luispego1@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-05-03', '2019-05-01', 11, NULL, NULL, 0),
(231, '1804101', NULL, 688, '6109012106980003', 1, 'Sanggau', 'ROITO ESTRADA', '', 'Jalan Merdeka GG karya RT 001 no 152', '6285267632655', 'estradaroito@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-04-21', '2019-05-01', 11, NULL, NULL, 0),
(232, '1804083', NULL, 691, '1371110803990010', 1, 'Padang', 'Naufal Busra', '', 'Gang. Hercules no.14 rt/002 rw/003 kel. Dadok tunggul hitam kec. Koto tangah kota padang kode pos : 25176', '082258327145', 'naufalbusra@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-03-08', '2019-05-01', 11, NULL, NULL, 0),
(233, '1804033', NULL, 694, '1606011011990004', 1, 'Kayuara', 'Eki Pratama', '', 'Jalan merdeka, dusun 2,rt 05/rw 03,kelurahan Kayuara, kecamatan Sekayu, kabupaten Musi Banyuasin, 30711', '082175754770', 'ekipratama1011@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1999-11-10', '2019-05-01', 11, NULL, NULL, 0),
(234, '1804014', NULL, 697, '1671071306970013', 1, 'Sukajadi', 'Arief ardiansyah', '', 'Perum puri 2 blok n no.01 rt.040 rw.103 kel.talang betutu kec.sukarami palembang', '085268398471', 'iefrdn@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-06-13', '2019-05-01', 11, NULL, NULL, 0),
(235, '1804031', NULL, 700, '1401075707970005', 2, 'Kuntu', 'Dwita Lestari', '', 'Dusun binaan rt/rw 02/03 desa kuntu kab.kampar kec.kampar kiri', '081276756835', 'dwitalestari17@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-07-17', '2019-05-01', 11, NULL, NULL, 0),
(236, '1804059', NULL, 703, '5202022912010002', 1, 'Batutulis', 'Lalu ahmad mariadi', '-', 'Jl.raya batutulis kecamatan jonggat kabupaten lombok tengah NTB kode pos:83561', '085338643420', 'laluahmadmariadi0027@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-27', '2019-05-01', 11, NULL, NULL, 0),
(237, '1804072', NULL, 706, '1506022102000002', 1, 'Jambi', 'Muhammad Chairil Fajrian', '', 'Jl. BTN MARTAPURA Rt. 009, kel. Sriwijaya, kec. Tungkal Ilir , 36512', '082277215624', 'chfajrian@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-02-21', '2019-05-01', 11, NULL, NULL, 0),
(238, '1804043', NULL, 709, '1802234104000003', 2, 'Kotagajah', 'Francisca Melisa Durita Widyarti', '-', 'Gajah timur I, rt/rw 059/060, kotagajah timur, kece. Kotagajah, kan. Lampung tengah, prov. Lampung', '082113178763', 'melisa01042000@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-04-01', '2019-05-01', 11, NULL, NULL, 0),
(239, '1804090', NULL, 712, '1671034708000002', 2, 'Palembang', 'Putri adelia', '', 'Jl.jaya 3 rt.27 rw.08 no.1489 kel.16 ulu kec.seberang ulu 2 Palembang', '089501942500', 'haniadella37@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-08-07', '2019-05-01', 11, NULL, NULL, 0),
(240, '1804070', NULL, 715, '3276031201000004', 1, 'Bogor', 'Mikala Zharfan Fasya', '', 'Sawangan Elok Blok AA1 No.13 RT01 RW10 Kel.Duren Seribu Kec.Bojongsari Kota Depok 16518', '085771465328', 'mikalazharfanfasya@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-12', '2019-05-01', 11, NULL, NULL, 0),
(241, '1804096', NULL, 718, '1271212501010002', 1, 'Medan', 'Reza Aswandi Peranginangin', '', 'Jl.Pijer Podi NO 32G, Kelurahan beringin, Kecamatan medan selayang', '081262734979', 'rezaaswandi0@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2001-01-25', '2019-05-01', 11, NULL, NULL, 0),
(242, '1804022', NULL, 721, '1506060106000001', 1, 'Jambi', 'Bayu Budiansyah', '', 'Jln. Budiman, Km. 3,Desa. Tebing Tinggi, Kec. Tebing Tinggi, Kab. Tanjung Jabung Barat, Prov. Jambi', '082186853021', 'bayubudi530@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-01', '2019-05-01', 11, NULL, NULL, 0),
(243, '1804027', NULL, 724, '1901056012000003', 2, 'Sempan', 'Devia amanda', '', 'Jl. Pusaka, desa sempan, RT 04 RW 04, Kec. Pemali, 33255', '083175993396', 'deviamandaa22@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-12-22', '2019-05-01', 11, NULL, NULL, 0),
(244, '1804078', NULL, 727, '5271015010840002', 1, 'Mataram', 'Muhammad saleh safi\'i', '', 'Jln leo lingkungan selaparang rt 08 kelurahan banjar kecamatan ampenan kota mataram', '087765168406', 'abangaleh1@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-01-02', '2019-05-01', 11, NULL, NULL, 0),
(245, '1804114', NULL, 730, '1671065104000011', 2, 'Palembang', 'Wita Pramedya Susanti', '', 'Jalan mayor laut wiratno lr al baraqoh no 137 rt 10 rw 001 kel sungai buah kec ilir timur 2 palembang Kp:30116', '0711711951', 'witasusanti.ws@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-04-11', '2019-05-01', 11, NULL, NULL, 0),
(246, '1804102', NULL, 733, '6371021905000008', 1, 'pekanbaru', 'rozy eka dharmawan', '-', 'jl.melati no.24 rt.02 rw.01', '087876004797', 'rozydharmawan81@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-05-19', '2019-05-01', 11, NULL, NULL, 0),
(247, '1804110', NULL, 736, '6404025105000004', 2, 'Long belua', 'Vella Karolina', '', 'Long belua, jalan niaga, RT 6 tanjung palas barat', '082255605505', 'vellakarolina@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-05-11', '2019-05-01', 11, NULL, NULL, 0),
(248, '1804088', NULL, 739, '1401061107970004', 1, 'Pekanbaru', 'Orion Dwi Saputra', '', 'Jl. Rengas VI Blok C 49 No. 17 RT 03 RW 07 Desa Pandau Jaya Kecamatan Siak Hulu Kabupaten Kampar 28452', '081266769975', 'oriondwisaputra@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '1997-07-11', '2019-05-01', 11, NULL, NULL, 0),
(249, '1804120', NULL, 742, '3515054206000002', 2, 'SURABAYA', 'YUN WIDYA RACHMADHANI', '', 'DESA KUPANG BADER RT 05/ RW 03 KECAMATAN JABON KABUPATEN SIDOARJO JAWA TIMUR. KODE POS 61276', '087881235559', 'yunwidyar@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2000-06-02', '2019-05-01', 11, NULL, NULL, 0),
(250, '1804115', NULL, 745, '3578232312990002', 1, 'surabaya', 'yoga ramadhan', '', 'jl. jambangan 7E no.9 rt 03 rw 03 kelurahan jambangan kecamatan jambangan kode pos : 60232', '081373896895', 'ramadahanyoga10@gmail.com', NULL, NULL, 0, 0, 0, 0, 0, '2019-12-23', '2019-05-01', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_has_agama`
--

CREATE TABLE `taruna_has_agama` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `taruna` int(11) NOT NULL,
  `agama` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_has_agama`
--

INSERT INTO `taruna_has_agama` (`id`, `tanggal`, `taruna`, `agama`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(372, '2019-05-01', 4, 'Islam', 1, '2019-04-30', 11, '2019-05-05 10:12:55', 1, 0),
(373, '2019-05-01', 6, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(374, '2019-05-01', 7, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(375, '2019-05-01', 8, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(376, '2019-05-01', 9, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(377, '2019-05-01', 10, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(378, '2019-05-01', 11, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(379, '2019-05-01', 12, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(380, '2019-05-01', 13, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(381, '2019-05-01', 14, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(382, '2019-05-01', 15, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(383, '2019-05-01', 16, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(384, '2019-05-01', 17, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(385, '2019-05-01', 18, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(386, '2019-05-01', 19, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(387, '2019-05-01', 20, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(388, '2019-05-01', 21, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(389, '2019-05-01', 22, 'Islam', 1, '2019-05-01', 11, '2019-05-13 02:51:56', 1, 0),
(390, '2019-05-01', 23, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(391, '2019-05-01', 24, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(392, '2019-05-01', 25, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(393, '2019-05-01', 26, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(394, '2019-05-01', 27, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(395, '2019-05-01', 28, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(396, '2019-05-01', 29, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(397, '2019-05-01', 30, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(398, '2019-05-01', 31, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(399, '2019-05-01', 32, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(400, '2019-05-01', 33, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(401, '2019-05-01', 34, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(402, '2019-05-01', 35, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(403, '2019-05-01', 36, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(404, '2019-05-01', 37, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(405, '2019-05-01', 38, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(406, '2019-05-01', 39, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(407, '2019-05-01', 40, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(408, '2019-05-01', 41, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(409, '2019-05-01', 42, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(410, '2019-05-01', 43, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(411, '2019-05-01', 44, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(412, '2019-05-01', 45, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(413, '2019-05-01', 46, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(414, '2019-05-01', 47, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(415, '2019-05-01', 48, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(416, '2019-05-01', 49, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(417, '2019-05-01', 50, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(418, '2019-05-01', 51, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(419, '2019-05-01', 52, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(420, '2019-05-01', 53, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(421, '2019-05-01', 54, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(422, '2019-05-01', 55, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(423, '2019-05-01', 56, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(424, '2019-05-01', 57, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(425, '2019-05-01', 58, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(426, '2019-05-01', 59, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(427, '2019-05-01', 60, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(428, '2019-05-01', 61, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(429, '2019-05-01', 62, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(430, '2019-05-01', 63, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(431, '2019-05-01', 64, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(432, '2019-05-01', 65, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(433, '2019-05-01', 66, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(434, '2019-05-01', 67, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(435, '2019-05-01', 68, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(436, '2019-05-01', 69, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(437, '2019-05-01', 70, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(438, '2019-05-01', 71, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(439, '2019-05-01', 72, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(440, '2019-05-01', 73, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(441, '2019-05-01', 74, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(442, '2019-05-01', 75, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(443, '2019-05-01', 76, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(444, '2019-05-01', 77, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(445, '2019-05-01', 78, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(446, '2019-05-01', 79, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(447, '2019-05-01', 80, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(448, '2019-05-01', 81, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(449, '2019-05-01', 82, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(450, '2019-05-01', 83, 'Perempuan', 1, '2019-05-01', 11, NULL, NULL, 0),
(451, '2019-05-01', 84, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(452, '2019-05-01', 85, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(453, '2019-05-01', 86, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(454, '2019-05-01', 87, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(455, '2019-05-01', 88, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(456, '2019-05-01', 89, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(457, '2019-05-01', 90, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(458, '2019-05-01', 91, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(459, '2019-05-01', 92, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(460, '2019-05-01', 93, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(461, '2019-05-01', 94, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(462, '2019-05-01', 95, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(463, '2019-05-01', 96, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(464, '2019-05-01', 97, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(465, '2019-05-01', 98, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(466, '2019-05-01', 99, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(467, '2019-05-01', 100, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(468, '2019-05-01', 101, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(469, '2019-05-01', 102, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(470, '2019-05-01', 103, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(471, '2019-05-01', 104, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(472, '2019-05-01', 105, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(473, '2019-05-01', 106, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(474, '2019-05-01', 107, 'Katholik', 1, '2019-05-01', 11, NULL, NULL, 0),
(475, '2019-05-01', 108, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(476, '2019-05-01', 109, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(477, '2019-05-01', 110, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(478, '2019-05-01', 111, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(479, '2019-05-01', 112, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(480, '2019-05-01', 113, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(481, '2019-05-01', 114, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(482, '2019-05-01', 115, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(483, '2019-05-01', 116, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(484, '2019-05-01', 117, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(485, '2019-05-01', 118, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(486, '2019-05-01', 119, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(487, '2019-05-01', 120, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(488, '2019-05-01', 121, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(489, '2019-05-01', 122, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(490, '2019-05-01', 123, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(491, '2019-05-01', 124, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(492, '2019-05-01', 125, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(493, '2019-05-01', 126, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(494, '2019-05-01', 127, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(495, '2019-05-01', 128, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(496, '2019-05-01', 129, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(497, '2019-05-01', 130, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(498, '2019-05-01', 131, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(499, '2019-05-01', 132, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(500, '2019-05-01', 133, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(501, '2019-05-01', 134, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(502, '2019-05-01', 135, 'Hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(503, '2019-05-01', 136, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(504, '2019-05-01', 137, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(505, '2019-05-01', 138, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(506, '2019-05-01', 139, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(507, '2019-05-01', 140, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(508, '2019-05-01', 141, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(509, '2019-05-01', 142, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(510, '2019-05-01', 143, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(511, '2019-05-01', 144, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(512, '2019-05-01', 145, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(513, '2019-05-01', 146, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(514, '2019-05-01', 147, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(515, '2019-05-01', 148, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(516, '2019-05-01', 149, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(517, '2019-05-01', 150, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(518, '2019-05-01', 151, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(519, '2019-05-01', 152, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(520, '2019-05-01', 153, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(521, '2019-05-01', 154, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(522, '2019-05-01', 155, 'Hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(523, '2019-05-01', 156, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(524, '2019-05-01', 157, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(525, '2019-05-01', 158, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(526, '2019-05-01', 159, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(527, '2019-05-01', 160, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(528, '2019-05-01', 161, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(529, '2019-05-01', 162, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(530, '2019-05-01', 163, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(531, '2019-05-01', 164, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(532, '2019-05-01', 165, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(533, '2019-05-01', 166, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(534, '2019-05-01', 167, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(535, '2019-05-01', 168, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(536, '2019-05-01', 169, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(537, '2019-05-01', 170, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(538, '2019-05-01', 171, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(539, '2019-05-01', 172, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(540, '2019-05-01', 173, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(541, '2019-05-01', 174, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(542, '2019-05-01', 175, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(543, '2019-05-01', 176, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(544, '2019-05-01', 177, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(545, '2019-05-01', 178, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(546, '2019-05-01', 179, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(547, '2019-05-01', 180, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(548, '2019-05-01', 181, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(549, '2019-05-01', 182, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(550, '2019-05-01', 183, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(551, '2019-05-01', 184, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(552, '2019-05-01', 185, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(553, '2019-05-01', 186, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(554, '2019-05-01', 187, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(555, '2019-05-01', 188, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(556, '2019-05-01', 189, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(557, '2019-05-01', 190, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(558, '2019-05-01', 191, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(559, '2019-05-01', 192, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(560, '2019-05-01', 193, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(561, '2019-05-01', 194, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(562, '2019-05-01', 195, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(563, '2019-05-01', 196, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(564, '2019-05-01', 197, 'Isla', 1, '2019-05-01', 11, NULL, NULL, 0),
(565, '2019-05-01', 198, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(566, '2019-05-01', 199, 'Hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(567, '2019-05-01', 200, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(568, '2019-05-01', 201, 'Hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(569, '2019-05-01', 202, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(570, '2019-05-01', 203, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(571, '2019-05-01', 204, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(572, '2019-05-01', 205, 'Kristen protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(573, '2019-05-01', 206, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(574, '2019-05-01', 207, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(575, '2019-05-01', 208, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(576, '2019-05-01', 209, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(577, '2019-05-01', 210, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(578, '2019-05-01', 211, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(579, '2019-05-01', 212, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(580, '2019-05-01', 213, 'Katolik', 1, '2019-05-01', 11, NULL, NULL, 0),
(581, '2019-05-01', 214, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(582, '2019-05-01', 215, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(583, '2019-05-01', 216, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(584, '2019-05-01', 217, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(585, '2019-05-01', 218, 'Hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(586, '2019-05-01', 219, 'Kristen', 1, '2019-05-01', 11, NULL, NULL, 0),
(587, '2019-05-01', 220, 'hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(588, '2019-05-01', 221, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(589, '2019-05-01', 222, 'Hindu', 1, '2019-05-01', 11, NULL, NULL, 0),
(590, '2019-05-01', 223, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(591, '2019-05-01', 224, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(592, '2019-05-01', 225, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(593, '2019-05-01', 226, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(594, '2019-05-01', 227, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(595, '2019-05-01', 228, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(596, '2019-05-01', 229, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(597, '2019-05-01', 230, 'KRISTEN PROTESTA', 1, '2019-05-01', 11, NULL, NULL, 0),
(598, '2019-05-01', 231, 'KRISTEN', 1, '2019-05-01', 11, NULL, NULL, 0),
(599, '2019-05-01', 232, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(600, '2019-05-01', 233, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(601, '2019-05-01', 234, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(602, '2019-05-01', 235, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(603, '2019-05-01', 236, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(604, '2019-05-01', 237, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(605, '2019-05-01', 238, 'Katolik', 1, '2019-05-01', 11, NULL, NULL, 0),
(606, '2019-05-01', 239, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(607, '2019-05-01', 240, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(608, '2019-05-01', 241, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(609, '2019-05-01', 242, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(610, '2019-05-01', 243, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(611, '2019-05-01', 244, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(612, '2019-05-01', 245, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(613, '2019-05-01', 246, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(614, '2019-05-01', 247, 'Kristen Protestan', 1, '2019-05-01', 11, NULL, NULL, 0),
(615, '2019-05-01', 248, 'Islam', 1, '2019-05-01', 11, NULL, NULL, 0),
(616, '2019-05-01', 249, 'ISLAM', 1, '2019-05-01', 11, NULL, NULL, 0),
(617, '2019-05-01', 250, 'islam', 1, '2019-05-01', 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_has_akademik`
--

CREATE TABLE `taruna_has_akademik` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `prodi` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `angkatan` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_has_akademik`
--

INSERT INTO `taruna_has_akademik` (`id`, `taruna`, `prodi`, `semester`, `tanggal`, `angkatan`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(370, 4, 3, 1, '2019-04-23', 'XV', 0, '2019-04-30', 11, '2019-05-05 10:12:55', 1, 0),
(371, 6, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(372, 7, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(373, 8, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(374, 9, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(375, 10, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(376, 11, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(377, 12, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(378, 13, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(379, 14, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(380, 15, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(381, 16, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(382, 17, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(383, 18, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(384, 19, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(385, 20, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(386, 21, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(387, 22, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, '2019-05-13 02:51:56', 1, 0),
(388, 23, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(389, 24, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(390, 25, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(391, 26, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(392, 27, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(393, 28, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(394, 29, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(395, 30, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(396, 31, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(397, 32, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(398, 33, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(399, 34, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(400, 35, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(401, 36, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(402, 37, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(403, 38, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(404, 39, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(405, 40, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(406, 41, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(407, 42, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(408, 43, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(409, 44, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(410, 45, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(411, 46, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(412, 47, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(413, 48, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(414, 49, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(415, 50, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(416, 51, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(417, 52, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(418, 53, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(419, 54, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(420, 55, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(421, 56, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(422, 57, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(423, 58, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(424, 59, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(425, 60, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(426, 61, 6, 6, '2019-05-01', '27', 1, '2019-05-01', 11, NULL, NULL, 0),
(427, 62, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(428, 63, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(429, 64, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(430, 65, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(431, 66, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(432, 67, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(433, 68, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(434, 69, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(435, 70, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(436, 71, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(437, 72, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(438, 73, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(439, 74, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(440, 75, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(441, 76, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(442, 77, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(443, 78, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(444, 79, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(445, 80, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(446, 81, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(447, 82, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(448, 83, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(449, 84, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(450, 85, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(451, 86, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(452, 87, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(453, 88, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(454, 89, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(455, 90, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(456, 91, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(457, 92, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(458, 93, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(459, 94, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(460, 95, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(461, 96, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(462, 97, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(463, 98, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(464, 99, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(465, 100, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(466, 101, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(467, 102, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(468, 103, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(469, 104, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(470, 105, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(471, 106, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(472, 107, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(473, 108, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(474, 109, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(475, 110, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(476, 111, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(477, 112, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(478, 113, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(479, 114, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(480, 115, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(481, 116, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(482, 117, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(483, 118, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(484, 119, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(485, 120, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(486, 121, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(487, 122, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(488, 123, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(489, 124, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(490, 125, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(491, 126, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(492, 127, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(493, 128, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(494, 129, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(495, 130, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(496, 131, 6, 4, '2019-05-01', '28', 1, '2019-05-01', 11, NULL, NULL, 0),
(497, 132, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(498, 133, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(499, 134, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(500, 135, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(501, 136, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(502, 137, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(503, 138, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(504, 139, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(505, 140, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(506, 141, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(507, 142, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(508, 143, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(509, 144, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(510, 145, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(511, 146, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(512, 147, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(513, 148, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(514, 149, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(515, 150, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(516, 151, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(517, 152, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(518, 153, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(519, 154, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(520, 155, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(521, 156, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(522, 157, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(523, 158, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(524, 159, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(525, 160, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(526, 161, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(527, 162, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(528, 163, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(529, 164, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(530, 165, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(531, 166, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(532, 167, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(533, 168, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(534, 169, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(535, 170, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(536, 171, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(537, 172, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(538, 173, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(539, 174, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(540, 175, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(541, 176, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(542, 177, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(543, 178, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(544, 179, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(545, 180, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(546, 181, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(547, 182, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(548, 183, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(549, 184, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(550, 185, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(551, 186, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(552, 187, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(553, 188, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(554, 189, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(555, 190, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(556, 191, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(557, 192, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(558, 193, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(559, 194, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(560, 195, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(561, 196, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(562, 197, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(563, 198, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(564, 199, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(565, 200, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(566, 201, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(567, 202, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(568, 203, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(569, 204, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(570, 205, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(571, 206, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(572, 207, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(573, 208, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(574, 209, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(575, 210, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(576, 211, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(577, 212, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(578, 213, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(579, 214, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(580, 215, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(581, 216, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(582, 217, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(583, 218, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(584, 219, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(585, 220, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(586, 221, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(587, 222, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(588, 223, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(589, 224, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(590, 225, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(591, 226, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(592, 227, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(593, 228, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(594, 229, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(595, 230, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(596, 231, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(597, 232, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(598, 233, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(599, 234, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(600, 235, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(601, 236, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(602, 237, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(603, 238, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(604, 239, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(605, 240, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(606, 241, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(607, 242, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(608, 243, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(609, 244, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(610, 245, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(611, 246, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(612, 247, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(613, 248, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(614, 249, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(615, 250, 6, 2, '2019-05-01', '29', 1, '2019-05-01', 11, NULL, NULL, 0),
(616, 4, 3, 3, '2019-05-14', 'XV', 0, '2019-05-04', 1, '2019-05-04 16:13:12', 1, 1),
(617, 4, 3, 3, '2019-05-14', 'XV', 1, '2019-05-04', 1, '2019-05-05 10:12:55', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_has_asrama`
--

CREATE TABLE `taruna_has_asrama` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `asrama` int(11) NOT NULL,
  `kamar` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_has_asrama`
--

INSERT INTO `taruna_has_asrama` (`id`, `taruna`, `tanggal`, `asrama`, `kamar`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 4, '2019-05-22', 1, 1, '2019-05-03', 1, NULL, NULL, 0),
(3, 5, '2019-05-15', 1, 1, '2019-05-14', 1, '2019-05-14 07:47:33', 1, 0),
(4, 5, '2019-05-14', 2, 1, '2019-05-14', 1, '2019-05-14 07:47:33', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_has_kesehatan`
--

CREATE TABLE `taruna_has_kesehatan` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `berat_badan` int(11) DEFAULT NULL,
  `tinggi_badan` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_has_kesehatan`
--

INSERT INTO `taruna_has_kesehatan` (`id`, `taruna`, `tanggal`, `berat_badan`, `tinggi_badan`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(4, 4, '2019-04-23', 57, 172, 1, '2019-04-30', 11, '2019-05-05 10:12:55', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_has_wali_murid`
--

CREATE TABLE `taruna_has_wali_murid` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `no_ktp` varchar(150) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_telp` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `pekerjaan` varchar(150) NOT NULL,
  `createddate` date DEFAULT NULL,
  `hubungan_wali` int(11) NOT NULL,
  `penghasilan` int(11) NOT NULL DEFAULT '0',
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_has_wali_murid`
--

INSERT INTO `taruna_has_wali_murid` (`id`, `taruna`, `user`, `no_ktp`, `nama`, `tanggal_lahir`, `no_telp`, `alamat`, `pekerjaan`, `createddate`, `hubungan_wali`, `penghasilan`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(734, 4, 8, '12313', 'eko', '2019-05-15', '12313', 'Blitar', 'Wiraswasta', '2019-04-30', 1, 1, 11, '2019-05-05 10:12:55', 1, 0),
(735, 6, 14, NULL, 'Ansari amanu', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(736, 6, 15, NULL, 'Syamsiah', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(737, 7, 17, NULL, 'Imran Heryadi', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(738, 7, 18, NULL, 'Rukinah', '0000-00-00', '', '', 'Pegawai Negeri Sipil', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(739, 8, 20, NULL, 'Mizan', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(740, 8, 21, NULL, 'Ernaini', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(741, 9, 23, NULL, '082374552477', '0000-00-00', '', '', 'Alm. Muslim', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(742, 9, 24, NULL, '7/16/1984', '0000-00-00', '', '', 'Mulia handayani', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(743, 10, 26, NULL, 'Sigit pranoto', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(744, 10, 27, NULL, 'Asna wati', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(745, 11, 29, NULL, 'Edy Sutowo', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(746, 11, 30, NULL, 'Asdunah', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(747, 12, 32, NULL, 'Fahmirzon (alm)', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(748, 12, 33, NULL, 'Efi Wijaya', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(749, 13, 35, NULL, 'Drs. Soleh Siswana', '0000-00-00', '', '', 'Pensiunan Guru/PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(750, 13, 36, NULL, 'Dra. Rita Herniati', '0000-00-00', '', '', 'Pensiunan Guru/PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(751, 14, 38, NULL, 'Fahmirzon (alm)', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(752, 14, 39, NULL, 'Efi Wijaya', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(753, 15, 41, NULL, 'Agus Hairudin', '0000-00-00', '', '', 'POLRI', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(754, 15, 42, NULL, 'Enong Mardalena', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(755, 16, 44, NULL, 'sellaislamiah1@gmail.com', '0000-00-00', '', '', '2/27/1968', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(756, 16, 45, NULL, 'Wiraswasta', '0000-00-00', '', '', '8/21/1970', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(757, 17, 47, NULL, 'Irfan Rianto', '0000-00-00', '', '', 'Buruh', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(758, 17, 48, NULL, 'Siti Hodijah', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(759, 18, 50, NULL, 'Hendra Bakti', '0000-00-00', '', '', 'POLRI', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(760, 18, 51, NULL, 'Reni Hasmi', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(761, 19, 53, NULL, 'Rozali', '0000-00-00', '', '', 'PPNPN BPTD VII Kemenhub', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(762, 19, 54, NULL, 'Tanti yusrini', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(763, 20, 56, NULL, 'H.Abdul Rasyid', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(764, 20, 57, NULL, 'Sheila Leonieta Andalucya', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(765, 21, 59, NULL, '6282184786967', '0000-00-00', '', '', 'Ahmad husni', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(766, 21, 60, NULL, '6/23/1968', '0000-00-00', '', '', 'Endang reli', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(767, 22, 62, '', 'Kusnadi', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 1, 1, 11, '2019-05-13 02:51:56', 1, 0),
(768, 22, 63, '', 'Netty Caroline', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 1, 11, '2019-05-13 02:51:56', 1, 0),
(769, 23, 65, NULL, 'Ahmad Bunayu', '0000-00-00', '', '', 'PT. LG. Internasional', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(770, 23, 66, NULL, 'Tri Rosfianti', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(771, 24, 68, NULL, 'Zuber', '0000-00-00', '', '', 'Guru', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(772, 24, 69, NULL, 'Yushartati', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(773, 25, 71, NULL, 'Johan', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(774, 25, 72, NULL, 'Risma Nirmala Dewi', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(775, 26, 74, NULL, 'M.Saidi', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(776, 26, 75, NULL, 'Syamsiah', '0000-00-00', '', '', 'Pegawai Negeri Sipil', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(777, 27, 77, NULL, 'Sukoharjo', '0000-00-00', '', '', '0817446745', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(778, 27, 78, NULL, 'anjasarya6@gmail.com', '0000-00-00', '', '', '2/1/1966', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(779, 28, 80, NULL, 'paryono', '0000-00-00', '', '', 'wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(780, 28, 81, NULL, 'hermiati', '0000-00-00', '', '', 'wiraswasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(781, 29, 83, NULL, 'Alm.Hermansyah', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(782, 29, 84, NULL, 'Herlinda Pebwarita', '0000-00-00', '', '', 'Pegawai Negeri Sipil', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(783, 30, 86, NULL, 'Ngadiman', '0000-00-00', '', '', 'Pegawai swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(784, 30, 87, NULL, 'Dwi Woro shanti', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(785, 31, 89, NULL, 'Andy Syamsuddin', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(786, 31, 90, NULL, 'Yusniar', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(787, 32, 92, NULL, 'uliyanideska@gmail.com', '0000-00-00', '', '', '10/20/1966', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(788, 32, 93, NULL, 'Guru', '0000-00-00', '', '', '5/20/1971', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(789, 33, 95, NULL, 'Suroharjo', '0000-00-00', '', '', 'Aparatur Sipil Negara ( ASN )', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(790, 33, 96, NULL, 'Saijem', '0000-00-00', '', '', 'Aparatur Sipil Negara ( ASN )', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(791, 34, 98, NULL, 'KEC. LUBUKLINGGAU SELATAN II', '0000-00-00', '', '', '082178915371', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(792, 34, 99, NULL, 'diniadeandani28@gmail.com', '0000-00-00', '', '', '9/29/1965', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(793, 35, 101, NULL, 'Martius', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(794, 35, 102, NULL, 'Roslina', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(795, 36, 104, NULL, 'Didy Sutiono', '0000-00-00', '', '', 'Pegawai BUMD (PDAM)', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(796, 36, 105, NULL, 'Khaerani', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(797, 37, 107, NULL, '081276676674', '0000-00-00', '', '', 'Rahman Damanik', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(798, 37, 108, NULL, '2/26/2019', '0000-00-00', '', '', 'Rapia Silalahi', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(799, 38, 110, NULL, 'Kec.sematang borang\"', '0000-00-00', '', '', 'christinoppa25@gmail.com', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(800, 38, 111, NULL, 'M Hutagalung', '0000-00-00', '', '', 'Buruh', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(801, 39, 113, NULL, 'Darso', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(802, 39, 114, NULL, 'Murdadiah', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(803, 40, 116, NULL, 'Rizal', '0000-00-00', '', '', 'Dagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(804, 40, 117, NULL, 'Lena diana', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(805, 41, 119, NULL, 'Berman sihombing', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(806, 41, 120, NULL, 'Ris mauli siringiringo', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(807, 42, 122, NULL, '30763\"', '0000-00-00', '', '', 'maulanaamex103@gmail.com', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(808, 42, 123, NULL, 'Harnadi', '0000-00-00', '', '', 'Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(809, 43, 125, NULL, 'iyanjuliansyah71@gmail.com', '0000-00-00', '', '', '11/7/1958', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(810, 43, 126, NULL, 'Alm', '0000-00-00', '', '', '6/16/1968', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(811, 44, 128, NULL, 'Idin Gustian ( alm)', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(812, 44, 129, NULL, 'HERLINA', '0000-00-00', '', '', 'PETANI', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(813, 45, 131, NULL, 'shintaatr@gmail.com', '0000-00-00', '', '', '10/27/1960', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(814, 45, 132, NULL, 'PNS (Pensiun)', '0000-00-00', '', '', '9/22/1971', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(815, 46, 134, NULL, 'Pramono Sidi', '0000-00-00', '', '', 'Tukang ojek', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(816, 46, 135, NULL, 'Puspa Nirmala', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(817, 47, 137, NULL, 'UMAR PILI', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(818, 47, 138, NULL, 'SUSRIATI', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(819, 48, 140, NULL, 'Bastoni', '0000-00-00', '', '', 'pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(820, 48, 141, NULL, 'Ruksana dewi', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(821, 49, 143, NULL, '082123091933', '0000-00-00', '', '', 'MAULANA SYUKUR', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(822, 49, 144, NULL, '5/25/1963', '0000-00-00', '', '', 'NURBAITI', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(823, 50, 146, NULL, 'Nizom', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(824, 50, 147, NULL, 'Sartika', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(825, 51, 149, NULL, 'Kelurahan Kebun Bunga', '0000-00-00', '', '', '30152\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(826, 51, 150, NULL, '081377680899', '0000-00-00', '', '', 'Edy Firdaus', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(827, 52, 152, NULL, 'Rt 69', '0000-00-00', '', '', 'kelurahan bukit lama', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(828, 52, 153, NULL, 'kecamatan ilir barat I', '0000-00-00', '', '', 'palembang\"', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(829, 53, 155, NULL, '082272195130', '0000-00-00', '', '', 'Robinson Siallagan', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(830, 53, 156, NULL, '1/10/1967', '0000-00-00', '', '', 'Rusmida Marpaung', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(831, 54, 158, NULL, 'Aceh Besar\"', '0000-00-00', '', '', 'ryan99haviz@gmail.com', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(832, 54, 159, NULL, 'Imam Habinajud', '0000-00-00', '', '', 'Pensiunan', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(833, 55, 161, NULL, 'RW:01', '0000-00-00', '', '', 'Kecamatan Prabumulih Utara', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(834, 55, 162, NULL, 'kode pos 31121\"', '0000-00-00', '', '', 'dwijayagtn@gmail.com', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(835, 56, 164, NULL, 'kecamatan TANJUNGPINANGTIMUR', '0000-00-00', '', '', '081274747012', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(836, 56, 165, NULL, 'pancamustikas@gmail.com', '0000-00-00', '', '', '11/10/1965', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(837, 57, 167, NULL, '081274759731', '0000-00-00', '', '', 'SAHABUDDIN', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(838, 57, 168, NULL, '6/7/1968', '0000-00-00', '', '', 'ROHANI AS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(839, 58, 170, NULL, 'Alm.Hermansyah', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(840, 58, 171, NULL, 'Herlinda Pebwarita', '0000-00-00', '', '', 'Pegawai Negeri Sipil', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(841, 59, 173, NULL, 'Surobin', '0000-00-00', '', '', 'TNI-AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(842, 59, 174, NULL, 'Rosiha', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(843, 60, 176, NULL, 'USDEHAR', '0000-00-00', '', '', 'WIRASWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(844, 60, 177, NULL, 'SITI NURHEPPIATI', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(845, 61, 179, NULL, 'Sahiridi', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(846, 61, 180, NULL, 'Agustinawati', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(847, 62, 182, NULL, 'ferry syahputra', '0000-00-00', '', '', 'karyawan bumn', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(848, 62, 183, NULL, 'okmalusiana', '0000-00-00', '', '', 'karyawan bumd', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(849, 63, 185, NULL, 'Jaspardin S.pd', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(850, 63, 186, NULL, 'Emilda', '0000-00-00', '', '', 'Ibu rumah tanvga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(851, 64, 188, NULL, 'Sugito', '0000-00-00', '', '', 'Buruh harian lepas', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(852, 64, 189, NULL, 'Wideti', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(853, 65, 191, NULL, 'Budianto', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(854, 65, 192, NULL, 'Trismawaty', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(855, 66, 194, NULL, 'Zaini', '0000-00-00', '', '', 'Tani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(856, 66, 195, NULL, 'Aksaria', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(857, 67, 197, NULL, '085268470241', '0000-00-00', '', '', 'Sodikin', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(858, 67, 198, NULL, '4/23/1969', '0000-00-00', '', '', 'Heni Maryani', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(859, 68, 200, NULL, 'Edy Gustinova', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(860, 68, 201, NULL, 'Sri Hartini Zuhro', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(861, 69, 203, NULL, 'Jaspardin S.pd', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(862, 69, 204, NULL, 'Emilda', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(863, 70, 206, NULL, '31463', '0000-00-00', '', '', '082175682195', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(864, 70, 207, NULL, 'wandaaaa100@gmail.com', '0000-00-00', '', '', '9/5/1967', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(865, 71, 209, NULL, 'Amir Hamzah', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(866, 71, 210, NULL, 'Susiananh', '0000-00-00', '', '', 'Guru SD', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(867, 72, 212, NULL, 'palembang', '0000-00-00', '', '', '0857 88659972', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(868, 72, 213, NULL, 'monicavlaviaa@gmail.com', '0000-00-00', '', '', '8/5/1964', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(869, 73, 215, NULL, 'kel. Kaimana kota', '0000-00-00', '', '', '98654\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(870, 73, 216, NULL, '6285230372796', '0000-00-00', '', '', 'Djanvandry Fraim', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(871, 74, 218, NULL, 'Kec.Belitang', '0000-00-00', '', '', '\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(872, 74, 219, NULL, '081377640727', '0000-00-00', '', '', 'Antoni', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(873, 75, 221, NULL, 'Andrias', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(874, 75, 222, NULL, 'Juni Astuti', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(875, 76, 224, NULL, 'Senen Sagas', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(876, 76, 225, NULL, 'Bukia Rengen', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(877, 77, 227, NULL, 'Indera gunawan', '0000-00-00', '', '', 'TNI AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(878, 77, 228, NULL, 'Lusiana', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(879, 78, 230, NULL, 'Sjahrial Effendi', '0000-00-00', '', '', 'Pensiunan PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(880, 78, 231, NULL, 'Aslamiyawati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(881, 79, 233, NULL, 'irmasapari@yahoo.com', '0000-00-00', '', '', '7/1/1956', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(882, 79, 234, NULL, 'Petani', '0000-00-00', '', '', '7/16/1962', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(883, 80, 236, NULL, 'irmasapari@yahoo.com', '0000-00-00', '', '', '7/1/1956', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(884, 80, 237, NULL, 'Petani', '0000-00-00', '', '', '7/16/1962', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(885, 81, 239, NULL, 'Obet Woy', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(886, 81, 240, NULL, 'Laurina kabes', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(887, 82, 242, NULL, 'Diharman', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(888, 82, 243, NULL, 'Suwarni', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(889, 83, 245, NULL, 'Kel/desa (ARGOSIGEMERAI)', '0000-00-00', '', '', '081366816019', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(890, 83, 246, NULL, 'noahbong29@gmail.com', '0000-00-00', '', '', '1/19/1972', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(891, 84, 248, NULL, 'SUBUR', '0000-00-00', '', '', 'GURU', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(892, 84, 249, NULL, 'LENY ADRIANI', '0000-00-00', '', '', 'GURU', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(893, 85, 251, NULL, 'wicaksonop75@gmail.com', '0000-00-00', '', '', '10/9/1968', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(894, 85, 252, NULL, 'Pegawai swasta', '0000-00-00', '', '', '3/13/2019', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(895, 86, 254, NULL, 'Kec. Banyuasin', '0000-00-00', '', '', '082278921737', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(896, 86, 255, NULL, 'wahyuhadinata21@gmail.com', '0000-00-00', '', '', '1/15/1970', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(897, 87, 257, NULL, 'klagete', '0000-00-00', '', '', '98416\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(898, 87, 258, NULL, '0877427379616', '0000-00-00', '', '', 'Usman Paknawan', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(899, 88, 260, NULL, 'hariyantonokia2690@gmail.com', '0000-00-00', '', '', '2/1/1959', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(900, 88, 261, NULL, 'Buruh', '0000-00-00', '', '', '11/2/1961', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(901, 89, 263, NULL, 'KEC AEKSONGSONGAN', '0000-00-00', '', '', '082125291839', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(902, 89, 264, NULL, 'jerilubis5@gmail.com', '0000-00-00', '', '', '7/14/1957', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(903, 90, 266, NULL, 'Kec.Belitang', '0000-00-00', '', '', '\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(904, 90, 267, NULL, '081377640727', '0000-00-00', '', '', 'Antoni', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(905, 91, 269, NULL, 'BENTENG', '0000-00-00', '', '', '92811\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(906, 91, 270, NULL, '082237968937', '0000-00-00', '', '', 'NASRUDDIN', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(907, 92, 272, NULL, 'Aimas', '0000-00-00', '', '', '98444\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(908, 92, 273, NULL, '081344471786', '0000-00-00', '', '', 'Muhammad Nur Badewi', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(909, 93, 275, NULL, 'kaimana kota', '0000-00-00', '', '', '98654\"', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(910, 93, 276, NULL, '087780136606', '0000-00-00', '', '', 'Juddar haddise', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(911, 94, 278, NULL, 'yueyiandaraxiipa2@gmail.com', '0000-00-00', '', '', '11/5/1979', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(912, 94, 279, NULL, 'Dagang', '0000-00-00', '', '', '10/8/1978', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(913, 95, 281, NULL, 'sorong manoi', '0000-00-00', '', '', '087719017570', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(914, 95, 282, NULL, 'lazeransus@gmail.com', '0000-00-00', '', '', '11/21/1977', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(915, 96, 284, NULL, 'Junaidi', '0000-00-00', '', '', 'Pegawai Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(916, 96, 285, NULL, 'Silaturahmi', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(917, 97, 287, NULL, 'KELURAHAN PENGANJURAN', '0000-00-00', '', '', 'KAB. BANYUWANGI', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(918, 97, 288, NULL, 'JATIM', '0000-00-00', '', '', '085928976140', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(919, 98, 290, NULL, 'KELURAHAN PENGANJURAN', '0000-00-00', '', '', 'KAB. BANYUWANGI', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(920, 98, 291, NULL, 'JATIM', '0000-00-00', '', '', '085928976140', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(921, 99, 293, NULL, '32111.\"', '0000-00-00', '', '', 'syariframadhan2212@gmail.com', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(922, 99, 294, NULL, 'Mohamad syamsuddin', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(923, 100, 296, NULL, 'kecamatan ilir barat 1', '0000-00-00', '', '', '081541260769', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(924, 100, 297, NULL, 'aismra65@gmail.com', '0000-00-00', '', '', '5/9/1968', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(925, 101, 299, NULL, 'Candra Irawan', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(926, 101, 300, NULL, 'Heriani', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(927, 102, 302, NULL, 'Arpan', '0000-00-00', '', '', 'Buruh', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(928, 102, 303, NULL, 'Nurasia', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(929, 103, 305, NULL, 'Ngatiman Wiji Kuntoro', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(930, 103, 306, NULL, 'Dariyanti', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(931, 104, 308, NULL, 'Agus supranto', '0000-00-00', '', '', 'swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(932, 104, 309, NULL, 'Rogaya', '0000-00-00', '', '', 'ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(933, 105, 311, NULL, '0895331483993', '0000-00-00', '', '', 'Dedi', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(934, 105, 312, NULL, '9/28/1972', '0000-00-00', '', '', 'Sri Astuti', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(935, 106, 314, NULL, 'HERMANSYAH SUMOR', '0000-00-00', '', '', 'GURU SWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(936, 106, 315, NULL, 'HARMAYUNA', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(937, 107, 317, NULL, 'Stenly Sambul', '0000-00-00', '', '', 'Supir', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(938, 107, 318, NULL, 'Novita Orintaman', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(939, 108, 320, NULL, 'Yakub', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(940, 108, 321, NULL, 'Maryani Spd', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(941, 109, 323, NULL, 'Rustam imron', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(942, 109, 324, NULL, 'Akiyu', '0000-00-00', '', '', 'Pns guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(943, 110, 326, NULL, 'Candra Irawan', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(944, 110, 327, NULL, 'Heriani', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(945, 111, 329, NULL, 'Nurul Hidayat', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(946, 111, 330, NULL, 'Nya. Azazah sopiah', '0000-00-00', '', '', 'guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(947, 112, 332, NULL, '081275692395', '0000-00-00', '', '', 'M Usyef Ahyani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(948, 112, 333, NULL, '9/19/1969', '0000-00-00', '', '', 'Marlina', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(949, 113, 335, NULL, 'Harsono', '0000-00-00', '', '', 'Buruh', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(950, 113, 336, NULL, 'Leni puspitasari', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(951, 114, 338, NULL, 'Slamet Murtaji', '0000-00-00', '', '', 'Pensiunan', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(952, 114, 339, NULL, 'Yanti', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(953, 115, 341, NULL, 'AIMIN', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(954, 115, 342, NULL, 'SURATMI', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(955, 116, 344, NULL, 'rw 13 plaju ulu\"', '0000-00-00', '', '', 'bkusuma13@gmail.com', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(956, 116, 345, NULL, 'Awaludin', '0000-00-00', '', '', 'Buruh', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(957, 117, 347, NULL, 'Abdul Hamid', '0000-00-00', '', '', 'KARYAWAN SWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(958, 117, 348, NULL, 'Mardiana', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(959, 118, 350, NULL, 'erikadinta.eds@gmail.com', '0000-00-00', '', '', '3/12/1972', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(960, 118, 351, NULL, 'Wiraswasta', '0000-00-00', '', '', '5/15/1972', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(961, 119, 353, NULL, 'KEC. PASAR MANNA', '0000-00-00', '', '', '081219825864', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(962, 119, 354, NULL, 'rakykusuma39@gmail.com', '0000-00-00', '', '', '4/10/1962', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(963, 120, 356, NULL, 'KEC AEKSONGSONGAN', '0000-00-00', '', '', '082125291839', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(964, 120, 357, NULL, 'jerilubis5@gmail.com', '0000-00-00', '', '', '7/14/1957', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(965, 121, 359, NULL, 'Pegangto', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(966, 121, 360, NULL, 'Suriani', '0000-00-00', '', '', 'Guru Honorer', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(967, 122, 362, NULL, '+62 857-8360-5385', '0000-00-00', '', '', 'Suharsono', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(968, 122, 363, NULL, '2/22/1956', '0000-00-00', '', '', 'Musriyatin', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(969, 123, 365, NULL, 'SUHRO WARNI', '0000-00-00', '', '', 'KARYAWAN SWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(970, 123, 366, NULL, 'ELSA DIANA', '0000-00-00', '', '', 'HONORER', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(971, 124, 368, NULL, 'Sony ferdynto', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(972, 124, 369, NULL, 'Christien Herlin', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(973, 125, 371, NULL, 'Elias. Manaruri', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(974, 125, 372, NULL, 'Linda. Rumere', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(975, 126, 374, NULL, 'Sjahrial Effendi', '0000-00-00', '', '', 'Pensiunan PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(976, 126, 375, NULL, 'Aslamiyawati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(977, 127, 377, NULL, 'Kemuning', '0000-00-00', '', '', '089677499931', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(978, 127, 378, NULL, 'agunghidayatullah858@gmail.com', '0000-00-00', '', '', '2/16/1967', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(979, 128, 380, NULL, 'sumatra utara', '0000-00-00', '', '', '082282226594', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(980, 128, 381, NULL, 'kevinmeliala13@gmail.com', '0000-00-00', '', '', '1/31/1976', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(981, 129, 383, NULL, 'kecamatan fakfak\"', '0000-00-00', '', '', 'Shusanfadilah99@gmail.com', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(982, 129, 384, NULL, 'Imran aditya warman', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(983, 130, 386, NULL, 'Mateus Mansumber', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(984, 130, 387, NULL, 'Marice Mansumber', '0000-00-00', '', '', 'Inu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(985, 131, 389, NULL, 'kecamatan sorong manoi', '0000-00-00', '', '', '081247662086', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(986, 131, 390, NULL, 'ghalioramandey96.@gmail.com', '0000-00-00', '', '', '3/17/1965', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(987, 132, 392, NULL, 'Ibnu Hajar', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(988, 132, 393, NULL, 'Umi Kalsum', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(989, 133, 395, NULL, 'Ikhrom', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(990, 133, 396, NULL, 'Neni Triyani', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(991, 134, 398, NULL, 'Yusri Achmad', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(992, 134, 399, NULL, 'Sri Banun Pribadi', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(993, 135, 401, NULL, 'I wayan surya dharma putra', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(994, 135, 402, NULL, 'Ni wayan sri utami', '0000-00-00', '', '', 'Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(995, 136, 404, NULL, 'Burhanuddin', '0000-00-00', '', '', 'Pegawai Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(996, 136, 405, NULL, 'Mardiani Gumala', '0000-00-00', '', '', 'Pegawai BUMN', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(997, 137, 407, NULL, 'M. Rais', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(998, 137, 408, NULL, 'Novi susanti', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(999, 138, 410, NULL, 'Syarkawi', '0000-00-00', '', '', 'Karyawan BUMN', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1000, 138, 411, NULL, 'Partin', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1001, 139, 413, NULL, 'Bambang Subekti', '0000-00-00', '', '', 'Polri', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1002, 139, 414, NULL, 'Puji Rahayu', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1003, 140, 416, NULL, 'Zulfahmi', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1004, 140, 417, NULL, 'Indrawati', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1005, 141, 419, NULL, 'Iwan setiawan', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1006, 141, 420, NULL, 'Umi kalsum', '0000-00-00', '', '', 'Pns', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1007, 142, 422, NULL, 'Humaedi', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1008, 142, 423, NULL, 'Yurneti', '0000-00-00', '', '', 'Mengurus Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1009, 143, 425, NULL, 'Mulyadi M', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1010, 143, 426, NULL, 'Rita Marlena', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1011, 144, 428, NULL, 'Djoni Andry', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1012, 144, 429, NULL, 'Rqtu Ina', '0000-00-00', '', '', 'PPNPN', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1013, 145, 431, NULL, 'HARWANI', '0000-00-00', '', '', 'KARYAWAN SWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1014, 145, 432, NULL, 'MARNILAH', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1015, 146, 434, NULL, 'Zakaria rumasukun', '0000-00-00', '', '', 'Pegawai pln', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1016, 146, 435, NULL, 'Siti fatiyah taulu', '0000-00-00', '', '', '-', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1017, 147, 437, NULL, 'SYAMSUDDIN', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1018, 147, 438, NULL, 'RATNA', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1019, 148, 440, NULL, 'Sudarto', '0000-00-00', '', '', 'PETANI', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1020, 148, 441, NULL, 'Darsiyem', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1021, 149, 443, NULL, 'Edi hasmi', '0000-00-00', '', '', 'Buruh', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1022, 149, 444, NULL, 'Trisna E Wati', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1023, 150, 446, NULL, 'Jhonny Sinaga', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1024, 150, 447, NULL, 'Berliana', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1025, 151, 449, NULL, 'Mun\'im', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1026, 151, 450, NULL, 'Nurlela Wati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1027, 152, 452, NULL, 'JUNANDI PANJAITAN', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1028, 152, 453, NULL, 'TETTY NAINGGOLAN', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1029, 153, 455, NULL, 'Fa\'aso zendrato', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1030, 153, 456, NULL, 'Sumeni zendrato', '0000-00-00', '', '', 'ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1031, 154, 458, NULL, 'Hadi Batara Yakub Pangihutan Marpaung, S.Pi', '0000-00-00', '', '', 'Pegawai Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1032, 154, 459, NULL, 'Maslina Rotia Sagala, S.Pi', '0000-00-00', '', '', 'Pegawai Negeri Sipil', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1033, 155, 461, NULL, 'I Gusti Ngurah Suandika', '0000-00-00', '', '', 'Pegawai Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1034, 155, 462, NULL, 'Ni Nyoman Ayu Padma', '0000-00-00', '', '', 'Pegawai Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1035, 156, 464, NULL, 'Agus Elian Efendi', '0000-00-00', '', '', 'TNI-AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1036, 156, 465, NULL, 'Kartika Purnama Sari', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1037, 157, 467, NULL, 'SYAM HERIADY', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1038, 157, 468, NULL, 'NURMAYA', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1039, 158, 470, NULL, 'Ibnu said sugito', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1040, 158, 471, NULL, 'Padia hawalima', '0000-00-00', '', '', 'Pns', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1041, 159, 473, NULL, 'Darul Kutni', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1042, 159, 474, NULL, 'Zuriyah', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1043, 160, 476, NULL, 'Fatroni', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1044, 160, 477, NULL, 'Sopiah', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1045, 161, 479, NULL, 'ZAINAL ARIFIN', '0000-00-00', '', '', 'TNI-AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1046, 161, 480, NULL, 'SRI WAHYUNI', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1047, 162, 482, NULL, 'Mun\'im', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1048, 162, 483, NULL, 'Nurlela Wati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1049, 163, 485, NULL, 'Iwan Rosadi', '0000-00-00', '', '', 'TNI-AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1050, 163, 486, NULL, 'Sugiartik', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1051, 164, 488, NULL, 'Mirzal', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1052, 164, 489, NULL, 'Hoiriah', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1053, 165, 491, NULL, 'Sugiyana', '0000-00-00', '', '', 'Purnawirawan POLRI', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1054, 165, 492, NULL, 'Maret Naningsih', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1055, 166, 494, NULL, 'Sunardi', '0000-00-00', '', '', 'SWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1056, 166, 495, NULL, 'Yuli Yanti', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1057, 167, 497, NULL, 'ALM. DIDI SURADI', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1058, 167, 498, NULL, 'ALMH. SUDARNI', '0000-00-00', '', '', '-', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1059, 168, 500, NULL, 'Sugito', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1060, 168, 501, NULL, 'Rusmiyati', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1061, 169, 503, NULL, 'Suripanto', '0000-00-00', '', '', 'TNI AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1062, 169, 504, NULL, 'Herni Putri Palupi', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1063, 170, 506, NULL, 'Syamsidi', '0000-00-00', '', '', 'SWASTA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1064, 170, 507, NULL, 'Sugiarsih', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1065, 171, 509, NULL, 'Dimro Akohar', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1066, 171, 510, NULL, 'Zurmiati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1067, 172, 512, NULL, 'H syafrizal', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1068, 172, 513, NULL, 'Noviana anggraini', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1069, 173, 515, NULL, 'HERMONO', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1070, 173, 516, NULL, 'FIRDA YURIKA', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1071, 174, 518, NULL, 'Agus Salim', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1072, 174, 519, NULL, 'Elmiza', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1073, 175, 521, NULL, 'Hafid', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1074, 175, 522, NULL, 'Siti hartina', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1075, 176, 524, NULL, 'Mohamad zainudin', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1076, 176, 525, NULL, 'Ernasyarfidah (almh)', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1077, 177, 527, NULL, 'Kariyadi (alm)', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1078, 177, 528, NULL, 'Wery Kasmiyati', '0000-00-00', '', '', 'Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1079, 178, 530, NULL, 'Djoko Purwono', '0000-00-00', '', '', 'Pensiunan', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1080, 178, 531, NULL, 'Enny indrayanti', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1081, 179, 533, NULL, 'ALAN ASLANI', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1082, 179, 534, NULL, 'ELFITA AGUSTINI', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1083, 180, 536, NULL, 'Lukman A Roni', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1084, 180, 537, NULL, 'Ernawati', '0000-00-00', '', '', 'Dosen', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1085, 181, 539, NULL, 'MUJIONO', '0000-00-00', '', '', 'PENSIUNAN PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1086, 181, 540, NULL, 'Ngadayati', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1087, 182, 542, NULL, 'Abdul Aziz', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1088, 182, 543, NULL, 'Khasanah', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1089, 183, 545, NULL, 'Mizan', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1090, 183, 546, NULL, 'Ernaini', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1091, 184, 548, NULL, 'Agus rahman', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1092, 184, 549, NULL, 'Yusminar', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1093, 185, 551, NULL, 'Sukatno', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1094, 185, 552, NULL, 'Sri Hartuti', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1095, 186, 554, NULL, 'Joni hendrik', '0000-00-00', '', '', 'Tukang kayu', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1096, 186, 555, NULL, 'Zuniati', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1097, 187, 557, NULL, 'Alm. Noldin Gagaramusu', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1098, 187, 558, NULL, 'Dewi Fitriawati', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1099, 188, 560, NULL, 'Lalu Readyman', '0000-00-00', '', '', 'Pensiunan pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1100, 188, 561, NULL, 'Bq Hartini Andayani', '0000-00-00', '', '', 'Pns', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1101, 189, 563, NULL, 'ERWIN IRAWAN', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1102, 189, 564, NULL, 'Asnawati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1103, 190, 566, NULL, 'Heryansa', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1104, 190, 567, NULL, 'Sumilawati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1105, 191, 569, NULL, 'Ma\'mun', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1106, 191, 570, NULL, 'Aan andriyani', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1107, 192, 572, NULL, 'Mappasessu', '0000-00-00', '', '', '-', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1108, 192, 573, NULL, 'Herniwati', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1109, 193, 575, NULL, 'SAFRI AGUSTIN', '0000-00-00', '', '', 'ASN', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1110, 193, 576, NULL, 'ROSMERYANTI', '0000-00-00', '', '', 'IBU RUMAH TANGGA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1111, 194, 578, NULL, 'Arinal Helmi Setiawan', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1112, 194, 579, NULL, 'Siti Arifiyah', '0000-00-00', '', '', 'PNS (guru)', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1113, 195, 581, NULL, 'SOBRI', '0000-00-00', '', '', 'Pensiunan PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1114, 195, 582, NULL, 'LEGIRA', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1115, 196, 584, NULL, 'Yafeth royke karwur', '0000-00-00', '', '', 'Karyawan BUMN', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1116, 196, 585, NULL, 'Yessy kristiani kalangi', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1117, 197, 587, NULL, 'Harmanzah', '0000-00-00', '', '', 'Pegawai swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1118, 197, 588, NULL, 'Elya Sumarni Mursy', '0000-00-00', '', '', 'Dagang', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1119, 198, 590, NULL, 'PUJIYONO', '0000-00-00', '', '', 'Guru BK', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1120, 198, 591, NULL, 'Wartini', '0000-00-00', '', '', 'Guru bahasa indonesia', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1121, 199, 593, NULL, 'I MADE MUSMULIADI', '0000-00-00', '', '', 'PNS (GURU SMK)', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1122, 199, 594, NULL, 'NI MADE YADIASIH', '0000-00-00', '', '', 'PNS (Kominfo)', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1123, 200, 596, NULL, 'Amad Supriadi', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1124, 200, 597, NULL, 'Sabaria Amir', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1125, 201, 599, NULL, 'I Gede Made Kamiartha', '0000-00-00', '', '', 'Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1126, 201, 600, NULL, 'Ni Luh Gede Sri Yuliani', '0000-00-00', '', '', 'Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1127, 202, 602, NULL, 'R Heru Whynarto', '0000-00-00', '', '', 'Purn. TNI AD', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1128, 202, 603, NULL, 'Susi Mulyati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1129, 203, 605, NULL, 'Surnata', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1130, 203, 606, NULL, 'Sasmita', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1131, 204, 608, NULL, 'Indi sudianto', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1132, 204, 609, NULL, 'Priana', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1133, 205, 611, NULL, 'Gokmanto siringoringo', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1134, 205, 612, NULL, 'Rauli gurning', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1135, 206, 614, NULL, 'Dody Kurniawan', '0000-00-00', '', '', 'Tidak Bekerja', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1136, 206, 615, NULL, 'Sri Utami', '0000-00-00', '', '', 'Swasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1137, 207, 617, NULL, 'Anas Susjanto', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1138, 207, 618, NULL, 'Sri Widyastuti', '0000-00-00', '', '', 'Guru PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1139, 208, 620, NULL, 'Paternus CAR', '0000-00-00', '', '', 'Pensiunan', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1140, 208, 621, NULL, 'Asiah', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1141, 209, 623, NULL, 'Fazani', '0000-00-00', '', '', 'Pegawai Negeri Sipil', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1142, 209, 624, NULL, 'Mainur Rohayati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1143, 210, 626, NULL, 'M. Irsyad Daud (alm)', '0000-00-00', '', '', 'karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1144, 210, 627, NULL, 'Daryati', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1145, 211, 629, NULL, 'Adiwarlis', '0000-00-00', '', '', 'pedagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1146, 211, 630, NULL, 'Novita Rahmi', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1147, 212, 632, NULL, 'Mulyadi (alm)', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1148, 212, 633, NULL, 'Kardilawati', '0000-00-00', '', '', 'Pns', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1149, 213, 635, NULL, 'Selius', '0000-00-00', '', '', 'Tani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1150, 213, 636, NULL, 'Marnelia pinel', '0000-00-00', '', '', 'Tani', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1151, 214, 638, NULL, 'Abu Bakar', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1152, 214, 639, NULL, 'Martini', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1153, 215, 641, NULL, 'Hermansyah', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1154, 215, 642, NULL, 'Alon Yulia Sari', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1155, 216, 644, NULL, 'Edy Surandi', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1156, 216, 645, NULL, 'Erwinda', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1157, 217, 647, NULL, 'Chairul Insani Ilham', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1158, 217, 648, NULL, 'Sri Heryani', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1159, 218, 650, NULL, 'Kadek Wiadnyana Yasa', '0000-00-00', '', '', 'Buruh Harian Lepas', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1160, 218, 651, NULL, 'Ketut Liang Kariani', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1161, 219, 653, NULL, 'Bontor Siahaan', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1162, 219, 654, NULL, 'Hotria Kristina Simarmata', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1163, 220, 656, NULL, 'i gede ngurah karang', '0000-00-00', '', '', 'pns tni-ad', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1164, 220, 657, NULL, 'ni ketut wertiani', '0000-00-00', '', '', 'ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1165, 221, 659, NULL, 'Yohanes Taka Dosi', '0000-00-00', '', '', 'PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1166, 221, 660, NULL, 'Meriyana', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1167, 222, 662, NULL, 'I Nyoman Lanus', '0000-00-00', '', '', 'WIRAUSAHA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1168, 222, 663, NULL, 'Ni Wayan Keneng Susilawati', '0000-00-00', '', '', 'ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1169, 223, 665, NULL, 'Alm.Setyo achmadi', '0000-00-00', '', '', 'Wirausaha', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1170, 223, 666, NULL, 'Varin karmila', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1171, 224, 668, NULL, 'Moh rizal', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1172, 224, 669, NULL, 'Sitti syamsuri', '0000-00-00', '', '', 'Guru', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1173, 225, 671, NULL, 'Eko Eddy Purnomo', '0000-00-00', '', '', 'Pegawai swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1174, 225, 672, NULL, 'Paryanti', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1175, 226, 674, NULL, 'H.Azhar', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1176, 226, 675, NULL, 'Hj.halimatussakdiah', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 2, 0, 11, NULL, NULL, 0);
INSERT INTO `taruna_has_wali_murid` (`id`, `taruna`, `user`, `no_ktp`, `nama`, `tanggal_lahir`, `no_telp`, `alamat`, `pekerjaan`, `createddate`, `hubungan_wali`, `penghasilan`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1177, 227, 677, NULL, 'Slamet', '0000-00-00', '', '', 'Buruh harian lepas', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1178, 227, 678, NULL, 'Jumaroh', '0000-00-00', '', '', 'Buruh', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1179, 228, 680, NULL, 'Misro Dianto', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1180, 228, 681, NULL, 'Marsiana', '0000-00-00', '', '', 'Buruh Pabrik', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1181, 229, 683, NULL, 'Ilhamsyah ugen', '0000-00-00', '', '', 'Pns', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1182, 229, 684, NULL, 'Syamsuryati', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1183, 230, 686, NULL, 'JONTER BUTAR BUTAR', '0000-00-00', '', '', 'WIRAUSAHA', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1184, 230, 687, NULL, 'FLORA GURNING', '0000-00-00', '', '', 'WIRASWASTA', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1185, 231, 689, NULL, 'Saut Parulian', '0000-00-00', '', '', 'Camat', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1186, 231, 690, NULL, 'Metty Panggabean', '0000-00-00', '', '', 'Guru SD', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1187, 232, 692, NULL, 'Busratul Mukmin', '0000-00-00', '', '', 'Jualan', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1188, 232, 693, NULL, 'Erniyusi', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1189, 233, 695, NULL, 'Asra Rudin', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1190, 233, 696, NULL, 'Vera Wati', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1191, 234, 698, NULL, 'Suhaili', '0000-00-00', '', '', 'Pns polda', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1192, 234, 699, NULL, 'Nudairoh', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1193, 235, 701, NULL, 'Herman', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1194, 235, 702, NULL, 'Irawati', '0000-00-00', '', '', 'IRT', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1195, 236, 704, NULL, 'Lalu wire jaye', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1196, 236, 705, NULL, 'Baiq mariati', '0000-00-00', '', '', 'Petani', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1197, 237, 707, NULL, 'Hazrin', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1198, 237, 708, NULL, 'Ike Novri Yarin', '0000-00-00', '', '', 'Guru Honorer', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1199, 238, 710, NULL, 'Sulpicius Widyono', '0000-00-00', '', '', 'Karyawan Swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1200, 238, 711, NULL, 'Agnes Eka Purwanti', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1201, 239, 713, NULL, 'Tabroni', '0000-00-00', '', '', 'Karyawan swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1202, 239, 714, NULL, 'Minarni', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1203, 240, 716, NULL, 'Bebeng Zuber Efendi', '0000-00-00', '', '', 'Pensiunan PNS', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1204, 240, 717, NULL, 'Halfiah', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1205, 241, 719, NULL, 'Biro Peranginangin', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1206, 241, 720, NULL, 'Santarina Pelawi', '0000-00-00', '', '', 'Pns', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1207, 242, 722, NULL, 'Adi Suyatno', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1208, 242, 723, NULL, 'Hari Yanti', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1209, 243, 725, NULL, 'Ayip Mahdi', '0000-00-00', '', '', 'Pekerja swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1210, 243, 726, NULL, 'Rodiyah', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1211, 244, 728, NULL, 'Ginun', '0000-00-00', '', '', 'Pedagang', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1212, 244, 729, NULL, 'Mahnim', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1213, 245, 731, NULL, 'Heri Purwanto', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1214, 245, 732, NULL, 'Yunita Nirwana Sari', '0000-00-00', '', '', 'PNS', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1215, 246, 734, NULL, 'syahril', '0000-00-00', '', '', 'swasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1216, 246, 735, NULL, 'melia', '0000-00-00', '', '', 'ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1217, 247, 737, NULL, 'Sarinberan', '0000-00-00', '', '', 'Petani', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1218, 247, 738, NULL, 'Marthatuban', '0000-00-00', '', '', 'Ibu rumah tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1219, 248, 740, NULL, 'Firdaus Roza', '0000-00-00', '', '', 'Pensiunan', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1220, 248, 741, NULL, 'Norti Nofriza', '0000-00-00', '', '', 'Ibu Rumah Tangga', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1221, 249, 743, NULL, 'MUKHAMAD', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1222, 249, 744, NULL, 'FITRI WIDYASTUTIK', '0000-00-00', '', '', 'Wiraswasta', '2019-05-01', 2, 0, 11, NULL, NULL, 0),
(1223, 250, 746, NULL, 'paino', '0000-00-00', '', '', 'guru', '2019-05-01', 1, 0, 11, NULL, NULL, 0),
(1224, 250, 747, NULL, 'winarsih', '0000-00-00', '', '', 'dosen', '2019-05-01', 2, 0, 11, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_naik_semester`
--

CREATE TABLE `taruna_naik_semester` (
  `id` int(11) NOT NULL,
  `naik_semester` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_naik_semester`
--

INSERT INTO `taruna_naik_semester` (`id`, `naik_semester`, `taruna`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 4, '2019-05-04', 1, '2019-05-04 16:13:12', 1, 1),
(2, 2, 4, '2019-05-04', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `taruna_sakit`
--

CREATE TABLE `taruna_sakit` (
  `id` int(11) NOT NULL,
  `taruna` int(11) NOT NULL,
  `keterangan` text,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `surat_dokter` text,
  `jam_awal` varchar(15) DEFAULT NULL,
  `jam_akhir` varchar(15) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taruna_sakit`
--

INSERT INTO `taruna_sakit` (`id`, `taruna`, `keterangan`, `tanggal_awal`, `tanggal_akhir`, `surat_dokter`, `jam_awal`, `jam_akhir`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 4, 'Tidak Ada Keterangan Dokter', '2019-05-01', '2019-05-02', NULL, '00:00', '00:00', '2019-05-01', 1, NULL, NULL, 0),
(2, 4, 'Tidak ada', '2019-05-01', '2019-05-02', NULL, '12:30', '12:30', '2019-05-02', 1, NULL, NULL, 0),
(3, 4, 'Sakit Perut', '2019-05-05', '2019-05-05', NULL, '12:08', '12:05', '2019-05-05', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tombol_action`
--

CREATE TABLE `tombol_action` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `tombol` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `priveledge` int(11) NOT NULL,
  `token` text NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `priveledge`, `token`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'admin', 'admin', 1, 'fXd-ztP3LOU:APA91bHxmYHzXBUZLZHYNeX1fRld_ZhL63jksAf_c3_Sw-D2Opw3Y-JcjXqYkSUdRRKPtY4ppbeGPVoW1kNAqA5g6RqR5IMO4qg7sYkHZBWe2fOWi3UmXDIs_ej21sWnvsZUFBhW7Duc', '0000-00-00', NULL, '2019-04-23 17:25:56', NULL, 0),
(2, 'manajemen', '1234', 5, 'ecGF6YOruzw:APA91bHYxSePqa5N7FLcaS9AON3eB7D64dYVtQj6QdbYGTUfAoLK_lCqss2S9owhP4zJFWdM0KI22gvLjtQjm7_hRWWztE1O4VAUXejurPgUtVZB7cu0dkz9gIw0FbWCwvh8v9qUke5T', NULL, NULL, '2019-04-23 17:26:12', NULL, 0),
(7, '1318033', '1234', 3, 'ecGF6YOruzw:APA91bHYxSePqa5N7FLcaS9AON3eB7D64dYVtQj6QdbYGTUfAoLK_lCqss2S9owhP4zJFWdM0KI22gvLjtQjm7_hRWWztE1O4VAUXejurPgUtVZB7cu0dkz9gIw0FbWCwvh8v9qUke5T', '2019-04-10', 1, '2019-04-23 17:25:21', NULL, 0),
(8, 'eko', '1234', 2, 'c446ADoHPY0:APA91bEO-76P2Q16pxqFSYP-ousgnjAe8OOWbMjdrK_ll_QxhJZKyyDNYeNNiskDwA25hSk2LJHiHX2VzGfRErs030Vex-z9dVi6sZZRPjycdnRpqRUhERWflScSxu6V0jQVQC-Bmp4C', '2019-04-10', 1, '2019-04-23 05:56:29', NULL, 0),
(9, '1318031', '1234', 3, '', '2019-04-11', 1, NULL, NULL, 0),
(10, '123131', 'lMJtAc', 2, '', '2019-04-11', 1, NULL, NULL, 0),
(11, 'superadmin', 'sasuke', 4, '', '2019-04-12', NULL, NULL, NULL, 0),
(12, 'tejo', 'tejo', 1, 'ecGF6YOruzw:APA91bHYxSePqa5N7FLcaS9AON3eB7D64dYVtQj6QdbYGTUfAoLK_lCqss2S9owhP4zJFWdM0KI22gvLjtQjm7_hRWWztE1O4VAUXejurPgUtVZB7cu0dkz9gIw0FbWCwvh8v9qUke5T', NULL, NULL, '2019-04-23 17:06:59', NULL, 0),
(13, '160418', '19961001', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(14, '19710406', '19710406', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(15, '19761012', '19761012', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(16, '1604025', '19990225', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(17, '19700314', '19700314', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(18, '19720515', '19720515', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(19, '160434', '19990508', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(20, '19640815', '19640815', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(21, '19640615', '19640615', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(22, '1604041', '19960309', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(23, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(24, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(25, '1604028', '19981003', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(26, '19700503', '19700503', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(27, '19750507', '19750507', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(28, '160422', '19990213', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(29, '19681115', '19681115', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(30, '19680914', '19680914', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(31, '1604020', '19990427', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(32, '19621110', '19621110', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(33, '19670820', '19670820', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(34, '1604027', '19950310', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(35, '18560313', '18560313', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(36, '19551206', '19551206', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(37, '1604020', '19990427', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(38, '19621110', '19621110', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(39, '19670820', '19670820', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(40, '160424', '19980112', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(41, '19650808', '19650808', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(42, '19670417', '19670417', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(43, '1604048', '19980722', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(44, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(45, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(46, '160429', '20000308', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(47, '19700206', '19700206', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(48, '19740903', '19740903', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(49, '1604032', '19971125', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(50, '19691006', '19691006', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(51, '19690707', '19690707', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(52, '160455', '19980316', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(53, '19700813', '19700813', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(54, '19770530', '19770530', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(55, '160437', '19980820', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(56, '19700718', '19700718', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(57, '19750802', '19750802', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(58, '1604023', '19990129', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(59, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(60, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(61, '1604001', '19970412', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(62, '19731201', '19731201', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(63, '19720820', '19720820', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(64, '1604052', '19981122', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(65, '19680914', '19680914', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(66, '19721012', '19721012', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(67, '160445', '19980520', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(68, '19630620', '19630620', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(69, '19680607', '19680607', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(70, '160430', '19980721', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(71, '19650817', '19650817', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(72, '19680705', '19680705', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(73, '1604038', '19980214', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(74, '19560101', '19560101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(75, '19650208', '19650208', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(76, '160405', '19980723', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(77, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(78, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(79, '160417', '19970429', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(80, '19721012', '19721012', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(81, '19760308', '19760308', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(82, '1604056', '19980107', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(83, '19600322', '19600322', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(84, '19620404', '19620404', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(85, '1604046', '19981224', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(86, '19691221', '19691221', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(87, '19730515', '19730515', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(88, '1604012', '19981001', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(89, '19701109', '19701109', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(90, '19700105', '19700105', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(91, '1604011', '19981219', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(92, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(93, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(94, '1604035', '19980614', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(95, '20190629', '20190629', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(96, '20190601', '20190601', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(97, '160413', '19980928', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(98, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(99, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(100, '160414', '19981205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(101, '19630401', '19630401', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(102, '19690702', '19690702', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(103, '1604053', '19980709', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(104, '19680823', '19680823', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(105, '19651011', '19651011', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(106, '1604007', '20190615', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(107, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(108, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(109, '1604009', '19981229', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(110, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(111, '19660514', '19660514', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(112, '160404', '19981013', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(113, '19571110', '19571110', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(114, '19670928', '19670928', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(115, '160419', '19990305', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(116, '19800818', '19800818', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(117, '19790611', '19790611', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(118, '1604015', '19970313', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(119, '19570112', '19570112', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(120, '19620510', '19620510', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(121, '1604002', '19980310', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(122, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(123, '19720427', '19720427', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(124, '1604039', '19980706', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(125, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(126, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(127, '1604031', '19960305', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(128, '19770628', '19770628', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(129, '19760816', '19760816', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(130, '160450', '19980809', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(131, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(132, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(133, '1604021', '19981130', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(134, '19660909', '19660909', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(135, '19651219', '19651219', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(136, '160433', '19980525', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(137, '19640310', '19640310', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(138, '19660621', '19660621', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(139, '1604036', '19981022', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(140, '19660622', '19660622', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(141, '19650113', '19650113', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(142, '160408', '19980902', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(143, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(144, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(145, '1604054', '20190528', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(146, '19601008', '19601008', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(147, '19710806', '19710806', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(148, '160416', '19981017', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(149, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(150, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(151, '1604044', '19980909', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(152, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(153, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(154, '1604003', '19980807', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(155, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(156, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(157, '1604047', '19960401', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(158, '59000501', '59000501', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(159, '19621112', '19621112', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(160, '1604026', '19980822', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(161, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(162, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(163, '160443', '19980601', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(164, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(165, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(166, '1604042', '19980220', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(167, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(168, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(169, '1604056', '19980107', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(170, '19600322', '19600322', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(171, '19620404', '19620404', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(172, '160451', '19970111', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(173, '19601024', '19601024', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(174, '19630711', '19630711', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(175, '160440', '19980329', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(176, '19670822', '19670822', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(177, '19690918', '19690918', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(178, '1604049', '19981005', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(179, '19670524', '19670524', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(180, '19670817', '19670817', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(181, '1704045', '20190601', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(182, '20190610', '20190610', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(183, '20191002', '20191002', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(184, '1704013', '20191101', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(185, '19640101', '19640101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(186, '19750801', '19750801', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(187, '1704042', '19991010', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(188, '19690606', '19690606', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(189, '19700712', '19700712', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(190, '1704056', '20000229', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(191, '19680111', '19680111', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(192, '19681028', '19681028', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(193, '1704039', '19980412', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(194, '19700714', '19700714', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(195, '19721026', '19721026', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(196, '1704057', '19991025', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(197, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(198, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(199, '1704016', '19980217', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(200, '20190804', '20190804', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(201, '20190506', '20190506', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(202, '1704013', '20190111', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(203, '20190101', '20190101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(204, '20190107', '20190107', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(205, '1704064', '20000325', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(206, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(207, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(208, '1704001', '19990918', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(209, '19670513', '19670513', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(210, '19700422', '19700422', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(211, '1704033', '19981107', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(212, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(213, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(214, '1704049', '20000216', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(215, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(216, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(217, '1704002', '20000201', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(218, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(219, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(220, '1704047', '19991228', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(221, '19660412', '19660412', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(222, '19720616', '19720616', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(223, '1704017', '19960126', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(224, '19670519', '19670519', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(225, '19760816', '19760816', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(226, '1704020', '19990505', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(227, '19710114', '19710114', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(228, '20191026', '20191026', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(229, '1704046', '19980302', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(230, '19540330', '19540330', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(231, '19550909', '19550909', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(232, '1704021', '19951205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(233, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(234, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(235, '1704021', '19951205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(236, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(237, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(238, '1704043', '19991001', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(239, '19721001', '19721001', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(240, '19730915', '19730915', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(241, '1704038', '20000325', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(242, '19620411', '19620411', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(243, '19701107', '19701107', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(244, '1704006', '19700101', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(245, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(246, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(247, '1704061', '19990916', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(248, '19710908', '19710908', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(249, '19780724', '19780724', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(250, '1704044', '20191116', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(251, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(252, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(253, '1704063', '19990820', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(254, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(255, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(256, '1704050', '20190713', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(257, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(258, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(259, '1704018', '19970617', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(260, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(261, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(262, '1704022', '19990606', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(263, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(264, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(265, '1704002', '20000201', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(266, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(267, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(268, '1704041', '20190623', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(269, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(270, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(271, '1704030', '20191209', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(272, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(273, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(274, '1704028', '19980905', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(275, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(276, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(277, '1704065', '20190711', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(278, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(279, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(280, '1704060', '19970518', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(281, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(282, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(283, '1704035', '20000305', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(284, '19720610', '19720610', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(285, '19740301', '19740301', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(286, '1704031', '19980905', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(287, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(288, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(289, '1704031', '19980905', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(290, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(291, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(292, '1704032', '19981222', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(293, '38170501', '38170501', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(294, '19741225', '19741225', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(295, '1704034', '19990524', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(296, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(297, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(298, '1704026', '19980808', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(299, '19680811', '19680811', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(300, '19740502', '19740502', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(301, '1704027', '19991006', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(302, '19720326', '19720326', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(303, '19750406', '19750406', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(304, '1704040', '19991110', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(305, '19710810', '19710810', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(306, '19761015', '19761015', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(307, '1704008', '19981003', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(308, '19660808', '19660808', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(309, '19740923', '19740923', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(310, '1704009', '19971231', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(311, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(312, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(313, '1704005', '19980709', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(314, '19671015', '19671015', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(315, '19731218', '19731218', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(316, '1704019', '19990628', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(317, '19720602', '19720602', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(318, '17731124', '17731124', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(319, '1704054', '20000313', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(320, '19670707', '19670707', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(321, '19690503', '19690503', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(322, '1704036', '20190503', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(323, '20191006', '20191006', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(324, '20190524', '20190524', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(325, '1704026', '19980808', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(326, '19680811', '19680811', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(327, '19740502', '19740502', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(328, '1704037', '19990724', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(329, '19851101', '19851101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(330, '19681212', '19681212', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(331, '1704029', '19950912', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(332, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(333, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(334, '1704058', '19990415', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(335, '19680614', '19680614', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(336, '19740827', '19740827', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(337, '1704059', '19960928', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(338, '19580605', '19580605', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(339, '19680805', '19680805', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(340, '1704012', '19991213', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(341, '19641111', '19641111', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(342, '19680405', '19680405', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(343, '1704007', '19990517', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(344, '44950501', '44950501', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(345, '19631004', '19631004', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(346, '1704053', '19990409', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(347, '19691128', '19691128', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(348, '19750310', '19750310', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(349, '1704015', '19991118', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(350, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(351, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(352, '1704048', '19960926', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(353, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(354, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(355, '1704022', '19990606', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(356, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(357, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(358, '1704052', '19991103', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(359, '19660105', '19660105', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(360, '19700628', '19700628', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(361, '1704004', '19991205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(362, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(363, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(364, '1704011', '20001202', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(365, '19720914', '19720914', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(366, '19710807', '19710807', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(367, '1704023', '19990306', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(368, '19720504', '19720504', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(369, '19720405', '19720405', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(370, '1704062', '20190408', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(371, '20190919', '20190919', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(372, '20190530', '20190530', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(373, '1704046', '19980302', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(374, '19540330', '19540330', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(375, '19550909', '19550909', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(376, '1704003', '19991024', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(377, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(378, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(379, '1704024', '19990213', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(380, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(381, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(382, '1704055', '19990222', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(383, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(384, '19780331', '19780331', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(385, '1704025', '19970920', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(386, '19610316', '19610316', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(387, '19611026', '19611026', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(388, '1704014', '19960601', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(389, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(390, '19700101', '19700101', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(391, '1804040', '19981111', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(392, '19990325', '19990325', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(393, '19991113', '19991113', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(394, '1804038', '20000619', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(395, '19700415', '19700415', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(396, '19740111', '19740111', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(397, '1804093', '20010121', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(398, '19700524', '19700524', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(399, '19740319', '19740319', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(400, '1804057', '20000312', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(401, '19700722', '19700722', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(402, '19710210', '19710210', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(403, '1804054', '20000630', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(404, '19681203', '19681203', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(405, '19700515', '19700515', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(406, '1804062', '20000727', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(407, '19810802', '19810802', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(408, '19840107', '19840107', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(409, '1804087', '19991022', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(410, '19690610', '19690610', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(411, '19720215', '19720215', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(412, '1804021', '19991009', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(413, '19700826', '19700826', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(414, '19710211', '19710211', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(415, '1804106', '19990704', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(416, '19600919', '19600919', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(417, '19651016', '19651016', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(418, '1804048', '20000219', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(419, '19740718', '19740718', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(420, '19741201', '19741201', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(421, '1804010', '19971008', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(422, '19730714', '19730714', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(423, '19740606', '19740606', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(424, '1804103', '19991115', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(425, '19620410', '19620410', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(426, '19750723', '19750723', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(427, '1804018', '20010321', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(428, '19700612', '19700612', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(429, '19730430', '19730430', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(430, '1804080', '20001109', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(431, '19731020', '19731020', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(432, '19721202', '19721202', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(433, '1804011', '19990403', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(434, '19740808', '19740808', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(435, '19741016', '19741016', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(436, '1804039', '20000117', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(437, '19730604', '19730604', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(438, '19720307', '19720307', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(439, '1804032', '20000205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(440, '19640310', '19640310', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(441, '19730505', '19730505', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(442, '1804013', '20000414', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(443, '19650906', '19650906', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(444, '19650427', '19650427', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(445, '1804008', '19980828', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(446, '19541030', '19541030', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(447, '19671124', '19671124', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(448, '1804117', '20190122', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(449, '20190824', '20190824', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(450, '20190420', '20190420', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(451, '1804061', '19990405', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(452, '19680424', '19680424', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(453, '19680210', '19680210', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(454, '1804086', '20000311', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(455, '19690525', '19690525', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(456, '19770415', '19770415', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(457, '1804068', '19950918', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(458, '19721106', '19721106', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(459, '19730104', '19730104', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(460, '1804047', '20001208', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(461, '19670421', '19670421', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(462, '19720522', '19720522', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(463, '1804091', '20010225', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(464, '19740808', '19740808', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(465, '19800430', '19800430', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(466, '1804113', '20001215', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(467, '19630314', '19630314', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(468, '19641005', '19641005', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(469, '1804025', '20001207', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(470, '19730130', '19730130', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(471, '19730814', '19730814', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(472, '1804005', '20010122', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(473, '19760912', '19760912', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(474, '19760730', '19760730', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(475, '1804063', '20000520', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(476, '19651212', '19651212', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(477, '19640705', '19640705', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(478, '1804045', '20000427', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(479, '19731121', '19731121', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(480, '19760312', '19760312', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(481, '1804117', '20010122', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(482, '19690824', '19690824', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(483, '19800420', '19800420', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(484, '1804028', '20000102', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(485, '19750205', '19750205', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(486, '19771231', '19771231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(487, '1804015', '20010818', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(488, '19690722', '19690722', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(489, '19710130', '19710130', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(490, '1804023', '20000317', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(491, '19580311', '19580311', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(492, '19650330', '19650330', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(493, '1804076', '20001214', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(494, '19760909', '19760909', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(495, '19770814', '19770814', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(496, '1804042', '20000324', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(497, '19650120', '19650120', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(498, '19681225', '19681225', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(499, '1804066', '20000630', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(500, '19650616', '19650616', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(501, '19700401', '19700401', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(502, '1804065', '20000205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(503, '20000205', '20000205', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(504, '19710728', '19710728', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(505, '1804002', '20000703', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(506, '19640704', '19640704', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(507, '19670904', '19670904', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(508, '1804026', '20000214', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(509, '19710628', '19710628', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(510, '19720323', '19720323', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(511, '1804075', '20001001', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(512, '19710404', '19710404', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(513, '19770217', '19770217', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(514, '1804109', '19971205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(515, '20191211', '20191211', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(516, '20190227', '20190227', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(517, '1804016', '19991204', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(518, '19640806', '19640806', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(519, '19680328', '19680328', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(520, '1804095', '19980811', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(521, '19790902', '19790902', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(522, '19781231', '19781231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(523, '1804001', '19981230', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(524, '19630817', '19630817', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(525, '19630614', '19630614', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(526, '1804089', '19990328', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(527, '19701202', '19701202', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(528, '19761120', '19761120', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(529, '1804118', '20000817', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(530, '19600131', '19600131', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(531, '19621125', '19621125', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(532, '1804077', '20001223', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(533, '19700724', '19700724', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(534, '19710817', '19710817', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(535, '1804098', '20000614', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(536, '19540812', '19540812', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(537, '19590927', '19590927', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(538, '1804058', '19991031', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(539, '19580609', '19580609', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(540, '19610208', '19610208', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(541, '1804071', '19981101', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(542, '19720104', '19720104', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(543, '19800716', '19800716', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(544, '1804073', '20010513', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(545, '19680815', '19680815', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(546, '19680615', '19680615', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(547, '1804097', '20000313', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(548, '19600508', '19600508', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(549, '19640405', '19640405', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(550, '1804024', '19991013', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(551, '19641112', '19641112', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(552, '19681021', '19681021', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(553, '1804051', '19990305', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(554, '19780204', '19780204', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(555, '19790210', '19790210', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(556, '1804081', '20190330', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(557, '19730612', '19730612', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(558, '19751001', '19751001', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(559, '1804060', '20000122', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(560, '19600826', '19600826', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(561, '19641231', '19641231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(562, '1804053', '19960803', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(563, '19750410', '19750410', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(564, '19760610', '19760610', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(565, '1804069', '20000910', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(566, '19670202', '19670202', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(567, '19671104', '19671104', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(568, '1804037', '20000201', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(569, '19720303', '19720303', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(570, '19710904', '19710904', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(571, '1804082', '20000316', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(572, '19661231', '19661231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(573, '19681222', '19681222', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(574, '1804012', '20000921', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(575, '19760816', '19760816', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(576, '19760415', '19760415', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(577, '1804074', '20000531', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(578, '19721216', '19721216', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(579, '19720801', '19720801', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(580, '1804099', '20000702', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(581, '19600201', '19600201', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(582, '19651207', '19651207', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(583, '1804036', '19990215', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(584, '19740131', '19740131', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(585, '19760113', '19760113', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(586, '1804017', '20010102', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(587, '19710329', '19710329', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(588, '19731221', '19731221', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(589, '1804007', '20000610', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(590, '19650407', '19650407', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(591, '19640316', '19640316', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(592, '1804050', '20000913', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(593, '19690204', '19690204', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(594, '19711115', '19711115', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(595, '1804079', '19980829', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(596, '19661204', '19661204', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(597, '19660714', '19660714', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(598, '1804044', '20000203', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(599, '19700723', '19700723', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(600, '19740701', '19740701', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(601, '1804100', '19990706', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(602, '19540408', '19540408', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(603, '19741231', '19741231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(604, '1804119', '20000609', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(605, '19660719', '19660719', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(606, '19660802', '19660802', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(607, '1804111', '20000717', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(608, '19720318', '19720318', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(609, '19780806', '19780806', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(610, '1804034', '19990102', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(611, '19730901', '19730901', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(612, '19710416', '19710416', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(613, '1804112', '19980502', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(614, '19561225', '19561225', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(615, '19680928', '19680928', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(616, '1804009', '20000428', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(617, '19710906', '19710906', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(618, '19710528', '19710528', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(619, '1804116', '20000506', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(620, '19570705', '19570705', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(621, '19600205', '19600205', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(622, '1804041', '20001215', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(623, '19630412', '19630412', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(624, '19640526', '19640526', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(625, '1804006', '20000711', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(626, '19620809', '19620809', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(627, '19621212', '19621212', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(628, '1804004', '19981105', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(629, '19690317', '19690317', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(630, '19721122', '19721122', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(631, '1804052', '19990606', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(632, '19581130', '19581130', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(633, '19681226', '19681226', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(634, '1804020', '20000113', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(635, '19690607', '19690607', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(636, '19710816', '19710816', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(637, '1804107', '19971120', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(638, '19660608', '19660608', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(639, '19721108', '19721108', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(640, '1804029', '20000425', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(641, '19780720', '19780720', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(642, '19790723', '19790723', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(643, '1804104', '20001120', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(644, '19670808', '19670808', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(645, '19730219', '19730219', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(646, '1804092', '20000628', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(647, '19601215', '19601215', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(648, '19740610', '19740610', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(649, '1804056', '20000205', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(650, '19750216', '19750216', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(651, '19761231', '19761231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(652, '1804030', '19991102', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(653, '19670303', '19670303', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(654, '19680626', '19680626', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(655, '1804019', '20000213', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(656, '19690205', '19690205', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(657, '19750829', '19750829', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(658, '1804055', '20000721', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(659, '19740722', '19740722', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(660, '19730506', '19730506', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(661, '1804084', '20000801', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(662, '19680517', '19680517', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(663, '19721231', '19721231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(664, '1804046', '20000630', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(665, '19790524', '19790524', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(666, '19831223', '19831223', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(667, '1804105', '20190926', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(668, '19710606', '19710606', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(669, '19650926', '19650926', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(670, '1804003', '20000618', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(671, '19770724', '19770724', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(672, '19790124', '19790124', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(673, '1804108', '20001216', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(674, '19710319', '19710319', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(675, '19780812', '19780812', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(676, '1804049', '20000118', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(677, '19660324', '19660324', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(678, '19730424', '19730424', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(679, '1804067', '19990421', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(680, '19700112', '19700112', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(681, '19680512', '19680512', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(682, '1804035', '20001028', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(683, '19690329', '19690329', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(684, '19620703', '19620703', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(685, '1804064', '20000503', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(686, '19711010', '19711010', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(687, '19740807', '19740807', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(688, '1804101', '20190421', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(689, '19610811', '19610811', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(690, '19630423', '19630423', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(691, '1804083', '19990308', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(692, '19681212', '19681212', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(693, '19690118', '19690118', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(694, '1804033', '19991110', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(695, '19761229', '19761229', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(696, '19810713', '19810713', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(697, '1804014', '19970613', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(698, '19670825', '19670825', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(699, '20190914', '20190914', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(700, '1804031', '20190717', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(701, '19771001', '19771001', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(702, '20190106', '20190106', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(703, '1804059', '20001227', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(704, '19780315', '19780315', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(705, '19711231', '19711231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(706, '1804072', '20000221', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(707, '19670616', '19670616', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(708, '19711116', '19711116', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(709, '1804043', '20000401', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(710, '19680420', '19680420', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(711, '19760407', '19760407', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(712, '1804090', '20000807', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(713, '19760921', '19760921', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(714, '19780928', '19780928', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(715, '1804070', '20000112', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(716, '19570707', '19570707', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(717, '20190416', '20190416', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(718, '1804096', '20010125', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(719, '19660302', '19660302', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(720, '19700320', '19700320', 2, '', '2019-05-01', 11, NULL, NULL, 0);
INSERT INTO `user` (`id`, `username`, `password`, `priveledge`, `token`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(721, '1804022', '20000601', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(722, '19670917', '19670917', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(723, '19810828', '19810828', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(724, '1804027', '20001222', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(725, '19810704', '19810704', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(726, '19820608', '19820608', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(727, '1804078', '20000102', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(728, '19551031', '19551031', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(729, '19651231', '19651231', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(730, '1804114', '20000411', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(731, '19710617', '19710617', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(732, '19780602', '19780602', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(733, '1804102', '20000519', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(734, '19750606', '19750606', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(735, '19751024', '19751024', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(736, '1804110', '20000511', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(737, '19730223', '19730223', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(738, '19790318', '19790318', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(739, '1804088', '19970711', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(740, '19610506', '19610506', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(741, '19691111', '19691111', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(742, '1804120', '20000602', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(743, '19640131', '19640131', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(744, '19770922', '19770922', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(745, '1804115', '20191223', 3, '', '2019-05-01', 11, NULL, NULL, 0),
(746, '19590310', '19590310', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(747, '19640419', '19640419', 2, '', '2019-05-01', 11, NULL, NULL, 0),
(748, 'tedi', 'tedi', 6, '', '2019-05-02', 11, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_kompi`
--
ALTER TABLE `admin_kompi`
  ADD PRIMARY KEY (`id`,`user`,`kompi`),
  ADD KEY `fk_admin_kompi_user1_idx` (`user`),
  ADD KEY `fk_admin_kompi_kompi1_idx` (`kompi`);

--
-- Indexes for table `asal_sekolah`
--
ALTER TABLE `asal_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asrama`
--
ALTER TABLE `asrama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bidang_penghargaan`
--
ALTER TABLE `bidang_penghargaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_admin`
--
ALTER TABLE `chat_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_manajemen`
--
ALTER TABLE `chat_manajemen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hubungan_wali`
--
ALTER TABLE `hubungan_wali`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hukuman`
--
ALTER TABLE `hukuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_luar`
--
ALTER TABLE `jadwal_luar`
  ADD PRIMARY KEY (`id`,`taruna`,`jenis_jadwal_luar`,`semester`),
  ADD KEY `fk_jadwal_luar_taruna1_idx` (`taruna`),
  ADD KEY `fk_jadwal_luar_jenis_jadwal_luar1_idx` (`jenis_jadwal_luar`),
  ADD KEY `fk_jadwal_luar_semester_idx` (`semester`);

--
-- Indexes for table `jadwal_luar_has_taruna`
--
ALTER TABLE `jadwal_luar_has_taruna`
  ADD PRIMARY KEY (`id`,`jadwal_luar_lain`,`taruna`,`prodi`,`semester`),
  ADD KEY `fk_jadwal_luar_has_taruna_jadwal_luar_lain1_idx` (`jadwal_luar_lain`),
  ADD KEY `fk_jadwal_luar_has_taruna_taruna1_idx` (`taruna`),
  ADD KEY `fk_jadwal_luar_has_taruna_semester1_idx` (`semester`),
  ADD KEY `fk_jadwal_luar_has_taruna_prodi1_idx` (`prodi`);

--
-- Indexes for table `jadwal_luar_lain`
--
ALTER TABLE `jadwal_luar_lain`
  ADD PRIMARY KEY (`id`,`jenis_jadwal_luar`,`prodi`,`semester`),
  ADD KEY `fk_jadwal_luar_lain_jenis_jadwal_luar1_idx` (`jenis_jadwal_luar`),
  ADD KEY `fk_jadwal_luar_lain_prodi1_idx` (`prodi`),
  ADD KEY `fk_jadwal_luar_lain_semester1_idx` (`semester`);

--
-- Indexes for table `jadwal_rutin`
--
ALTER TABLE `jadwal_rutin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_jadwal_luar`
--
ALTER TABLE `jenis_jadwal_luar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_pelanggaran`
--
ALTER TABLE `kategori_pelanggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kesalahan`
--
ALTER TABLE `kesalahan`
  ADD PRIMARY KEY (`id`,`pelanggaran`,`taruna`,`semester`),
  ADD KEY `fk_kesalahan_taruna1_idx` (`taruna`),
  ADD KEY `fk_kesalahan_semester1_idx` (`semester`),
  ADD KEY `fk_kesalahan_pelanggaran1_idx` (`pelanggaran`);

--
-- Indexes for table `kesalahan_status`
--
ALTER TABLE `kesalahan_status`
  ADD PRIMARY KEY (`id`,`user`,`kesalahan`),
  ADD KEY `fk_psikologi_status_user1_idx` (`user`),
  ADD KEY `fk_kesalahan_status_kesalahan1_idx` (`kesalahan`);

--
-- Indexes for table `kompi`
--
ALTER TABLE `kompi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`,`propinsi`),
  ADD KEY `fk_kota_propinsi1_idx` (`propinsi`);

--
-- Indexes for table `log_histori`
--
ALTER TABLE `log_histori`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_log_histori_user1_idx` (`user`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_web`
--
ALTER TABLE `module_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `naik_semester`
--
ALTER TABLE `naik_semester`
  ADD PRIMARY KEY (`id`,`prodi`),
  ADD KEY `fk_naik_semester_prodi1_idx` (`prodi`);

--
-- Indexes for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD PRIMARY KEY (`id`,`kategori_pelanggaran`),
  ADD KEY `fk_pelanggaran_kategori_pelanggaran1_idx` (`kategori_pelanggaran`);

--
-- Indexes for table `penghargaan`
--
ALTER TABLE `penghargaan`
  ADD PRIMARY KEY (`id`,`taruna`,`prestasi`,`semester`),
  ADD KEY `fk_penghargaan_taruna1_idx` (`taruna`),
  ADD KEY `fk_penghargaan_semester1_idx` (`semester`),
  ADD KEY `fk_penghargaan_prestasi1_idx` (`prestasi`);

--
-- Indexes for table `penghasilan`
--
ALTER TABLE `penghasilan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `point_kesalahan`
--
ALTER TABLE `point_kesalahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `point_penghargaan`
--
ALTER TABLE `point_penghargaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`,`bidang_penghargaan`),
  ADD KEY `fk_prestasi_bidang_penghargaan1_idx` (`bidang_penghargaan`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `propinsi`
--
ALTER TABLE `propinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `psikologi`
--
ALTER TABLE `psikologi`
  ADD PRIMARY KEY (`id`,`taruna`),
  ADD KEY `fk_psikologi_taruna1_idx` (`taruna`);

--
-- Indexes for table `role_admin`
--
ALTER TABLE `role_admin`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_role_admin_user1_idx` (`user`);

--
-- Indexes for table `role_priveledge`
--
ALTER TABLE `role_priveledge`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_role_priveledge_priveledge1_idx` (`priveledge`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seperasuhan`
--
ALTER TABLE `seperasuhan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taruna`
--
ALTER TABLE `taruna`
  ADD PRIMARY KEY (`id`,`user`,`jenis_kelamin`),
  ADD KEY `fk_taruna_jenis_kelamin1_idx` (`jenis_kelamin`),
  ADD KEY `fk_taruna_user1_idx` (`user`);

--
-- Indexes for table `taruna_has_agama`
--
ALTER TABLE `taruna_has_agama`
  ADD PRIMARY KEY (`id`,`taruna`),
  ADD KEY `fk_taruna_has_agama_taruna1_idx` (`taruna`);

--
-- Indexes for table `taruna_has_akademik`
--
ALTER TABLE `taruna_has_akademik`
  ADD PRIMARY KEY (`id`,`taruna`,`prodi`,`semester`),
  ADD KEY `fk_taruna_has_akademik_taruna1_idx` (`taruna`),
  ADD KEY `fk_taruna_has_akademik_semester1_idx` (`semester`),
  ADD KEY `fk_taruna_has_akademik_prodi1_idx` (`prodi`);

--
-- Indexes for table `taruna_has_asrama`
--
ALTER TABLE `taruna_has_asrama`
  ADD PRIMARY KEY (`id`,`taruna`,`asrama`,`kamar`),
  ADD KEY `fk_taruna_has_asrama_asrama1_idx` (`asrama`),
  ADD KEY `fk_taruna_has_asrama_kamar1_idx` (`kamar`),
  ADD KEY `fk_taruna_has_asrama_taruna1_idx` (`taruna`);

--
-- Indexes for table `taruna_has_kesehatan`
--
ALTER TABLE `taruna_has_kesehatan`
  ADD PRIMARY KEY (`id`,`taruna`),
  ADD KEY `fk_taruna_has_kesehatan_taruna1_idx` (`taruna`);

--
-- Indexes for table `taruna_has_wali_murid`
--
ALTER TABLE `taruna_has_wali_murid`
  ADD PRIMARY KEY (`id`,`taruna`,`user`,`hubungan_wali`),
  ADD KEY `fk_taruna_has_wali_murid_taruna1_idx` (`taruna`),
  ADD KEY `fk_taruna_has_wali_murid_hubungan_wali1_idx` (`hubungan_wali`),
  ADD KEY `fk_taruna_has_wali_murid_user1_idx` (`user`);

--
-- Indexes for table `taruna_naik_semester`
--
ALTER TABLE `taruna_naik_semester`
  ADD PRIMARY KEY (`id`,`naik_semester`,`taruna`),
  ADD KEY `fk_taruna_naik_semester_naik_semester1_idx` (`naik_semester`),
  ADD KEY `fk_taruna_naik_semester_taruna1_idx` (`taruna`);

--
-- Indexes for table `taruna_sakit`
--
ALTER TABLE `taruna_sakit`
  ADD PRIMARY KEY (`id`,`taruna`),
  ADD KEY `fk_taruna_sakit_taruna1_idx` (`taruna`);

--
-- Indexes for table `tombol_action`
--
ALTER TABLE `tombol_action`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_tombol_action_user1_idx` (`user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge_idx` (`priveledge`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_kompi`
--
ALTER TABLE `admin_kompi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asal_sekolah`
--
ALTER TABLE `asal_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `asrama`
--
ALTER TABLE `asrama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bidang_penghargaan`
--
ALTER TABLE `bidang_penghargaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `chat_admin`
--
ALTER TABLE `chat_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_manajemen`
--
ALTER TABLE `chat_manajemen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hubungan_wali`
--
ALTER TABLE `hubungan_wali`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hukuman`
--
ALTER TABLE `hukuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jadwal_luar`
--
ALTER TABLE `jadwal_luar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwal_luar_has_taruna`
--
ALTER TABLE `jadwal_luar_has_taruna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwal_luar_lain`
--
ALTER TABLE `jadwal_luar_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jadwal_rutin`
--
ALTER TABLE `jadwal_rutin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `jenis_jadwal_luar`
--
ALTER TABLE `jenis_jadwal_luar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jenis_kelamin`
--
ALTER TABLE `jenis_kelamin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori_pelanggaran`
--
ALTER TABLE `kategori_pelanggaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kesalahan`
--
ALTER TABLE `kesalahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kesalahan_status`
--
ALTER TABLE `kesalahan_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kompi`
--
ALTER TABLE `kompi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `log_histori`
--
ALTER TABLE `log_histori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `naik_semester`
--
ALTER TABLE `naik_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `penghargaan`
--
ALTER TABLE `penghargaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `penghasilan`
--
ALTER TABLE `penghasilan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `point_kesalahan`
--
ALTER TABLE `point_kesalahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `point_penghargaan`
--
ALTER TABLE `point_penghargaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `propinsi`
--
ALTER TABLE `propinsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `psikologi`
--
ALTER TABLE `psikologi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_admin`
--
ALTER TABLE `role_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role_priveledge`
--
ALTER TABLE `role_priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `seperasuhan`
--
ALTER TABLE `seperasuhan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `taruna`
--
ALTER TABLE `taruna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `taruna_has_agama`
--
ALTER TABLE `taruna_has_agama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=618;

--
-- AUTO_INCREMENT for table `taruna_has_akademik`
--
ALTER TABLE `taruna_has_akademik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=618;

--
-- AUTO_INCREMENT for table `taruna_has_asrama`
--
ALTER TABLE `taruna_has_asrama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `taruna_has_kesehatan`
--
ALTER TABLE `taruna_has_kesehatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `taruna_has_wali_murid`
--
ALTER TABLE `taruna_has_wali_murid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1225;

--
-- AUTO_INCREMENT for table `taruna_naik_semester`
--
ALTER TABLE `taruna_naik_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `taruna_sakit`
--
ALTER TABLE `taruna_sakit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tombol_action`
--
ALTER TABLE `tombol_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=749;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
