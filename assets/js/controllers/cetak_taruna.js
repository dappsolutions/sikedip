var CetakTaruna = {
 module: function () {
  return 'cetak_taruna';
 },

 detail: function (id) {
  window.location.href = url.base_url(CetakTaruna.module()) + "detail/" + id;
 },

 setDate: function () {
  $('input#tanggal').daterangepicker({

  }, function (start, end, label) {
   var waktu = start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD');
  });
 },

 tampilkan: function () {
  var semester = $('#semester').val();
  var prodi = $('#jurusan').val();
  var angkatan = $('#angkatan').val();
  var propinsi = $('#propinsi').val();
  var kota = $('#kota').val();
  var asal = $('#asal').val();
  var kompi = $('#kompi').val();
  var asrama = $('#asrama').val();
  var seperasuhan = $('#seperasuhan').val();

  $.ajax({
   type: 'POST',
   data: {
    semester: semester,
    prodi: prodi,
    angkatan: angkatan,
    propinsi: propinsi,
    kota: kota,
    asal: asal,
    kompi: kompi,
    asrama: asrama,
    seperasuhan: seperasuhan
   },
   dataType: 'html',
   async: false,
   url: url.base_url(CetakTaruna.module()) + "tampilkan",
   error: function () {
    toastr.error("Program Error");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    $('div#table_izin').html(resp);
    message.closeLoading();
   }
  });
 },

 cetakData: function () {
  var semester = $('#semester').val();
  var prodi = $('#jurusan').val();
  var angkatan = $('#angkatan').val();
  var propinsi = $('#propinsi').val();
  var kota = $('#kota').val();
  var asal = $('#asal').val();
  var kompi = $('#kompi').val();
  var asrama = $('#asrama').val();
  var seperasuhan = $('#seperasuhan').val();


  if (semester == '') {
   semester = '0';
  }
  if (angkatan == '') {
   angkatan = '0';
  }
  
  if (propinsi == '') {
   propinsi = '0';
  }
  
  if (kota == '') {
   kota = '0';
  }
  
  if (asal == '') {
   asal = '0';
  }
  
  if (kompi == '') {
   kompi = '0';
  }
  
  if (asrama == '') {
   asrama = '0';
  }
  
  if (seperasuhan == '') {
   seperasuhan = '0';
  }
  
  window.open(url.base_url(CetakTaruna.module()) + "cetak/" + prodi + '/' + semester + '/' + angkatan+'/'+propinsi+'/'+kota+"/"+asal+"/"+kompi+"/"+asrama+"/"+seperasuhan);
 },

 getKotaData: function (elm) {
  var idPropinsi = $(elm).val();
  var kota = $('select#kota').find('option');

  $.each(kota, function () {
   $(this).removeClass('display-none');
  });

  var i = 0;
  if (idPropinsi != '') {
   $.each(kota, function () {
    var propinsi = $(this).attr('propinsi');
    console.log('pro', propinsi);
    if (idPropinsi != propinsi) {
     $(this).addClass('display-none');
    } else {
     if (i == 0) {
      $(this).prop("selected", true);
      i += 1;
     }
    }
   });
  } else {
   $.each(kota, function () {
    if (i == 0) {
     $(this).prop('selected', true);
     i += 1;
    }
   });
  }
 },
};

$(function () {
 CetakTaruna.setDate();
});