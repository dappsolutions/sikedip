var Role = {
 module: function () {
  return 'role';
 },

 add: function () {
  window.location.href = url.base_url(Role.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Role.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Role.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Role.module()) + "index";
   }
  }
 },

 getPostDataHubWali: function () {
  var tr = $('table#table_hubungan').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   data.push({
    'no_ktp': $(this).find('#no_ktp_wali').val(),
    'nama': $(this).find('#nama_wali').val(),
    'hubungan_wali': $(this).find('#hubungan_wali').val(),
   });
  });
  return data;
 },

 getPostAdmin: function () {
  var data = [];
  $.each($('div.admin'), function () {
   var checked = $(this).find('input').is(':checked');
   data.push({
    'checked': checked ? 1 : 0,
    'id_role': $.trim($(this).find('label').attr('id_role')),
    'module': $.trim($(this).find('label').attr('for'))
   })
  });
  
  return data;
 },
 
 getPostWali: function () {
  var data = [];
  $.each($('div.wali'), function () {
   var checked = $(this).find('input').is(':checked');
   data.push({
    'checked': checked ? 1 : 0,
    'id_role': $.trim($(this).find('label').attr('id_role')),
    'module': $.trim($(this).find('label').attr('for'))
   })
  });
  
  return data;
 },
 
 getPostTaruna: function () {
  var data = [];
  $.each($('div.taruna'), function () {
   var checked = $(this).find('input').is(':checked');
   data.push({
    'checked': checked ? 1 : 0,
    'id_role': $.trim($(this).find('label').attr('id_role')),
    'module': $.trim($(this).find('label').attr('for'))
   })
  });
  
  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'admin': Role.getPostAdmin(),
   'wali': Role.getPostWali(),
   'taruna': Role.getPostTaruna(),
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Role.getPostData();  
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Role.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Role.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Role.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Role.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Role.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Role.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Role.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Role.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Role.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Role.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Role.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_agama').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_kesehatan').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_masuk').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },
};

$(function () {
 Role.setDate();
});