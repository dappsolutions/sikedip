var NaikSemester = {
 module: function () {
  return 'naik_semester';
 },

 add: function () {
  window.location.href = url.base_url(NaikSemester.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(NaikSemester.module()) + "index";
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(NaikSemester.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(NaikSemester.module()) + "index";
   }
  }
 },

 getPostTaruna: function () {
  var data = [];
  var tr = $('table#table_taruna').find('tbody').find('tr');
  $.each(tr, function () {
   if ($(this).hasClass('baru')) {
    var check = $(this).find('input.check').is(':checked');
    if ($(this).find('input#taruna').attr('id') != '' && check) {
     data.push({
      'taruna': $(this).attr('id'),
      'angkatan': $(this).attr('angkatan'),
      'prodi': $('select#prodi').val(),
      'semester': $('select#semester_naik').val(),
     });
    }
   }
  });

  return data;
 },

 getPostTarunaEdit: function () {
  var data = [];
  var tr = $('table#table_taruna').find('tbody').find('tr');
  $.each(tr, function () {
   if (!$(this).hasClass('baru')) {
    var check = $(this).find('input.check').is(':checked');
    data.push({
     'id': $(this).attr('id'),
     'angkatan': $(this).attr('angkatan'),
     'is_edit': check ? 1 : 0,
     'taruna': $(this).attr('taruna'),
     'prodi': $('select#prodi').val(),
     'semester': $('select#semester_naik').val(),
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'tanggal': $('#tanggal').val(),
   'prodi': $('#prodi').val(),
   'semester': $('#semester').val(),
   'semester_naik': $('#semester_naik').val(),
   'taruna': NaikSemester.getPostTaruna(),
   'taruna_edit': NaikSemester.getPostTarunaEdit(),
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = NaikSemester.getPostData();
//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  //check validation
  var check_data = $('input.check_data');
  var checked = 0;
  $.each(check_data, function () {
   if ($(this).is(':checked')) {
    checked += 1;
   }else{
    if(!$(this).closest('tr').hasClass('baru')){
     checked +=1;
    }
   }
  });

  if (checked == 0) {
   toastr.error("Taruna Belum Ada yang Dipilih");
   return;
  }

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(NaikSemester.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(NaikSemester.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(NaikSemester.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(NaikSemester.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(NaikSemester.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(NaikSemester.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="NaikSemester.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  NaikSemester.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(NaikSemester.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='NaikSemester.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="NaikSemester.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="NaikSemester.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDateRangePicker: function () {
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 getListTaruna: function (elm) {
  var prodi = $(elm).val();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(NaikSemester.module()) + "getContentListTaruna/" + prodi,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving...");
   },

   success: function (resp) {
    $('div#content_taruna').html(resp);
    message.closeLoading();
   }
  });
 },

 setWaktuJadwal: function () {
  $('input#jam').clockpicker({
   placement: 'bottom',
   align: 'left',
   autoclose: true,
   'default': 'now'
  });
 },

 removeDetailData: function (elm) {
  $(elm).closest('tr').addClass('display-none');
 },

 getDataTaruna: function (elm) {
  var data = [];
  $.ajax({
   type: 'POST',
   data: {
    keyword: $(elm).val()
   },
   dataType: 'json',
   async: false,
   url: url.base_url(NaikSemester.module()) + "getDataTaruna",
   error: function () {
    toastr.error("Gagal");
    $(elm).closest('td').find('.mdi-spin').addClass('display-none');
   },

   beforeSend: function () {
    $(elm).closest('td').find('.mdi-spin').removeClass('display-none');
   },
   success: function (resp) {
    data = NaikSemester.getDataListTaruna(resp.data);
    $(elm).closest('td').find('.mdi-spin').addClass('display-none');
   }
  });

  return data;
 },

 getDataListTaruna: function (taruna) {
  var data = [];
  $.each(taruna, function () {
   data.push(this.no_taruna.toString() + " - " + this.nama.toString());
  });

  return data;
 },

 setAutoCompleteData: function (elm) {
  var data = NaikSemester.getDataTaruna(elm);

  $(elm).autocomplete({
   source: data,
   select: function (event, ui) {
    var taruna = ui.item.value;
    var elm_auto = $(event.target);
    var data_taruna = taruna.toString().split('-');
    var no_taruna = $.trim(data_taruna[0].toString());
    elm_auto.attr('no_taruna', no_taruna);
    elm_auto.closest('tr').find('input.ui-autocomplete-input').attr('value', taruna);
   }
  });
 },

 getAutoCompleteDataTaruna: function (elm, e) {
  NaikSemester.setAutoCompleteData($(elm));
 },

 showLoading: function (elm) {
  var td = $(elm).closest('td');
  td.find('.mdi-spin').removeClass('hide');
 },

 checkAll: function (elm) {
  var checkData = $('input.check_data');
  $.each(checkData, function () {
   if ($(elm).is(':checked')) {
    $(this).prop('checked', true);
   } else {
    $(this).prop('checked', false);
   }
  });
 },

 checkTaruna: function (elm) {
  var check = $('input.check_data');
  var jumlah = 0;

  $.each(check, function () {
   if ($(this).is(':checked')) {
    jumlah += 1;
   }
  });

  if (jumlah == check.length) {
   $('input.check_header').prop('checked', true);
  } else {
   $('input.check_header').prop('checked', false);
  }
 },

 getDataTarunaDetail: function (elm) {
  var prodi = $('select#prodi').val();
  var semester = $('select#semester').val();

  $.ajax({
   type: 'POST',
   data: {
    prodi: prodi,
    semester: semester
   },
   dataType: 'html',
   async: false,
   url: url.base_url(NaikSemester.module()) + "getDataTarunaDetail",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('table#table_taruna').find('tbody').html(resp);
   }
  });
 }
};

$(function () {
// NaikSemester.getAutoCompleteDataTaruna();
 NaikSemester.setDateRangePicker();
});