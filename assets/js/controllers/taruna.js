var Taruna = {
 module: function () {
  return 'taruna';
 },

 add: function () {
  window.location.href = url.base_url(Taruna.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Taruna.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Taruna.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Taruna.module()) + "index";
   }
  }
 },

 getPostDataHubWali: function () {
  var tr = $('table#table_hubungan').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if ($(this).hasClass('baru')) {
    if ($(this).find('#no_ktp_wali').val() != '') {
     data.push({
      'no_ktp': $(this).find('#no_ktp_wali').val(),
      'nama': $(this).find('#nama_wali').val(),
      'hubungan_wali': $(this).find('#hubungan_wali').val(),
      'no_telp': $(this).find('#no_telp').val(),
      'alamat_wali': $(this).find('#alamat_wali').val(),
      'penghasilan': $(this).find('#penghasilan').val(),
      'pekerjaan': $(this).find('#pekerjaan').val(),
      'tanggal_lahir': $(this).find('td:eq(2)').find('input').val(),
     });
    }
   }
  });
  return data;
 },

 getPostDataHubWaliEdit: function () {
  var tr = $('table#table_hubungan').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if (!$(this).hasClass('baru')) {
    data.push({
     'id': $(this).attr('id'),
     'no_ktp': $(this).find('#no_ktp_wali').val(),
     'nama': $(this).find('#nama_wali').val(),
     'hubungan_wali': $(this).find('#hubungan_wali').val(),
     'is_edit': $(this).hasClass('display-none') ? 0 : 1,
     'no_telp': $(this).find('#no_telp').val(),
     'alamat_wali': $(this).find('#alamat_wali').val(),
     'penghasilan': $(this).find('#penghasilan').val(),
     'pekerjaan': $(this).find('#pekerjaan').val(),
     'tanggal_lahir': $(this).find('td:eq(2)').find('input').val(),
    });
   }
  });
  return data;
 },

 getPostDataAsrama: function () {
  var tr = $('table#table_asrama').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if ($(this).hasClass('baru')) {
    if ($(this).find('#asrama').val() != '') {
     data.push({
      'tanggal': $(this).find('td:eq(0)').find('input').val(),
      'asrama': $(this).find('#asrama').val(),
      'kamar': $(this).find('#kamar').val(),
     });
    }
   }
  });
  return data;
 },

 getPostDataAsramaEdit: function () {
  var tr = $('table#table_asrama').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if (!$(this).hasClass('baru')) {
    data.push({
     'id': $(this).attr('id'),
     'tanggal': $(this).find('td:eq(0)').find('input').val(),
     'asrama': $(this).find('#asrama').val(),
     'kamar': $(this).find('#kamar').val(),
     'is_edit': $(this).hasClass('display-none') ? 0 : 1,
    });
   }
  });
  return data;
 },

 getPostDataAgama: function () {
  var tr = $('table#table_agama').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if ($(this).hasClass('baru')) {
    if ($(this).find('#agama').val() != '') {
     data.push({
      'tanggal_agama': $(this).find('td:eq(0)').find('input').val(),
      'agama': $(this).find('#agama').val(),
     });
    }
   }
  });
  return data;
 },

 getPostAgamaEdit: function () {
  var tr = $('table#table_agama').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   var id = $(this).attr('id');
   if (!$(this).hasClass('baru')) {
    data.push({
     'id': id,
     'tanggal_agama': $(this).find('td:eq(0)').find('input').val(),
     'agama': $(this).find('#agama').val(),
     'is_edit': $(this).hasClass('display-none') ? 0 : 1
    });
   }
  });
  return data;
 },

 getPostDataMedic: function () {
  var tr = $('table#table_medic').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if ($(this).hasClass('baru')) {
    if ($(this).find('td:eq(0)').find('input').val() != '') {
     data.push({
      'tanggal_kesehatan': $(this).find('td:eq(0)').find('input').val(),
      'berat_badan': $(this).find('#berat_badan').val(),
      'tinggi_badan': $(this).find('#tinggi_badan').val(),
     });
    }
   }
  });
  return data;
 },

 getPostDataMedicEdit: function () {
  var tr = $('table#table_medic').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if (!$(this).hasClass('baru')) {
    data.push({
     'id': $(this).attr('id'),
     'tanggal_kesehatan': $(this).find('td:eq(0)').find('input').val(),
     'berat_badan': $(this).find('#berat_badan').val(),
     'tinggi_badan': $(this).find('#tinggi_badan').val(),
     'is_edit': $(this).hasClass('display-none') ? 0 : 1
    });
   }
  });
  return data;
 },

 getPostDataAkademik: function () {
  var tr = $('table#table_akademik').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if ($(this).hasClass('baru')) {
    if ($(this).find('td:eq(2)').find('input').val() != '') {
     data.push({
      'tanggal_masuk': $(this).find('td:eq(2)').find('input').val(),
      'prodi': $(this).find('#prodi').val(),
      'semester': $(this).find('#semester').val(),
      'angkatan': $(this).find('#angkatan').val(),
     });
    }
   }
  });
  return data;
 },

 getPostDataAkademikEdit: function () {
  var tr = $('table#table_akademik').find('tbody').find('tr');
  var data = [];

  $.each(tr, function () {
   if (!$(this).hasClass('baru')) {
    data.push({
     'id': $(this).attr('id'),
     'tanggal_masuk': $(this).find('td:eq(2)').find('input').val(),
     'prodi': $(this).find('#prodi').val(),
     'semester': $(this).find('#semester').val(),
     'angkatan': $(this).find('#angkatan').val(),
     'is_edit': $(this).hasClass('display-none') ? 0 : 1
    });
   }
  });
  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'taruna': {
    'no_taruna': $('#no_taruna').val(),
    'nama': $('#nama').val(),
    'tempat_lahir': $('#tempat_lahir').val(),
    'jenis_kelamin': $('#jenis_kelamin').val(),
    'tanggal_lahir': $('#tanggal_lahir').val(),
    'golongan_darah': $('#golongan_darah').val(),
    'no_ktp': $('#no_ktp').val(),
    'no_sim': $('#no_sim').val(),
    'no_hp': $('#front_number').val() + $('#no_hp').val(),
    'email': $('#email').val(),
    'npwp': $('#npwp').val(),
    'alamat': $('#alamat').val(),
    'propinsi': $('#propinsi').val(),
    'kota': $('#kota').val(),
    'asal': $('#asal').val(),
    'kompi': $('#kompi').val(),
    'seperasuhan': $('#seperasuhan').val(),
   },

   'agama': Taruna.getPostDataAgama(),

   'agama_edit': Taruna.getPostAgamaEdit(),

   'medic': Taruna.getPostDataMedic(),

   'medic_edit': Taruna.getPostDataMedicEdit(),

   'akademik': Taruna.getPostDataAkademik(),

   'akademik_edit': Taruna.getPostDataAkademikEdit(),

   'hub_wali': Taruna.getPostDataHubWali(),

   'hub_wali_edit': Taruna.getPostDataHubWaliEdit(),

   'asrama': Taruna.getPostDataAsrama(),
   
   'asrama_edit': Taruna.getPostDataAsramaEdit(),
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Taruna.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Taruna.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Taruna.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Taruna.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Taruna.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Taruna.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Taruna.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(2)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_lahir_wali_', ''));
  var next_id = seq + 1;

  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(2)').find('input')
          .attr('id', 'tanggal_lahir_wali_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(8)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAgama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_agama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAsrama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_asrama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_asrama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailKesehatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_kesehatan_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAkademik: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(2)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_masuk_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(2)').find('input')
          .attr('id', 'tanggal_masuk_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(4)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Taruna.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png' || type_file == 'jpg') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Taruna.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Taruna.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Taruna.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Taruna.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal_lahir').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_agama_1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_kesehatan_1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_masuk_1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_lahir_wali_1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });

  $('input#tanggal_asrama_1').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 removeDetailData: function (elm) {
  $(elm).closest('tr').addClass('display-none');
 },

 getKotaData: function (elm) {
  var idPropinsi = $(elm).val();
  var kota = $('select#kota').find('option');

  $.each(kota, function () {
   $(this).removeClass('display-none');
  });

  var i = 0;
  if (idPropinsi != '') {
   $.each(kota, function () {
    var propinsi = $(this).attr('propinsi');
    console.log('pro', propinsi);
    if (idPropinsi != propinsi) {
     $(this).addClass('display-none');
    } else {
     if (i == 0) {
      $(this).prop("selected", true);
      i += 1;
     }
    }
   });
  } else {
   $.each(kota, function () {
    if (i == 0) {
     $(this).prop('selected', true);
     i += 1;
    }
   });
  }
 },

 cetakDetail: function (id) {
  window.open(url.base_url(Taruna.module()) + "cetak/" + id)
 }
};

$(function () {
 Taruna.setDate();
});