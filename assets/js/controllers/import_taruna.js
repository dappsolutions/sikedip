var ImportTaruna = {
 module: function () {
  return 'import_taruna';
 },

 add: function () {
  window.location.href = url.base_url(ImportTaruna.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(ImportTaruna.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(ImportTaruna.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(ImportTaruna.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'upt': $('#upt').val(),
   'alamat': $('#alamat').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = ImportTaruna.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ImportTaruna.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ImportTaruna.module()) + "detail" + '/' + resp.upt;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(ImportTaruna.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(ImportTaruna.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(ImportTaruna.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ImportTaruna.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getUploadedData: function (elm) {
  if (window.FileReader) {
   var file_csv = $(elm).get(0).files[0];
   var file_name = file_csv.name;
   var data_from_file = file_csv.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);

   var setNameFiletoTextInput = $(elm).closest('.input-field').find('.file-path').val(file_name).css({
    'font-size': '12px'
   });
   if (type_file == 'csv') {
    if (file_csv.size <= 512000) {
     var reader = new FileReader();
     reader.readAsText(file_csv);

     reader.onload = function (event) {
      var data_csv;
      var csv = event.target.result;
      var alltextLine = csv.split(/\r\n|\n/);
      var lines = [];
      for (var i = 0; i < alltextLine.length; i++) {
       //   var data = alltextLine[i].split(',');
       var data = alltextLine[i].split(';');
       var tarr = [];
       for (var j = 0; j < data.length; j++) {
        tarr.push(data[j]);
       }
       lines.push(tarr);
      }
      data_csv = lines;
      var csv_data = [];
      for (var i = 0; i < data_csv.length; i++) {
       csv_data.push(data_csv[i]);
      }


      var data = csv_data;
      var formData = new FormData();
      formData.append('data', JSON.stringify(csv_data));

      $.ajax({
       type: 'POST',
       data: formData,
       dataType: 'html',
       processData: false,
       contentType: false,
       async: false,
       url: url.base_url(ImportTaruna.module()) + "import",
       beforeSend: function () {
        message.loadingProses("Proses Import Alat..");
       },

       error: function () {
        toastr.error('Gagal Upload');
        message.closeLoading();
       },

       success: function (resp) {
        message.closeLoading();
        toastr.success('Data Berhasil Dimasukkan');
        $('#file').val('');
        $('div#table_alat').html(resp);
       }
      });
     };
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 512 KB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat csv');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 }
};