var LapWali = {
 module: function () {
  return 'laporan_wali';
 },

 detail: function (id) {
  window.location.href = url.base_url(LapWali.module()) + "detail/" + id;
 },

 setDate: function () {
  $('input#tanggal').daterangepicker({

  }, function (start, end, label) {
   var waktu = start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD');
  });
 },

 tampilkan: function () {
  var status = $('#status').val();

  $.ajax({
   type: 'POST',
   data: {
    status: status,
   },
   dataType: 'html',
   async: false,
   url: url.base_url(LapWali.module()) + "tampilkan",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    $('div#table_laporan').html(resp);
    message.closeLoading();
   }
  });
 }
};

$(function () {

});