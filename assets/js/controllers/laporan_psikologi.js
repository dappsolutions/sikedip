var LapPsikologi = {
 module: function () {
  return 'laporan_psikologi';
 },

 detail: function (id) {
  window.location.href = url.base_url(LapPsikologi.module()) + "detail/" + id;
 },

 setDate: function () {
  $('input#tanggal').daterangepicker({

  }, function (start, end, label) {
   var waktu = start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD');
  });
 },

 tampilkan: function () {
  var tanggal = $('#tanggal').val();
  var nik = $('#nik').val();
  var semester = $('#semester').val();
  var prodi = $('#jurusan').val();

  $.ajax({
   type: 'POST',
   data: {
    tanggal: tanggal,
    nik: nik,
    semester: semester,
    prodi: prodi
   },
   dataType: 'html',
   async: false,
   url: url.base_url(LapPsikologi.module()) + "tampilkan",
   error: function () {
    toastr.error("Program Error");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    $('div#table_izin').html(resp);
    message.closeLoading();
   }
  });
 }
};

$(function () {
 LapPsikologi.setDate();
});