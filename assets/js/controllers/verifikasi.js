var Verifikasi = {
 module: function () {
  return 'verifikasi';
 },

 add: function () {
  window.location.href = url.base_url(Verifikasi.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Verifikasi.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Verifikasi.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Verifikasi.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'taruna': $('#taruna').val(),
   'semester': $('#semester').val(),
   'pelanggaran': $('#pelanggaran').val(),
   'keterangan': $('#keterangan').val(),
   'tanggal': $('#tanggal').val(),
  };

  return data;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = Verifikasi.getPostData();
//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Verifikasi.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Verifikasi.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Verifikasi.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Verifikasi.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Verifikasi.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Verifikasi.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="Verifikasi.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Verifikasi.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    foto: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Verifikasi.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='Verifikasi.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="Verifikasi.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Verifikasi.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',
   autoclose: true,
  });
 },

 getListTaruna: function (elm) {
  var prodi = $(elm).val();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Verifikasi.module()) + "getContentListTaruna/" + prodi,
   error: function () {
    toastr.error("Program Error");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving...");
   },

   success: function (resp) {
    $('div#content_taruna').html(resp);
    message.closeLoading();
   }
  });
 },

 getListJenisPelanggaran: function (elm) {
  var kategori = $(elm).val();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Verifikasi.module()) + "getListJenisPelanggaran/" + kategori,
   error: function () {
    toastr.error("Program Error");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving...");
   },

   success: function (resp) {
    $('div#content_pelanggaran').html(resp);
    message.closeLoading();
   }
  });
 },

 approve: function () {
  var id = $('input#id').val();
  var taruna = $('input#taruna_id').val();
  var nama_taruna = $('input#nama_taruna').val();
  $.ajax({
   type: 'POST',
   data:{
    taruna : taruna,
    nama_taruna : nama_taruna,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Verifikasi.module()) + "approve/" + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Disetujui...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Disetujui");
     var reload = function () {
      window.location.href = url.base_url(Verifikasi.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Disetujui");
    }
    message.closeLoading();
   }
  });
 },

 reject: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12'>";
  html += "<h4>Isi Keterangan</h4>";
  html += '<textarea class="form-control" id="keterangan"></textarea><br/>';
  html += "<div class='text-right'>";
  html += "<button class='btn btn-success font-10' onclick='Verifikasi.exceRejected(" + id + ")'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 exceRejected: function (id) {
  var keterangan = $('#keterangan').val();
  if (keterangan == '') {
   toastr.error("Keterangan Harus Diisi");
   return;
  }
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   data: {
    keterangan: keterangan
   },
   url: url.base_url(Verifikasi.module()) + "rejected/" + id,
   error: function () {
    message.closeLoading();
    toastr.error("Gagal");
   },

   beforeSend: function () {
    message.loadingProses("Proses Reject Usulan")
   },

   success: function (resp) {
    message.closeLoading();
    toastr.success("Proses Rejected Sukses");
    var reload = function () {
     window.location.href = url.base_url(Verifikasi.module()) + "index";
    };

    setTimeout(reload(), 1000);
   }
  });
 },
};

$(function () {
 Verifikasi.setDate();
});