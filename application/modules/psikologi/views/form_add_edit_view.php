<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Taruna Psikologi <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Program Studi</label>
           <div class="col-md-9">
            <select id='prodi' class="form-control required" error='Program Studi' onchange="Psikologi.getDataTarunaDetail(this)">
             <option value="">--Pilih Prodi--</option>
             <?php if (!empty($list_prodi)) { ?>
              <?php foreach ($list_prodi as $v_p) { ?>
               <?php $selected = '' ?>
               <?php if (isset($prodi)) { ?>
                <?php $selected = $v_p['id'] == $prodi ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['prodi'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">No. Taruna</label>
           <div class="col-md-9" id="content_taruna">
            <?php if (isset($list_taruna)) { ?>
             <select id="taruna" class="form-control required" error="Taruna">
              <?php if (!empty($list_taruna)) { ?>
               <option value="">Pilih Taruna</option>    
               <?php foreach ($list_taruna as $value) { ?>
                <?php $selected = $value['id'] == $taruna ? 'selected' : '' ?>
                <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_taruna'] . ' - ' . $value['nama'] ?></option>     
               <?php } ?>
              <?php } else { ?>
               <option value="">Pilih Taruna</option>
              <?php } ?>
             </select>
            <?php } else { ?>
             <select name="" id="taruna" error="Taruna" class="form-control required">
              <option value="">Pilih Taruna</option>
             </select>
            <?php } ?>
           </div>
          </div>
         </div>         
         <!--/span-->
        </div>

        <div class="row">
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Tanggal</label>
           <div class="col-md-9">
            <input type="text" class="form-control required" id='tanggal' 
                   error='Tanggal' placeholder="yyyy-mm-dd"
                   value="<?php echo isset($tanggal) ? $tanggal : '' ?>"> 
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Jam</label>
           <div class="col-md-9">
            <input type="text" class="form-control required" id='jam' 
                   error='Jam' placeholder=""
                   value="<?php echo isset($jam) ? $jam : '' ?>"> 
           </div>
          </div>
         </div>
        </div>


        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Keterangan</label>
           <div class="col-md-9">
            <textarea class="form-control" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
           </div>
          </div>
         </div>        
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="Psikologi.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="Psikologi.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
