<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Taruna Sakit <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No. Taruna
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_taruna . ' - ' . $nama ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tanggal
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tgl_awal.' - '.$tgl_akhir ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-3">
          Jam 
         </div>
         <div class="col-md-6 text-primary text-left">
          <?php echo $jam_awal . ' - ' . $jam_akhir ?>
         </div>
        </div> 
        <br/>

        <div class="row">
         <div class="col-md-3">
          Keterangan
         </div>
         <div class="col-md-6 text-primary text-left">
          <?php echo $keterangan ?>
         </div>
        </div>
        <br/>

        <div class='row manual_detail <?php echo!isset($surat_dokter) ? 'display-none' : '' ?>'>
         <div class='col-md-3'>
          <?php
          $img = base_url() . 'assets/images/no_available.png';
          if (isset($surat_dokter)) {
           if ($surat_dokter == '') {
            $img = base_url() . 'assets/images/no_available.png';
           } else {
            $img = base_url() . 'files/berkas/taruna_sakit/' . $surat_dokter;
           }
          }
          ?>
          <img src="<?php echo $img ?>" height="180" width="180" class="hover" />
         </div>
         <div class='col-md-4'>
          <a href="#" onclick="TarunaSakit.showFoto(this, event)" class="badge badge-primary text-white"><?php echo $surat_dokter ?></a>        
         </div>

        </div>
        <br/>

       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="button" class="btn btn-default" onclick="TarunaSakit.back()">Kembali</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
