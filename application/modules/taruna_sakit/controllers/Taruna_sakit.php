<?php

class Taruna_sakit extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'taruna_sakit';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/taruna_sakit.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'taruna_sakit';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Taruna Sakit";
  $data['title_content'] = 'Data Taruna Sakit';
  $content = $this->getDataTaruna();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataTaruna($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('tt.no_taruna', $keyword),
       array('tt.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'tt.no_taruna', 'tt.nama'),
              'join' => array(
                  array('taruna tt', 't.taruna = tt.id')
              ),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataTaruna($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('tt.no_taruna', $keyword),
       array('tt.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'tt.no_taruna', 'tt.nama'),
              'join' => array(
                  array('taruna tt', 't.taruna = tt.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataTaruna($keyword)
  );
 }

 public function getDetailDataTaruna($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'tt.no_taruna', 'tt.nama', 'tha.prodi'),
              'join' => array(
                  array('taruna tt', 't.taruna = tt.id'),
                  array('taruna_has_akademik tha', 't.taruna = tha.taruna and (tha.status = 1 and tha.deleted = 0)'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListJenisKelamin() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_kelamin',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Taruna Sakit";
  $data['title_content'] = 'Tambah Taruna Sakit';
  $data['list_kelamin'] = $this->getListJenisKelamin();
  $data['list_prodi'] = $this->getListProdi();
  echo Modules::run('template', $data);
 }

 public function getListTarunaData($prodi = '') {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'join' => array(
                  array('taruna_has_akademik tha', 't.id = tha.taruna and (tha.status = 1 and tha.deleted = 0)')
              ),
              'where' => "t.deleted = 0 and tha.prodi = '".$prodi."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $content, true);
 }
 
 
 public function getListPenghasilan() {
  $data = Modules::run('database/get', array(
              'table' => 'penghasilan',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTaruna($prodi) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'join' => array(
                  array('taruna_has_akademik tha', 't.id = tha.taruna and (tha.status = 1 and tha.deleted = 0)')
              ),
              'where' => "t.deleted = 0 and tha.prodi = '".$prodi."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function ubah($id) {
  $data = $this->getDetailDataTaruna($id);
  
  $data['surat_dokter'] = str_replace(' ', '_', $data['surat_dokter']);

  $tgl_awal = date('m/d/Y', strtotime($data['tanggal_awal']));
  $tgl_akhir = date('m/d/Y', strtotime($data['tanggal_akhir']));
  $data['tanggal'] = $tgl_awal . ' - ' . $tgl_akhir;

  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Taruna";
  $data['title_content'] = 'Ubah Taruna';
  $data['list_prodi'] = $this->getListProdi();
  $data['list_taruna'] = $this->getListTaruna($data['prodi']);
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusTaruna($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan', 'ph.penghasilan as penghasilan_wali'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali', 'left'),
                  array('penghasilan ph', 'thw.penghasilan = ph.id', 'left'),
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array('ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id', 'left'),
                  array('semester s', 'ta.semester = s.id', 'left'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataTaruna($id);
  $data['surat_dokter'] = str_replace(' ', '_', $data['surat_dokter']);
  $data['tgl_awal'] = Modules::run('helper/getIndoDate', $data['tanggal_awal']);
  $data['tgl_akhir'] = Modules::run('helper/getIndoDate', $data['tanggal_akhir']);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Taruna Sakit";
  $data['title_content'] = "Detail Taruna Sakit";
  echo Modules::run('template', $data);
 }

 function generateRandomString($length = 6) {
  return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
 }

 public function getPostDataUser($value) {
  $data['username'] = $value->no_taruna;
  $data['password'] = $this->generateRandomString();
  $data['priveledge'] = 3;
  return $data;
 }

 public function getPostDataUserWali($value) {
  $data['username'] = $value->no_ktp;
  $data['password'] = $this->generateRandomString();
  $data['priveledge'] = 2;
  return $data;
 }

 public function getTarunaId($no_taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna',
              'where' => array('no_taruna' => $no_taruna)
  ));

  $id = '';
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  }

  return $id;
 }

 public function getPostDataTaruna($value) {
  list($tgl_awal, $tgl_akhir) = explode('-', $value->tanggal);
  $tgl_awal = date('Y-m-d', strtotime($tgl_awal));
  $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));
  $data['taruna'] = $value->taruna;
  $data['keterangan'] = $value->keterangan;
  $data['jam_awal'] = $value->jam_awal;
  $data['jam_akhir'] = $value->jam_akhir;
  $data['tanggal_awal'] = $tgl_awal;
  $data['tanggal_akhir'] = $tgl_akhir;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataTaruna($data->taruna);
   if ($id == '') {
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post['surat_dokter'] = $_FILES['file']['name'];
    }
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post['surat_dokter'] = $_FILES['file']['name'];
    }
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Taruna";
  $data['title_content'] = 'Data Taruna';
  $content = $this->getDataTaruna($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna_sakit/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function getDataTarunaSakit($id = '') {
  $data = $this->getListDataTaruna($id);
  echo json_encode(array('data' => $data));
 }

 public function getListDataTaruna($id = '') {
  $keyword = $this->input->post('keyword');
  $where = array();
  if ($id != '') {
   $where = array('p.id' => $id);
  }

  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_taruna', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna p',
              'field' => array('p.*'),
              'where' => $where,
              'limit' => 5,
              'like' => $like,
              'is_or_like' => true
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

}
