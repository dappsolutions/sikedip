<table class="table color-bordered-table info-bordered-table" id="laporan">
 <thead>
  <tr>
   <td colspan="8">Wali Login Aplikasi Mobile</td>
  </tr>
  <tr class="">
   <th class="font-12">No</th>
   <th class="font-12">Nama Wali</th>
   <th class="font-12">No. Taruna</th>
   <th class="font-12">Taruna</th>
   <th class="font-12">Tanggal</th>
   <th class="font-12">Jam</th>
   <th class="font-12">Status</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($content)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($content as $value) { ?>
    <tr>
     <td class='font-12'><?php echo $no++ ?></td>
     <td class='font-12'><?php echo $value['nama_wali'] ?></td>
     <td class='font-12'><?php echo $value['no_taruna'] ?></td>
     <td class='font-12'><?php echo $value['nama_taruna'] ?></td>
     <td class='font-12'><?php echo $value['tanggal'] ?></td>
     <td class='font-12'><?php echo $value['jam'] ?></td>
     <td class='font-12'>
      <label class="label <?php echo $value['label_color'] ?>"><?php echo $value['status'] ?></label>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
   </tr>
  <?php } ?>         
 </tbody>
</table>