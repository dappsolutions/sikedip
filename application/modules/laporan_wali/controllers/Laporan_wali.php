<?php

class Laporan_wali extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'laporan_wali';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/laporan_wali.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'taruna_has_wali_murid';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Laporan Login Wali";
  $data['title_content'] = 'Data Laporan Login Wali';
  $content = $this->getDataWali();
  $data['content'] = $content;
  echo Modules::run('template', $data);
 }

 public function getDataWali($status = '') {
  $status = $status == '' ? '' : $status;

  $status = $this->input->post('status');

  if ($status != '') {

   $where = "where lh.jam is null";
   if ($status == 'aktif') {
    $where = "where lh.jam is not null";
   }
   $query = "select thw.id, thw.nama as nama_wali, t.nama as nama_taruna, lh.tanggal, 
   lh.jam, t.no_taruna
  from ".$this->getTableName()." thw 
  join taruna t
   on t.id = thw.taruna
  left join (select min(user) as user, id from log_histori GROUP by user) lhs 
   on lhs.user = thw.user
  left join log_histori lh
   on lhs.id = lh.id 
  " . $where . "
  order by t.no_taruna";
//
  } else {
   $query = "select thw.id, thw.nama as nama_wali, t.nama as nama_taruna, lh.tanggal, 
   lh.jam, t.no_taruna
  from ".$this->getTableName()." thw 
  join taruna t
   on t.id = thw.taruna
  left join (select min(user) as user, id from log_histori GROUP by user) lhs 
   on lhs.user = thw.user
  left join log_histori lh
   on lhs.id = lh.id 
  order by t.no_taruna";
  }

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['tanggal'] != '') {
     $value['tanggal'] = Modules::run('helper/getIndoDate', $value['tanggal']);
    } else {
     $value['tanggal'] = '-';
    }
    $value['status'] = $value['jam'] == '' ? 'Tidak Aktif' : 'Aktif';
    $value['label_color'] = $value['jam'] == '' ? 'label-danger' : 'label-success';
    if ($value['jam'] == '') {
     $value['jam'] = '-';
    }
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataWali($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Sakit";
  $data['title_content'] = "Detail Sakit";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['wali']);
  echo Modules::run('template', $data);
 }

 public function tampilkan() {
  $data['content'] = $this->getDataWali();
  echo $this->load->view('data_laporan', $data, true);
 }

}
