<table class="table color-bordered-table info-bordered-table" id="laporan_kesalahan">
 <thead>
  <tr>
   <td colspan="9">Poin Pelanggaran Kesalahan</td>
  </tr>
  <tr class="">
   <th class="font-12">No</th>
   <th class="font-12">No. Taruna</th>
   <th class="font-12">Nama</th>
   <th class="font-12">Tanggal</th>
   <th class="font-12">Semester</th>
   <th class="font-12">Jenis</th>
   <th class="font-12">Tanggal</th>
   <th class="font-12">Poin</th>
   <th class="font-12">Keterangan</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($content)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($content as $value) { ?>
    <tr>
     <td class='font-12'><?php echo $no++ ?></td>
     <td class='font-12'><?php echo $value['no_taruna'] ?></td>
     <td class='font-12'><?php echo $value['nama'] ?></td>
     <td class='font-12'><?php echo $value['tanggal'] ?></td>
     <td class='font-12'><?php echo $value['semester_taruna'] ?></td>
     <td class='font-12'><?php echo $value['jenis'] ?></td>
     <td class='font-12'><?php echo $value['tanggal'] ?></td>
     <td class='font-12'><?php echo $value['point'] ?></td>
     <td class='font-12'><?php echo $value['keterangan'] ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
   </tr>
  <?php } ?>         
 </tbody>
</table>