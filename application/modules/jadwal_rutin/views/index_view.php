<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
      <button id="" class="btn btn-block btn-warning" onclick="JadwalRutin.add()">Tambah</button>
     <?php } ?>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="JadwalRutin.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table success-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">Hari</th>
         <th class="font-12">Pukul</th>
         <th class="font-12">Kegiatan</th>
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['hari'] ?></td>
           <td class='font-12'><?php echo $value['pukul'] ?></td>
           <td class='font-12'><?php echo $value['kegiatan'] ?></td>
           <td class="text-center">
            <?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
             <label id="" class="label label-warning font-12" 
                    onclick="JadwalRutin.ubah('<?php echo $value['id'] ?>')">Ubah</label>
             &nbsp;
            <?php } ?>
            <label id="" class="label label-success font-12" 
                   onclick="JadwalRutin.detail('<?php echo $value['id'] ?>')">Detail</label>
            &nbsp;
            <?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
             <label id="" class="label label-danger font-12 hover" 
                    onclick="JadwalRutin.delete('<?php echo $value['id'] ?>')">Hapus</label>
             &nbsp;
            <?php } ?>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>