<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'Jadwal Rutin' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Hari</label>
           <div class="col-md-9">
            <input value="<?php echo isset($hari) ? $hari : '' ?>" type="text" id='hari' class="form-control required" error='Hari' placeholder="Hari"></div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Pukul Mulai</label>
           <div class="col-md-9">
            <input value="<?php echo isset($pukul_mulai) ? $pukul_mulai : '' ?>" type="text" id='pukul_mulai' class="form-control required" error='Pukul Mulai' placeholder="Pukul Mulai"></div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Pukul Akhir</label>
           <div class="col-md-9">
            <input value="<?php echo isset($pukul_akhir) ? $pukul_akhir : '' ?>" type="text" id='pukul_akhir' class="form-control required" error='Pukul Akhir' placeholder="Pukul Akhir"></div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Kegiatan</label>
           <div class="col-md-9">
            <input value="<?php echo isset($kegiatan) ? $kegiatan : '' ?>" type="text" id='kegiatan' class="form-control required" error='Kegiatan' placeholder="Kegiatan"></div>
          </div>
         </div>
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="JadwalRutin.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="JadwalRutin.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
