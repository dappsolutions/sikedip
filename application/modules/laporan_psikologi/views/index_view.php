<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>    
    <div class='col-md-3'>
     <input type='text' name='' id='tanggal' class='form-control' value='' placeholder="Tanggal"/>    
    </div>     
    <div class='col-md-3'>
     <select id='jurusan' class="form-control">
      <?php if (!empty($list_jurusan)) { ?>
       <?php foreach ($list_jurusan as $value) { ?>
        <?php $selected = '' ?>
        <?php if (isset($semester)) { ?>
         <?php $selected = $value['id'] == $jurusan ? 'selected' : '' ?>
        <?php } ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['prodi'] ?></option>
       <?php } ?>
      <?php } else { ?>
       <option value="">Tidak Ada Data</option>  
      <?php } ?>
     </select>
    </div>     
    <div class='col-md-3'>
     <input type='text' name='' id='nik' class='form-control' value='' placeholder="NIK Taruna"/>    
    </div>     
    <div class='col-md-3'>
     <select id='semester' class="form-control">
      <option value="">Pilih Semester</option>  
      <?php if (!empty($list_semester)) { ?>
       <?php foreach ($list_semester as $value) { ?>
        <?php $selected = '' ?>
        <?php if (isset($semester)) { ?>
         <?php $selected = $value['id'] == $semester ? 'selected' : '' ?>
        <?php } ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo 'Semester ' . $value['semester'] ?></option>
       <?php } ?>
      <?php } else { ?>
       <option value="">Tidak Ada Data</option>  
      <?php } ?>
     </select>
    </div>         
   </div>   
   <br/>   
   <div class="row">
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="LapPsikologi.tampilkan()">Tampilkan</button>
    </div>
    <div class='col-md-2'>
     <a class="btn btn-block btn-success" download="<?php echo 'laporan_psikologi' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'laporan_izin', 'Laporan Psikologi');">Export</a>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive" id="table_izin">
      <table class="table color-bordered-table info-bordered-table" id="laporan_izin">
       <thead>
        <tr>
         <td colspan="8">Psikologi</td>
        </tr>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No. Taruna</th>
         <th class="font-12">Nama</th>
         <th class="font-12">Tanggal</th>
         <th class="font-12">Semester</th>
         <th class="font-12">Jam</th>
         <th class="font-12">Keterangan</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['no_taruna'] ?></td>
           <td class='font-12'><?php echo $value['nama'] ?></td>
           <td class='font-12'><?php echo $value['tanggal'] ?></td>
           <td class='font-12'><?php echo $value['semester_taruna'] ?></td>
           <td class='font-12'><?php echo $value['jam'] ?></td>
           <td class='font-12'><?php echo $value['keterangan'] ?></td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>