<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'ADMIN KOMPI' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Admin</label>
           <div class="col-md-9">
            <select class="form-control required" id='admin' error="Admin">
             <option value="">Pilih Admin</option>
             <?php if (!empty($list_admin)) { ?>
              <?php foreach ($list_admin as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($user)) { ?>
                <?php $selected = $user == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['username'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Kompi</label>
           <div class="col-md-9">
            <select class="form-control required" id='kompi' error="Kompi">
             <option value="">Pilih Kompi</option>
             <?php if (!empty($list_kompi)) { ?>
              <?php foreach ($list_kompi as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($kompi)) { ?>
                <?php $selected = $kompi == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>


        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="submit" class="btn btn-success" onclick="AdminKompi.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
             <button type="button" class="btn btn-default" onclick="AdminKompi.back()">Batal</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
