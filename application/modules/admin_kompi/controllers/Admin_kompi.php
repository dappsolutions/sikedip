<?php

class Admin_kompi extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'admin_kompi';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/admin_kompi.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'admin_kompi';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Admin Kompi";
  $data['title_content'] = 'Data Admin Kompi';
  $content = $this->getDataAdminKompi();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAdminKompi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('k.nama', $keyword)
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'k.nama'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('kompi k', 't.kompi = k.id')
              ),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataAdminKompi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('k.nama', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'k.nama'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('kompi k', 't.kompi = k.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAdminKompi($keyword)
  );
 }

 public function getDetailDataAdminKompi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'k.nama'),
              'join' => array(
                  array('user u', 't.user = u.id'),
                  array('kompi k', 't.kompi = k.id')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListAdmin() {
  $data = Modules::run('database/get', array(
              'table' =>  'user u',
              'field' => array('u.*'),
              'where' => "u.deleted = 0 and u.priveledge = '1'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getListKompi() {
  $data = Modules::run('database/get', array(
              'table' =>  'kompi',
              'where' => "deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Admin Kompi";
  $data['title_content'] = 'Tambah Admin Kompi';
  $data['list_admin'] = $this->getListAdmin();
  $data['list_kompi'] = $this->getListKompi();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataAdminKompi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Admin Kompi";
  $data['title_content'] = 'Ubah Admin Kompi';
  $data['list_admin'] = $this->getListAdmin();
  $data['list_kompi'] = $this->getListKompi();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataAdminKompi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Admin Kompi";
  $data['title_content'] = "Detail Admin Kompi";
  echo Modules::run('template', $data);
 }

 public function getPostDataAdminKompi($value) {
  $data['user'] = $value->admin;
  $data['kompi'] = $value->kompi;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_prodi = $this->getPostDataAdminKompi($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prodi);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_prodi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Admin Kompi";
  $data['title_content'] = 'Data Admin Kompi';
  $content = $this->getDataAdminKompi($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
