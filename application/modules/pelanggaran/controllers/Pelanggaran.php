<?php

class Pelanggaran extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pelanggaran';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pelanggaran.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pelanggaran';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggaran";
  $data['title_content'] = 'Data Pelanggaran';
  $content = $this->getDataPelanggaran();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);  
  echo Modules::run('template', $data);
 }

 public function getTotalDataPelanggaran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.jenis', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'kp.kategori'),
              'join' => array(
                  array('kategori_pelanggaran kp', 'kp.id = t.kategori_pelanggaran')
              ),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataPelanggaran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.jenis', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'kp.kategori'),
              'join' => array(
                  array('kategori_pelanggaran kp', 'kp.id = t.kategori_pelanggaran')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPelanggaran($keyword)
  );
 }

 public function getDetailDataPelanggaran($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'kp.kategori'),
              'join' => array(
                   array('kategori_pelanggaran kp', 'kp.id = t.kategori_pelanggaran')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }
 
 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_pelanggaran kp',
              'field' => array('kp.*'),
              'where' => "kp.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pelanggaran";
  $data['title_content'] = 'Tambah Pelanggaran';
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPelanggaran($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pelanggaran";
  $data['title_content'] = 'Ubah Pelanggaran';
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPelanggaran($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pelanggaran";
  $data['title_content'] = "Detail Pelanggaran";
  echo Modules::run('template', $data);
 }
 public function getPostDataPelanggaran($value) {
  $data['jenis'] = $value->jenis;
  $data['point'] = $value->point;
  $data['kategori_pelanggaran'] = $value->kategori;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_prodi = $this->getPostDataPelanggaran($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prodi);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_prodi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggaran";
  $data['title_content'] = 'Data Pelanggaran';
  $content = $this->getDataPelanggaran($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
