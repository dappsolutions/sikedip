<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'PENGUMUMAN' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Judul</label>
           <div class="col-md-9">
            <input value="<?php echo isset($judul) ? $judul : '' ?>" type="text" id='judul' class="form-control required" error='Judul' placeholder="Judul"></div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Isi</label>
           <div class="col-md-9">
            <textarea id="isi" class="form-control required"><?php echo isset($isi) ? $isi : '' ?></textarea>
           </div>
          </div>
         </div>
        </div>


        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Ditujukan</label>
           <div class="col-md-9">
            <select id='ditujukan' class="form-control required" error='Ditujukan'>
             <?php if (!empty($list_tujuan)) { ?>
              <?php foreach ($list_tujuan as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($ditujukan)) { ?>
                <?php $selected = $value == $ditujukan ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value ?>"><?php echo $value ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>




         <div class="form-actions">
          <div class="row">
           <div class="col-md-12">
            <div class="row">
             <div class="col-md-offset-3 col-md-9 text-right">
              <button type="submit" class="btn btn-success" onclick="Pengumuman.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
              <button type="button" class="btn btn-default" onclick="Pengumuman.back()">Batal</button>
             </div>
            </div>
           </div>
           <div class="col-md-6"> </div>
          </div>
         </div>
         </form>
        </div>
       </div>
     </div>
    </div>
   </div>
   <!--./row--> 
  </div>
