<?php

class Pengumuman extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pengumuman';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pengumuman.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pengumuman';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengumuman";
  $data['title_content'] = 'Data Pengumuman';
  $content = $this->getDataPengumuman();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPengumuman($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.isi', $keyword),
       array('t.judul', $keyword),
       array('t.ditujukan', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataPengumuman($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.isi', $keyword),
       array('t.judul', $keyword),
       array('t.ditujukan', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPengumuman($keyword)
  );
 }

 public function getDetailDataPengumuman($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListTujuan() {
  return array(
      'SEMUA',
      'TARUNA',
      'WALI'
  );
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pengumuman";
  $data['title_content'] = 'Tambah Pengumuman';
  $data['list_tujuan'] = $this->getListTujuan();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPengumuman($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pengumuman";
  $data['title_content'] = 'Ubah Pengumuman';
  $data['list_tujuan'] = $this->getListTujuan();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPengumuman($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pengumuman";
  $data['title_content'] = "Detail Pengumuman";
  echo Modules::run('template', $data);
 }

 public function getPostDataPengumuman($value) {
  $data['judul'] = $value->judul;
  $data['isi'] = $value->isi;
  $data['ditujukan'] = $value->ditujukan;
  return $data;
 }

 public function getDataToken($tujuan) {
  $join = "";
  switch ($tujuan) {
   case 'SEMUA':
    $join = array(
        array('taruna t', 'u.id = t.user', 'left'),
        array('taruna_has_wali_murid thw', 'u.id = thw.user', 'left'),
    );
    break;
   case 'TARUNA':
    $join = array(
        array('taruna t', 'u.id = t.user'),
    );
    break;
   case 'WALI':
    $join = array(
        array('taruna_has_wali_murid thw', 'u.id = thw.user')
    );
    break;
   default:
    break;
  }

  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('distinct(u.token) as token'),
              'join' => $join,
              'where' => "(u.deleted = 0 and u.token != '')"
  ));



  $result = array();

  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value['token']);
   }
  }


  return $result;
 }

 public function sendNotification($tujuan, $judul, $isi) {
  $token = $this->getDataToken($tujuan);
  $url = 'https://fcm.googleapis.com/fcm/send';

  $fields = array(
      'registration_ids' => $token,
      'notification' => array(
          'title' => "Pengumuman",
          'body' => $isi,
      ),
      'data' => array(
          'title'=> 'pengumuman',
          'isi' => $isi,
          'judul' => $judul,
      )
  );

  // echo '<pre>';
  // print_r($fields);die;
  $headers = array(
      'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
      'Content-Type:application/json'
  );



  // echo json_encode($fields);die;

  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);

  curl_setopt($ch, CURLOPT_POST, true);

  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

  $result = curl_exec($ch);


  // echo 'asdad';die;
  // echo '<pre>';
  // print_r($result);die;

//  if ($result === false) {
//
//   die('Curl failed: ' . curl_error($ch));
//  }

  curl_close($ch);
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_prodi = $this->getPostDataPengumuman($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prodi);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_prodi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
   if ($is_valid) {
    //send notification
    $this->sendNotification($data->ditujukan, $data->judul, $data->isi);
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengumuman";
  $data['title_content'] = 'Data Pengumuman';
  $content = $this->getDataPengumuman($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
