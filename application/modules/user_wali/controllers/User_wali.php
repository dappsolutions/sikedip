<?php

class User_wali extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'user_wali';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/user_wali.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'user';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data User";
  $data['title_content'] = 'Data User';
  $content = $this->getDataUser();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataUser($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('ta.nama', $keyword),
       array('ta.no_taruna', $keyword),
   );
  }
  if ($keyword != '') {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'ta.nama as nama_taruna', 'ta.no_taruna'),
               'join' => array(
                   array('taruna_has_wali_murid thw', 'thw.user = t.id'),
                   array('taruna ta', 'thw.taruna = ta.id'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_brackets' => true,
               'where' => "t.deleted = 0 and t.priveledge = 2"
   ));
  } else {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'ta.nama as nama_taruna', 'ta.no_taruna'),
               'join' => array(
                   array('taruna_has_wali_murid thw', 'thw.user = t.id'),
                   array('taruna ta', 'thw.taruna = ta.id'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'where' => "t.deleted = 0 and t.priveledge = 2"
   ));
  }

  return $total;
 }

 public function getDataUser($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.username', $keyword),
       array('ta.nama', $keyword),
       array('ta.no_taruna', $keyword),
   );
  }

  if ($keyword != '') {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'ta.nama as nama_taruna', 'ta.no_taruna'),
               'join' => array(
                   array('taruna_has_wali_murid thw', 'thw.user = t.id'),
                   array('taruna ta', 'thw.taruna = ta.id'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'inside_brackets' => true,
               'where' => "t.deleted = 0 and t.priveledge = 2"
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*', 'ta.nama as nama_taruna', 'ta.no_taruna'),
               'join' => array(
                   array('taruna_has_wali_murid thw', 'thw.user = t.id'),
                   array('taruna ta', 'thw.taruna = ta.id'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => "t.deleted = 0 and t.priveledge = 2"
   ));
  }

  //  echo "<pre>";
  //  echo $this->db->last_query();
  //  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataUser($keyword)
  );
 }

 public function getDetailDataUser($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah User";
  $data['title_content'] = 'Tambah User';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataUser($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah User";
  $data['title_content'] = 'Ubah User';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataUser($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail User";
  $data['title_content'] = "Detail User";
  echo Modules::run('template', $data);
 }

 public function getPostDataUser($value) {
  $data['username'] = $value->username;
  $data['password'] = $value->password;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_prodi = $this->getPostDataUser($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prodi);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_prodi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data User";
  $data['title_content'] = 'Data User';
  $content = $this->getDataUser($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
