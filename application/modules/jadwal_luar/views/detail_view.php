<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Jadwal Luar <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tanggal
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tanggal_awal .' - '.$tanggal_akhir  ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Jam
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $jam_awal.' - '.$jam_akhir ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Taruna
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_taruna.' - '.$nama ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Program Studi
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $data_prodi['program_studi'] ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>
       
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Semester
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $semester_taruna ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>
       
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Jenis Izin
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $jenis ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Keterangan
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $keterangan ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>


        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="button" class="btn btn-default" onclick="JadwalLuar.back()">Kembali</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
