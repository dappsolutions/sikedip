<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <a class="btn btn-block btn-success" download="<?php echo 'laporan_log' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'laporan_log', 'Laporan Log Wali Murid');">Export</a>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="Log.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table success-bordered-table" id="laporan_log">
       <thead>
        <tr>
         <td colspan="8">Log Orang Tua</td>
        </tr>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No Taruna</th>
         <th class="font-12">Nama Taruna</th>
         <th class="font-12">Nama Wali</th>
         <th class="font-12">Tanggal</th>
         <th class="font-12">Jam</th>
         <th class="font-12">Aktifitas</th>
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['no_taruna'] ?></td>
           <td class='font-12'><?php echo $value['taruna'] ?></td>
           <td class='font-12'><?php echo $value['wali'] ?></td>
           <td class='font-12'><?php echo $value['tanggal'] ?></td>
           <td class='font-12'><?php echo $value['jam'] ?></td>
           <td class='font-12'><?php echo $value['aktifitas'] ?></td>
           <td class="text-center">
            <label id="" class="label label-danger font-10 hover" 
                   onclick="Log.delete('<?php echo $value['id'] ?>')">Hapus</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>