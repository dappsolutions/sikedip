<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'HUKUMAN' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Angka</label>
           <div class="col-md-9">
            <input value="<?php echo isset($angka) ? $angka : '' ?>" type="text" id='angka' class="form-control required" error='Angka' placeholder="Angka"></div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Hukuman</label>
           <div class="col-md-9">
            <input value="<?php echo isset($hukuman) ? $hukuman : '' ?>" type="text" id='hukuman' class="form-control required" error='Hukuman' placeholder="Hukuman"></div>
          </div>
         </div>
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="Hukuman.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="Hukuman.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
