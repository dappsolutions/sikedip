<table class="table color-bordered-table info-bordered-table" id="laporan_izin">
 <thead>
  <tr>
   <td colspan="8">Izin Keluar</td>
  </tr>
  <tr class="">
   <th class="font-12">No</th>
   <th class="font-12">No. Taruna</th>
   <th class="font-12">Nama</th>
   <th class="font-12">Tanggal</th>
   <th class="font-12">Semester</th>
   <th class="font-12">Jam</th>
   <th class="font-12">Jenis Izin</th>
   <th class="font-12">Keterangan</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($content)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($content as $value) { ?>
    <tr>
     <td class='font-12'><?php echo $no++ ?></td>
     <td class='font-12'><?php echo $value['no_taruna'] ?></td>
     <td class='font-12'><?php echo $value['nama'] ?></td>
     <td class='font-12'><?php echo $value['tanggal_awal'] . ' s/d ' . $value['tanggal_akhir'] ?></td>
     <td class='font-12'><?php echo $value['semester_taruna'] ?></td>
     <td class='font-12'><?php echo $value['jam_awal'] . ' - ' . $value['jam_akhir'] ?></td>
     <td class='font-12'><?php echo $value['jenis'] ?></td>
     <td class='font-12'><?php echo $value['keterangan'] ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
   </tr>
  <?php } ?>         
 </tbody>
</table>