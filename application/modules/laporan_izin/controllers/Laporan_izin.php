<?php

class Laporan_izin extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'laporan_izin';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/laporan_izin.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'jadwal_luar';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Laporan Izin";
  $data['title_content'] = 'Data Laporan Izin';
  $data['list_semester'] = $this->getListSemester();
  $content = $this->getDataIzin();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['list_jurusan'] = $this->getListProdi();
  echo Modules::run('template', $data);
 }

 public function getTotalDataIzin($date) {
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
              ),
              'where' => "k.deleted = 0 and k.tanggal_awal = '" . $date . "'"
  ));

  return $total;
 }

 public function getDataIzin($date = '') {
  $date = $date == '' ? date('Y-m-d') : $date;

  if (!empty($_POST)) {
   $tanggal = $this->input->post('tanggal');
   $semester = $this->input->post('semester');
   $nik = $this->input->post('nik');
   $prodi = $this->input->post('prodi');

   if ($tanggal != '') {
    list($tgl_awal, $tgl_akhir) = explode('-', $tanggal);
    $tgl_awal = trim(date('Y-m-d', strtotime($tgl_awal)));
    $tgl_akhir = trim(date('Y-m-d', strtotime($tgl_akhir)));
   } else {
    $tgl_awal = $date;
    $tgl_akhir = $date;
   }


   $where = "k.deleted = 0 and (k.tanggal_awal >= '" . $tgl_awal . "' and k.tanggal_awal <= '" . $tgl_akhir . "')";

   $like = array(
       array('s.id', $semester),
       array('tha.prodi', $prodi),
       array('t.no_taruna', $nik),
   );
//   if ($tanggal != '' && $semester != '' && $prodi != '') {
//    $where = "k.deleted = 0 and (k.tanggal_awal >= '" . $tgl_awal . "' and k.tanggal_awal <= '" . $tgl_akhir . "') "
//            . "and s.id = '" . $semester . "' and tha.prodi = '" . $prodi . "'";
//   }
//
//   if ($tanggal != '' && $nik != '' && $prodi != '') {
//    $where = "k.deleted = 0 and (k.tanggal_awal >= '" . $tgl_awal . "' and k.tanggal_awal <= '" . $tgl_akhir . "') "
//            . "and t.no_taruna = '" . $nik . "' and tha.prodi = '" . $prodi . "'";
//   }
//   echo $where;die;
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' k',
               'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                   's.semester as semester_taruna'),
               'join' => array(
                   array('taruna t', 'k.taruna = t.id'),
                   array('semester s', 'k.semester = s.id'),
                   array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
                   array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1'),
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_brackets' => true,
               'where' => $where
   ));
//
//   echo "<pre>";
//   echo $this->db->last_query();
//   die;
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' k',
               'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                   's.semester as semester_taruna'),
               'join' => array(
                   array('taruna t', 'k.taruna = t.id'),
                   array('semester s', 'k.semester = s.id'),
                   array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
               ),
               'where' => "k.deleted = 0 and k.tanggal_awal = '" . $date . "'"
   ));
  }


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal_awal'] = Modules::run('helper/getIndoDate', $value['tanggal_awal']);
    $value['tanggal_akhir'] = Modules::run('helper/getIndoDate', $value['tanggal_akhir']);
    array_push($result, $value);
   }
  }

  //izin lain
  if (isset($_POST)) {
   $tanggal = $this->input->post('tanggal');
   $semester = $this->input->post('semester');
   $nik = $this->input->post('nik');
   $prodi = $this->input->post('prodi');

   if ($tanggal != '') {
    list($tgl_awal, $tgl_akhir) = explode('-', $tanggal);
    $tgl_awal = trim(date('Y-m-d', strtotime($tgl_awal)));
    $tgl_akhir = trim(date('Y-m-d', strtotime($tgl_akhir)));
   } else {
    $tgl_awal = $date;
    $tgl_akhir = $date;
   }


   $where = "k.deleted = 0 and (k.tanggal_awal >= '" . $tgl_awal . "' and k.tanggal_awal <= '" . $tgl_akhir . "')";

//   if ($tanggal != '' && $semester != '' && $prodi != '') {
//    $where = "k.deleted = 0 and (k.tanggal_awal >= '" . $tgl_awal . "' and k.tanggal_awal <= '" . $tgl_akhir . "') "
//            . "and s.id = '" . $semester . "' and tha.prodi = '" . $prodi . "'";
//   }
//
//   if ($tanggal != '' && $nik != '' && $prodi != '') {
//    $where = "k.deleted = 0 and (k.tanggal_awal >= '" . $tgl_awal . "' and k.tanggal_awal <= '" . $tgl_akhir . "') "
//            . "and t.no_taruna = '" . $nik . "' and tha.prodi = '" . $prodi . "'";
//   }

   $like = array(
       array('s.id', $semester),
       array('tha.prodi', $prodi),
       array('t.no_taruna', $nik),
   );
   $data = Modules::run('database/get', array(
               'table' => 'jadwal_luar_lain k',
               'field' => array('k.*', 'p.jenis', 'pr.prodi as program_studi',
                   's.semester as semester_taruna', 't.no_taruna', 't.nama'),
               'join' => array(
                   array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
                   array('prodi pr', 'k.prodi = pr.id'),
                   array('semester s', 'k.semester = s.id'),
                   array('jadwal_luar_has_taruna jlh', 'k.id = jlh.jadwal_luar_lain'),
                   array('taruna t', 'jlh.taruna = t.id'),
                   array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1'),
               ),
               'is_or_like'=> true,
               'like'=> $like,
               'inside_brackets'=> true,
               'where' => $where
   ));

//   echo "<pre>";
//   echo $this->db->last_query();
//   die;
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'jadwal_luar_lain k',
               'field' => array('k.*', 'p.jenis', 'pr.prodi as program_studi',
                   's.semester as semester_taruna', 't.no_taruna', 't.nama'),
               'join' => array(
                   array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
                   array('prodi pr', 'k.prodi = pr.id'),
                   array('semester s', 'k.semester = s.id'),
                   array('jadwal_luar_has_taruna jlh', 'k.id = jlh.jadwal_luar_lain'),
                   array('taruna t', 'jlh.taruna = t.id'),
                   array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1'),
               ),
               'where' => "k.deleted = 0 and k.tanggal_awal = '" . $date . "'"
   ));
  }

  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal_awal'] = Modules::run('helper/getIndoDate', $value['tanggal_awal']);
    $value['tanggal_akhir'] = Modules::run('helper/getIndoDate', $value['tanggal_akhir']);
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);
//  die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataIzin($date)
  );
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataIzin($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Izin";
  $data['title_content'] = "Detail Izin";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['taruna']);
  echo Modules::run('template', $data);
 }

 public function tampilkan() {
  $data['content'] = $this->getDataIzin()['data'];
  echo $this->load->view('data_laporan', $data, true);
 }

}
