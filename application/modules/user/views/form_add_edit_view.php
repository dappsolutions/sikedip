<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'User' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Hak Akses</label>
           <div class="col-md-9">
            <select id='hak_akses' class="form-control required" error='Hak Akses'>
             <?php if (!empty($list_hak_akses)) { ?>
              <?php foreach ($list_hak_akses as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($priveledge)) { ?>
                <?php $selected = $value['id'] == $priveledge ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Username</label>
           <div class="col-md-9">
            <input value="<?php echo isset($username) ? $username : '' ?>" type="text" id='username' class="form-control required" error='Username' placeholder="Username"></div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Password</label>
           <div class="col-md-9">
            <input value="<?php echo isset($password) ? $password : '' ?>" type="text" id='password' class="form-control required" error='Password' placeholder="Password"></div>
          </div>
         </div>
        </div>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="User.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="User.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
