<?php

class Laporan_penghargaan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'laporan_penghargaan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/laporan_penghargaan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'penghargaan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Laporan Penghargaan";
  $data['title_content'] = 'Data Laporan Penghargaan';
  $data['list_semester'] = $this->getListSemester();
  $content = $this->getDataPenghargaan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['list_jurusan'] = $this->getListProdi();
  echo Modules::run('template', $data);
 }

 public function getTotalDataPenghargaan($date) {
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi',
                  's.semester as semester_taruna', 'p.skor'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('prestasi p', 'k.prestasi = p.id'),
              ),
              'where' => "k.deleted = 0 and k.tanggal = '" . $date . "'"
  ));

  return $total;
 }

 public function getDataPenghargaan($date = '') {
  $date = $date == '' ? date('Y-m-d') : $date;

  if (!empty($_POST)) {
   $tanggal = $this->input->post('tanggal');
   $semester = $this->input->post('semester');
   $nik = $this->input->post('nik');
   $prodi = $this->input->post('prodi');

   if ($tanggal != '') {
    list($tgl_awal, $tgl_akhir) = explode('-', $tanggal);
    $tgl_awal = trim(date('Y-m-d', strtotime($tgl_awal)));
    $tgl_akhir = trim(date('Y-m-d', strtotime($tgl_akhir)));
   }else{
    $tgl_awal = $date;
    $tgl_akhir = $date;
   }

   
   $where = "k.deleted = 0 and (k.tanggal >= '" . $tgl_awal . "' and k.tanggal <= '" . $tgl_akhir . "') "
           . " and tha.prodi = '" . $prodi . "'";

   if ($tanggal != '' && $semester != '' && $prodi != '') {
    $where = "k.deleted = 0 and (k.tanggal >= '" . $tgl_awal . "' and k.tanggal <= '" . $tgl_akhir . "') "
            . "and s.id = '" . $semester . "' and tha.prodi = '" . $prodi . "'";
   }
   
   if ($tanggal != '' && $nik != '' && $prodi != '') {
    $where = "k.deleted = 0 and (k.tanggal >= '" . $tgl_awal . "' and k.tanggal <= '" . $tgl_akhir . "') "
            . "and t.no_taruna = '" . $nik . "' and tha.prodi = '" . $prodi . "'";
   }  

   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' k',
               'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi',
                   's.semester as semester_taruna', 'p.skor', 'k.keterangan'),
               'join' => array(
                   array('taruna t', 'k.taruna = t.id'),
                   array('semester s', 'k.semester = s.id'),
                   array('prestasi p', 'k.prestasi = p.id'),
                   array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1'),
               ),
               'where' => $where
   ));
//
//   echo "<pre>";
//   echo $this->db->last_query();
//   die;
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' k',
               'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi',
                   's.semester as semester_taruna', 'p.skor'),
               'join' => array(
                   array('taruna t', 'k.taruna = t.id'),
                   array('semester s', 'k.semester = s.id'),
                   array('prestasi p', 'k.prestasi = p.id'),
               ),
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => "k.deleted = 0 and k.tanggal = '" . $date . "'"
   ));
  }


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal'] = Modules::run('helper/getIndoDate', $value['tanggal']);
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPenghargaan($date)
  );
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPenghargaan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Penghargaan";
  $data['title_content'] = "Detail Penghargaan";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['taruna']);
  echo Modules::run('template', $data);
 }

 public function tampilkan() {
  $data['content'] = $this->getDataPenghargaan()['data'];
  echo $this->load->view('data_laporan', $data, true);
 }

}
