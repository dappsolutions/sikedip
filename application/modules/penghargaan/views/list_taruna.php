<select id='taruna' class="form-control required" error='Kategori'>
 <option value="">--Pilih Taruna--</option>
 <?php if (!empty($list_taruna)) { ?>
  <?php foreach ($list_taruna as $value) { ?>
   <?php $selected = '' ?>
   <?php if (isset($taruna)) { ?>
    <?php $selected = $value['id'] == $taruna ? 'selected' : '' ?>
   <?php } ?>
   <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_taruna'] . ' - ' . $value['nama'] ?></option>
  <?php } ?>
 <?php } else { ?>
  <option value="">Tidak Ada Data</option>  
 <?php } ?>
</select>