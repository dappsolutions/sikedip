<select id='prestasi' class="form-control required" error='Prestasi'>
 <option value="">--Pilih Prestasi--</option>
 <?php if (!empty($list_prestasi)) { ?>
  <?php foreach ($list_prestasi as $value) { ?>
   <?php $selected = '' ?>
   <?php if (isset($prestasi)) { ?>
    <?php $selected = $value['id'] == $prestasi ? 'selected' : '' ?>
   <?php } ?>
   <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['prestasi'] . ' (' . $value['skor'] . ' skor)' ?></option>
  <?php } ?>
 <?php } else { ?>
  <option value="">Tidak Ada Data</option>  
 <?php } ?>
</select>