<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'Penghargaan' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">        

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Tanggal</label>
           <div class="col-md-9">
            <input id="tanggal" type="text" value="<?php echo isset($tanggal) ? $tanggal : '' ?>" id="" class="form-control required" error="Tanggal" placeholder="Tanggal"/>
           </div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Program Studi</label>
           <div class="col-md-9">
            <select id='prodi' class="form-control required" error='Program Studi' onchange="Penghargaan.getListTaruna(this)">
             <option value="">--Pilih Prodi--</option>
             <?php if (!empty($list_prodi)) { ?>
              <?php foreach ($list_prodi as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($prodi)) { ?>
                <?php $selected = $value['id'] == $prodi ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['prodi'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Taruna</label>
           <div class="col-md-9" id="content_taruna">
            <select id='taruna' class="form-control required" error='Taruna'>
             <option value="">--Pilih Taruna--</option>
             <?php if (!empty($list_taruna)) { ?>
              <?php foreach ($list_taruna as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($taruna)) { ?>
                <?php $selected = $value['id'] == $taruna ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_taruna'] . ' - ' . $value['nama'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Semester</label>
           <div class="col-md-9">
            <select id='semester' class="form-control required" error='Kategori'>
             <?php if (!empty($list_semester)) { ?>
              <?php foreach ($list_semester as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($semester)) { ?>
                <?php $selected = $value['id'] == $semester ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['semester'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Bidang Penghargaan</label>
           <div class="col-md-9">
            <select id='bidang' class="form-control required" error='Bidang' onchange="Penghargaan.getListJenisPrestasi(this)">
             <option value="">--Pilih Bidang Penghargaan--</option>
             <?php if (!empty($list_bidang)) { ?>
              <?php foreach ($list_bidang as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($bidang_penghargaan)) { ?>
                <?php $selected = $value['id'] == $bidang_penghargaan ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['bidang_penghargaan'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Prestasi</label>
           <div class="col-md-9" id="content_pelanggaran">
            <select id='prestasi' class="form-control required" error='Prestasi'>
             <option value="">--Pilih Prestasi--</option>
             <?php if (!empty($list_prestasi)) { ?>
              <?php foreach ($list_prestasi as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($prestasi_id)) { ?>
                <?php $selected = $value['id'] == $prestasi_id ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['prestasi'].' ('.$value['skor'].' skor)' ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Keterangan</label>
           <div class="col-md-9">
            <textarea class="form-control" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
           </div>
          </div>
         </div>
        </div>


        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="submit" class="btn btn-success" onclick="Penghargaan.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
             <button type="button" class="btn btn-default" onclick="Penghargaan.back()">Batal</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
