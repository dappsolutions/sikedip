<?php

class Penghargaan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $kompi;
 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->akses = $this->session->userdata('hak_akses');
  $this->kompi = $this->session->userdata('kompi');
 }

 public function getModuleName() {
  return 'penghargaan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/penghargaan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'penghargaan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Penghargaan";
  $data['title_content'] = 'Data Penghargaan';
  $content = $this->getDataPenghargaan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPenghargaan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
       array('t.no_taruna', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi', 
                  's.semester as semester_taruna', 'p.skor'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('prestasi p', 'k.prestasi = p.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted = 0"
  ));

  return $total;
 }

 public function getDataPenghargaan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
       array('t.no_taruna', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi', 
                  's.semester as semester_taruna', 'p.skor'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('prestasi p', 'k.prestasi = p.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPenghargaan($keyword)
  );
 }

 public function getDetailDataPenghargaan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi', 'p.id as prestasi_id', 
                  's.semester as semester_taruna', 'kp.bidang_penghargaan as bidang', 'p.bidang_penghargaan', 'p.skor'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('prestasi p', 'k.prestasi = p.id'),
                  array('bidang_penghargaan kp', 'p.bidang_penghargaan = kp.id'),
              ),
              'where' => "k.id = '".$id."'"
  ));

  $data = $data->row_array();
  return $data;
 }
 
 public function getListTaruna() {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'bidang_penghargaan p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPrestasi() {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Penghargaan";
  $data['title_content'] = 'Tambah Penghargaan';
  $data['list_prodi'] = $this->getListProdi();
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_bidang'] = $this->getListKategori();
  $data['list_prestasi'] = $this->getListPrestasi();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPenghargaan($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Penghargaan";
  $data['title_content'] = 'Ubah Penghargaan';
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_prestasi'] = $this->getListPrestasi();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_bidang'] = $this->getListKategori();
  $prodi = $this->getDataProdiTaruna($data['taruna']);
  $data['prodi'] = $prodi['prodi'];
  echo Modules::run('template', $data);
 }

 public function getDataProdiTaruna($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('tha.*', 'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id')
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '".$taruna."' and tha.status = 1"
  ));
  
  return $data->row_array();
 }
 
 public function detail($id) {
  $data = $this->getDetailDataPenghargaan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Penghargaan";
  $data['title_content'] = "Detail Penghargaan";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['taruna']);
  echo Modules::run('template', $data);
 }
 public function getPostDataPenghargaan($value) {
  $data['taruna'] = $value->taruna;
  $data['semester'] = $value->semester;
  $data['prestasi'] = $value->prestasi;
  $data['keterangan'] = $value->keterangan;
  $data['tanggal'] = $value->tanggal;
  return $data;
 }

 public function getContentListTaruna($prodi) {
  $where = "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "' "
          . "and t.kompi = '".$this->kompi."'";
  if($this->akses == 'Superadmin'){
   $where = "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "'";
  }
  $data = Modules::run('database/get', array(
              'table' =>  'taruna_has_akademik tha',
              'field' => array('t.*'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna t', 'tha.taruna = t.id'),
              ),
              'where' => $where
  ));
  
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $conten, true);
 }
 
 public function getListJenisPrestasi($bidang) {
  $data = Modules::run('database/get', array(
              'table' =>  'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.bidang_penghargaan = '".$bidang."'"
  ));
  
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_prestasi'] = $result;
  echo $this->load->view('list_prestasi', $conten, true);
 }
 
 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataPenghargaan($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Penghargaan";
  $data['title_content'] = 'Data Penghargaan';
  $content = $this->getDataPenghargaan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
