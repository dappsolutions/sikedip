<?php

class Role_admin extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'role_admin';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/role_admin.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'role_admin';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Akses";
  $data['title_content'] = 'Data Akses';
  $content = $this->getDataAkses();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAkses($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('m.nama_module', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' ra',
              'field' => array('distinct(u.username) as username, u.id as user'),
              'join' => array(
                  array('module_web mw', 'ra.module = mw.id', 'left'),
                  array('user u', 'ra.user = u.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "ra.deleted = 0"
  ));

  return $total;
 }

 public function getDataAkses($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('m.nama_module', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' ra',
              'field' => array('distinct(u.username) as username, u.id as user'),
              'join' => array(
                  array('module_web mw', 'ra.module = mw.id', 'left'),
                  array('user u', 'ra.user = u.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "ra.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAkses($keyword)
  );
 }

 public function getDetailDataAkses($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'u.username', 'kk.nama as nama_kompi'),
              'join' => array(
                  array('user u', 'k.user = u.id'),
                  array('admin_kompi ak', 'ak.user = u.id', 'left'),
                  array('kompi kk', 'ak.kompi = kk.id', 'left'),
              ),
              'where' => "u.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListTaruna() {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenis() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_jadwal_luar p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPrestasi() {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListAdmin() {
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('u.*', 'k.nama as nama_kompi'),
              'join' => array(
                  array('admin_kompi ak', 'u.id = ak.user', 'left'),
                  array('kompi k', 'ak.kompi = k.id', 'left')
              ),
              'where' => "u.deleted = 0 and u.priveledge = 1"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListDataModule($id = '') {
  $where = array();
  if($id != ''){
   $where = array(
       'u.id'=> $id
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'module_web mw',
              'field' => array('mw.*', 'u.username', 'u.id as user'),
              'join' => array(
                  array('role_admin ra', 'mw.id = ra.module', 'left'),
                  array('user u', 'u.id = ra.user', 'left'),
              ),
              'orderby' => 'mw.parent',
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Akses";
  $data['title_content'] = 'Tambah Akses';
  $data['list_admin'] = $this->getListAdmin();
  $data['detail_module'] = $this->getListDataModule();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataAkses($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Akses";
  $data['title_content'] = 'Ubah Akses';
  $data['detail_module'] = $this->getListDataModule($id);
  echo Modules::run('template', $data);
 }

 public function getDataProdiTaruna($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('tha.*', 'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id')
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '" . $taruna . "' and tha.status = 1"
  ));

  return $data->row_array();
 }

 public function getDetailTarunaAkses($id) {
  $data = Modules::run('database/get', array(
              'table' => 'jadwal_luar_has_taruna` jl',
              'field' => array('jl.*', 't.no_taruna', 't.nama',
                  'p.prodi as prodi_taruna', 's.semester as semester_taruna'),
              'join' => array(
                  array('taruna t', 'jl.taruna = t.id'),
                  array('prodi p', 'jl.prodi = p.id'),
                  array('semester s', 'jl.semester = s.id'),
              ),
              'where' => array('jl.jadwal_luar_lain' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataAkses($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Akses";
  $data['title_content'] = "Detail Akses";
  $data['detail_module'] = $this->getListDataModule($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataAkses($value) {
  $data['user'] = $value->user;
  return $data;
 }

 public function getContentListTaruna($prodi) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('t.*'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna t', 'tha.taruna = t.id'),
              ),
              'where' => "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $conten, true);
 }

 public function getListJenisPrestasi($bidang) {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.bidang_penghargaan = '" . $bidang . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_prestasi'] = $result;
  echo $this->load->view('list_prestasi', $conten, true);
 }

 public function getIdTaruna($no_taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna',
              'where' => "no_taruna = '" . $no_taruna . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  }

  return $id;
 }

 public function getPostTarunaAkses($value, $jadwal) {
  $data['jadwal_luar_lain'] = $jadwal;
  $data['taruna'] = $value->taruna;
  $data['prodi'] = $value->prodi;
  $data['semester'] = $value->semester;

  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $id = $data->user;
   if (!empty($data->module)) {
    foreach ($data->module as $value) {
     $post['user'] = $data->user;
     $post['module'] = $value->module;
     $post['status'] = 1;
     $akses = Modules::run('database/_insert', $this->getTableName(), $post);
//     echo $akses;
//     die;
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Akses";
  $data['title_content'] = 'Data Akses';
  $content = $this->getDataAkses($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataTaruna($id = '') {
  $data = $this->getListDataTaruna($id);
  echo json_encode(array('data' => $data));
 }

 public function getListDataTaruna($id = '') {
  $keyword = $this->input->post('keyword');
  $where = array();
  if ($id != '') {
   $where = array('p.id' => $id);
  }

  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_taruna', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna p',
              'field' => array('p.*'),
              'where' => $where,
              'limit' => 5,
              'like' => $like,
              'is_or_like' => true
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataTarunaDetail() {
  $prodi = $this->input->post('prodi');
  $semester = $this->input->post('semester');
  if ($semester != '') {
   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' "
           . "and tha.semester = '" . $semester . "'";
  } else {
   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'join' => array(
                  array('taruna_has_akademik tha', 'tha.taruna = t.id and tha.status = 1')
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['detail'] = $result;
  echo $this->load->view('list_taruna', $content, true);
 }

}
