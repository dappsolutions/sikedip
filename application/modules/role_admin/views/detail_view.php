<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Akses Admin <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            User
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $username . ' - ' . $nama_kompi ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <h3 class="box-title">DATA MODULE <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <!--/row-->
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table warning-bordered-table" id="table_taruna">
            <thead>
             <tr class="">
              <th class="font-12">Module</th>
              <th class="font-12">Parent</th>
             </tr>
            </thead>
            <tbody>
             <?php if (!empty($detail_module)) { ?>
              <?php foreach ($detail_module as $value) { ?>
               <tr>
                <td class='font-12'>
                 <?php echo $value['nama_module']?>
                </td>
                <td class='font-12'>
                 <?php echo $value['parent']?>
                </td>
               </tr>  
              <?php } ?>
             <?php } ?>         
            </tbody>
           </table>
          </div>
         </div>
        </div>

        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="button" class="btn btn-default" onclick="RoleAdmin.back()">Kembali</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
