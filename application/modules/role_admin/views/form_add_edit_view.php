<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'Akses Admin' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">        

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">User Admin</label>
           <div class="col-md-9">
            <select class="form-control required" error="User Admin" id="user">
             <option value="">Pilih Admin</option>
             <?php if (!empty($list_admin)) { ?>
              <?php foreach ($list_admin as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($user)) { ?>
                <?php $selected = $user == $value['id'] ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['username'] . ' - ' . $value['nama_kompi'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>


        <h3 class="box-title">DATA MODULE  <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <!--/row-->
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table warning-bordered-table" id="table_module">
            <thead>
             <tr class="">
              <th class="font-12">Module</th>
              <th class="font-12">Parent</th>
              <th class="text-center font-12">
               <input type="checkbox" class="check check_header" id="flat-checkbox-1" data-checkbox="icheckbox_flat-red" onchange="RoleAdmin.checkAll(this)" <?php echo isset($detail_jadwal) ? !empty($detail_jadwal) ? 'checked' : '' : '' ?>>
              </th>
             </tr>
            </thead>
            <tbody>
             <?php if (isset($detail_module)) { ?>
              <?php if (!empty($detail_module)) { ?>
               <?php foreach ($detail_module as $value) { ?>
                <tr module="<?php echo $value['id'] ?>" id="<?php echo $value['id'] ?>">
                 <td class="font-12">
                  <?php echo $value['nama_module'] ?>
                 </td>
                 <td class="font-12">
                  <?php echo $value['parent'] ?>
                 </td>
                 <td class="text-center">
                  <div class="icheckbox_flat-red">
                   <input type="checkbox" class="check check_data" id="flat-checkbox-1" data-checkbox="icheckbox_flat-red" onchange="RoleAdmin.checkTaruna(this)" <?php echo $value['user'] != '' ? 'checked' : '' ?>>
                  </div>
                 </td>
                </tr>  
               <?php } ?>
              <?php } else { ?> 
               <tr class="baru">
                <td colspan="2" class="font-12">Tidak Ada Data</td>
               </tr>        
              <?php } ?>
             <?php } else { ?>
              <tr class="baru">
               <td colspan="2" class="font-12">Tidak Ada Data</td>
              </tr>        
             <?php } ?>             
            </tbody>
           </table>
          </div>
         </div>
        </div>

        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="submit" class="btn btn-success" onclick="RoleAdmin.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
             <button type="button" class="btn btn-default" onclick="RoleAdmin.back()">Batal</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
