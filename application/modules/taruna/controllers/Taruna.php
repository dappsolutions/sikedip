<?php

class Taruna extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'taruna';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/taruna.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'taruna';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Taruna";
  $data['title_content'] = 'Data Taruna';
  $content = $this->getDataTaruna();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataTaruna($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_taruna', $keyword),
       array('t.nama', $keyword),
   );
  }

  if ($keyword != "") {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*'),
               'like' => $like,
               'is_or_like' => true,
               'inside_brackets' => true,
               'where' => "t.deleted = 0"
   ));
  } else {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*'),
               'like' => $like,
               'is_or_like' => true,
               'where' => "t.deleted = 0"
   ));
  }

  return $total;
 }

 public function getDataTaruna($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.no_taruna', $keyword),
       array('t.nama', $keyword),
   );
  }

  if ($keyword != "") {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*'),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => "t.deleted = 0",
               'inside_brackets' => true,
               'orderby' => 't.no_taruna asc'
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' t',
               'field' => array('t.*'),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => "t.deleted = 0",
               'orderby' => 't.no_taruna asc'
   ));
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataTaruna($keyword)
  );
 }

 public function getDetailDataTaruna($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array(
                  't.*', 'jk.kelamin',
                  'p.propinsi as propinsi_taruna',
                  'k.nama_kota', 'ass.sekolah', 'kk.nama as nama_kompi',
                  'sp.seperasuhan as seperasuhan_taruna'
              ),
              'join' => array(
                  array('jenis_kelamin jk', 't.jenis_kelamin = jk.id'),
                  array('propinsi p', 't.propinsi = p.id', 'left'),
                  array('kota k', 't.kota = k.id', 'left'),
                  array('asal_sekolah ass', 't.asal_sekolah = ass.id', 'left'),
                  array('kompi kk', 't.kompi  = kk.id', 'left'),
                  array('seperasuhan sp', 't.seperasuhan = sp.id', 'left'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListJenisKelamin() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_kelamin',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListHubunganWali() {
  $data = Modules::run('database/get', array(
              'table' => 'hubungan_wali',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListAsrama() {
  $data = Modules::run('database/get', array(
              'table' => 'asrama',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListKamar() {
  $data = Modules::run('database/get', array(
              'table' => 'kamar',
              'where' => 'deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Taruna";
  $data['title_content'] = 'Tambah Taruna';
  $data['list_kelamin'] = $this->getListJenisKelamin();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_wali'] = $this->getListHubunganWali();
  $data['list_semester'] = $this->getListSemester();
  $data['list_penghasilan'] = $this->getListPenghasilan();
  $data['list_propinsi'] = $this->getListPropinsi();
  $data['list_kota'] = $this->getListKota();
  $data['list_asal'] = $this->getListAsalSekolah();
  $data['list_kompi'] = $this->getListKompi();
  $data['list_seperasuhan'] = $this->getListSeperasuhan();
  $data['list_asrama'] = $this->getListAsrama();
  $data['list_kamar'] = $this->getListKamar();

  echo Modules::run('template', $data);
 }

 public function getListPenghasilan() {
  $data = Modules::run('database/get', array(
              'table' => 'penghasilan',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPropinsi() {
  $data = Modules::run('database/get', array(
              'table' => 'propinsi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKota() {
  $data = Modules::run('database/get', array(
              'table' => 'kota',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListAsalSekolah() {
  $data = Modules::run('database/get', array(
              'table' => 'asal_sekolah',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKompi() {
  $data = Modules::run('database/get', array(
              'table' => 'kompi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSeperasuhan() {
  $data = Modules::run('database/get', array(
              'table' => 'seperasuhan',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function ubah($id) {
  $data = $this->getDetailDataTaruna($id);
  $data['foto'] = str_replace(' ', '_', $data['foto']);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Taruna";
  $data['title_content'] = 'Ubah Taruna';
  $data['list_kelamin'] = $this->getListJenisKelamin();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_wali'] = $this->getListHubunganWali();
  $data['list_semester'] = $this->getListSemester();
  $data['list_penghasilan'] = $this->getListPenghasilan();
  $data['list_propinsi'] = $this->getListPropinsi();
  $data['list_kota'] = $this->getListKota();
  $data['list_asal'] = $this->getListAsalSekolah();
  $data['list_kompi'] = $this->getListKompi();
  $data['list_seperasuhan'] = $this->getListSeperasuhan();
  $data['list_asrama'] = $this->getListAsrama();
  $data['list_kamar'] = $this->getListKamar();


  $data['detail_wali'] = $this->getDataWaliMurid($id);
  $data['detail_agama'] = $this->getDataAgama($id);
  $data['detail_medic'] = $this->getDataKesehatan($id);
  $data['detail_akademik'] = $this->getDataAkademik($id);
  $data['detail_asrama'] = $this->getDataAsrama($id);
  echo Modules::run('template', $data);
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_organisasi mho',
              'field' => array('mho.*', 'o.nama_organisasi'),
              'join' => array(
                  array('organisasi o', 'mho.organisasi = o.id')
              ),
              'where' => "mho.deleted = 0 and mho.masjid = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDetailPengurusTaruna($masjid) {
  $data = Modules::run('database/get', array(
              'table' => 'masjid_has_pengurus mhp',
              'field' => array('mhp.*', 'j.jabatan as nama_jabatan'),
              'join' => array(
                  array('jabatan j', 'mhp.jabatan = j.id')
              ),
              'where' => array('mhp.deleted' => 0, 'mhp.masjid' => $masjid)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataWaliMurid($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_wali_murid thw',
              'field' => array('thw.*', 'hw.hubungan', 'ph.penghasilan as penghasilan_wali'),
              'join' => array(
                  array('hubungan_wali hw', 'hw.id = thw.hubungan_wali', 'left'),
                  array('penghasilan ph', 'thw.penghasilan = ph.id', 'left'),
              ),
              'where' => "thw.deleted = 0 and thw.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAgama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_agama ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataKesehatan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_kesehatan ta',
              'field' => array('ta.*'),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAkademik($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik ta',
              'field' => array(
                  'ta.*', 's.semester as semester_taruna',
                  'p.prodi as program_studi'
              ),
              'join' => array(
                  array('prodi p', 'ta.prodi = p.id', 'left'),
                  array('semester s', 'ta.semester = s.id', 'left'),
              ),
              'where' => "ta.deleted = 0 and ta.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['status'] = $value['status'] == 1 ? 'Aktif' : 'Tidak Aktif';
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataAsrama($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_asrama tha',
              'field' => array('tha.*', 'a.nama_asrama', 'k.kamar as kamar_asrama'),
              'join' => array(
                  array('asrama a', 'tha.asrama = a.id'),
                  array('kamar k', 'tha.kamar = k.id'),
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '" . $taruna . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataTaruna($id);
  $data['foto'] = str_replace(' ', '_', $data['foto']);
  $data['foto'] = str_replace('.png_', '_png_', $data['foto']);
  //   echo '<pre>';
  //   print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Taruna";
  $data['title_content'] = "Detail Taruna";
  $data['detail_wali'] = $this->getDataWaliMurid($id);
  $data['detail_agama'] = $this->getDataAgama($id);
  $data['detail_medic'] = $this->getDataKesehatan($id);
  $data['detail_akademik'] = $this->getDataAkademik($id);
  $data['detail_asrama'] = $this->getDataAsrama($id);
  $data['list_wali'] = $this->getListHubunganWali();
  $data['list_semester'] = $this->getListSemester();
  echo Modules::run('template', $data);
 }

 function generateRandomString($length = 6) {
  return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
 }

 public function getPostDataUser($value) {
  $data['username'] = $value->no_taruna;
  $data['password'] = $value->tanggal_lahir;
  $data['priveledge'] = 3;
  return $data;
 }

 public function getPostDataUserWali($value) {
  $data['password'] = $value->tanggal_lahir;
  $data['priveledge'] = 2;
  return $data;
 }

 public function getPostDataTaruna($value, $user = '') {
  $data['no_taruna'] = $value->no_taruna;
  $data['no_ktp'] = $value->no_ktp;
  $data['jenis_kelamin'] = $value->jenis_kelamin;
  if ($value->tanggal_lahir != '') {
   $data['tanggal_lahir'] = $value->tanggal_lahir;
  }
  $data['tempat_lahir'] = $value->tempat_lahir;
  $data['nama'] = $value->nama;
  $data['sim'] = $value->no_sim;
  $data['gol_darah'] = $value->golongan_darah;
  $data['no_hp'] = $value->no_hp;
  $data['email'] = $value->email;
  if ($user != '') {
   $data['user'] = $user;
  }
  $data['alamat'] = $value->alamat;
  $data['npwp'] = $value->npwp;
  $data['propinsi'] = $value->propinsi;
  $data['kota'] = $value->kota;
  $data['asal_sekolah'] = $value->asal;
  $data['kompi'] = $value->kompi;
  $data['seperasuhan'] = $value->seperasuhan;
  return $data;
 }

 public function getDataPostAgama($value, $taruna) {
  $data['tanggal'] = $value->tanggal_agama;
  $data['agama'] = $value->agama;
  $data['taruna'] = $taruna;
  return $data;
 }

 public function getDataPostMedic($value, $taruna) {
  $data['tanggal'] = $value->tanggal_kesehatan;
  $data['berat_badan'] = $value->berat_badan;
  $data['tinggi_badan'] = $value->tinggi_badan;
  $data['taruna'] = $taruna;
  return $data;
 }

 public function getDataPostAkedmik($value, $taruna) {
  $data['prodi'] = $value->prodi;
  $data['semester'] = $value->semester;
  $data['tanggal'] = $value->tanggal_masuk;
  $data['angkatan'] = $value->angkatan;
  $data['taruna'] = $taruna;
  return $data;
 }

 public function getDataPostWali($value, $taruna, $user) {
  $data['no_ktp'] = $value->no_ktp;
  $data['nama'] = $value->nama;
  $data['hubungan_wali'] = $value->hubungan_wali;
  $data['no_telp'] = $value->no_telp;
  $data['alamat'] = $value->alamat_wali;
  $data['penghasilan'] = $value->penghasilan;
  $data['pekerjaan'] = $value->pekerjaan;
  $data['tanggal_lahir'] = $value->tanggal_lahir;
  $data['taruna'] = $taruna;
  $data['user'] = $user;
  return $data;
 }

 public function getDataPostAsrama($value, $taruna) {
  $data['taruna'] = $taruna;
  $data['tanggal'] = $value->tanggal;
  $data['asrama'] = $value->asrama;
  $data['kamar'] = $value->kamar;
  return $data;
 }

 public function getUserTaruna($no_taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'user',
              'where' => "deleted = 0 and username = '" . $no_taruna . "'"
  ));

  //  echo "<pre>";
  //  echo $this->db->last_query();
  //  die;
  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  }


  return $id;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;

  //  echo '<pre>';
  //  print_r($data);die;
  $message = "";
  $this->db->trans_begin();
  try {
   $user = $this->getUserTaruna($data->taruna->no_taruna);
   $is_uploaded = true;


   if ($id == '') {
    $post_user = $this->getPostDataUser($data->taruna);
    $user = Modules::run('database/_insert', 'user', $post_user);
    $post_taruna = $this->getPostDataTaruna($data->taruna, $user);
    if (!empty($_FILES)) {
     $data_file = $this->uploadData("file");
     $is_uploaded = $data_file['is_valid'];
     if ($is_uploaded) {
      $post_taruna['foto'] = $_FILES['file']['name'];
     } else {
      $is_valid = false;
      $message = $data_file['response'];
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post_taruna);
     //agama
     foreach ($data->agama as $value) {
      $post_agama = $this->getDataPostAgama($value, $id);
      Modules::run('database/_insert', 'taruna_has_agama', $post_agama);
     }

     //medic
     foreach ($data->medic as $value) {
      $post_medic = $this->getDataPostMedic($value, $id);
      Modules::run('database/_insert', 'taruna_has_kesehatan', $post_medic);
     }

     //akademik
     foreach ($data->akademik as $value) {
      $post_academic = $this->getDataPostAkedmik($value, $id);
      Modules::run('database/_insert', 'taruna_has_akademik', $post_academic);
     }

     //wali murid
     foreach ($data->hub_wali as $value) {
      $post_user_wali = $this->getPostDataUserWali($value);
      $post_user_wali['username'] = 'W' . $data->taruna->no_taruna;
      $user_wali = Modules::run('database/_insert', 'user', $post_user_wali);
      $post_wali = $this->getDataPostWali($value, $id, $user_wali);
      Modules::run('database/_insert', 'taruna_has_wali_murid', $post_wali);
     }

     //Asrama
     foreach ($data->asrama as $value) {
      $post_asrama = $this->getDataPostAsrama($value, $id);
      Modules::run('database/_insert', 'taruna_has_asrama', $post_asrama);
     }
    }
   } else {
    //update
    if (!empty($_FILES)) {
     $data_file = $this->uploadData("file");
     $is_uploaded = $data_file['is_valid'];
     if ($is_uploaded) {
      $post_taruna['foto'] = $_FILES['file']['name'];
     } else {
      $is_valid = false;
      $message = $data_file['response'];
     }
    }

    if ($is_uploaded) {
     $post_taruna = $this->getPostDataTaruna($data->taruna);

     Modules::run('database/_update', $this->getTableName(), $post_taruna, array('id' => $id));

     //agama insert
     if (!empty($data->agama)) {
      foreach ($data->agama as $value) {
       $post_agama = $this->getDataPostAgama($value, $id);
       Modules::run('database/_insert', 'taruna_has_agama', $post_agama);
      }
     }

     //update and delete agama
     if (!empty($data->agama_edit)) {
      foreach ($data->agama_edit as $value) {
       $post_agama = $this->getDataPostAgama($value, $id);
       if ($value->is_edit != 1) {
        $post_agama['deleted'] = 1;
       }
       Modules::run('database/_update', 'taruna_has_agama', $post_agama, array('id' => $value->id));
      }
     }

     //medic
     if (!empty($data->medic)) {
      foreach ($data->medic as $value) {
       $post_medic = $this->getDataPostMedic($value, $id);
       Modules::run('database/_insert', 'taruna_has_kesehatan', $post_medic);
      }
     }

     //update and delete medic
     if (!empty($data->medic_edit)) {
      foreach ($data->medic_edit as $value) {
       $post_medic = $this->getDataPostMedic($value, $id);
       if ($value->is_edit != 1) {
        $post_medic['deleted'] = 1;
       }
       Modules::run('database/_update', 'taruna_has_kesehatan', $post_medic, array('id' => $value->id));
      }
     }

     //akademik
     if (!empty($data->akademik)) {
      if (!empty($data->akademik)) {
       foreach ($data->akademik as $value) {
        $post_academic = $this->getDataPostAkedmik($value, $id);
        Modules::run('database/_insert', 'taruna_has_akademik', $post_academic);
       }
      }
     }

     //update and delete akademik
     if (!empty($data->akademik_edit)) {
      foreach ($data->akademik_edit as $value) {
       $post_academic = $this->getDataPostAkedmik($value, $id);
       if ($value->is_edit != 1) {
        $post_academic['deleted'] = 1;
       }
       Modules::run('database/_update', 'taruna_has_akademik', $post_academic, array('id' => $value->id));
      }
     }

     //akademik
     if (!empty($data->akademik)) {
      if (!empty($data->akademik)) {
       foreach ($data->akademik as $value) {
        $post_academic = $this->getDataPostAkedmik($value, $id);
        Modules::run('database/_insert', 'taruna_has_akademik', $post_academic);
       }
      }
     }

     //Asrama
     if (!empty($data->asrama)) {
      foreach ($data->asrama as $value) {
       $post_asrama = $this->getDataPostAsrama($value, $id);
       Modules::run('database/_insert', 'taruna_has_asrama', $post_asrama);
      }
     }

     //     echo '<pre>';
     //     print_r($data);die;
     //update and delete asrama
     if (!empty($data->asrama_edit)) {
      foreach ($data->asrama_edit as $value) {
       $post_asrama = $this->getDataPostAsrama($value, $id);
       if ($value->is_edit != 1) {
        $post_asrama['deleted'] = 1;
       }
       Modules::run('database/_update', 'taruna_has_asrama', $post_asrama, array('id' => $value->id));
      }
     }

     //wali murid
     if (!empty($data->hub_wali)) {
      foreach ($data->hub_wali as $value) {
       $post_user_wali = $this->getPostDataUserWali($value);
       $post_user_wali['username'] = 'W' . $data->taruna->no_taruna;
       $user_wali = Modules::run('database/_insert', 'user', $post_user_wali);
       $post_wali = $this->getDataPostWali($value, $id, $user_wali);
       Modules::run('database/_insert', 'taruna_has_wali_murid', $post_wali);
      }
     }

     //update and delete wali murid
     if (!empty($data->hub_wali_edit)) {
      foreach ($data->hub_wali_edit as $value) {
       $post_wali['no_ktp'] = $value->no_ktp;
       $post_wali['nama'] = $value->nama;
       if ($value->tanggal_lahir != '') {
        $post_wali['tanggal_lahir'] = $value->tanggal_lahir;
       }
       $post_wali['hubungan_wali'] = $value->hubungan_wali;
       $post_wali['no_telp'] = $value->no_telp;
       $post_wali['alamat'] = $value->alamat_wali;
       $post_wali['penghasilan'] = $value->penghasilan;
       $post_wali['pekerjaan'] = $value->pekerjaan;
       if ($value->is_edit != 1) {
        $post_wali['deleted'] = 1;
       }
       Modules::run('database/_update', 'taruna_has_wali_murid', $post_wali, array('id' => $value->id));
      }
     }

     //     echo '<pre>';
     //     print_r($data);die;          
    }
   }
   if ($is_uploaded) {
    $this->db->trans_commit();
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Taruna";
  $data['title_content'] = 'Data Taruna';
  $content = $this->getDataTaruna($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function cetak($id = '') {
  $mpdf = Modules::run('mpdf/getInitPdf');
  $data = $this->getDetailDataTaruna($id);
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pembelian Customer";
  $data['title_content'] = 'Detail Pembelian Customer';

  $data['detail_wali'] = $this->getDataWaliMurid($id);
  $data['detail_agama'] = $this->getDataAgama($id);
  $data['detail_medic'] = $this->getDataKesehatan($id);
  $data['detail_akademik'] = $this->getDataAkademik($id);
  $data['detail_asrama'] = $this->getDataAsrama($id);
  //  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak_taruna', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('TARUNA - ' . $data['no_taruna'] . '.pdf', 'I');
 }

}
