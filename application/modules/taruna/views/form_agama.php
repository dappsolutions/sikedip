<h3 class="box-title">AGAMA <i class="fa fa-arrow-down"></i></h3>
<hr class="m-t-0 m-b-40">
<!--/row-->
<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table color-bordered-table warning-bordered-table" id="table_agama">
    <thead>
     <tr class="">
      <th class="font-12">Tanggal</th>
      <th class="font-12">Agama</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($detail_agama)) { ?>
      <?php if (!empty($detail_agama)) { ?>
       <?php foreach ($detail_agama as $value) { ?>
        <tr id="<?php echo $value['id'] ?>">
         <td class='font-12'>
          <input type="text" value="<?php echo $value['tanggal'] ?>" id="tanggal_agama_1" class="form-control required" 
                 error='Tanggal' placeholder="Tanggal"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['agama'] ?>" id="agama" class="form-control required" error='Agama' placeholder="Agama"/>
         </td>
         <td class="text-center">
          <i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetailData(this)"></i>
         </td>
        </tr>  
       <?php } ?>
      <?php } ?>
      <tr class="baru">
       <td class='font-12'>
        <input type="text" value="" id="tanggal_agama_1" class="form-control" error='Tanggal' placeholder="Tanggal"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="agama" class="form-control" error='Agama' placeholder="Agama"/>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailAgama(this)"></i>
       </td>
      </tr>    
     <?php } else { ?>
      <tr class="baru">
       <td class='font-12'>
        <input type="text" value="" id="tanggal_agama_1" class="form-control required" error='Tanggal' placeholder="Tanggal"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="agama" class="form-control required" error='Agama' placeholder="Agama"/>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailAgama(this)"></i>
       </td>
      </tr>        
     <?php } ?>             
    </tbody>
   </table>
  </div>
 </div>
</div>