<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12 text-right">
   <button class="btn btn-danger" onclick="Taruna.cetakDetail('<?php echo $id ?>')"><span class="mdi mdi-file-pdf mdi-18px"></span>Cetak</button>
  </div>
 </div>
 <br/>
 
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Identitas Taruna <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No. Taruna
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_taruna ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Nama
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tempat Lahir
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tempat_lahir ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tanggal Lahir
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tanggal_lahir ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <!--/row-->
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Jenis Kelamin
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $kelamin ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No. KTP
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_ktp ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Golongan Darah
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $gol_darah ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No. Sim
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $sim ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            No. HP
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_hp ?>
           </div>
          </div>         
         </div>

         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Email
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $email ?>
           </div>
          </div>
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Npwp
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $npwp ?>
           </div>
          </div>         
         </div>
         
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Alamat
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $alamat ?>
           </div>
          </div>
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Propinsi
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $propinsi_taruna ?>
           </div>
          </div>         
         </div>
         
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Kota
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_kota ?>
           </div>
          </div>
         </div>
        </div>
        <br/>
        
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Asal Sekolah
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $sekolah ?>
           </div>
          </div>         
         </div>
         
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Kompi
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $nama_kompi ?>
           </div>
          </div>
         </div>
        </div>
        <br/>

        <div class='row manual_detail <?php echo!isset($foto) ? 'display-none' : '' ?>'>
         <div class='col-md-3'>
          <?php
          $img = base_url() . 'assets/images/no_available.png';
          if (isset($foto)) {
           if ($foto == '') {
            $img = base_url() . 'assets/images/no_available.png';
           } else {
            $img = base_url() . 'files/berkas/taruna/' . $foto;
           }
          }
          ?>
          <img src="<?php echo $img ?>" height="180" width="180" class="hover" />
         </div>
         <div class='col-md-3'>
          <a href="#" onclick="Taruna.showFoto(this, event)" class="badge badge-primary text-white"><?php echo $foto ?></a>        
         </div>
         <div class="col-md-3">
          Seperasuhan
         </div>
         <div class="col-md-3 text-primary text-left">
          <?php echo $seperasuhan_taruna ?>
         </div>
        </div>
        <br/>

        <h3 class="box-title">AGAMA <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <!--/row-->
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table table-bordered" id="table_hubungan">
            <thead>
             <tr class="">
              <th class="font-12">Tanggal</th>
              <th class="font-12">Agama</th>
              <th class="font-12">Status</th>
             </tr>
            </thead>
            <tbody>            
             <?php foreach ($detail_agama as $value) { ?>
              <tr>
               <td class="font-12 text-primary"><?php echo $value['tanggal'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['agama'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['status'] ?></td>
              </tr>
             <?php } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
        <br/>

        <h3 class="box-title">KESEHATAN <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <!--/row-->
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table table-bordered" id="table_hubungan">
            <thead>
             <tr class="">
              <th class="font-12">Tanggal</th>
              <th class="font-12">Tinggi Badan</th>
              <th class="font-12">Berat Badan</th>
              <th class="font-12">Status</th>
             </tr>
            </thead>
            <tbody>            
             <?php foreach ($detail_medic as $value) { ?>
              <tr>
               <td class="font-12 text-primary"><?php echo $value['tanggal'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['tinggi_badan'] . ' cm' ?></td>
               <td class="font-12 text-primary"><?php echo $value['berat_badan'] . ' kg' ?></td>
               <td class="font-12 text-primary"><?php echo $value['status'] ?></td>
              </tr>
             <?php } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
        <br/>

        <h3 class="box-title">HUBUNGAN WALI <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table table-bordered" id="table_hubungan">
            <thead>
             <tr class="">
              <th class="font-12">No. KTP</th>
              <th class="font-12">Nama</th>
              <th class="font-12">Tanggal LAhir</th>
              <th class="font-12">Hubungan Wali</th>
              <th class="font-12">No. Telp</th>
              <th class="font-12">Alamat</th>
              <th class="font-12">Pekerjaan</th>
              <th class="font-12">Penghasilan</th>
             </tr>
            </thead>
            <tbody>            
             <?php foreach ($detail_wali as $value) { ?>
              <tr>
               <td class="font-12 text-primary"><?php echo $value['no_ktp'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['nama'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['tanggal_lahir'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['hubungan'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['no_telp'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['alamat'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['pekerjaan'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['penghasilan_wali'] ?></td>
              </tr>
             <?php } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
        <br/>

        <h3 class="box-title">TARUNA AKADEMIK <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table table-bordered" id="table_hubungan">
            <thead>
             <tr class="">
              <th class="font-12">Program Studi</th>
              <th class="font-12">Semester</th>
              <th class="font-12">Tanggal</th>
              <th class="font-12">Angkatan</th>
              <th class="font-12">Status</th>
             </tr>
            </thead>
            <tbody>            
             <?php foreach ($detail_akademik as $value) { ?>
              <tr>
               <td class="font-12 text-primary"><?php echo $value['program_studi'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['semester_taruna'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['tanggal'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['angkatan'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['status'] ?></td>
              </tr>
             <?php } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
        <br/>
        
        <h3 class="box-title">TARUNA ASRAMA <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table table-bordered" id="table_hubungan">
            <thead>
             <tr class="">
              <th class="font-12">Tanggal</th>
              <th class="font-12">Asrama</th>
              <th class="font-12">Kamar</th>
             </tr>
            </thead>
            <tbody>            
             <?php foreach ($detail_asrama as $value) { ?>
              <tr>
               <td class="font-12 text-primary"><?php echo $value['tanggal'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['nama_asrama'] ?></td>
               <td class="font-12 text-primary"><?php echo $value['kamar_asrama'] ?></td>
              </tr>
             <?php } ?>
            </tbody>
           </table>
          </div>
         </div>
        </div>
        <br/>
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="button" class="btn btn-default" onclick="Taruna.back()">Kembali</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
