<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
  <meta charset="utf-8">
  <title><?php echo 'TARUNA'; ?></title>
  <link rel="icon" type="image/png" href="<?php echo base_url() . 'assets/images/logo_taruna.jpb' ?>" />

  <style>
   body {
    font-family: "Helvetica", sans-serif;
    font-size: 11px;
   }
   table {
    width: 100%;
   }
   .text-center {
    text-align: center;
   }
   .text-right {
    text-align: right;
   }
   .font-bold {
    font-weight: bold;
   }
   .mb-32px {
    margin-bottom: 32px;
   }
   .mr-8px {
    margin-right: 8px;
   }
   .ml-8px {
    margin-left: 8px;
   }
   .table-logo {
    width: 100%;
   }
   table.table-logo > tbody > tr > td {
    padding: 16px;
   }
   table.table-logo td {
    vertical-align: top;
   }
   .table-item {
    width: 100%;
    margin-top: 16px;
    border-collapse: collapse;
    font-size: 8px;
   }
   table.table-item th,  table.table-item td {
    border: 1px solid #333;
    padding: 4px 8px;
    border-collapse: collapse;
    vertical-align: top;
   }
   .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
   }
   .media-body {
    -ms-flex: 1;
    flex: 1;
   }

   .none-border{
    border:none;
   }
  </style>
 </head>
 <body>
  <table class="table-logo">
   <tbody>
    <tr>
     <td style="width: 50%">
      <table style="margin-bottom: 8px">
       <tr>
        <td width="40"><img src="<?php echo base_url() ?>assets/images/logo_taruna.jpg" alt="" width="40"></td>         
       </tr>
<!--       <tr>
        <td>
         <div></div>
         <div class="font-bold" style="">   POLTEKTRANS SDP PALEMBANG</div>
        </td>
       </tr>-->
      </table>
     </td>
    </tr>
   </tbody>
  </table>
  <table class="table-item">   
   <tbody>
    <tr>
     <td colspan="6" style="border:none;"><b><u>TARUNA</u></b></td>
    </tr>
    <tr>
     <td style="border:none;">No. Taruna</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $no_taruna ?></td>
     <td style="border:none;">Nama</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $nama ?></td>
    </tr>
    <tr>
     <td style="border:none;">Tempat Lahir</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $tempat_lahir ?></td>
     <td style="border:none;">Tanggal Lahir</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $tanggal_lahir ?></td>
    </tr>
    <tr>
     <td style="border:none;">Jenis Kelamin</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $kelamin ?></td>
     <td style="border:none;">No. KTP</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $no_ktp ?></td>
    </tr>
    <tr>
     <td style="border:none;">Golongan Darah</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $gol_darah ?></td>
     <td style="border:none;">No. SIM</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $sim ?></td>
    </tr>
    <tr>
     <td style="border:none;">No. HP</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $no_hp ?></td>
     <td style="border:none;">Email</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $email ?></td>
    </tr>
    <tr>
     <td style="border:none;">NPWP</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $npwp ?></td>
     <td style="border:none;">Alamat</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $alamat ?></td>
    </tr>
    <tr>
     <td style="border:none;">Propinsi</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $propinsi_taruna ?></td>
     <td style="border:none;">Kota</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $nama_kota ?></td>
    </tr>
    <tr>
     <td style="border:none;">Asal Sekolah</td>     
     <td style="border:none;">:</td>     
     <td style="border:none;"><?php echo $sekolah ?></td>
     <td style="border:none;">Kompi</td>
     <td style="border:none;">:</td>
     <td style="border:none;"><?php echo $nama_kompi ?></td>
    </tr>
   </tbody>
  </table>
  <br/>

  <table class="table-item" style="width: 100%;">   
   <tbody>
    <tr>
     <td colspan="6" style="border:none;"><b>AGAMA</b></td>
    </tr>
    <?php if (!empty($detail_agama)) { ?>
     <?php foreach ($detail_agama as $value) { ?>
      <tr>
       <td style="border:none;">Tanggal</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['tanggal'] ?></td>                       
       <td style="border:none;">Agama</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['agama'] ?></td>                
       <td style="border:none;">Status</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['status'] ?></td>                
      </tr>
     <?php } ?>
    <?php } ?>
   </tbody>
  </table>
  <br/>

  <table class="table-item" width="100">   
   <tbody>
    <tr>
     <td colspan="6" style="border:none;"><b><u>KESEHATAN</u></b></td>
    </tr>
    <?php if (!empty($detail_medic)) { ?>
     <?php foreach ($detail_medic as $value) { ?>
      <tr>
       <td style="border:none;">Tanggal</td>                
       <td style="border:none;">:</td>                
       <td style="border:none;"><?php echo $value['tanggal'] ?></td>                            
       <td style="border:none;">Tinggi Badan</td>     
       <td style="border:none;">:</td>                
       <td style="border:none;"><?php echo $value['tinggi_badan'] . ' cm' ?></td>
       <td style="border:none;">Berat Badan</td>
       <td style="border:none;">:</td>                
       <td style="border:none;"><?php echo $value['berat_badan'] . ' kg' ?></td>                
       <td style="border:none;">Status</td>
       <td style="border:none;">:</td>                
       <td style="border:none;"><?php echo $value['status'] ?></td>
      </tr>
     <?php } ?>
    <?php } ?>
   </tbody>
  </table>
  <br/>

  <table class="table-item" width="100">       
   <tbody>
    <tr>
     <td colspan="8" style="border:none;"><b>HUBUNGAN WALI</b></td>
    </tr>
    <?php if (!empty($detail_wali)) { ?>
     <?php foreach ($detail_wali as $value) { ?>
      <tr>
       <td style="border:none;">No. KTP</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['no_ktp'] ?></td>
       <td style="border:none;">Nama</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['nama'] ?></td>
       <td style="border:none;">Tanggal Lahir</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['tanggal_lahir'] ?></td>                       
      </tr>
      <tr>
       <td style="border:none;">Hubungan</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['hubungan'] ?></td>
       <td style="border:none;">No. Telp</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['no_telp'] ?></td>
       <td style="border:none;">Alamat</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['alamat'] ?></td>
      </tr>
      <tr>
       <td style="border:none;">Pekerjaan</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['pekerjaan'] ?></td>
       <td style="border:none;">Penghasilan</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['penghasilan_wali'] ?></td>
      </tr>
     <?php } ?>
    <?php } ?>
   </tbody>
  </table>
  <br/>

  <table class="table-item" width="100">       
   <tbody>
    <tr>
     <td colspan="8" style="border:none;"><b>AKADEMIK</b></td>
    </tr>
    <?php if (!empty($detail_akademik)) { ?>
     <?php foreach ($detail_akademik as $value) { ?>
      <tr>
       <td style="border:none;">Program Studi</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['program_studi'] ?></td>                
       <td style="border:none;">Semester</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['semester_taruna'] ?></td>
       <td style="border:none;">Tanggal</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['tanggal'] ?></td>                       
      </tr>
      <tr>
       <td style="border:none;">Angkatan</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['angkatan'] ?></td>
       <td style="border:none;">Status</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['status'] ?></td>
      </tr>
     <?php } ?>
    <?php } ?>
   </tbody>
  </table>
  <br/>

  <table class="table-item" width="100">       
   <tbody>
    <tr>
     <td colspan="8" style="border:none;"><b>TARUNA ASRAMA</b></td>
    </tr>
    <?php if (!empty($detail_asrama)) { ?>
     <?php foreach ($detail_asrama as $value) { ?>
      <tr>
       <td style="border:none;">Tanggal</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['tanggal'] ?></td>                
       <td style="border:none;">Asrama</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['nama_asrama'] ?></td>
       <td style="border:none;">Kamar</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $value['kamar_asrama'] ?></td>                
      </tr>
     <?php } ?>
    <?php } ?>
   </tbody>
  </table>
  <br/>
  <table style="width: 100%; margin-top: 32px">
   <tbody>
    <tr>
     <td class="text-center">Poltektrans Sdp Palembang,</td>
     <td class="text-center">&nbsp;</td>
    </tr>
    <tr>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
    </tr>
    <tr>
     <td class="text-center">(------------------------------)</td>
    </tr>
   </tbody>
  </table>
 </body>
</html>
