<h3 class="box-title">KESEHATAN <i class="fa fa-arrow-down"></i></h3>
<hr class="m-t-0 m-b-40">
<!--/row-->
<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table color-bordered-table warning-bordered-table" id="table_medic">
    <thead>
     <tr class="">
      <th class="font-12">Tanggal</th>
      <th class="font-12">Berat Badan</th>
      <th class="font-12">Tinggi Badan</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($detail_medic)) { ?>
      <?php if (!empty($detail_medic)) { ?>
       <?php foreach ($detail_medic as $value) { ?>    
        <tr id="<?php echo $value['id'] ?>">
         <td class='font-12'>
          <input type="text" value="<?php echo $value['tanggal'] ?>" id="tanggal_kesehatan_1" class="form-control required" 
                 error='Tanggal' placeholder="Tanggal"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['berat_badan'] ?>" id="berat_badan" 
                 class="form-control required" error='Berat Badan' placeholder="Berat Badan"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['tinggi_badan'] ?>" id="tinggi_badan" class="form-control required" 
                 error='Tinggi Badan' placeholder="Tinggi Badan"/>
         </td>
         <td class="text-center">
          <i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetailData(this)"></i>
         </td>
        </tr>  
       <?php } ?>
      <?php } ?> 
      <tr class="baru">
       <td class='font-12'>
        <input type="text" value="" id="tanggal_kesehatan_1" class="form-control" error='Tanggal' placeholder="Tanggal"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="berat_badan" class="form-control" error='Berat Badan' placeholder="Berat Badan"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="tinggi_badan" class="form-control" error='Tinggi Badan' placeholder="Tinggi Badan"/>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailKesehatan(this)"></i>
       </td>
      </tr>        
     <?php } else { ?>             
      <tr class="baru">
       <td class='font-12'>
        <input type="text" value="" id="tanggal_kesehatan_1" class="form-control required" error='Tanggal' placeholder="Tanggal"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="berat_badan" class="form-control required" error='Berat Badan' placeholder="Berat Badan"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="tinggi_badan" class="form-control required" error='Tinggi Badan' placeholder="Tinggi Badan"/>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailKesehatan(this)"></i>
       </td>
      </tr>        
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>