<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <?php echo $this->load->view('form_taruna') ?>
        <?php echo $this->load->view('form_agama') ?>
        <?php echo $this->load->view('form_kesehatan') ?>
        <?php echo $this->load->view('form_wali') ?>
        <?php echo $this->load->view('form_akademik') ?>
        <?php echo $this->load->view('form_asrama') ?>
                
       </div>


       <div class="form-actions">
        <div class="row">
         <div class="col-md-12">
          <div class="row">
           <div class="col-md-offset-3 col-md-9 text-right">
            <button type="submit" class="btn btn-success" onclick="Taruna.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
            <button type="button" class="btn btn-default" onclick="Taruna.back()">Batal</button>
           </div>
          </div>
         </div>
         <div class="col-md-6"> </div>
        </div>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
