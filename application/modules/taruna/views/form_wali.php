<h3 class="box-title">HUBUNGAN WALI <i class="fa fa-arrow-down"></i></h3>
<hr class="m-t-0 m-b-40">
<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table color-bordered-table warning-bordered-table" id="table_hubungan">
    <thead>
     <tr class="">
      <th class="font-12">NIK</th>
      <th class="font-12">Nama</th>
      <th class="font-12">Tanggal Lahir</th>
      <th class="font-12">Hubungan Wali</th>
      <th class="font-12">No HP</th>
      <th class="font-12">Alamat</th>
      <th class="font-12">Pekerjaan</th>
      <th class="font-12">Penghasilan</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($detail_wali)) { ?>
      <?php if (!empty($detail_wali)) { ?>
       <?php foreach ($detail_wali as $value) { ?>
        <tr id="<?php echo $value['id'] ?>">
         <td class='font-12'>
          <input type="text" value="<?php echo $value['no_ktp'] ?>" id="no_ktp_wali" class="form-control" error='NIK' 
                 placeholder="NIK"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['nama'] ?>" id="nama_wali" class="form-control" error='Nama' 
                 placeholder="Nama"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['tanggal_lahir'] == '0000-00-00' ? '' : $value['tanggal_lahir'] ?>" id="tanggal_lahir_wali_1" class="form-control" error='Tanggal Lahir' 
                 placeholder="Tanggal Lahir"/>
         </td>                 
         <td class='font-12'>
          <select id='hubungan_wali' class="form-control" error='Hubungan Wali'>
           <?php if (!empty($list_wali)) { ?>
            <?php foreach ($list_wali as $vw) { ?>
             <?php $selected = $value['hubungan_wali'] == $vw['id'] ? 'selected' : '' ?>                     
             <option <?php echo $selected ?> value="<?php echo $vw['id'] ?>"><?php echo $vw['hubungan'] ?></option>
            <?php } ?>
           <?php } else { ?>
            <option value="">Tidak Ada Data</option>  
           <?php } ?>
          </select>
         </td>                 
         <td class='font-12'>
          <input type="text" value="<?php echo $value['no_telp'] ?>" id="no_telp" class="form-control" error='No. Telp' 
                 placeholder="No. Telp"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['alamat'] ?>" id="alamat_wali" class="form-control" error='Alamat' 
                 placeholder="Alamat"/>
         </td>
         <td class='font-12'>
          <input type="text" value="<?php echo $value['pekerjaan'] ?>" id="pekerjaan" class="form-control" error='Pekerjaan' 
                 placeholder="Pekerjaan"/>
         </td>
         <td class='font-12'>
          <select id='penghasilan' class="form-control" error='Penghasilan'>
           <?php if (!empty($list_penghasilan)) { ?>
            <?php foreach ($list_penghasilan as $vp) { ?>
             <?php $selected = $value['pegnhasilan'] == $vp['id'] ? 'selected' : '' ?>                     
             <option <?php echo $selected ?> value="<?php echo $vp['id'] ?>"><?php echo $vp['penghasilan'] ?></option>
            <?php } ?>
           <?php } else { ?>
            <option value="">Tidak Ada Data</option>  
           <?php } ?>
          </select>
         </td>
         <td class="text-center">
          <i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetailData(this)"></i>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
      <tr class="baru">
       <td class='font-12'>
        <input type="text" value="" id="no_ktp_wali" class="form-control" error='No. KTP' placeholder="No. KTP"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="nama_wali" class="form-control" error='Nama' placeholder="Nama"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="tanggal_lahir_wali_1" class="form-control" error='Tanggal Lahir' 
               placeholder="Tanggal Lahir"/>
       </td>
       <td class='font-12'>
        <select id='hubungan_wali' class="form-control" error='Hubungan Wali'>
         <?php if (!empty($list_wali)) { ?>
          <?php foreach ($list_wali as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['hubungan'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="no_telp" class="form-control" error='No. Telp' 
               placeholder="No. Telp"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="alamat_wali" class="form-control" error='Alamat' 
               placeholder="Alamat"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="pekerjaan" class="form-control" error='Pekerjaan' 
               placeholder="Pekerjaan"/>
       </td>
       <td class='font-12'>
        <select id='penghasilan' class="form-control" error='Penghasilan'>
         <?php if (!empty($list_penghasilan)) { ?>
          <?php foreach ($list_penghasilan as $vp) { ?>
           <option value="<?php echo $vp['id'] ?>"><?php echo $vp['penghasilan'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetail(this)"></i>
       </td>
      </tr>
     <?php } else { ?>             
      <tr class="baru">
       <td class='font-12'>
        <input type="text" value="" id="no_ktp_wali" class="form-control required" error='No. KTP' placeholder="No. KTP"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="nama_wali" class="form-control required" error='Nama' placeholder="Nama"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="tanggal_lahir_wali_1" class="form-control required" error='Tanggal Lahir' 
               placeholder="Tanggal Lahir"/>
       </td>
       <td class='font-12'>
        <select id='hubungan_wali' class="form-control required" error='Hubungan Wali'>
         <?php if (!empty($list_wali)) { ?>
          <?php foreach ($list_wali as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['hubungan'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="no_telp" class="form-control" error='No. Telp' 
               placeholder="No. Telp"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="alamat_wali" class="form-control" error='Alamat' 
               placeholder="Alamat"/>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="pekerjaan" class="form-control" error='Pekerjaan' 
               placeholder="Pekerjaan"/>
       </td>
       <td class='font-12'>
        <select id='penghasilan' class="form-control" error='Penghasilan'>
         <?php if (!empty($list_penghasilan)) { ?>
          <?php foreach ($list_penghasilan as $vp) { ?>
           <option value="<?php echo $vp['id'] ?>"><?php echo $vp['penghasilan'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetail(this)"></i>
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>