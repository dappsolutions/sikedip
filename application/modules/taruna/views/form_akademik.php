<h3 class="box-title">TARUNA AKADEMIK <i class="fa fa-arrow-down"></i></h3>
<hr class="m-t-0 m-b-40">
<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table color-bordered-table warning-bordered-table" id="table_akademik">
    <thead>
     <tr class="">
      <th class="font-12">Program Studi</th>
      <th class="font-12">Semester</th>
      <th class="font-12">Tanggal</th>
      <th class="font-12">Angkatan</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($detail_akademik)) { ?>
      <?php if (!empty($detail_akademik)) { ?>
       <?php foreach ($detail_akademik as $value) { ?>
        <?php $disabled = $value['status'] == 'Aktif' ? '' : 'disabled' ?>
        <tr id="<?php echo $value['id'] ?>">
         <td class='font-12'>
          <select <?php echo $disabled ?> id='prodi' class="form-control required" error='Program Studi'>
           <option value="">Pilih Program Studi</option>
           <?php if (!empty($list_prodi)) { ?>
            <?php foreach ($list_prodi as $vp) { ?>
             <?php $selected = $vp['id'] == $value['prodi'] ? 'selected' : '' ?>
             <option <?php echo $selected ?> value="<?php echo $vp['id'] ?>"><?php echo $vp['prodi'] ?></option>
            <?php } ?>
           <?php } else { ?>
            <option value="">Tidak Ada Data</option>  
           <?php } ?>
          </select>
         </td>
         <td class='font-12'>
          <select <?php echo $disabled ?> id='semester' class="form-control required" error='Semester'>
           <option value="">Pilih Semester</option>
           <?php if (!empty($list_semester)) { ?>
            <?php foreach ($list_semester as $vs) { ?>
             <?php $selected = $vs['id'] == $value['semester'] ? 'selected' : '' ?>
             <option <?php echo $selected ?> value="<?php echo $vs['id'] ?>"><?php echo $vs['semester'] ?></option>
            <?php } ?>
           <?php } else { ?>
            <option value="">Tidak Ada Data</option>  
           <?php } ?>
          </select>
         </td>
         <td class='font-12'>
          <input <?php echo $disabled ?> type="text" value="<?php echo $value['tanggal'] ?>" id="tanggal_masuk_1" class="form-control required" 
                                         error='Tanggal' placeholder="Tanggal"/>
         </td>
         <td class="font-12">
          <input <?php echo $disabled ?>  type="text" id='angkatan' class="form-control required" 
                                          value="<?php echo $value['angkatan'] ?>"
                                          error='Angkatan'>
         </td>
         <td class="text-center">
          <?php if ($disabled == 'Aktif') { ?>
           <i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetailData(this)"></i>
          <?php } ?>
         </td>
        </tr>        
       <?php } ?>
      <?php } ?>
      <tr class="baru">
       <td class='font-12'>
        <select id='prodi' class="form-control" error='Program Studi'>
         <option value="">Pilih Program Studi</option>
         <?php if (!empty($list_prodi)) { ?>
          <?php foreach ($list_prodi as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['prodi'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <select id='semester' class="form-control" error='Semester'>
         <option value="">Pilih Semester</option>
         <?php if (!empty($list_semester)) { ?>
          <?php foreach ($list_semester as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['semester'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="tanggal_masuk_1" class="form-control" error='Tanggal' placeholder="Tanggal"/>
       </td>
       <td class="font-12">
        <input type="text" id='angkatan' class="form-control" error='Angkatan'>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailAkademik(this)"></i>
       </td>
      </tr> 
     <?php } else { ?>             
      <tr class="baru">
       <td class='font-12'>
        <select id='prodi' class="form-control required" error='Program Studi'>
         <option value="">Pilih Program Studi</option>
         <?php if (!empty($list_prodi)) { ?>
          <?php foreach ($list_prodi as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['prodi'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <select id='semester' class="form-control required" error='Semester'>
         <option value="">Pilih Semester</option>
         <?php if (!empty($list_semester)) { ?>
          <?php foreach ($list_semester as $value) { ?>
           <option value="<?php echo $value['id'] ?>"><?php echo $value['semester'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <input type="text" value="" id="tanggal_masuk_1" class="form-control required" error='Tanggal' placeholder="Tanggal"/>
       </td>
       <td class="font-12">
        <input type="text" id='angkatan' class="form-control required" error='Angkatan'>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailAkademik(this)"></i>
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>