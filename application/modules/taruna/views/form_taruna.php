<h3 class="box-title">Identitas Taruna <i class="fa fa-arrow-down"></i></h3>
<hr class="m-t-0 m-b-40">
<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">No. Taruna</label>
   <div class="col-md-9">
    <input type="text" id='no_taruna' maxlength="15" class="form-control required" 
           error='No. Taruna' 
           placeholder="No. Taruna" value="<?php echo isset($no_taruna) ? $no_taruna : '' ?>">
   </div>
  </div>
 </div>
 <!--/span-->
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Nama</label>
   <div class="col-md-9">
    <input type="text" id='nama' class="form-control required" 
           placeholder="Nama Lengkap" 
           error='Nama Taruna'
           value="<?php echo isset($nama) ? $nama : '' ?>">
   </div>
  </div>
 </div>
 <!--/span-->
</div>
<!--/row-->
<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Tempat Lahir</label>
   <div class="col-md-9">
    <input type="text" class="form-control" id='tempat_lahir' error='Tempat Lahir' 
           placeholder="Tempat Lahir"
           value="<?php echo isset($tempat_lahir) ? $tempat_lahir : '' ?>"> 
   </div>
  </div>
 </div>

 <!--/span-->
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Tanggal Lahir</label>
   <div class="col-md-9">
    <input type="text" class="form-control required" id='tanggal_lahir' 
           error='Tanggal Lahir' placeholder="yyyy-mm-dd"
           value="<?php echo isset($tanggal_lahir) ? $tanggal_lahir : '' ?>"> 
   </div>
  </div>
 </div>
 <!--/span-->
</div>
<!--/row-->
<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Jenis Kelamin</label>
   <div class="col-md-9">
    <select id='jenis_kelamin' class="form-control required" error='Jenis Kelamin'>
     <?php if (!empty($list_kelamin)) { ?>
      <?php foreach ($list_kelamin as $value) { ?>
       <?php $selected = '' ?>
       <?php if (isset($jenis_kelamin)) { ?>
        <?php $selected = $jenis_kelamin == $value['id'] ? 'selected' : '' ?>
       <?php } ?>
       <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kelamin'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>
  </div>
 </div>

 <!--/span-->
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">NIK</label>
   <div class="col-md-9">
    <input type="text" class="form-control required" id='no_ktp' error='No. KTP' 
           placeholder="NIK"
           value="<?php echo isset($no_ktp) ? $no_ktp : '' ?>"> 
   </div>
  </div>
 </div>                  
 <!--/span-->
</div>

<div class="row">                           
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Golongan Darah</label>
   <div class="col-md-9">
    <select class="form-control required" id="golongan_darah" error='Golongan Darah' data-placeholder="Pilih Golongan Darah" tabindex="1">
     <option value="A" <?php echo isset($gol_darah) ? $gol_darah == 'A' ? 'selected' : '' : '' ?>>A</option>
     <option value="B" <?php echo isset($gol_darah) ? $gol_darah == 'B' ? 'selected' : '' : ''?>>B</option>
     <option value="AB" <?php echo isset($gol_darah) ? $gol_darah == 'C' ? 'selected' : '' : '' ?>>AB</option>
     <option value="O" <?php echo isset($gol_darah) ? $gol_darah == 'O' ? 'selected' : '' : ''?>>O</option>
    </select>
   </div>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">No. Sim</label>
   <div class="col-md-9">
    <input type="text" class="form-control" id='no_sim' error='No. Sim' 
           placeholder="No. Sim"
           value="<?php echo isset($no_sim) ? $no_sim : '' ?>"> 
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Foto</label>
   <div class="col-sm-9">
    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
     <div class="form-control" data-trigger="fileinput"> 
      <i class="glyphicon glyphicon-file fileinput-exists"></i> 
      <span class="fileinput-filename"><?php echo isset($foto) ? $foto : '' ?></span>
     </div> 
     <span class="input-group-addon btn btn-default btn-file">
      <span class="fileinput-new">Select file</span> 
      <span class="fileinput-exists">Change</span>
      <input id='file' type="file" name="..."> 
     </span> 
     <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
    </div>
   </div>
  </div>
 </div>  

 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">No. HP</label>            
   <div class="col-md-9">
    <input type="text" disabled="" id="front_number" value="+62" class="form-control"/>
    <input type="text" class="form-control" id='no_hp' error='No. HP' 
           placeholder="No. HP"
           value="<?php echo isset($no_hp) ? str_replace('+62', '', $no_hp) : '' ?>"> 
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Email</label>
   <div class="col-md-9">
    <input type="text" class="form-control" id='email' error='Email' 
           placeholder="Email"
           value="<?php echo isset($email) ? $email : '' ?>"> 
   </div>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Npwp</label>
   <div class="col-md-9">
    <input type="text" class="form-control" id='npwp' error='Npwp' 
           placeholder="Npwp"
           value="<?php echo isset($npwp) ? $npwp : '' ?>"> 
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Alamat</label>
   <div class="col-md-9">
    <textarea class="form-control required" error="Alamat" id="alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
   </div>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Propinsi</label>
   <div class="col-md-9">
    <select class="form-control required" id='propinsi' error="Propinsi" onchange="Taruna.getKotaData(this)">
     <option value="">Pilih Propinsi</option>
     <?php if (!empty($list_propinsi)) { ?>
      <?php foreach ($list_propinsi as $value) { ?>
       <?php $selected = '' ?>
       <?php if (isset($propinsi)) { ?>
        <?php $selected = $propinsi == $value['id'] ? 'selected' : '' ?>
       <?php } ?>
       <option  <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['propinsi'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Kota</label>
   <div class="col-md-9">
    <select class="form-control required" error="Kota" id='kota'>
     <option value="">Pilih Kota</option>
     <?php if (!empty($list_kota)) { ?>
      <?php foreach ($list_kota as $value) { ?>
       <?php $selected = '' ?>
       <?php if (isset($kota)) { ?>
        <?php $selected = $kota == $value['id'] ? 'selected' : '' ?>
       <?php } ?>
       <option <?php echo $selected ?> propinsi='<?php echo $value['propinsi'] ?>' value="<?php echo $value['id'] ?>"><?php echo $value['nama_kota'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Asal Sekolah</label>
   <div class="col-md-9">
    <select class="form-control required" id='asal' error="Asal Sekolah">
     <option value="">Pilih Asal</option>
     <?php if (!empty($list_asal)) { ?>
      <?php foreach ($list_asal as $value) { ?>
       <?php $selected = '' ?>
       <?php if (isset($asal_sekolah)) { ?>
        <?php $selected = $asal_sekolah == $value['id'] ? 'selected' : '' ?>
       <?php } ?>
       <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['sekolah'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Kompi</label>
   <div class="col-md-9">
    <select class="form-control required" error="Kompi" id='kompi'>
     <option value="">Pilih Kompi</option>
     <?php if (!empty($list_kompi)) { ?>
      <?php foreach ($list_kompi as $value) { ?>
       <?php $selected = '' ?>
       <?php if (isset($kompi)) { ?>
        <?php $selected = $kompi == $value['id'] ? 'selected' : '' ?>
       <?php } ?>
       <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label class="control-label col-md-3">Seperasuhan</label>
   <div class="col-md-9">
    <select class="form-control required" id='seperasuhan' error="Seperasuhan">
     <option value="">Pilih Seperasuhan</option>
     <?php if (!empty($list_seperasuhan)) { ?>
      <?php foreach ($list_seperasuhan as $value) { ?>
       <?php $selected = '' ?>
       <?php if (isset($seperasuhan)) { ?>
        <?php $selected = $seperasuhan == $value['id'] ? 'selected' : '' ?>
       <?php } ?>
       <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['seperasuhan'] ?></option>
      <?php } ?>
     <?php } ?>
    </select>
   </div>
  </div>
 </div>
</div>