<h3 class="box-title">ASRAMA TARUNA <i class="fa fa-arrow-down"></i></h3>
<hr class="m-t-0 m-b-40">
<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table color-bordered-table warning-bordered-table" id="table_asrama">
    <thead>
     <tr class="">
      <th class="font-12">Tanggal</th>
      <th class="font-12">Asrama</th>
      <th class="font-12">Kamar</th>
      <th class="text-center font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($detail_asrama)) { ?>
      <?php if (!empty($detail_asrama)) { ?>
       <?php foreach ($detail_asrama as $value) { ?>
        <tr id="<?php echo $value['id'] ?>">
         <td class="font-12">
          <input type="text" value="<?php echo $value['tanggal'] ?>" id="tanggal_asrama_1" class="form-control" />
         </td>
         <td class='font-12'>
          <select id='asrama' class="form-control required" error='Asrama'>
           <option value="">Pilih Asrama</option>
           <?php if (!empty($list_asrama)) { ?>
            <?php foreach ($list_asrama as $vp) { ?>
             <?php $selected = $vp['id'] == $value['asrama'] ? 'selected' : '' ?>
             <option <?php echo $selected ?> value="<?php echo $vp['id'] ?>"><?php echo $vp['nama_asrama'] ?></option>
            <?php } ?>
           <?php } else { ?>
            <option value="">Tidak Ada Data</option>  
           <?php } ?>
          </select>
         </td>
         <td class='font-12'>
          <select id='kamar' class="form-control required" error='Kamar'>
           <option value="">Pilih Kamar</option>
           <?php if (!empty($list_kamar)) { ?>
            <?php foreach ($list_kamar as $vs) { ?>
             <?php $selected = $vs['id'] == $value['kamar'] ? 'selected' : '' ?>
             <option <?php echo $selected ?> value="<?php echo $vs['id'] ?>"><?php echo $vs['kamar'] ?></option>
            <?php } ?>
           <?php } else { ?>
            <option value="">Tidak Ada Data</option>  
           <?php } ?>
          </select>
         </td>
         <td class="text-center">
          <i class="fa fa-minus-circle fa-2x hover" onclick="Taruna.removeDetailData(this)"></i>
         </td>
        </tr>        
       <?php } ?>
      <?php } ?>
      <tr class="baru">
       <td class="font-12">
        <input type="text" value="" id="tanggal_asrama_1" class="form-control" />
       </td>
       <td class='font-12'>
        <select id='asrama' class="form-control" error='Asrama'>
         <option value="">Pilih Asrama</option>
         <?php if (!empty($list_asrama)) { ?>
          <?php foreach ($list_asrama as $vp) { ?>
           <option value="<?php echo $vp['id'] ?>"><?php echo $vp['nama_asrama'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <select id='kamar' class="form-control" error='Kamar'>
         <option value="">Pilih Kamar</option>
         <?php if (!empty($list_kamar)) { ?>
          <?php foreach ($list_kamar as $vs) { ?>
           <option value="<?php echo $vs['id'] ?>"><?php echo $vs['kamar'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailAsrama(this)"></i>
       </td>
      </tr> 
     <?php } else { ?>             
      <tr class="baru">
       <td class="font-12">
        <input type="text" value="" id="tanggal_asrama_1" class="form-control required" error="Tanggal"/>
       </td>
       <td class='font-12'>
        <select id='asrama' class="form-control required" error='Asrama'>
         <option value="">Pilih Asrama</option>
         <?php if (!empty($list_asrama)) { ?>
          <?php foreach ($list_asrama as $vp) { ?>
           <option value="<?php echo $vp['id'] ?>"><?php echo $vp['nama_asrama'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class='font-12'>
        <select id='kamar' class="form-control required" error='Kamar'>
         <option value="">Pilih Kamar</option>
         <?php if (!empty($list_kamar)) { ?>
          <?php foreach ($list_kamar as $vs) { ?>
           <option value="<?php echo $vs['id'] ?>"><?php echo $vs['kamar'] ?></option>
          <?php } ?>
         <?php } else { ?>
          <option value="">Tidak Ada Data</option>  
         <?php } ?>
        </select>
       </td>
       <td class="text-center">
        <i class="fa fa-plus-circle fa-2x hover" onclick="Taruna.addDetailAsrama(this)"></i>
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>