<?php

class Naik_semester extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'naik_semester';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/naik_semester.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'naik_semester';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Naik";
  $data['title_content'] = 'Data Naik';
  $content = $this->getDataNaik();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataNaik($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.tanggal', $keyword),
       array('tr.no_taruna', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'pr.prodi as program_studi',
                  's.semester as semester_sekarang', 'ss.semester as semester_lanjut'),
              'join' => array(
                  array('prodi pr', 'k.prodi = pr.id'),
                  array('semester s', 'k.semester_sekarang = s.id'),
                  array('semester ss', 'k.semester_lanjut = ss.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted = 0"
  ));

  return $total;
 }

 public function getDataNaik($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.tanggal', $keyword),
       array('tr.no_taruna', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'pr.prodi as program_studi',
                  's.semester as semester_sekarang', 'ss.semester as semester_lanjut'),
              'join' => array(
                  array('prodi pr', 'k.prodi = pr.id'),
                  array('semester s', 'k.semester_sekarang = s.id'),
                  array('semester ss', 'k.semester_lanjut = ss.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataNaik($keyword)
  );
 }

 public function getDetailDataNaik($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'pr.prodi as program_studi',
                  's.semester as semester_sebelum', 'ss.semester as semester_naik'),
              'join' => array(
                  array('prodi pr', 'k.prodi = pr.id'),
                  array('semester s', 'k.semester_sekarang = s.id'),
                  array('semester ss', 'k.semester_lanjut = ss.id'),
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListTaruna() {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenis() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_jadwal_luar p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPrestasi() {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Naik";
  $data['title_content'] = 'Tambah Naik';
  $data['list_prodi'] = $this->getListProdi();
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_izin'] = $this->getListJenis();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataNaik($id);
//  echo $data['tanggal'];die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Naik";
  $data['title_content'] = 'Ubah Naik';
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_izin'] = $this->getListJenis();
  $data['detail_naik'] = $this->getDetailTarunaNaik($id);
  echo Modules::run('template', $data);
 }

 public function getDataProdiTaruna($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('tha.*', 'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id')
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '" . $taruna . "' and tha.status = 1"
  ));

  return $data->row_array();
 }

 public function getDetailTarunaNaik($id) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_naik_semester ths',
              'field' => array('ths.*', 't.no_taruna', 't.nama', 'tha.angkatan'),
              'join' => array(
                  array('taruna t', 'ths.taruna = t.id'),
                  array('taruna_has_akademik tha', 'ths.taruna = tha.taruna and tha.status = 1'),
              ),
              'where' => array('ths.naik_semester' => $id, 'ths.deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataNaik($id);

  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Naik";
  $data['title_content'] = "Detail Naik";
  $data['detail_naik'] = $this->getDetailTarunaNaik($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getPostDataNaik($value) {
  $data['tanggal'] = $value->tanggal;
  $data['prodi'] = $value->prodi;
  $data['semester_sekarang'] = $value->semester;
  $data['semester_lanjut'] = $value->semester_naik;
  return $data;
 }

 public function getContentListTaruna($prodi) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('t.*'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna t', 'tha.taruna = t.id'),
              ),
              'where' => "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $conten, true);
 }

 public function getListJenisPrestasi($bidang) {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.bidang_penghargaan = '" . $bidang . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_prestasi'] = $result;
  echo $this->load->view('list_prestasi', $conten, true);
 }

 public function getIdTaruna($no_taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna',
              'where' => "no_taruna = '" . $no_taruna . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  }

  return $id;
 }

 public function getPostTarunaNaik($value, $naik) {
  $data['naik_semester'] = $naik;
  $data['taruna'] = $value->taruna;

  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataNaik($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);

    if (!empty($data->taruna)) {
     foreach ($data->taruna as $value) {
      $post = $this->getPostTarunaNaik($value, $id);
      Modules::run('database/_insert', 'taruna_naik_semester', $post);

      Modules::run('database/_update', 'taruna_has_akademik', array('status' => 0), array('taruna' => $value->taruna));
      $post_update['tanggal'] = $data->tanggal;
      $post_update['taruna'] = $value->taruna;
      $post_update['prodi'] = $value->prodi;
      $post_update['semester'] = $value->semester;
      $post_update['angkatan'] = $value->angkatan;
      $post_update['status'] = 1;
      Modules::run('database/_insert', 'taruna_has_akademik', $post_update);
     }
    }
   } else {
    //update
    if (!empty($data->taruna)) {
     Modules::run('database/_update', 'taruna_naik_semester', array('deleted' => true), array('naik_semester' => $id));
     foreach ($data->taruna as $value) {
      $post = $this->getPostTarunaNaik($value, $id);
      Modules::run('database/_insert', 'taruna_naik_semester', $post);

      Modules::run('database/_update', 'taruna_has_akademik', array('status' => 0), array('taruna' => $value->taruna));
      $post_update['tanggal'] = $data->tanggal;
      $post_update['taruna'] = $value->taruna;
      $post_update['prodi'] = $value->prodi;
      $post_update['semester'] = $value->semester;
      $post_update['angkatan'] = $value->angkatan;
      $post_update['status'] = 1;
      Modules::run('database/_insert', 'taruna_has_akademik', $post_update);
     }
    }

    if (!empty($data->taruna_edit)) {
     foreach ($data->taruna_edit as $value) {
      $post = $this->getPostTarunaNaik($value, $id);
      if ($value->is_edit != 1) {
       $post['deleted'] = 1;
       Modules::run('database/_update', 'taruna_has_akademik', array('semester' => $value->semester), array('taruna' => $value->taruna, 'deleted' => 0, 'status' => 1));
       Modules::run('database/_update', 'taruna_has_akademik', array('status' => 1), array('taruna' => $value->taruna, 'deleted' => 0));
       Modules::run('database/_update', 'taruna_has_akademik', array('deleted' => 1, 'status' => 0), array('taruna' => $value->taruna, 'semester' => $value->semester));
      }

      Modules::run('database/_update', 'taruna_naik_semester', $post, array('id' => $value->id));
      if ($value->is_edit) {
       Modules::run('database/_update', 'taruna_has_akademik', array('semester' => $value->semester), array('taruna' => $value->taruna, 'deleted' => 0, 'status' => 1));
      }
     }
    }

    $post = $this->getPostDataNaik($data);
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function getExistAkademik($taruna, $semester) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik',
              'where' => "deleted = 0 and taruna = '" . $taruna . "' "
              . "and semester = '" . $semester . "' and status = 1"
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }


  return $is_exist;
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Naik";
  $data['title_content'] = 'Data Naik';
  $content = $this->getDataNaik($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataTaruna($id = '') {
  $data = $this->getListDataTaruna($id);
  echo json_encode(array('data' => $data));
 }

 public function getListDataTaruna($id = '') {
  $keyword = $this->input->post('keyword');
  $where = array();
  if ($id != '') {
   $where = array('p.id' => $id);
  }

  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_taruna', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna p',
              'field' => array('p.*'),
              'where' => $where,
              'limit' => 5,
              'like' => $like,
              'is_or_like' => true
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataTarunaDetail() {
  $prodi = $this->input->post('prodi');
  $semester = $this->input->post('semester');
  if ($semester != '') {
   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' "
           . "and tha.semester = '" . $semester . "'";
  } else {
   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*', 'tha.angkatan'),
              'join' => array(
                  array('taruna_has_akademik tha', 'tha.taruna = t.id and tha.status = 1')
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['detail'] = $result;
  echo $this->load->view('list_taruna', $content, true);
 }

}
