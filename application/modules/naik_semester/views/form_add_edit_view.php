<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'Naik Semester' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">        

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Tanggal</label>
           <div class="col-md-9">
            <input id="tanggal" type="text" value="<?php echo isset($tanggal) ? $tanggal : '' ?>" id="" class="form-control required" error="Tanggal" placeholder="Tanggal"/>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Program Studi</label>
           <div class="col-md-9">
            <select id='prodi' class="form-control required" error='Program Studi' onchange="NaikSemester.getDataTarunaDetail(this)">
             <option value="">--Pilih Prodi--</option>
             <?php if (!empty($list_prodi)) { ?>
              <?php foreach ($list_prodi as $v_p) { ?>
               <?php $selected = '' ?>
               <?php if (isset($prodi)) { ?>
                <?php $selected = $v_p['id'] == $prodi ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['prodi'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Semester Sekarang</label>
           <div class="col-md-9">
            <select id='semester' class="form-control required" error='Semester' onchange="NaikSemester.getDataTarunaDetail(this)">
             <option value="">--Pilih Semester--</option>
             <?php if (!empty($list_semester)) { ?>
              <?php foreach ($list_semester as $v_p) { ?>
               <?php $selected = '' ?>
               <?php if (isset($semester_sekarang)) { ?>
                <?php $selected = $v_p['id'] == $semester_sekarang ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['semester'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Naik Semester</label>
           <div class="col-md-9">
            <select id='semester_naik' class="form-control required" error='Naik Semester'>
             <option value="">--Pilih Semester--</option>
             <?php if (!empty($list_semester)) { ?>
              <?php foreach ($list_semester as $v_p) { ?>
               <?php $selected = '' ?>
               <?php if (isset($semester_lanjut)) { ?>
                <?php $selected = $v_p['id'] == $semester_lanjut ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['semester'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>


        <h3 class="box-title">DATA TARUNA <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <!--/row-->
        <div class="row">
         <div class="col-md-12">
          <div class="table-responsive">
           <table class="table color-bordered-table warning-bordered-table" id="table_taruna">
            <thead>
             <tr class="">
              <th class="font-12">Taruna</th>
              <th class="text-center font-12">
               <input type="checkbox" class="check check_header" id="flat-checkbox-1" data-checkbox="icheckbox_flat-red" onchange="NaikSemester.checkAll(this)" <?php echo isset($detail_naik) ? !empty($detail_naik) ? 'checked' : '' : '' ?>>
              </th>
             </tr>
            </thead>
            <tbody>
             <?php if (isset($detail_naik)) { ?>
              <?php if (!empty($detail_naik)) { ?>
               <?php foreach ($detail_naik as $value) { ?>
                <tr taruna="<?php echo $value['taruna'] ?>" angkatan="<?php echo $value['angkatan'] ?>" id="<?php echo $value['id'] ?>">
                 <td class="font-12">
                  <?php echo $value['no_taruna'] . ' - ' . $value['nama'] ?>
                 </td>
                 <td class="text-center">
                  <div class="icheckbox_flat-red">
                   <input type="checkbox" class="check check_data" id="flat-checkbox-1" data-checkbox="icheckbox_flat-red" onchange="NaikSemester.checkTaruna(this)" checked="true">
                  </div>
                 </td>
                </tr>  
               <?php } ?>
              <?php } else { ?> 
               <tr class="baru">
                <td colspan="2">Tidak Ada Data</td>
               </tr>        
              <?php } ?>
             <?php } else { ?>
              <tr class="baru">
               <td colspan="2" class="font-12">Tidak Ada Data</td>
              </tr>        
             <?php } ?>             
            </tbody>
           </table>
          </div>
         </div>
        </div>

        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="submit" class="btn btn-success" onclick="NaikSemester.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
             <button type="button" class="btn btn-default" onclick="NaikSemester.back()">Batal</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
