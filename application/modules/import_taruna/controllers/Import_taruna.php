<?php

class Import_taruna extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'import_taruna';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/import_taruna.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'taruna';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Import Taruna";
  $data['title_content'] = 'Import Taruna';
  echo Modules::run('template', $data);
 }

 public function getGarduInduk($gardu, $upt) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk',
              'where' => array('gardu' => $gardu, 'deleted' => 0, 'upt' => $upt)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'gardu_induk', array('gardu' => $gardu, 'upt' => $upt));
  }

  return $id;
 }

 public function getKategoriTaruna($kategori) {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_taruna',
              'where' => array('kategori' => $kategori, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'kategori_taruna', array('kategori' => $kategori));
  }

  return $id;
 }

 public function getDataTaruna($nama_taruna, $kategori_taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'nama_taruna',
              'where' => array('nama_taruna' => $nama_taruna, 'deleted' => 0, 'kategori_taruna' => $kategori_taruna)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'nama_taruna', array('nama_taruna' => $nama_taruna,
               'kategori_taruna' => $kategori_taruna));
  }

  return $id;
 }

 public function getUpt($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('upt' => $upt, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'upt', array('upt' => $upt));
  }

  return $id;
 }

 public function getTahun($tahun) {
  $data = Modules::run('database/get', array(
              'table' => 'tahun',
              'where' => array('tahun' => $tahun, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'tahun', array('tahun' => $tahun));
  }

  return $id;
 }

 public function getRegu($regu) {
  $data = Modules::run('database/get', array(
              'table' => 'regu_pemeliharaan',
              'where' => array('regu' => $regu, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'regu_pemeliharaan', array('regu' => $regu));
  }

  return $id;
 }

 public function getDataIdPenghasilan($penghasilan) {
  $data = Modules::run('database/get', array(
              'table' => 'penghasilan',
              'where' => "penghasilan = '".$penghasilan."'"
  ));
  
  $id = 0;
  if(!empty($data)){
   $id = $data->row_array()['id'];
  }else{
   $post['penghasilan'] = $penghasilan;
   $id = Modules::run('database/_insert', 'penghasilan', $post);
  }
  
  return $id;
 }
 
 public function getIdProdi($prodi) {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0 and prodi = '".$prodi."'"
  ));
  
  $id = 0;
  if(!empty($data)){
   $id = $data->row_array()['id'];
  }else{
   $id = Modules::run('database/_insert', 'prodi', array('prodi'=> $prodi));
  }
  
  return $id;
 }
 
 public function import() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();
  foreach ($data as $value) {
   $data_taruna = $value;   
   
   if (is_numeric(trim($data_taruna[0]))) {
    $is_valid_insert = false;
    $this->db->trans_begin();
    try {
     //user taruna
     $post_user_taruna['username'] = str_replace(' ', '', trim($data_taruna[4]));
     $post_user_taruna['password'] = trim(date('Ymd', strtotime($data_taruna[6])));
     $post_user_taruna['priveledge'] = 3;
     $user_taruna = Modules::run('database/_insert', 'user', $post_user_taruna);

     //taruna
     $post_taruna['no_taruna'] = str_replace(' ', '', trim($data_taruna[4]));
     $post_taruna['user'] = $user_taruna;
     $post_taruna['no_ktp'] = trim($data_taruna[9]);
     $post_taruna['no_hp'] = trim($data_taruna[13]);
     $post_taruna['email'] = trim($data_taruna[14]);
     $post_taruna['alamat'] = trim($data_taruna[12]);
     $jk = strtolower(trim($data_taruna[7]));
     if ($jk == 'laki-laki') {
      $post_taruna['jenis_kelamin'] = 1;
     } else {
      $post_taruna['jenis_kelamin'] = 2;
     }

     $post_taruna['tempat_lahir'] = trim($data_taruna[5]);
     $post_taruna['nama'] = trim($data_taruna[1]);
     $post_taruna['npwp'] = trim($data_taruna[10]);
     $post_taruna['tanggal_lahir'] = trim(date('Y-m-d', strtotime($data_taruna[6])));
     $taruna = Modules::run('database/_insert', $this->getTableName(), $post_taruna);

     //insert taruna has agama
     $post_agama['taruna'] = $taruna;
     $post_agama['tanggal'] = date('Y-m-d');
     $post_agama['agama'] = trim($data_taruna[8]);
     $agama = Modules::run('database/_insert', 'taruna_has_agama', $post_agama);

     //insert taruna has akademik
     $post_akademik['taruna'] = $taruna;
     $post_akademik['tanggal'] = date('Y-m-d');
     $post_akademik['angkatan'] = trim($data_taruna[2]);
     $post_akademik['prodi'] = $this->getIdProdi(trim($data_taruna[3]));
     $akademik = Modules::run('database/_insert', 'taruna_has_akademik', $post_akademik);

     //user wali bapak
     $post_user_wali['username'] = trim(date('Ymd', strtotime($data_taruna[16])));
     $post_user_wali['password'] = trim(date('Ymd', strtotime($data_taruna[16])));
     $post_user_wali['priveledge'] = 2;
     $user_wali = Modules::run('database/_insert', 'user', $post_user_wali);

     //taruna has wali bapak
     $post_wali['taruna'] = $taruna;
     $post_wali['user'] = $user_wali;
     $post_wali['nama'] = trim($data_taruna[15]);
     $post_wali['pekerjaan'] = trim($data_taruna[17]);
     $post_wali['hubungan_wali'] = 1;
     $wali = Modules::run('database/_insert', 'taruna_has_wali_murid', $post_wali);

     //user ibu
     $post_user_wali['username'] = trim(date('Ymd', strtotime($data_taruna[19])));
     $post_user_wali['password'] = trim(date('Ymd', strtotime($data_taruna[19])));
     $post_user_wali['priveledge'] = 2;
     $user_wali = Modules::run('database/_insert', 'user', $post_user_wali);

     //taruna has wali ibu
     $post_wali['taruna'] = $taruna;
     $post_wali['user'] = $user_wali;
     $post_wali['nama'] = trim($data_taruna[18]);
     $post_wali['hubungan_wali'] = 2;
     $post_wali['pekerjaan'] = trim($data_taruna[20]);
     $wali = Modules::run('database/_insert', 'taruna_has_wali_murid', $post_wali);
     $this->db->trans_commit();
     $is_valid_insert = true;
     if ($is_valid_insert) {
      $post_taruna_data['no_taruna'] = $data_taruna[4];
      $post_taruna_data['nama_taruna'] = trim($data_taruna[1]);
      array_push($result, $post_taruna_data);
     }
    } catch (Exception $ex) {
     $this->db->trans_rollback();
    }
   }
  }

  $content['list_upload'] = $result;
  echo $this->load->view('tabel_taruna', $content, true);
 }

 public function download() {
  $this->load->helper("download");
  $data = file_get_contents('files/template_import/template_import_taruna.csv');
  $name = 'template_import_taruna.csv';
  force_download($name, $data);
 }

}
