<div class='container-fluid'>
 <div class="white-box">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-6'>
     <div class="form-group">
      <label class="control-label col-md-3">File upload (.csv)</label>
      <div class="col-sm-9">
       <div class="fileinput fileinput-new input-group" data-provides="fileinput">
        <div class="form-control" data-trigger="fileinput"> 
         <i class="glyphicon glyphicon-file fileinput-exists"></i> 
         <span class="fileinput-filename"></span>
        </div> 
        <span class="input-group-addon btn btn-default btn-file"> 
         <span class="fileinput-new" onclick="ImportTaruna.upload(this)">Select file</span> 
         <span class="fileinput-exists">Change</span>
         <input id='file' type="file" name="..." onchange="ImportTaruna.getUploadedData(this)"> 
        </span> 
        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
       </div>
      </div>
     </div>
     <!--     <div class="form-group">
           <label>File upload (.csv)</label>
           <input name="img[]" class="file-upload-default" type="file">
           <div class="input-group col-xs-12">
            <input class="form-control file-upload-info" disabled="" placeholder="Upload File Csv" type="text">
            <span class="input-group-append">
             <button class="file-upload-browse btn btn-success" type="button" onclick="ImportTaruna.upload(this)">Import</button>
             <input type="file" style="display: none;" id="file" onchange="ImportTaruna.getUploadedData(this)"/>
            </span>
           </div>
          </div>-->
    </div>
    <div class='col-md-6 text-right'>
     <a href="<?php echo base_url() . $module . '/download' ?>" id="" class="btn btn-warning">Download Template</a>
    </div>
   </div>
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive" id="table_alat">
      <table class="table color-bordered-table success-bordered-table">
       <thead>
        <tr class="bg-success text-white">
         <th class="font-12">No</th>
         <th class="font-12">Alat</th>
         <th class="font-12">UPT</th>
         <th class="font-12">Gardu Induk / ULTG</th>
         <th class="font-12">Status</th>
        </tr>
       </thead>
       <tbody>
        <tr>
         <td class="text-center font-12" colspan="5">Tidak Ada Data yang Diimport</td>
        </tr>
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
  </div>
 </div>
</div>