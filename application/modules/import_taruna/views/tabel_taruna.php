<table class="table color-bordered-table success-bordered-table">
 <thead>
  <tr class="bg-success text-white">
   <th class="font-12">No</th>
   <th class="font-12">No Taruna</th>
   <th class="font-12">Nama Taruna</th>
   <th class="font-12">Status</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($list_upload)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($list_upload as $value) { ?>
    <tr>
     <td><?php echo $no++ ?></td>
     <td><?php echo $value['no_taruna'] ?></td>
     <td><?php echo $value['nama_taruna'] ?></td>
     <td class="text-center">
      <label id="" class="badge badge-success font-12">Sukses</label>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-12" colspan="4">Tidak Ada Data yang Diimport</td>
   </tr>
  <?php } ?>         
 </tbody>
</table>