<?php

class Verifikasi extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'verifikasi';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/verifikasi.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kesalahan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Verifikasi";
  $data['title_content'] = 'Data Verifikasi';
  $content = $this->getDataVerifikasi();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataVerifikasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
       array('t.no_taruna', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna', 'p.point', 'ks.status'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted = 0 and k.deleted =0 and (ks.status = 'DRAFT' or ks.status is null)"
  ));

  return $total;
 }

 public function getDataVerifikasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
       array('t.no_taruna', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna', 'p.point', 'ks.status'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted = 0 and k.deleted =0 and (ks.status = 'DRAFT' or ks.status is null)"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataVerifikasi($keyword)
  );
 }

 public function getDetailDataVerifikasi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna', 'kp.kategori',
                  'kp.id as kategori_id', 'p.point', 'ks.status'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('kategori_pelanggaran kp', 'p.kategori_pelanggaran = kp.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id and (ks.status != 'REJECTED' and ks.status != 'APPROVED')", 'left')
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListTaruna() {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_pelanggaran p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPelanggaran() {
  $data = Modules::run('database/get', array(
              'table' => 'pelanggaran p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Verifikasi";
  $data['title_content'] = 'Tambah Verifikasi';
  $data['list_prodi'] = $this->getListProdi();
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_kategori'] = $this->getListKategori();
  $data['list_pelanggaran'] = $this->getListPelanggaran();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataVerifikasi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Verifikasi";
  $data['title_content'] = 'Ubah Verifikasi';
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_pelanggaran'] = $this->getListPelanggaran();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_kategori'] = $this->getListKategori();
  $prodi = $this->getDataProdiTaruna($data['taruna']);
  $data['prodi'] = $prodi['prodi'];
  echo Modules::run('template', $data);
 }

 public function getDataProdiTaruna($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('tha.*', 'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id')
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '" . $taruna . "' and tha.status = 1"
  ));


  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataVerifikasi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Verifikasi";
  $data['title_content'] = "Detail Verifikasi";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['taruna']);
  echo Modules::run('template', $data);
 }

 public function getPostDataVerifikasi($value) {
  $data['taruna'] = $value->taruna;
  $data['semester'] = $value->semester;
  $data['pelanggaran'] = $value->pelanggaran;
  $data['keterangan'] = $value->keterangan;
  $data['tanggal'] = $value->tanggal;
  return $data;
 }

 public function getContentListTaruna($prodi) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('t.*'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna t', 'tha.taruna = t.id'),
              ),
              'where' => "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $conten, true);
 }

 public function getListJenisPelanggaran($kategori) {
  $data = Modules::run('database/get', array(
              'table' => 'pelanggaran p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.kategori_pelanggaran = '" . $kategori . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_pelanggaran'] = $result;
  echo $this->load->view('list_pelanggaran', $conten, true);
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataVerifikasi($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Verifikasi";
  $data['title_content'] = 'Data Verifikasi';
  $content = $this->getDataVerifikasi($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function approve($id) {
  $taruna = $this->input->post('taruna');
  $nama_taruna = $this->input->post('nama_taruna');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['user'] = $this->session->userdata('user_id');
   $post['status'] = 'APPROVED';
   $post['kesalahan'] = $id;
   Modules::run('database/_insert', 'kesalahan_status', $post);
   $jumlah_poin = $this->getJumlahPoinKesalahan($taruna);
   if (intval($jumlah_poin) >= 50) {
    $this->sendNotifKeWaliMurid($taruna, $nama_taruna);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function rejected($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['user'] = $this->session->userdata('user_id');
   $post['status'] = 'REJECTED';
   $post['kesalahan'] = $id;
   $post['keterangan'] = $this->input->post('keterangan');
   Modules::run('database/_insert', 'kesalahan_status', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getJumlahPoinKesalahan($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'kesalahan k',
              'field' => "sum(p.point) as jumlah",
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'where' => array(
                  'k.taruna' => $taruna,
                  'ks.status' => 'APPROVED'
              )
  ));

  $jumlah = 0;
  if (!empty($data)) {
   $jumlah = $data->row_array()['jumlah'];
   $jumlah = $jumlah == null ? 0 : $jumlah;
  }


  return $jumlah;
 }

 public function getDataTokenWali($taruna) {

  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'join' => array(
                  array('priveledge p', 'u.priveledge = p.id and p.id = 2'),
                  array('taruna_has_wali_murid thw', 'u.id = thw.user'),
              ),
              'where' => "u.deleted = 0 and u.token != '' and thw.taruna = '" . $taruna . "'"
  ));



  $result = array();

  if (!empty($data)) {

   foreach ($data->result_array() as $value) {

    array_push($result, $value['token']);
   }
  }


  return $result;
 }

 public function sendNotifKeWaliMurid($taruna, $nama_taruna) {
  $token = $this->getDataTokenWali($taruna);
  
  if (!empty($token)) {
   $url = 'https://fcm.googleapis.com/fcm/send';

   $fields = array(
       'registration_ids' => $token,
       'notification' => array(
           'title' => "Kesalahan Taruna",
           'body' => "Taruna " . $nama_taruna . " sudah mendapatkan poin kesalahan >=50",
       ),
       'data' => array(
           'nama_taruna' => $nama_taruna,
       )
   );

   $headers = array(
       'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
       'Content-Type:application/json'
   );



   // echo json_encode($fields);die;

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, $url);

   curl_setopt($ch, CURLOPT_POST, true);

   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

   $result = curl_exec($ch);



   if ($result === false) {

    die('Curl failed: ' . curl_error($ch));
   }
   curl_close($ch);
  }
 }

}
