<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Kesalahan <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Tanggal
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $tanggal ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Taruna
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $no_taruna . ' - ' . $nama ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Program Studi
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php if (!empty($data_prodi)) { ?>
             <?php echo $data_prodi['program_studi'] ?>
            <?php } else { ?>
             -
            <?php } ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Semester
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $semester_taruna ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Kategori Pelanggaran
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $kategori ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Pelanggaran
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $jenis . ' (' . $point . ' poin)' ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <div class="row">
         <div class="col-md-6">
          <div class="row">
           <div class="col-md-3">
            Keterangan
           </div>
           <div class="col-md-9 text-primary text-left">
            <?php echo $keterangan ?>
           </div>
          </div>         
         </div>
        </div>
        <br/>

        <?php if ($status_kesalahan != '') { ?>
         <div class="row">
          <div class="col-md-6">
           <div class="row">
            <div class="col-md-3">
             Status 
            </div>
            <div class="col-md-9 text-primary text-left">
             <?php if ($status_kesalahan == 'REJECTED') { ?>
              <?php echo $status_kesalahan . ' (' . $keterangan_kesalahan . ')' ?>
             <?php } else { ?>
              <?php echo $status_kesalahan ?>
             <?php } ?>
            </div>
           </div>         
          </div>
         </div>
         <br/>
        <?php } ?>        


        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="button" class="btn btn-default" onclick="Kesalahan.back()">Kembali</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
