<select id='pelanggaran' class="form-control required" error='Kategori'>
 <?php if (!empty($list_pelanggaran)) { ?>
  <?php foreach ($list_pelanggaran as $value) { ?>
   <?php $selected = '' ?>
   <?php if (isset($pelanggaran)) { ?>
    <?php $selected = $value['id'] == $pelanggaran ? 'selected' : '' ?>
   <?php } ?>
   <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'].' ('.$value['point'].' poin)' ?></option>
  <?php } ?>
 <?php } else { ?>
  <option value="">Tidak Ada Data</option>  
 <?php } ?>
</select>