<?php

class Kesalahan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $kompi;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->akses = $this->session->userdata('hak_akses');
  $this->kompi = $this->session->userdata('kompi');
 }

 public function getModuleName() {
  return 'kesalahan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/kesalahan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kesalahan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kesalahan";
  $data['title_content'] = 'Data Kesalahan';
  $content = $this->getDataKesalahan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKesalahan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
       array('t.no_taruna', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna', 'p.point'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted = 0"
  ));

  return $total;
 }

 public function getDataKesalahan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('t.nama', $keyword),
       array('t.no_taruna', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna', 'p.point', 'ks.status'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataKesalahan($keyword)
  );
 }

 public function getDetailDataKesalahan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
                  's.semester as semester_taruna', 'kp.kategori', 'kp.id as kategori_id', 'p.point'),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('semester s', 'k.semester = s.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('kategori_pelanggaran kp', 'p.kategori_pelanggaran = kp.id'),
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListTaruna() {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_pelanggaran p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPelanggaran() {
  $data = Modules::run('database/get', array(
              'table' => 'pelanggaran p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Kesalahan";
  $data['title_content'] = 'Tambah Kesalahan';
  $data['list_prodi'] = $this->getListProdi();
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_kategori'] = $this->getListKategori();
  $data['list_pelanggaran'] = $this->getListPelanggaran();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKesalahan($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kesalahan";
  $data['title_content'] = 'Ubah Kesalahan';
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_pelanggaran'] = $this->getListPelanggaran();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_kategori'] = $this->getListKategori();
  $prodi = $this->getDataProdiTaruna($data['taruna']);
  $data['prodi'] = !empty($prodi) ? $prodi['prodi'] : '';
  echo Modules::run('template', $data);
 }

 public function getDataProdiTaruna($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('tha.*', 'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id')
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '" . $taruna . "' and tha.status = 1"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataStatus($id) {
  $data = Modules::run('database/get', array(
              'table' => 'kesalahan_status ks',
              'where' => "ks.deleted = 0 and ks.kesalahan = '".$id."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }
 
 public function detail($id) {
  $data = $this->getDetailDataKesalahan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kesalahan";
  $data['title_content'] = "Detail Kesalahan";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['taruna']);
  $status = $this->getDataStatus($id);
  $data['status_kesalahan'] = !empty($status) ? $status['status'] : '';
  $data['keterangan_kesalahan'] = !empty($status) ? $status['keterangan'] : '';
  echo Modules::run('template', $data);
 }

 public function getPostDataKesalahan($value) {
  $data['taruna'] = $value->taruna;
  $data['semester'] = $value->semester;
  $data['pelanggaran'] = $value->pelanggaran;
  $data['keterangan'] = $value->keterangan;
  $data['tanggal'] = $value->tanggal;
  return $data;
 }

 public function getContentListTaruna($prodi) {
  $where = "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "' "
          . "and t.kompi = '".$this->kompi."'";
  if($this->akses == 'Superadmin'){
   $where = "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "'";
  }
  
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('t.*'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna t', 'tha.taruna = t.id'),
              ),
              'where' => $where
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $conten, true);
 }

 public function getListJenisPelanggaran($kategori) {
  $data = Modules::run('database/get', array(
              'table' => 'pelanggaran p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.kategori_pelanggaran = '" . $kategori . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_pelanggaran'] = $result;
  echo $this->load->view('list_pelanggaran', $conten, true);
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataKesalahan($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
    $post_status['user'] = $this->session->userdata('user_id');
    $post_status['status'] = 'DRAFT';
    $post_status['kesalahan'] = $id;

    Modules::run('database/_insert', 'kesalahan_status', $post_status);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kesalahan";
  $data['title_content'] = 'Data Kesalahan';
  $content = $this->getDataKesalahan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
