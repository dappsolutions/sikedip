<?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
 <li>
  <a class="active waves-effect" href="<?php echo base_url() . 'role' ?>" aria-expanded="false"><i class="icon-settings fa-fw"></i> <span class="hide-menu"> Role User </span></a>
 </li>
 <li>
  <a class="active waves-effect" href="<?php echo base_url() . 'role_admin' ?>" aria-expanded="false"><i class="icon-settings fa-fw"></i> <span class="hide-menu"> Role Admin </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'user' ?>" aria-expanded="false"><i class="icon-user fa-fw"></i> <span class="hide-menu"> User </span></a>
 </li> 
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'kompi' ?>" aria-expanded="false"><i class="fa fa-group fa-fw"></i> <span class="hide-menu"> Kompi </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'admin_kompi' ?>" aria-expanded="false"><i class="fa fa-user-plus fa-fw"></i> <span class="hide-menu"> Admin Kompi </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'chat_admin' ?>" aria-expanded="false"><i class="fa fa-inbox fa-fw"></i> <span class="hide-menu"> Inbox Admin </span></a>
 </li>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'chat_manajemen' ?>" aria-expanded="false"><i class="fa fa-inbox fa-fw"></i> <span class="hide-menu"> Inbox Manajemen </span></a>
 </li> 
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'pengumuman' ?>" aria-expanded="false"><i class="icon-drawar fa-fw"></i> <span class="hide-menu"> <?php echo 'Pengumuman' ?> </span></a>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-user fa-fw"></i> <span class="hide-menu"> <?php echo 'Taruna' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'taruna' ?>"><?php echo 'Data Taruna' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'cetak_taruna' ?>"><?php echo 'Cetak Data Taruna' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'naik_semester' ?>"><?php echo 'Naik Semester' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'semester' ?>"><?php echo 'Semester' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'prodi' ?>"><?php echo 'Program Studi' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'penghasilan' ?>"><?php echo 'Penghasilan Wali' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'asal' ?>"><?php echo 'Asal Sekolah' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'seperasuhan' ?>"><?php echo 'Seperasuhan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'asrama' ?>"><?php echo 'Asrama' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'kamar' ?>"><?php echo 'Kamar Asrama' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'user_taruna' ?>"><?php echo 'Taruna User' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'user_wali' ?>"><?php echo 'Wali Taruna User' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'import_taruna' ?>"><?php echo 'Impor Taruna' ?></a> </li>  
  </ul>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-book-open fa-fw"></i> <span class="hide-menu"> <?php echo 'Alamat' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'propinsi' ?>"><?php echo 'Propinsi' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'kota' ?>"><?php echo 'Kota' ?></a> </li>
  </ul>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-close fa-fw"></i> <span class="hide-menu"> <?php echo 'Pelanggaran' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'hukuman' ?>"><?php echo 'Hukuman' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'point_kesalahan' ?>"><?php echo 'Point Kesalahan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'kategori_pelanggaran' ?>"><?php echo 'Kategori Pelanggaran' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'pelanggaran' ?>"><?php echo 'Pelanggaran' ?></a> </li>  
  </ul>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-trophy fa-fw"></i> <span class="hide-menu"> <?php echo 'Penghargaan' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'prestasi' ?>"><?php echo 'Prestasi' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'bidang_penghargaan' ?>"><?php echo 'Bidang Penghargaan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'point_penghargaan' ?>"><?php echo 'Point Penghargaan' ?></a> </li>  
  </ul>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span class="hide-menu"> <?php echo 'Jadwal' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'izin' ?>"><?php echo 'Daftar Izin' ?></a> </li>  
  </ul>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-note fa-fw"></i> <span class="hide-menu"> <?php echo 'Laporan' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'laporan_izin' ?>"><?php echo 'Laporan Izin' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_kesalahan' ?>"><?php echo 'Laporan Kesalahan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_penghargaan' ?>"><?php echo 'Laporan Penghargaan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_sakit' ?>"><?php echo 'Laporan Sakit' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_psikologi' ?>"><?php echo 'Laporan Psikologi' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_taruna' ?>"><?php echo 'Laporan Login Taruna' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_wali' ?>"><?php echo 'Laporan Login Wali' ?></a> </li>  
  </ul>
 </li>

 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'log' ?>" aria-expanded="false"><i class="fa fa-group fa-fw"></i> <span class="hide-menu"> Log User Wali </span></a>
 </li>
<?php } ?>

