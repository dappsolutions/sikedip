<?php if ($this->session->userdata('hak_akses') == 'Wali Murid') { ?>
 <?php if (!empty($role[1]['wali'])) { ?>
  <?php if ($role[1]['wali']['status'] == 1) { ?>
   <li>
    <a class="active waves-effect" href="<?php echo base_url() . $role[1]['id'] ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> <?php echo $role[1]['nama_module'] ?> </span></a>
   </li>
  <?php } ?>
 <?php } ?>
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'Wali Murid') { ?>
 <?php if (!empty($role[20]['wali'])) { ?>
  <?php if ($role[20]['wali']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-user fa-fw"></i> <span class="hide-menu"> <?php echo $role[20]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[20]['wali'])) { ?>
      <?php if ($role[20]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[20]['id'] ?>"><?php echo $role[20]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[19]['wali'])) { ?>
      <?php if ($role[19]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[19]['id'] ?>"><?php echo $role[19]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[18]['wali'])) { ?>
      <?php if ($role[18]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[18]['id'] ?>"><?php echo $role[18]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>     
     <?php if (!empty($role[14]['wali'])) { ?>
      <?php if ($role[14]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[14]['id'] ?>"><?php echo $role[14]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>     
    </ul>
   </li>
  <?php } ?>
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Wali Murid') { ?>
 <?php if (!empty($role[2]['wali'])) { ?>
  <?php if ($role[2]['wali']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-close fa-fw"></i> <span class="hide-menu"> <?php echo $role[2]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[2]['wali'])) { ?>
      <?php if ($role[2]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[2]['id'] ?>"><?php echo $role[2]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if ($role[15]['wali']['status'] == 1) { ?>
      <?php if (!empty($role[15]['wali'])) { ?>
       <li> <a href="<?php echo base_url() . $role[15]['id'] ?>"><?php echo $role[15]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[7]['wali'])) { ?>
      <?php if ($role[7]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[7]['id'] ?>"><?php echo $role[7]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[12]['wali'])) { ?>
      <?php if ($role[12]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[12]['id'] ?>"><?php echo $role[12]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[8]['wali'])) { ?>
      <li> <a href="<?php echo base_url() . $role[8]['id'] ?>"><?php echo $role[8]['nama_module'] ?></a> </li>
     <?php } ?>
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Wali Murid') { ?>
 <?php if (!empty($role[13]['wali'])) { ?>
  <?php if ($role[13]['wali']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-trophy fa-fw"></i> <span class="hide-menu"> <?php echo $role[13]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[17]['wali'])) { ?>
      <?php if ($role[17]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[17]['id'] ?>"><?php echo $role[17]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[0]['wali'])) { ?>
      <?php if ($role[0]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[0]['id'] ?>"><?php echo $role[0]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[16]['wali'])) { ?>
      <?php if ($role[16]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[16]['id'] ?>"><?php echo $role[16]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[13]['wali'])) { ?>
      <?php if ($role[13]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[13]['id'] ?>"><?php echo $role[13]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'Wali Murid') { ?>
 <?php if (!empty($role[3]['wali'])) { ?>
  <?php if ($role[3]['wali']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span class="hide-menu"> <?php echo $role[3]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[3]['wali'])) { ?>
      <?php if ($role[3]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[3]['id'] ?>"><?php echo $role[3]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[4]['wali'])) { ?>
      <?php if ($role[4]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[4]['id'] ?>"><?php echo $role[4]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[5]['wali'])) { ?>
      <?php if ($role[5]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[5]['id'] ?>"><?php echo $role[5]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Wali Murid') { ?>
 <?php if (!empty($role[9]['wali'])) { ?>
  <?php if ($role[9]['wali']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-note fa-fw"></i> <span class="hide-menu"> <?php echo $role[9]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[9]['wali'])) { ?>
      <?php if ($role[9]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[9]['id'] ?>"><?php echo $role[9]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[10]['wali'])) { ?>
      <?php if ($role[10]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[10]['id'] ?>"><?php echo $role[10]['nama_module'] ?>  </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[11]['wali'])) { ?>
      <?php if ($role[11]['wali']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[11]['id'] ?>"><?php echo $role[11]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <li> <a href="<?php echo base_url() . 'laporan_sakit' ?>"><?php echo 'Laporan Sakit' ?></a> </li>  
     <li> <a href="<?php echo base_url() . 'laporan_psikologi' ?>"><?php echo 'Laporan Psikologi' ?></a> </li>  
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>     