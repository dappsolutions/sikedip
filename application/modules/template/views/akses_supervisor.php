<?php if ($this->session->userdata('hak_akses') == 'Supervisor') { ?>
 <li>
  <a class="active waves-effect" href="<?php echo base_url() . 'verifikasi' ?>" aria-expanded="false"><i class="icon-close fa-fw"></i> <span class="hide-menu"> Verifikasi Kesalahan </span></a>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-note fa-fw"></i> <span class="hide-menu"> <?php echo 'Laporan' ?> </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'laporan_izin' ?>"><?php echo 'Laporan Izin' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_kesalahan' ?>"><?php echo 'Laporan Kesalahan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_penghargaan' ?>"><?php echo 'Laporan Penghargaan' ?></a> </li>  
   <li> <a href="<?php echo base_url() . 'laporan_sakit' ?>"><?php echo 'Laporan Sakit' ?></a> </li>  
  </ul>
 </li>
<?php } ?>