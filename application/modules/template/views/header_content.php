<!-- ===== Top-Navigation ===== -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
 <div class="navbar-header">
  <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
   <i class="fa fa-bars"></i>
  </a>
  <div class="top-left-part">
   <a class="logo" href="<?php echo base_url() . 'dashboard' ?>">
    <b>
     <img src="<?php echo base_url() ?>assets/images/logo_taruna.jpg" alt="home" width="40" height="40" class="img-circle"/>
    </b>
    <span>
     <b><label class="bold" style="font-weight: bold;">SIKEDIP</label></b> 
    </span>
   </a>
  </div>
  <ul class="nav navbar-top-links navbar-left hidden-xs">
   <li>
    <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
   </li>
   <li>
    <form role="search" class="app-search hidden-xs">     
     <label class="text-white">SISTEM INFORMASI KEDISIPLINAN POLTEKTRANS SDP PALEMBANG</label>
    </form>
   </li>
  </ul>

  <ul class="nav navbar-top-links navbar-right pull-right">
   <li class="dropdown">
    <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
     <img class="img-circle" width="30" height="30" src="<?php echo base_url() . 'assets/images/header_taruna1.jpg' ?>"/>     
    </a>
   </li>
   <li class="dropdown">
    <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
     <img class="img-circle" width="30" height="30" src="<?php echo base_url() . 'assets/images/header_taruna2.jpg' ?>"/>     
    </a>
   </li>
   <li class="dropdown">
    <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
     <img class="img-circle" width="30" height="30" src="<?php echo base_url() . 'assets/images/header_taruna3.jpg' ?>"/>     
    </a>
   </li>
   <li class="dropdown">
    <a class="dropdown-toggle waves-effect waves-light font-20" data-toggle="dropdown" href="javascript:void(0);">
     <img class="img-circle" width="30" height="30" src="<?php echo base_url() . 'assets/images/header_taruna4.jpg' ?>"/>     
    </a>
   </li>
  </ul>
 </div>
</nav>
<!-- ===== Top-Navigation-End ===== -->