<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <?php if (!empty($role[1]['admin'])) { ?>
  <?php if ($role[1]['admin']['status'] == 1) { ?>
   <li>
    <a class="active waves-effect" href="<?php echo base_url() . $role[1]['id'] ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> <?php echo $role[1]['nama_module'] ?> </span></a>
   </li>
   <li>
    <a class="waves-effect" href="<?php echo base_url() . 'pengumuman' ?>" aria-expanded="false"><i class="icon-drawar fa-fw"></i> <span class="hide-menu"> <?php echo 'Pengumuman' ?> </span></a>
   </li>
  <?php } ?>
 <?php } ?>
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <?php if (!empty($role[20]['admin'])) { ?>
  <?php if ($role[20]['admin']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-user fa-fw"></i> <span class="hide-menu"> <?php echo $role[20]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[20]['admin'])) { ?>
      <?php if ($role[20]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[20]['id'] ?>"><?php echo $role[20]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[19]['admin'])) { ?>
      <?php if ($role[19]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[19]['id'] ?>"><?php echo $role[19]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[18]['admin'])) { ?>
      <?php if ($role[18]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[18]['id'] ?>"><?php echo $role[18]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>     
     <?php if (!empty($role[14]['admin'])) { ?>
      <?php if ($role[14]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[14]['id'] ?>"><?php echo $role[14]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>     
     <li> <a href="<?php echo base_url() . 'naik_semester' ?>"><?php echo 'Naik Semester' ?></a> </li>  
     <li> <a href="<?php echo base_url() . 'cetak_taruna' ?>"><?php echo 'Cetak Data Taruna' ?></a> </li>  
    </ul>    
   </li>
  <?php } ?>      
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <?php if (!empty($role[2]['admin'])) { ?>
  <?php if ($role[8]['admin']['status'] == 1 || $role[2]['admin']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-close fa-fw"></i> <span class="hide-menu"> <?php echo $role[2]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[2]['admin'])) { ?>
      <?php if ($role[2]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[2]['id'] ?>"><?php echo $role[2]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if ($role[15]['admin']['status'] == 1) { ?>
      <?php if (!empty($role[15]['admin'])) { ?>
       <li> <a href="<?php echo base_url() . $role[15]['id'] ?>"><?php echo $role[15]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[7]['admin'])) { ?>
      <?php if ($role[7]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[7]['id'] ?>"><?php echo $role[7]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[12]['admin'])) { ?>
      <?php if ($role[12]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[12]['id'] ?>"><?php echo $role[12]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[8]['admin'])) { ?>
      <li> <a href="<?php echo base_url() . $role[8]['id'] ?>"><?php echo $role[8]['nama_module'] ?></a> </li>
     <?php } ?>
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <?php if (!empty($role[13]['admin'])) { ?>
  <?php if ($role[13]['admin']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-trophy fa-fw"></i> <span class="hide-menu"> <?php echo $role[13]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[17]['admin'])) { ?>
      <?php if ($role[17]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[17]['id'] ?>"><?php echo $role[17]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[0]['admin'])) { ?>
      <?php if ($role[0]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[0]['id'] ?>"><?php echo $role[0]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[16]['admin'])) { ?>
      <?php if ($role[16]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[16]['id'] ?>"><?php echo $role[16]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[13]['admin'])) { ?>
      <?php if ($role[13]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[13]['id'] ?>"><?php echo $role[13]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <?php if (!empty($role[4]['admin'])) { ?>
  <?php if ($role[4]['admin']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span class="hide-menu"> <?php echo $role[3]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[3]['admin'])) { ?>
      <?php if ($role[3]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[3]['id'] ?>"><?php echo $role[3]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[4]['admin'])) { ?>
      <?php if ($role[4]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[4]['id'] ?>"><?php echo $role[4]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[5]['admin'])) { ?>
      <?php if ($role[5]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[5]['id'] ?>"><?php echo $role[5]['nama_module'] ?></a> </li>
      <?php } ?>
     <?php } ?>     
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <li>
  <a class="active waves-effect" href="<?php echo base_url() . 'taruna_sakit' ?>" aria-expanded="false"><i class="icon-arrow-right fa-fw"></i> <span class="hide-menu"> <?php echo 'Izin Sakit' ?> </span></a>
 </li>
 <li>
  <a class="active waves-effect" href="<?php echo base_url() . 'psikologi' ?>" aria-expanded="false"><i class="icon-people fa-fw"></i> <span class="hide-menu"> <?php echo 'Psikologi' ?> </span></a>
 </li>
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <?php if (!empty($role[9]['admin'])) { ?>
  <?php if ($role[9]['admin']['status'] == 1) { ?>
   <li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-note fa-fw"></i> <span class="hide-menu"> <?php echo $role[9]['parent'] ?> </span></a>
    <ul aria-expanded="false" class="collapse">     
     <?php if (!empty($role[9]['admin'])) { ?>
      <?php if ($role[9]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[9]['id'] ?>"><?php echo $role[9]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[10]['admin'])) { ?>
      <?php if ($role[10]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[10]['id'] ?>"><?php echo $role[10]['nama_module'] ?>  </a> </li>
      <?php } ?>
     <?php } ?>
     <?php if (!empty($role[11]['admin'])) { ?>
      <?php if ($role[11]['admin']['status'] == 1) { ?>
       <li> <a href="<?php echo base_url() . $role[11]['id'] ?>"><?php echo $role[11]['nama_module'] ?> </a> </li>
      <?php } ?>
     <?php } ?>
     <li> <a href="<?php echo base_url() . 'laporan_sakit' ?>"><?php echo 'Laporan Sakit' ?></a> </li>  
     <li> <a href="<?php echo base_url() . 'laporan_psikologi' ?>"><?php echo 'Laporan Psikologi' ?></a> </li>  
     <li> <a href="<?php echo base_url() . 'laporan_taruna' ?>"><?php echo 'Laporan Login Taruna' ?></a> </li>  
     <li> <a href="<?php echo base_url() . 'laporan_wali' ?>"><?php echo 'Laporan Login Wali' ?></a> </li>  
    </ul>
   </li>
  <?php } ?>     
 <?php } ?>     
<?php } ?>     

<?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
 <li>
  <a class="waves-effect" href="<?php echo base_url() . 'log' ?>" aria-expanded="false"><i class="fa fa-group fa-fw"></i> <span class="hide-menu"> Log User Wali </span></a>
 </li>
<?php } ?>

