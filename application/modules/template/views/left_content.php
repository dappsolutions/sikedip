<!-- ===== Left-Sidebar ===== -->
<aside class="sidebar">
    <div class="scroll-sidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div class="profile-image">
                    <img src="<?php echo base_url() ?>assets/images/users/images.png" alt="profile image" class="img-circle">
                    <a href="javascript:void(0);" class="dropdown-toggle u-dropdown text-blue" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="badge badge-danger">
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated flipInY">
                        <!--      <li><a href="javascript:void(0);"><i class="fa fa-user"></i> Profile</a></li>
      <li><a href="javascript:void(0);"><i class="fa fa-inbox"></i> Inbox</a></li>-->
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);" onclick="Template.changePassword(this, event)"><i class="fa fa-cog"></i> Change Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </div>
                <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> <?php echo strtoupper($this->session->userdata('hak_akses')) ?></a></p>
            </div>
        </div>
        <nav class="sidebar-nav">
            <ul id="side-menu">
                <?php echo $this->load->view('akses_admin'); ?>
                <?php echo $this->load->view('akses_wali'); ?>
                <?php echo $this->load->view('akses_taruna'); ?>
                <?php echo $this->load->view('akses_superadmin'); ?>
                <?php echo $this->load->view('akses_supervisor'); ?>


                <?php if ($this->session->userdata('hak_akses') == 'Manajemen') { ?>
                    <li>
                        <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-user fa-fw"></i> <span class="hide-menu"> <?php echo $role[20]['parent'] ?> </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li> <a href="<?php echo base_url() . 'cetak_taruna' ?>"><?php echo 'Cetak Data Taruna' ?></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-note fa-fw"></i> <span class="hide-menu"> <?php echo 'Laporan' ?> </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li> <a href="<?php echo base_url() . 'laporan_kesalahan' ?>">Laporan Kesalahan </a> </li>
                            <li> <a href="<?php echo base_url() . 'laporan_penghargaan' ?>">Laporan Penghargaan </a> </li>
                            <li> <a href="<?php echo base_url() . 'laporan_izin' ?>">Laporan Izin </a> </li>
                            <li> <a href="<?php echo base_url() . 'laporan_sakit' ?>"><?php echo 'Laporan Sakit' ?></a> </li>
                        </ul>
                    </li>
                <?php } ?>
                <li>
                    <a class="waves-effect" href="<?php echo base_url() . 'login/sign_out' ?>" aria-expanded="false"><i class="icon-power fa-fw"></i> <span class="hide-menu"> Log out </span></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
<!-- ===== Left-Sidebar-End ===== -->