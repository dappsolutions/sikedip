<input type='hidden' name='' id='hak_akses' class='form-control' value='<?php echo $this->session->userdata('hak_akses') ?>'/>
<div class="container-fluid">
 <div class='row'>
  <div class='col-md-12'>
   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <div class="row">
       <div class="col-md-12">
        <h3 class="text-warning">SISTEM INFORMASI KEDISIPLINAN POLTEKTRANS SDP PALEMBANG</h3>
       </div>
      </div>      
     </div>
    </div>
   </div>
   <br/>

   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <h4>Top <label class="badge badge-danger">5</label> Taruna Psikologi</h4>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table color-bordered-table success-bordered-table">
        <thead>
         <tr class="">
          <th class="font-12">No</th>
          <th class="font-12">No. Taruna</th>
          <th class="font-12">Nama Taruna</th>
          <th class="font-12">Tanggal</th>
          <th class="font-12">Keterangan</th>
          <!--<th class="text-center font-12">Action</th>-->
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($taruna_psikologi)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($taruna_psikologi as $value) { ?>
           <tr>
            <td class='font-12'><?php echo $no++ ?></td>
            <td class='font-12'><?php echo $value['no_taruna'] ?></td>
            <td class='font-12'><?php echo $value['nama'] ?></td>
            <td class='font-12'><?php echo $value['tanggal'] ?></td>
            <td class='font-12'><?php echo $value['keterangan'] ?></td>        
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
          </tr>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>

   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <h4>Top <label class="badge badge-danger">5</label> Taruna Sakit</h4>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table color-bordered-table success-bordered-table">
        <thead>
         <tr class="">
          <th class="font-12">No</th>
          <th class="font-12">No. Taruna</th>
          <th class="font-12">Nama</th>
          <th class="font-12">Tanggal</th>
          <th class="font-12">Jam</th>
          <th class="font-12">Keterangan</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($taruna_sakit)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($taruna_sakit as $value) { ?>
           <tr>
            <td class='font-12'><?php echo $no++ ?></td>
            <td class='font-12'><?php echo $value['no_taruna'] ?></td>
            <td class='font-12'><?php echo $value['nama'] ?></td>
            <td class='font-12'><?php echo $value['tanggal'] ?></td>
            <td class='font-12'><?php echo $value['jam_awal'] . ' - ' . $value['jam_akhir'] ?></td>
            <td class='font-12'><?php echo $value['keterangan'] ?></td>            
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
          </tr>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>

   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <h4>Top <label class="badge badge-danger">5</label> Penghargaan</h4>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table color-bordered-table success-bordered-table">
        <thead>
         <tr class="">
          <th class="font-12">No</th>
          <th class="font-12">No. Taruna</th>
          <th class="font-12">Nama</th>
          <!--<th class="font-12">Tanggal</th>-->
          <th class="font-12">Semester</th>
          <!--<th class="font-12">Prestasi</th>-->
          <th class="font-12">Skor</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($penghargaan)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($penghargaan as $value) { ?>
           <tr>
            <td class='font-12'><?php echo $no++ ?></td>
            <td class='font-12'><?php echo $value['no_taruna'] ?></td>
            <td class='font-12'><?php echo $value['nama_taruna'] ?></td>
            <!--<td class='font-12'><?php  echo $value['tanggal'] ?></td>-->
            <td class='font-12'><?php echo $value['semester'] ?></td>
            <!--<td class='font-12'><?php  echo $value['prestasi'] ?></td>-->
            <td class='font-12'><?php echo $value['skor'] ?></td>            
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
          </tr>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>

   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <h4>Top <label class="badge badge-danger">5</label> Kesalahan</h4>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table color-bordered-table success-bordered-table">
        <thead>
         <tr class="">
          <th class="font-12">No</th>
          <th class="font-12">No. Taruna</th>
          <th class="font-12">Nama</th>
          <!--<th class="font-12">Tanggal</th>-->
          <th class="font-12">Semester</th>
          <!--<th class="font-12">Jenis</th>-->
          <th class="font-12">Poin</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($kesalahan)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($kesalahan as $value) { ?>
           <tr>
            <td class='font-12'><?php echo $no++ ?></td>
            <td class='font-12'><?php echo $value['no_taruna'] ?></td>
            <td class='font-12'><?php echo $value['nama_taruna'] ?></td>
            <!--<td class='font-12'><?php echo Modules::run('helper/getIndoDate', $value['tanggal']) ?></td>-->
            <td class='font-12'><?php echo $value['semester'] ?></td>
            <!--<td class='font-12'><?php echo $value['jenis'] ?></td>-->
            <td class='font-12'><?php echo $value['point'] ?></td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
          </tr>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div> 
</div>