<?php

class Dashboard extends MX_Controller {

 public $hak_akses;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['penghargaan'] = $this->getDataPenghargaan();
  $data['kesalahan'] = $this->getDataKesalahan();
  $data['taruna_sakit'] = $this->getDataTarunaSakit();
  $data['taruna_psikologi'] = $this->getDataTarunaPsikologi();
//  echo '<pre>';
//  print_r($data['data_hadir']);
//  die;
  echo Modules::run('template', $data);
 }

 public function getDataTarunaSakit() {  
  $data = Modules::run('database/get', array(
              'table' =>  'taruna_sakit t',
              'field' => array('t.*', 'tt.no_taruna', 'tt.nama'),
              'join' => array(
                  array('taruna tt', 't.taruna = tt.id')
              ),
              'where' => "t.deleted = 0",
              'orderby'=> 't.id desc',
              'limit'=> 5
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal'] = $value['tanggal_awal'].' - '.$value['tanggal_akhir'];
    array_push($result, $value);
   }
  }

  return $result;
 }
 
 public function getDataTarunaPsikologi() {  
  $data = Modules::run('database/get', array(
              'table' => 'psikologi t',
              'field' => array('t.*', 'tt.no_taruna', 'tt.nama'),
              'join' => array(
                  array('taruna tt', 't.taruna = tt.id')
              ),
              'where' => "t.deleted = 0",
              'limit' => 5
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }
 
 public function getDataPenghargaan() {
//  $data = Modules::run('database/get', array(
//              'table' => 'penghargaan k',
//              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.prestasi',
//                  's.semester as semester_taruna', 'p.skor'),
//              'join' => array(
//                  array('taruna t', 'k.taruna = t.id'),
//                  array('semester s', 'k.semester = s.id'),
//                  array('prestasi p', 'k.prestasi = p.id'),
//              ),
//              'where' => "k.deleted = 0",
//              'orderby' => 'k.id desc',
//              'limit' => 5
//  ));
  
  $query = "select 
  SUM(p.skor) as skor
  , ph.taruna
  , t.no_taruna
  , t.nama as nama_taruna
  , ph.semester
  , pr.prodi
  from penghargaan ph
join prestasi p 
  on ph.prestasi = p.id
join taruna t
  on t.id = ph.taruna and t.deleted = 0
left join taruna_has_akademik tha
  on ph.taruna = tha.taruna and tha.status = 1 and tha.deleted = 0
left join prodi pr
  on pr.id = tha.prodi
where ph.deleted = 0
GROUP by ph.taruna, t.nama, t.no_taruna, ph.semester, pr.prodi
ORDER by skor DESC";

 $data = Modules::run('database/get_custom', $query, 5);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataKesalahan() {
//  $data = Modules::run('database/get', array(
//              'table' => 'kesalahan k',
//              'field' => array('k.*', 't.no_taruna', 't.nama', 'p.jenis',
//                  's.semester as semester_taruna', 'p.point'),
//              'join' => array(
//                  array('taruna t', 'k.taruna = t.id'),
//                  array('semester s', 'k.semester = s.id'),
//                  array('pelanggaran p', 'k.pelanggaran = p.id'),
//                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
//                  array('kesalahan_status ks', "st.id = ks.id", 'left')
//              ),
//              'where' => "k.deleted = 0 and (ks.status = 'APPROVED')",
//              'orderby' => "k.id desc",
//              'limit' => 5
//  ));
  $query = "        
		select SUM(p.point) as point, 
        k.taruna
        , t.no_taruna
        , t.nama as nama_taruna
        , k.semester
        , pr.prodi
            from kesalahan k 
        join pelanggaran p
            on p.id = k.pelanggaran
        left join (select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st
            on k.id = st.kesalahan
        left join kesalahan_status ks 
            on st.id = ks.id and ks.status = 'APPROVED'
        join taruna t 
            on t.id = k.taruna and t.deleted = 0
        left join taruna_has_akademik tha
            on k.taruna = tha.taruna and tha.status = 1 and tha.deleted = 0
        left join prodi pr
            on pr.id = tha.prodi
        where k.deleted = 0
        GROUP by k.taruna, t.nama, t.no_taruna,k.semester, pr.prodi
        ORDER by point DESC";

  $data = Modules::run('database/get_custom', $query, 5);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

}
