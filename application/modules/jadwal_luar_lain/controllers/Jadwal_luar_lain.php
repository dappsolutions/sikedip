<?php

class Jadwal_luar_lain extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'jadwal_luar_lain';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/jadwal_luar_lain.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'jadwal_luar_lain';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jadwal";
  $data['title_content'] = 'Data Jadwal';
  $content = $this->getDataJadwal();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataJadwal($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.tanggal_awal', $keyword),
       array('k.tanggal_akhir', $keyword),
       array('k.jam_awal', $keyword),
       array('k.jam_akhir', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'p.jenis', 'pr.prodi as program_studi',
                  's.semester as semester_taruna'),
              'join' => array(
                  array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
                  array('prodi pr', 'k.prodi = pr.id'),
                  array('semester s', 'k.semester = s.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "k.deleted = 0"
  ));

  return $total;
 }

 public function getDataJadwal($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('k.tanggal_awal', $keyword),
       array('k.tanggal_akhir', $keyword),
       array('k.jam_awal', $keyword),
       array('k.jam_akhir', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'p.jenis', 'pr.prodi as program_studi',
                  's.semester as semester_taruna'),
              'join' => array(
                  array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
                  array('prodi pr', 'k.prodi = pr.id'),
                  array('semester s', 'k.semester = s.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataJadwal($keyword)
  );
 }

 public function getDetailDataJadwal($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'p.jenis', 'pr.prodi as prodi_taruna',
                  's.semester as semester_taruna'),
              'join' => array(
                  array('jenis_jadwal_luar p', 'k.jenis_jadwal_luar = p.id'),
                  array('prodi pr', 'k.prodi = pr.id'),
                  array('semester s', 'k.semester = s.id'),
              ),
              'where' => "k.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListTaruna() {
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenis() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_jadwal_luar p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPrestasi() {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Jadwal";
  $data['title_content'] = 'Tambah Jadwal';
  $data['list_prodi'] = $this->getListProdi();
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_izin'] = $this->getListJenis();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataJadwal($id);
  $data['tanggal_awal'] = date('m/d/Y', strtotime($data['tanggal_awal']));
  $data['tanggal_akhir'] = date('m/d/Y', strtotime($data['tanggal_akhir']));
  $data['tanggal'] = $data['tanggal_awal'] . ' - ' . $data['tanggal_akhir'];
//  echo $data['tanggal'];die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Jadwal";
  $data['title_content'] = 'Ubah Jadwal';
  $data['list_taruna'] = $this->getListTaruna();
  $data['list_semester'] = $this->getListSemester();
  $data['list_prodi'] = $this->getListProdi();
  $data['list_izin'] = $this->getListJenis();
  $data['detail_jadwal'] = $this->getDetailTarunaJadwal($id);
  echo Modules::run('template', $data);
 }

 public function getDataProdiTaruna($taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('tha.*', 'p.prodi as program_studi'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id')
              ),
              'where' => "tha.deleted = 0 and tha.taruna = '" . $taruna . "' and tha.status = 1"
  ));

  return $data->row_array();
 }

 public function getDetailTarunaJadwal($id) {
  $data = Modules::run('database/get', array(
              'table' => 'jadwal_luar_has_taruna` jl',
              'field' => array('jl.*', 't.no_taruna', 't.nama',
                  'p.prodi as prodi_taruna', 's.semester as semester_taruna'),
              'join' => array(
                  array('taruna t', 'jl.taruna = t.id'),
                  array('prodi p', 'jl.prodi = p.id'),
                  array('semester s', 'jl.semester = s.id'),
              ),
              'where' => array('jl.jadwal_luar_lain' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataJadwal($id);

  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Jadwal";
  $data['title_content'] = "Detail Jadwal";
  $data['detail_jadwal'] = $this->getDetailTarunaJadwal($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataJadwal($value) {
  list($tgl_awal, $tgl_akhir) = explode('-', $value->tanggal);
  $tgl_awal = date('Y-m-d', strtotime($tgl_awal));
  $tgl_akhir = date('Y-m-d', strtotime($tgl_akhir));
  $data['jenis_jadwal_luar'] = $value->izin;
  $data['jam_awal'] = $value->jam;
  $data['jam_akhir'] = $value->jam_akhir;
  $data['keterangan'] = $value->keterangan;
  $data['prodi'] = $value->prodi;
  $data['semester'] = $value->semester;
  $data['tanggal_awal'] = $tgl_awal;
  $data['tanggal_akhir'] = $tgl_akhir;
  return $data;
 }

 public function getContentListTaruna($prodi) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna_has_akademik tha',
              'field' => array('t.*'),
              'join' => array(
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna t', 'tha.taruna = t.id'),
              ),
              'where' => "tha.deleted = 0 and tha.status = 1 and p.id = '" . $prodi . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_taruna'] = $result;
  echo $this->load->view('list_taruna', $conten, true);
 }

 public function getListJenisPrestasi($bidang) {
  $data = Modules::run('database/get', array(
              'table' => 'prestasi p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 and p.bidang_penghargaan = '" . $bidang . "'"
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $conten['list_prestasi'] = $result;
  echo $this->load->view('list_prestasi', $conten, true);
 }

 public function getIdTaruna($no_taruna) {
  $data = Modules::run('database/get', array(
              'table' => 'taruna',
              'where' => "no_taruna = '" . $no_taruna . "'"
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  }

  return $id;
 }

 public function getPostTarunaJadwal($value, $jadwal) {
  $data['jadwal_luar_lain'] = $jadwal;
  $data['taruna'] = $value->taruna;
  $data['prodi'] = $value->prodi;
  $data['semester'] = $value->semester;

  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post = $this->getPostDataJadwal($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);

    if (!empty($data->taruna)) {
     foreach ($data->taruna as $value) {
      $post = $this->getPostTarunaJadwal($value, $id);
      Modules::run('database/_insert', 'jadwal_luar_has_taruna', $post);
     }
    }
   } else {
    //updates
    if (!empty($data->taruna)) {
     Modules::run('database/_update', 'jadwal_luar_has_taruna', array('deleted' => true), array('jadwal_luar_lain' => $id));
     foreach ($data->taruna as $value) {
      $post = $this->getPostTarunaJadwal($value, $id);
      Modules::run('database/_insert', 'jadwal_luar_has_taruna', $post);
     }
    }

    if (!empty($data->taruna_edit)) {
     foreach ($data->taruna_edit as $value) {
      $post = $this->getPostTarunaJadwal($value, $id);
      if ($value->is_edit != 1) {
       $post['deleted'] = 0;
      }      
      Modules::run('database/_update', 'jadwal_luar_has_taruna', $post, array('id' => $value->id));
     }
    }
    
    $post = $this->getPostDataJadwal($data);
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jadwal";
  $data['title_content'] = 'Data Jadwal';
  $content = $this->getDataJadwal($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataTaruna($id = '') {
  $data = $this->getListDataTaruna($id);
  echo json_encode(array('data' => $data));
 }

 public function getListDataTaruna($id = '') {
  $keyword = $this->input->post('keyword');
  $where = array();
  if ($id != '') {
   $where = array('p.id' => $id);
  }

  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_taruna', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna p',
              'field' => array('p.*'),
              'where' => $where,
              'limit' => 5,
              'like' => $like,
              'is_or_like' => true
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataTarunaDetail() {
  $prodi = $this->input->post('prodi');
  $semester = $this->input->post('semester');
  if ($semester != '') {
   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' "
           . "and tha.semester = '" . $semester . "'";
  } else {
   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*'),
              'join' => array(
                  array('taruna_has_akademik tha', 'tha.taruna = t.id and tha.status = 1')
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['detail'] = $result;
  echo $this->load->view('list_taruna', $content, true);
 }

}
