<table class="table color-bordered-table info-bordered-table" id="laporan_izin">
 <thead>
  <tr class="">
   <th class="font-12">No</th>
   <th class="font-12">No. Taruna</th>
   <th class="font-12">Nama Taruna</th>
   <th class="font-12">Tempat Lahir</th>
   <th class="font-12">Tanggal Lahir</th>
   <th class="font-12">Golongan Darah</th>
   <th class="font-12">NIK</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($content)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($content as $value) { ?>
    <tr>
     <td class='font-12'><?php echo $no++ ?></td>
     <td class='font-12'><?php echo $value['no_taruna'] ?></td>
     <td class='font-12'><?php echo $value['nama'] ?></td>
     <td class='font-12'><?php echo $value['tempat_lahir'] ?></td>
     <td class='font-12'><?php echo $value['tanggal_lahir'] ?></td>
     <td class='font-12'><?php echo $value['gol_darah'] ?></td>
     <td class='font-12'><?php echo $value['no_ktp'] ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
   </tr>
  <?php } ?>         
 </tbody>
</table>