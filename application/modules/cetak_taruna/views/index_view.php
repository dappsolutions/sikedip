<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>     
    <div class='col-md-3'>
     <select id='jurusan' class="form-control">
      <?php if (!empty($list_jurusan)) { ?>
       <?php foreach ($list_jurusan as $value) { ?>
        <?php $selected = '' ?>
        <?php if (isset($semester)) { ?>
         <?php $selected = $value['id'] == $jurusan ? 'selected' : '' ?>
        <?php } ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['prodi'] ?></option>
       <?php } ?>
      <?php } else { ?>
       <option value="">Tidak Ada Data</option>  
      <?php } ?>
     </select>
    </div>     
    <div class='col-md-3'>
     <select id='semester' class="form-control">
      <option value="">Pilih Semester</option>  
      <?php if (!empty($list_semester)) { ?>
       <?php foreach ($list_semester as $value) { ?>
        <?php $selected = '' ?>
        <?php if (isset($semester)) { ?>
         <?php $selected = $value['id'] == $semester ? 'selected' : '' ?>
        <?php } ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo 'Semester ' . $value['semester'] ?></option>
       <?php } ?>
      <?php } else { ?>
       <option value="">Tidak Ada Data</option>  
      <?php } ?>
     </select>
    </div>    
    <div class="col-md-3">
     <input type="text" value="" id="angkatan" class="form-control" placeholder="Angkatan"/>
    </div>
    <div class='col-md-3'>
     <select id='propinsi' class="form-control" onchange="CetakTaruna.getKotaData(this)">
      <option value="">Pilih Propinsi</option>  
      <?php if (!empty($list_propinsi)) { ?>
       <?php foreach ($list_propinsi as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['propinsi'] ?></option>
       <?php } ?>
      <?php } else { ?>
       <option value="">Tidak Ada Data</option>  
      <?php } ?>
     </select>
    </div>     
   </div>   
   <br/>   
   <div class="row">
    <div class="col-md-3">
     <select class="form-control" error="Kota" id='kota'>
      <option value="">Pilih Kota</option>
      <?php if (!empty($list_kota)) { ?>
       <?php foreach ($list_kota as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> propinsi='<?php echo $value['propinsi'] ?>' value="<?php echo $value['id'] ?>"><?php echo $value['nama_kota'] ?></option>
       <?php } ?>
      <?php } ?>
     </select>
    </div>

    <div class="col-md-3">
     <select class="form-control required" id='asal' error="Asal Sekolah">
      <option value="">Pilih Asal</option>
      <?php if (!empty($list_asal)) { ?>
       <?php foreach ($list_asal as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['sekolah'] ?></option>
       <?php } ?>
      <?php } ?>
     </select>
    </div>

    <div class="col-md-3">
     <select class="form-control" id='kompi'>
      <option value="">Pilih Kompi</option>
      <?php if (!empty($list_kompi)) { ?>
       <?php foreach ($list_kompi as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
       <?php } ?>
      <?php } ?>
     </select>
    </div>

    <div class="col-md-3">
     <select class="form-control" id='asrama'>
      <option value="">Pilih Asrama</option>
      <?php if (!empty($list_asrama)) { ?>
       <?php foreach ($list_asrama as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_asrama'] ?></option>
       <?php } ?>
      <?php } ?>
     </select>
    </div>
   </div>
   <br/>

   <div class="row">
    <div class="col-md-3">
     <select class="form-control required" id='seperasuhan' error="Seperasuhan">
      <option value="">Pilih Seperasuhan</option>
      <?php if (!empty($list_seperasuhan)) { ?>
       <?php foreach ($list_seperasuhan as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['seperasuhan'] ?></option>
       <?php } ?>
      <?php } ?>
     </select>
    </div>
   </div>
   <br/>

   <div class="row">
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="CetakTaruna.tampilkan()">Tampilkan</button>
    </div>
    <div class='col-md-2'>
     <a class="btn btn-block btn-success" download="<?php echo 'data_taruna' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'laporan_izin', 'DATA TARUNA');">Export</a>
    </div>
    <div class='col-md-2'>
     <button class="btn btn-block btn-danger" onclick="CetakTaruna.cetakData()">Cetak</button>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive" id="table_izin">
      <table class="table color-bordered-table info-bordered-table" id="laporan_izin">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No. Taruna</th>
         <th class="font-12">Nama Taruna</th>
         <th class="font-12">Tempat Lahir</th>
         <th class="font-12">Tanggal Lahir</th>
         <th class="font-12">Golongan Darah</th>
         <th class="font-12">NIK</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['no_taruna'] ?></td>
           <td class='font-12'><?php echo $value['nama'] ?></td>
           <td class='font-12'><?php echo $value['tempat_lahir'] ?></td>
           <td class='font-12'><?php echo $value['tanggal_lahir'] ?></td>
           <td class='font-12'><?php echo $value['gol_darah'] ?></td>
           <td class='font-12'><?php echo $value['no_ktp'] ?></td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php //echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>