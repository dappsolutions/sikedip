<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
  <meta charset="utf-8">
  <title><?php echo 'TARUNA'; ?></title>
  <link rel="icon" type="image/png" href="<?php echo base_url() . 'assets/images/logo_taruna.jpb' ?>" />

  <style>
   body {
    font-family: "Helvetica", sans-serif;
    font-size: 11px;
   }
   table {
    width: 100%;
   }
   .text-center {
    text-align: center;
   }
   .text-right {
    text-align: right;
   }
   .font-bold {
    font-weight: bold;
   }
   .mb-32px {
    margin-bottom: 32px;
   }
   .mr-8px {
    margin-right: 8px;
   }
   .ml-8px {
    margin-left: 8px;
   }
   .table-logo {
    width: 100%;
   }
   table.table-logo > tbody > tr > td {
    padding: 16px;
   }
   table.table-logo td {
    vertical-align: top;
   }
   .table-item {
    width: 100%;
    margin-top: 16px;
    border-collapse: collapse;
    font-size: 8px;
   }
   table.table-item th,  table.table-item td {
    border: 1px solid #333;
    padding: 4px 8px;
    border-collapse: collapse;
    vertical-align: top;
   }
   .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
   }
   .media-body {
    -ms-flex: 1;
    flex: 1;
   }

   .none-border{
    border:none;
   }
  </style>
 </head>
 <body>
  <table class="table-logo">
   <tbody>
    <tr>
     <td style="width: 50%">
      <table style="margin-bottom: 8px">
       <tr>
        <td width="40"><img src="<?php echo base_url() ?>assets/images/logo_taruna.jpg" alt="" width="40"></td>         
        <td style="padding: 16px;"><b>POLTEKTRANS SDP PALEMBANG</b></td>
       </tr>
<!--       <tr>
        <td>
         <div></div>
         <div class="font-bold" style="">   POLTEKTRANS SDP PALEMBANG</div>
        </td>
       </tr>-->
      </table>
     </td>
    </tr>
   </tbody>
  </table>
  <table class="table-item">   
   <tbody>
    <tr>
     <td colspan="6" style="border:none;"><b><u>TARUNA</u></b></td>
    </tr>
    <tr class="">
     <th class="font-12">No</th>
     <th class="font-12">No. Taruna</th>
     <th class="font-12">Nama Taruna</th>
     <th class="font-12">Tempat Lahir</th>
     <th class="font-12">Tanggal Lahir</th>
     <th class="font-12">Golongan Darah</th>
     <th class="font-12">NIK</th>
    </tr>
    <?php if (!empty($data)) { ?>
     <?php $no = 1; ?>
     <?php foreach ($data as $value) { ?>
      <tr>
       <td class='font-12'><?php echo $no++ ?></td>
       <td class='font-12'><?php echo $value['no_taruna'] ?></td>
       <td class='font-12'><?php echo $value['nama'] ?></td>
       <td class='font-12'><?php echo $value['tempat_lahir'] ?></td>
       <td class='font-12'><?php echo $value['tanggal_lahir'] ?></td>
       <td class='font-12'><?php echo $value['gol_darah'] ?></td>
       <td class='font-12'><?php echo $value['no_ktp'] ?></td>
      </tr>
     <?php } ?>
    <?php } else { ?>
     <tr>
      <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
     </tr>
    <?php } ?>  
   </tbody>
  </table>
  <br/>

  <table style="width: 100%; margin-top: 32px">
   <tbody>
    <tr>
     <td class="text-center">Poltektrans Sdp Palembang,</td>
     <td class="text-center">&nbsp;</td>
    </tr>
    <tr>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
    </tr>
    <tr>
     <td class="text-center">(------------------------------)</td>
    </tr>
   </tbody>
  </table>
 </body>
</html>
