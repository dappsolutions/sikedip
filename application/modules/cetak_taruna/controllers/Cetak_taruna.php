<?php

class Cetak_taruna extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'cetak_taruna';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/cetak_taruna.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'taruna';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Cetak Data Taruna";
  $data['title_content'] = 'Cetak Data Taruna';
  $data['list_semester'] = $this->getListSemester();
  $content = $this->getDataTaruna();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['list_jurusan'] = $this->getListProdi();
  $data['list_propinsi'] = $this->getListPropinsi();
  $data['list_kota'] = $this->getListKota();
  $data['list_asal'] = $this->getListAsalSekolah();
  $data['list_kompi'] = $this->getListKompi();
  $data['list_asrama'] = $this->getListAsrama();
  $data['list_seperasuhan'] = $this->getListSeperasuhan();

  echo Modules::run('template', $data);
 }

 public function getTotalDataTaruna() {
  $total = Modules::run('database/count_all', array(
              'table' => 'taruna t',
              'field' => array('t.*', 's.semester as semester_taruna'),
              'join' => array(
                  array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1 and tha.deleted = 0'),
                  array('semester s', 's.id = tha.semester'),
                  array('prodi p', 'tha.prodi = p.id'),
              ),
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataTaruna($date = '') {
  if (!empty($_POST)) {
   $semester = $this->input->post('semester');
   $prodi = $this->input->post('prodi');
   $angkatan = $this->input->post('angkatan');
   $propinsi = $this->input->post('propinsi');
   $kota = $this->input->post('kota');
   $asal = $this->input->post('asal');
   $kompi = $this->input->post('kompi');
   $asrama = $this->input->post('asrama');
   $seperasuhan = $this->input->post('seperasuhan');


   $where = "t.deleted = 0";

//   if ($prodi != '') {
//    $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "'";
//   }
//
//   if ($prodi != '' && ($semester != '' and $semester != '0')) {
//    $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' and tha.semester = '" . $semester . "'";
//   }
//
//   if ($prodi != '' && $angkatan != '') {
//    $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' and tha.angkatan = '" . $angkatan . "'";
//   }
//
//   if ($prodi != '' && ($semester != '' and $semester != '0') && $angkatan != '') {
//    $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' "
//            . "and tha.semester = '" . $semester . "' and tha.angkatan = '" . $angkatan . "'";
//   }

   $like = array(
       array('tha.prodi', $prodi),
       array('tha.semester', $semester),
       array('tha.angkatan', $angkatan),
       array('t.kota', $kota),
       array('t.propinsi', $propinsi),
       array('t.asal_sekolah', $asal),
       array('t.kompi', $kompi),
       array('t.seperasuhan', $seperasuhan),
       array('ths.asrama', $asrama),
   );
//   echo $where;die;
   $data = Modules::run('database/get', array(
               'table' => 'taruna t',
               'field' => array('t.*', 's.semester as semester_taruna'),
               'join' => array(
                   array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1 and tha.deleted = 0'),
                   array('semester s', 's.id = tha.semester'),
                   array('prodi p', 'tha.prodi = p.id'),
                   array('taruna_has_asrama ths', 't.id = ths.taruna and ths.deleted = 0'),
               ),
               'like' => $like,
               'is_or_like' => $like,
               'inside_brackets' => true,
               'where' => $where
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => 'taruna t',
               'field' => array('t.*', 's.semester as semester_taruna'),
               'join' => array(
                   array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1 and tha.deleted = 0'),
                   array('semester s', 's.id = tha.semester'),
                   array('prodi p', 'tha.prodi = p.id'),
               ),
               'where' => "t.deleted = 0"
   ));
  }

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataTaruna($date)
  );
 }

 public function getListSemester() {
  $data = Modules::run('database/get', array(
              'table' => 'semester s',
              'field' => array('s.*'),
              'where' => "s.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdi() {
  $data = Modules::run('database/get', array(
              'table' => 'prodi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPropinsi() {
  $data = Modules::run('database/get', array(
              'table' => 'propinsi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKota() {
  $data = Modules::run('database/get', array(
              'table' => 'kota',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListAsalSekolah() {
  $data = Modules::run('database/get', array(
              'table' => 'asal_sekolah',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListKompi() {
  $data = Modules::run('database/get', array(
              'table' => 'kompi',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListAsrama() {
  $data = Modules::run('database/get', array(
              'table' => 'asrama',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSeperasuhan() {
  $data = Modules::run('database/get', array(
              'table' => 'seperasuhan',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataTaruna($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Taruna";
  $data['title_content'] = "Detail Taruna";
  $data['data_prodi'] = $this->getDataProdiTaruna($data['taruna']);
  echo Modules::run('template', $data);
 }

 public function tampilkan() {
  $data['content'] = $this->getDataTaruna()['data'];
  echo $this->load->view('data_laporan', $data, true);
 }

 public function cetak($prodi, $semester, $angkatan, $propinsi,
         $kota, $asal, $kompi, $asrama, $seperasuhan) {
  $where = "t.deleted = 0";

//  if ($prodi != '') {
//   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "'";
//  }
//
//  if ($prodi != '' && ($semester != '' && $semester != '0')) {
//   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' and tha.semester = '" . $semester . "'";
//  }
//
//  if ($prodi != '' && ($angkatan != '' && $angkatan != '0' )) {
//   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' and tha.angkatan = '" . $angkatan . "'";
//  }
//
//  if ($prodi != '' && ($semester != '' and $semester != '0') && ($angkatan != '' && $angkatan != '0' )) {
//   $where = "t.deleted = 0 and tha.prodi = '" . $prodi . "' "
//           . "and tha.semester = '" . $semester . "' and tha.angkatan = '" . $angkatan . "'";
//  }
//   echo $where;die;

  $like = array(
       array('tha.prodi', $prodi),
       array('tha.semester', $semester),
       array('tha.angkatan', $angkatan),
       array('t.kota', $kota),
       array('t.propinsi', $propinsi),
       array('t.asal_sekolah', $asal),
       array('t.kompi', $kompi),
       array('t.seperasuhan', $seperasuhan),
       array('ths.asrama', $asrama),
   );
  $data = Modules::run('database/get', array(
              'table' => 'taruna t',
              'field' => array('t.*', 's.semester as semester_taruna'),
              'join' => array(
                  array('taruna_has_akademik tha', 't.id = tha.taruna and tha.status = 1 and tha.deleted = 0'),
                  array('semester s', 's.id = tha.semester'),
                  array('prodi p', 'tha.prodi = p.id'),
                  array('taruna_has_asrama ths', 't.id = ths.taruna and ths.deleted = 0'),
              ),
              'like'=> $like, 
              'is_or_like'=> true,
              'inside_brackets'=> true,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $mpdf = Modules::run('mpdf/getInitPdf');
  $content['data'] = $result;
  $view = $this->load->view('cetak_taruna', $content, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('DATA-TARUNA.pdf', 'I');
 }

}
