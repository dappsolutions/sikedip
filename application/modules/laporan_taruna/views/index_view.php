<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>         
    <div class='col-md-3'>
     <select id='status' class="form-control">   
      <option value="">Semua</option>  
      <option value="aktif">Aktif</option>  
      <option value="nonaktif">Tidak Aktif</option>  
     </select>
    </div>     
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="LapTaruna.tampilkan()">Tampilkan</button>
    </div>   
    <div class='col-md-2'>
     <a class="btn btn-block btn-success" download="<?php echo 'laporan_login_taruna' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'laporan', 'Laporan Login Taruna');">Export</a>
    </div>
   </div>   
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive" id="table_laporan">
      <table class="table color-bordered-table info-bordered-table" id="laporan">
       <thead>
        <tr>
         <td colspan="8">Taruna Login Aplikasi Mobile</td>
        </tr>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">No. Taruna</th>
         <th class="font-12">Nama</th>
         <th class="font-12">Tanggal</th>
         <th class="font-12">Jam</th>
         <th class="font-12">Status</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['no_taruna'] ?></td>
           <td class='font-12'><?php echo $value['nama'] ?></td>
           <td class='font-12'><?php echo $value['tanggal'] ?></td>
           <td class='font-12'><?php echo $value['jam'] ?></td>
           <td class='font-12'>
            <label class="label <?php echo $value['label_color'] ?>"><?php echo $value['status'] ?></label>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="9">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">

     </div>
    </div>
   </div>
  </div>
 </div>
</div>