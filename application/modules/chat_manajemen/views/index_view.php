<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="ChatManajemen.add()">Tambah</button>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="ChatManajemen.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table success-bordered-table">
       <thead>
        <tr class="">
         <th class="font-12">No</th>
         <th class="font-12">Dari</th>
         <th class="font-12">Pesan</th>
         <th class="text-center font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no']+1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class='font-12'><?php echo $no++ ?></td>
           <?php $from = '' ?>
           <?php $color = 'label-warning' ?>
           <?php if ($value['nama_wali'] == '') { ?>
            <?php $from = "Taruna" ?>
            <?php $color = 'label-success' ?>
           <?php } else { ?>
            <?php $from = "Wali" ?>
           <?php } ?>
           <td class='font-12'><?php echo $value['username'] . ' <label class="font-10 label ' . $color . '">' . $from . '</label>' ?></td>
           <td class='font-12'><?php echo $value['message'] ?></td>
           <td class="text-center">
            <label id="" class="label label-danger font-10 hover" 
                   onclick="ChatManajemen.delete('<?php echo $value['id'] ?>')">Hapus</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>