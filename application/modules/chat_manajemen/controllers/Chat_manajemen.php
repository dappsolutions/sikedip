<?php

class Chat_manajemen extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'chat_manajemen';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/chat_manajemen.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'chat_manajemen';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Chat";
  $data['title_content'] = 'Data Chat';
  $content = $this->getDataChat();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataChat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('t.message', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'ta.nama as nama_taruna', 'thw.nama as nama_wali'),
              'join' => array(
                  array('user u', 't.from = u.id'),
                  array('taruna ta', 'u.id = ta.user', 'left'),
                  array('taruna_has_wali_murid thw', 'u.id = thw.user', 'left'),
              ),
              'where' => "t.deleted = 0 and t.to = 5"
  ));

  return $total;
 }

 public function getDataChat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.username', $keyword),
       array('t.message', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'u.username', 'ta.nama as nama_taruna', 'thw.nama as nama_wali'),
              'join' => array(
                  array('user u', 't.from = u.id'),
                  array('taruna ta', 'u.id = ta.user', 'left'),
                  array('taruna_has_wali_murid thw', 'u.id = thw.user', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0 and t.to = 5",
              'orderby'=> "t.tgl_kirim desc"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataChat($keyword)
  );
 }

 public function getDetailDataChat($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'p.hak_akses'),
              'join' => array(
                  array('priveledge p', 't.priveledge = p.id')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListHakAkses() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge js',
              'field' => array('js.*'),
              'where' => "js.id != 2 and js.id != 3"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Chat";
  $data['title_content'] = 'Tambah Chat';
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataChat($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Chat";
  $data['title_content'] = 'Ubah Chat';
  $data['list_hak_akses'] = $this->getListHakAkses();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataChat($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Chat";
  $data['title_content'] = "Detail Chat";
  echo Modules::run('template', $data);
 }

 public function getPostDataChat($value) {
  $data['username'] = $value->username;
  $data['password'] = $value->password;
  $data['priveledge'] = $value->hak_akses;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_prodi = $this->getPostDataChat($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prodi);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_prodi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Chat";
  $data['title_content'] = 'Data Chat';
  $content = $this->getDataChat($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
