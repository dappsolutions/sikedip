<?php

class Sakit extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'sakit';
  }

  public function getDataIzinTaruna()
  {
    $taruna = $this->input->post('taruna');
    // $taruna = '4';
    $data = Modules::run('database/get', array(
      'table' => 'taruna_sakit ts',
      'field' => array(
        'ts.id', 't.nama as nama_taruna', 't.no_taruna',
        'ts.tanggal_awal', 'ts.tanggal_akhir', 'ts.jam_awal', 'ts.jam_akhir', 'ts.keterangan'
      ),
      'join' => array(
        array('taruna t', 'ts.taruna = t.id'),
      ),
      'where' => "ts.taruna = '" . $taruna . "' and (ts.tanggal_awal >= '" . date('Y-m-d') . "' or '" . date('Y-m-d') . "' <= ts.tanggal_akhir) and ts.deleted = 0",
      'orderby' => "ts.id desc",
      'limit' => 5
    ));


    // echo '<pre>';
    // echo $this->db->last_query();die;
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal_awal"] = Modules::run("helper/getIndoDate", $value["tanggal_awal"]);
        $value["tanggal_akhir"] = Modules::run("helper/getIndoDate", $value["tanggal_akhir"]);
        $value['tanggal'] = $value['tanggal_awal'] . ' - ' . $value['tanggal_akhir'];
        $value['jam'] = $value['jam_awal'] . ' - ' . $value['jam_akhir'];
        array_push($result, $value);
      }
    }


    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getDataIzinTarunaList()
  {
    $taruna = $this->input->post('taruna');
    // $taruna = '4';
    $data = Modules::run('database/get', array(
      'table' => 'taruna_sakit ts',
      'field' => array(
        'ts.id', 't.nama as nama_taruna', 't.no_taruna',
        'ts.tanggal_awal', 'ts.tanggal_akhir', 'ts.jam_awal', 'ts.jam_akhir', 'ts.keterangan'
      ),
      'join' => array(
        array('taruna t', 'ts.taruna = t.id'),
      ),
      'where' => "ts.taruna = '" . $taruna . "' and ts.deleted = 0",
      'orderby' => "ts.id desc",
      'limit' => 5
    ));


    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal_awal"] = Modules::run("helper/getIndoDate", $value["tanggal_awal"]);
        $value["tanggal_akhir"] = Modules::run("helper/getIndoDate", $value["tanggal_akhir"]);
        $value['tanggal'] = $value['tanggal_awal'] . ' - ' . $value['tanggal_akhir'];
        $value['jam'] = $value['jam_awal'] . ' - ' . $value['jam_akhir'];
        array_push($result, $value);
      }
    }


    $post['user'] = $this->input->post('user');
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = 'Izin Sakit';
    Modules::run("database/_insert", 'log_histori', $post);
    echo json_encode(array(
      'data' => $result
    ));
  }
}
