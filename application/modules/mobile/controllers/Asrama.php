<?php

class Asrama extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'asrama';
  }

  public function getDataAsramaTaruna()
  {
    $taruna = $this->input->post('taruna');
  //  $taruna = '235';
    $data = Modules::run('database/get', array(
      'table' => 'taruna t',
      'field' => array('t.*', 'a.nama_asrama', 'k.kamar as kamar_taruna', 'a.id as asrama', 'k.id as kamar_id'),
      'join' => array(
        array('taruna_has_asrama tha', 't.id = tha.taruna and (tha.deleted = 0)'),
        array('asrama a', 'a.id = tha.asrama'),
        array('kamar k', 'k.id = tha.kamar'),
      ),
      'where' => "t.id = '" . $taruna . "'",
      'limit' => 5
    ));


    // echo '<pre>';
    // echo $this->db->last_query();die;

    $asrama = "-";
    $kamar_taruna = '-';
    $result = array();
    if (!empty($data)) {
      $asrama = $data->row_array()['nama_asrama'];
      $kamar_taruna = $data->row_array()['kamar_taruna'];
      $result = $this->getDataListPenghuniAsrama($data->row_array()['asrama'], $data->row_array()['kamar_id']);
    }

    $post['user'] = $this->input->post('userId');
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = 'Asrama';
    Modules::run("database/_insert", 'log_histori', $post);
    echo json_encode(array(
      'data' => $result,
      'asrama' => $asrama,
      'kamar_taruna' => $kamar_taruna
    ));
  }

  public function getDataListPenghuniAsrama($asrama, $kamar)
  {
    $data = Modules::run('database/get', array(
      'table' => 'taruna t',
      'field' => array('t.*', 'a.nama_asrama', 'k.kamar as kamar_taruna', 'a.id as asrama', 'p.prodi', 's.semester'),
      'join' => array(
        array('taruna_has_asrama tha', 't.id = tha.taruna and (tha.deleted = 0)'),
        array('asrama a', 'a.id = tha.asrama'),
        array('kamar k', 'k.id = tha.kamar'),
        array('taruna_has_akademik thk', 'tha.taruna = thk.taruna and thk.status = 1 and thk.deleted = 0', 'left'),
        array('prodi p', 'thk.prodi = p.id', 'left'),
        array('semester s', 'thk.semester = s.id', 'left'),
      ),
      'where' => "tha.asrama = '" . $asrama . "' and tha.kamar = '".$kamar."'",
      'orderby' => "t.no_taruna asc",
      'limit' => 5
    ));


    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['nama_taruna'] = $value['nama'];
        array_push($result, $value);
      }
    }


    return $result;
  }
}
