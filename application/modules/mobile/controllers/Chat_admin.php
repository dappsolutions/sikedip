<?php

class Chat_admin extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'chat_admin';
  }

  public function getDataChat()
  {
    $user = $this->input->post('user');
    $table = $this->input->post('table');
    $kompi = $this->input->post('kompi');

    $where = "ca.from = '" . $user . "' or ca.to = '" . $user . "'";
    if ($table == 'chat_admin') {
      $where = "(ca.from = '" . $user . "' or ca.to = '" . $user . "') and ca.kompi = '" . $kompi . "'";
    }
    $data = Modules::run('database/get', array(
      'table' => $table . ' ca',
      'field' => array('ca.*', 'p.hak_akses'),
      'join' => array(
        array('user u', 'u.id = ca.from'),
        array('priveledge p', 'u.priveledge = p.id'),
      ),
      'where' => $where,
      'orderby' => 'ca.id asc'
    ));

    // echo '<pre>';
    // echo $this->db->last_query();die;
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    $post['user'] = $user;
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = 'Chat Admin';  
    Modules::run("database/_insert", 'log_histori', $post);
    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getDataChatManajemen()
  {
    $user = $this->input->post('user');
    $data = Modules::run('database/get', array(
      'table' => 'chat_manajemen ca',
      'field' => array('ca.*', 'p.hak_akses'),
      'join' => array(
        array('user u', 'u.id = ca.from'),
        array('priveledge p', 'u.priveledge = p.id'),
      ),
      'where' => "ca.from = '" . $user . "' or ca.to = '" . $user . "'",
      'orderby' => 'ca.id asc'
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    $post['user'] = $user;
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = 'Chat Manajemen';  
    Modules::run("database/_insert", 'log_histori', $post);
    echo json_encode(array(
      'data' => $result
    ));
  }

  public function sendMessage()
  {
    $post_data["message"] = $this->input->post("message");
    // $post_data["message"] = 'tes';
    $post_data["from"] = $this->input->post("from");
    // $post_data["from"] = '7';

    $post_data["from_label"] = $this->input->post("from_label");
    // $post_data["from_label"] = 'dodik';
    $hak_akses = $this->input->post('hak_akses');
    // $hak_akses = 'Taruna';
    if ($hak_akses == 'Admin' || $hak_akses == 'Manajemen') {
      $post_data["to"] = $this->input->post("to");
    } else {
      //hak akses
      $post_data["to"] = 1;
    }

    if ($hak_akses != 'Manajemen') {
      $post_data["kompi"] = $this->input->post("kompi");
      // $post_data["kompi"] = 1;
    }
    $post_data["tgl_kirim"] = date('Y-m-d H:i:s');

    // echo '<pre>';
    // print_r($post_data);die;
    $table = $this->input->post("table");
    // $table = 'chat_admin';
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run("database/_insert", $table, $post_data);      
      $this->db->trans_commit();
      $is_valid = true;
    } catch (\Throwable $th) {
      $is_valid = false;
      $this->db->trans_rollback();
    }


    echo json_encode(array(
      "is_valid" => $is_valid
    ));
  }

  public function sendMessageManajemen()
  {
    $post_data["message"] = $this->input->post("message");
    $post_data["from"] = $this->input->post("from");
    $post_data["from_label"] = $this->input->post("from_label");
    //hak akses
    $post_data["to"] = 5;
    $post_data["tgl_kirim"] = date('Y-m-d H:i:s');

    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run("database/_insert", 'chat_manajemen', $post_data);
      $this->db->trans_commit();
      $is_valid = true;
    } catch (\Throwable $th) {
      $is_valid = false;
      $this->db->trans_rollback();
    }

    echo json_encode(array(
      "is_valid" => $is_valid
    ));
  }

  public function getListChatMasuk()
  {
    $table = $this->input->post("table");
    $to = $this->input->post("to");
    $kompi = $this->input->post("kompi");
    $data = Modules::run('database/get', array(
      'table' => $table . ' ca',
      'field' => array('ca.*', 'u.username', 't.foto', 't.nama'),
      'join' => array(
        array('user u', 'ca.from = u.id'),        
        array('taruna t', 'u.id = t.user'),
      ),
      'where' => "ca.to = '" . $to . "' and t.kompi = '".$kompi."'",
      'orderby' => "ca.id desc"
    ));

    $result = array();
    if (!empty($data)) {
      $temp = array();
      foreach ($data->result_array() as $value) {
        if (!in_array($value['from'], $temp)) {
          $value['foto'] = str_replace(' ', '_', $value['foto']);
          $value['foto'] = base_url() . 'files/berkas/taruna/' . $value['foto'];
          array_push($result, $value);
          $temp[] = $value['from'];
        }
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getListChatMasukWali()
  {
    $table = $this->input->post("table");
    $to = $this->input->post("to");
    $kompi = $this->input->post("kompi");
    $data = Modules::run('database/get', array(
      'table' => $table . ' ca',
      'field' => array('ca.*', 'u.username', 't.nama'),
      'join' => array(
        array('user u', 'ca.from = u.id'),
        array('taruna_has_wali_murid t', 'u.id = t.user'),
        array('taruna tt', 't.taruna = tt.id'),
      ),
      'where' => "ca.to = '" . $to . "' and tt.kompi = '".$kompi."'",
      'orderby' => "ca.id desc"
    ));

    $result = array();
    if (!empty($data)) {
      $temp = array();
      foreach ($data->result_array() as $value) {
        if (!in_array($value['from'], $temp)) {
          $value['foto'] = "";
          array_push($result, $value);
          $temp[] = $value['from'];
        }
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }
}
