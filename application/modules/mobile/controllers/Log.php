<?php

class Log extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'log';
  }
  
  public function simpanLog($aktifitas, $user){
    $post['user'] = $user;
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = $aktifitas;
    Modules::run("database/_insert", 'log_histori', $post);
  }
}
