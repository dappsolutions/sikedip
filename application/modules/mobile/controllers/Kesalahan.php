<?php

class Kesalahan extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'kesalahan';
 }

 public function getDataStatusKesalahan($jumlah) {
  $data = Modules::run('database/get', array(
              'table' => 'point_kesalahan',
              'where' => array('deleted' => 0)
  ));


  $status = "";
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $nilai = explode('-', $value['angka']);
    if (count($nilai) > 1) {
     $atas = $nilai[1];
     $bawah = $nilai[0];
     if ($jumlah <= $atas && $jumlah >= $bawah) {
      $status = $value['kondite'];
      break;
     }
    } else {
     if ($jumlah <= $nilai[0]) {
      $status = $value['kondite'];
      break;
     }
    }
   }
  }

  return $status;
 }

 public function getDataKesalahanTaruna() {
  $taruna = $this->input->post('taruna');
  //  $taruna = 4;
  $data = Modules::run('database/get', array(
              'table' => 'kesalahan k',
              'field' => "sum(p.point) as jumlah",
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'where' => array(
                  'k.taruna' => $taruna,
                  'ks.status' => 'APPROVED',
                  'k.deleted' => 0,
              )
  ));

  $jumlah = 0;
  $status = '';
  if (!empty($data)) {
   $jumlah = $data->row_array()['jumlah'];
   $jumlah = $jumlah == null ? 0 : $jumlah;
   $status = $this->getDataStatusKesalahan($jumlah);
  }

  $post['user'] = $this->input->post('user');
  $post['tanggal'] = date('Y-m-d');
  $post['jam'] = date('H:i:s');
  $post['aktifitas'] = 'Dashboard';
  Modules::run("database/_insert", 'log_histori', $post);

  //   Modules::run('log/simpanLog', 'Dashboard', $this->input->post('user'));

  echo json_encode(array(
      'jumlah' => $jumlah,
      'status' => $status
  ));
 }

 public function getTotalPoinDataKesalahanTaruna($taruna) {
  //  $taruna = 4;
  $data = Modules::run('database/get', array(
              'table' => 'kesalahan k',
              'field' => "sum(p.point) as jumlah",
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'where' => array(
                  'k.taruna' => $taruna,
                  'ks.status' => 'APPROVED',
                  'k.deleted' => 0,
              )
  ));

  $jumlah = 0;
  $status = '';
  if (!empty($data)) {
   $jumlah = $data->row_array()['jumlah'];
   $jumlah = $jumlah == null ? 0 : $jumlah;
   // $status = $this->getDataStatusKesalahan($jumlah);
  }

  //   Modules::run('log/simpanLog', 'Dashboard', $this->input->post('user'));

  return $jumlah;
 }

 public function getDataKesalahan() {
  $taruna = $this->input->post('taruna');
  $data = Modules::run('database/get', array(
              'table' => 'kesalahan k',
              'field' => array(
                  'k.id', 'p.jenis', 'p.point', 'k.tanggal', 't.nama as nama_taruna', 'k.semester',
                  'k.keterangan'
              ),
              'join' => array(
                  array('taruna t', 'k.taruna = t.id'),
                  array('pelanggaran p', 'k.pelanggaran = p.id'),
                  array('(select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st', 'k.id = st.kesalahan', 'left'),
                  array('kesalahan_status ks', "st.id = ks.id", 'left')
              ),
              'where' => array(
                  'k.taruna' => $taruna,
                  'k.deleted' => 0,
                  'ks.status' => 'APPROVED'
              ),
              'orderby' => 'k.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
    array_push($result, $value);
   }
  }

  $post['user'] = $this->input->post('user');
  $post['tanggal'] = date('Y-m-d');
  $post['jam'] = date('H:i:s');
  $post['aktifitas'] = 'Kesalahan';
  Modules::run("database/_insert", 'log_histori', $post);
  echo json_encode(array(
      'data' => $result
  ));
 }

 public function getDataKesalahanTopTen() {
  // $taruna = 4;
  $query = "        
		select SUM(p.point) as point, 
        k.taruna        
        , t.nama as nama_taruna
        , k.semester
        , pr.prodi
            from kesalahan k 
        join pelanggaran p
            on p.id = k.pelanggaran
        left join (select max(id) as id, kesalahan from kesalahan_status GROUP by kesalahan) st
            on k.id = st.kesalahan
        left join kesalahan_status ks 
            on st.id = ks.id and ks.status = 'APPROVED'
        join taruna t 
            on t.id = k.taruna and t.deleted = 0
        left join taruna_has_akademik tha
            on k.taruna = tha.taruna and tha.status = 1 and tha.deleted = 0
        left join prodi pr
            on pr.id = tha.prodi
        where k.deleted = 0
        GROUP by k.taruna, t.nama, k.semester, pr.prodi
        ORDER by point DESC";

  $data = Modules::run('database/get_custom', $query, 10);
  $result = array();

  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    // $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
    $value['keterangan'] = $this->getDataStatusKesalahan($value['point']);
    array_push($result, $value);
   }
  }

  // echo '<pre>';
  // print_r($result);die;
  $post['user'] = $this->input->post('user');
  $post['tanggal'] = date('Y-m-d');
  $post['jam'] = date('H:i:s');
  $post['aktifitas'] = 'Top 10 Kesalahan';
  // Modules::run("database/_insert", 'log_histori', $post);
  echo json_encode(array(
      'data' => $result,
  ));
 }

}
