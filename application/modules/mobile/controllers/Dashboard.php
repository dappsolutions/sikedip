<?php

class Dashboard extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getModuleName()
    {
        return 'dashboard';
    }

    public function panicBtnActive()
    {
        $kompi = $this->input->post('kompi');
        $token = $this->getDataToken($kompi);
// echo '<pre>';
// print_r($_POST);die;
        $lat = $this->input->post("lat");
        $lng = $this->input->post("lng");
        $user = $this->input->post("user");
        $taruna = $this->input->post("taruna");
        $no_telp = $this->input->post("no_hp");
        $kompi = $this->input->post("kompi");

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(

            'registration_ids' => $token,

            'notification' => array(

                'title' => "Emergency",

                'body' => $taruna." telah menekan tombol emergency segera hubungi no telp. ".$no_telp,
            ),
            'data' => array(
				'title'=> 'panic',
                'lat'=> $lat,
                'lng'=> $lng,
                'no_hp'=> $no_telp
            )

        );

        // echo '<pre>';
        // print_r($fields);die;
        $headers = array(
            'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
            'Content-Type:application/json'

        );



        // echo json_encode($fields);die;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);


        // echo 'asdad';die;
        // echo '<pre>';
        // print_r($result);die;

        if ($result === false) {

            die('Curl failed: ' . curl_error($ch));
        }

        Modules::run("database/_insert", "tombol_action", array(
           'user' => $user,
           'tombol'=> $taruna." telah menekan tombol emergency segera hubungi no telp. ".$no_telp,
        ));
        curl_close($ch);
    }

    public function getDataToken($kompi)
    {

        $data = Modules::run('database/get', array(

            'table' => 'user u',
            'field' => array('distinct(u.token) as token'),
            'join' => array(

                array('priveledge p', 'u.priveledge = p.id'),
                array('admin_kompi ak', 'u.id = ak.user', 'left'),
                array('taruna t', 'u.id = t.user', 'left'),

            ),

            'where' => "(u.deleted = 0 and u.token != '')"

        ));



        $result = array();

        if (!empty($data)) {

            foreach ($data->result_array() as $value) {

                array_push($result, $value['token']);
            }
        }

// echo '<pre>';
// print_r($result);die;
        return $result;
    }


    function tesMap(){
        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=-7.2899079,112.7392342&destination=-8.039228,112.1804662&sensor=false&key=AIzaSyDRjkdMDx94jbFdDKloaSAgTbS1Q2fByng";
       
        // $url = "https://maps.googleapis.com/maps/api/directions/json?origin=Toledo&destination=Madrid&region=es&key=AIzaSyDRjkdMDx94jbFdDKloaSAgTbS1Q2fByng";
        $data = file_get_contents($url);
        $data = json_decode($data);
        $routes = $data->routes;        

        // echo '<pre>';
        // print_r($fields);die;
        // $headers = array(
        //     'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
        //     'Content-Type:application/json'

        // );



        // echo json_encode($fields);die;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);

        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);


        // echo 'asdad';die;
        echo '<pre>';
        print_r($result);die;

        if ($result === false) {

            die('Curl failed: ' . curl_error($ch));
        }
    }
}
