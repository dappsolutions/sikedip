<?php

class Password extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'password';
  }

  public function changePassword()
  {
    $user_id = $this->input->post('user');
    $password_lama = $this->input->post('password_lama');
    $password_baru = $this->input->post('password_baru');


    $is_valid = false;
    $message = "Gagal";
    //update password
    if ($password_baru == $password_lama) {
      $is_valid = false;
      $message = "Password Tidak Boleh Sama";
    } else {
      $password_baru = $password_baru;
      Modules::run('database/_update', 'user', array('password' => $password_baru), array('id' => $user_id));
      $is_valid = true;
    }

    echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
  }
}
