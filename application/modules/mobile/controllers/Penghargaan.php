<?php

class Penghargaan extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'penghargaan';
 }

 public function getDataStatusPenghargaan($jumlah) {
  $data = Modules::run('database/get', array(
              'table' => 'point_penghargaan',
              'where' => array('deleted' => 0),
              'orderby' => 'point asc'
  ));

  $status = "";
  if (!empty($data)) {
   $i = 1;
   foreach ($data->result_array() as $value) {
    $nilai = $value['point'];
    if ($jumlah <= $nilai) {
     $status = $value['kondite'];
     break;
    } else {
     if ($i == count($data->result_array())) {
      $status = $value['kondite'];
      break;
     }
    }
    $i += 1;
   }
  }

  return $status;
 }

 public function getDataPenghargaanTaruna() {
  $taruna = $this->input->post('taruna');
  //$taruna = 5;
  $data = Modules::run('database/get', array(
              'table' => 'penghargaan ph',
              'field' => "sum(p.skor) as jumlah",
              'join' => array(
                  array('taruna t', 'ph.taruna = t.id'),
                  array('prestasi p', 'ph.prestasi = p.id'),
              ),
              'where' => array(
                  'ph.taruna' => $taruna,
                  'ph.deleted'=> 0
              )
  ));

  $jumlah = 0;
  $status = '';
  if (!empty($data)) {
   $jumlah = $data->row_array()['jumlah'];
   $jumlah = $jumlah == null ? 0 : $jumlah;
   $status = $this->getDataStatusPenghargaan($jumlah);
  }


  echo json_encode(array(
      'jumlah' => $jumlah,
      'status' => $status
  ));
 }

 public function getTotapPoinPenghargaanTaruna($taruna) {
  //$taruna = 5;
  $data = Modules::run('database/get', array(
              'table' => 'penghargaan ph',
              'field' => "sum(p.skor) as jumlah",
              'join' => array(
                  array('taruna t', 'ph.taruna = t.id'),
                  array('prestasi p', 'ph.prestasi = p.id'),
              ),
              'where' => array(
                  'ph.taruna' => $taruna,
                  'ph.deleted'=> 0
              )
  ));

  $jumlah = 0;
  $status = '';
  if (!empty($data)) {
   $jumlah = $data->row_array()['jumlah'];
   $jumlah = $jumlah == null ? 0 : $jumlah;
   // $status = $this->getDataStatusPenghargaan($jumlah);
  }


  return $jumlah;
 }

 public function getDataPenghargaan() {
  $taruna = $this->input->post('taruna');
  $data = Modules::run('database/get', array(
              'table' => 'penghargaan ph',
              'field' => array(
                  'ph.id', 'p.prestasi', 'p.skor', 'ph.tanggal', 't.nama as nama_taruna', 'ph.semester',
                  'ph.keterangan'
              ),
              'join' => array(
                  array('taruna t', 'ph.taruna = t.id'),
                  array('prestasi p', 'ph.prestasi = p.id'),
              ),
              'where' => array(
                  'ph.taruna' => $taruna,
                  'ph.deleted'=> 0
              ),
              'orderby' => "ph.id desc"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
    array_push($result, $value);
   }
  }

  $post['user'] = $this->input->post('user');
  $post['tanggal'] = date('Y-m-d');
  $post['jam'] = date('H:i:s');
  $post['aktifitas'] = 'Penghargaan';
  Modules::run("database/_insert", 'log_histori', $post);

  echo json_encode(array(
      'data' => $result
  ));
 }

 public function getDataPenghargaanTopTen() {
  //   $taruna = 4;
  $query = "select 
  SUM(p.skor) as skor
  , ph.taruna
  , t.nama as nama_taruna
  , ph.semester
  , pr.prodi
  from penghargaan ph
join prestasi p 
  on ph.prestasi = p.id
join taruna t
  on t.id = ph.taruna and t.deleted = 0
left join taruna_has_akademik tha
  on ph.taruna = tha.taruna and tha.status = 1 and tha.deleted = 0
left join prodi pr
  on pr.id = tha.prodi
where ph.deleted = 0
GROUP by ph.taruna, t.nama, ph.semester, pr.prodi
ORDER by skor DESC";

 $data = Modules::run('database/get_custom', $query, 10);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    //  $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
     $value['keterangan'] = $this->getDataStatusPenghargaan($value['skor']);
     array_push($result, $value);
   }
  }

  $post['user'] = $this->input->post('user');
  $post['tanggal'] = date('Y-m-d');
  $post['jam'] = date('H:i:s');
  $post['aktifitas'] = 'Top 10 Penghargaan';
  Modules::run("database/_insert", 'log_histori', $post);

  echo json_encode(array(
      'data' => $result,
  ));
 }

}
