<?php

class Login extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'login';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/controllers/login.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Login";
  $data['title_content'] = 'Login';
  echo $this->load->view('index', $data, true);
 }

 public function checkDataUser($username, $password) {
  $data = Modules::run('database/get', array(
  'table' => 'user u',
  'field'=> array('u.*', 'p.hak_akses', 'tr.foto as foto_taruna', 'tr.nama', 
  'tr.id as taruna_id', 'thw.taruna as taruna_id_wali', 'thw.nama as nama_wali', 
  'tr.no_hp', 'ttw.foto as foto_taruna_wali', 'ak.kompi as kompi_admin', 
  'tr.kompi as kompi_taruna', 
  'ttw.nama as nama_taruna_wali', 
  'ttw.kompi as kompi_wali'),
  'join' => array(
  array('priveledge p', 'u.priveledge = p.id'),
  array('taruna tr', 'u.id = tr.user and tr.deleted = 0', 'left'),
  array('taruna_has_wali_murid thw', 'thw.user = u.id', 'left'),
  array('taruna ttw', 'thw.taruna = ttw.id', 'left'),
  array('admin_kompi ak', 'u.id = ak.user', 'left'),
  ),
  'where' => array(
  'u.username' => $username,
  'u.password' => $password,
  'u.deleted' => 0
  )
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   if($result['foto_taruna'] != ''){
    $result['foto_taruna'] = str_replace(' ', '_', $result['foto_taruna']);
    $result['foto_taruna'] = base_url(). 'files/berkas/taruna/'.$result['foto_taruna'];
   }else{
     if($result['foto_taruna_wali'] != ''){
      $result['foto_taruna_wali'] = str_replace(' ', '_', $result['foto_taruna_wali']);
      $result['foto_taruna'] = base_url(). 'files/berkas/taruna/'.$result['foto_taruna_wali'];
     }
   }
  }
  
  echo '<pre>';
  print_r($result);die;
 }

 public function getDataUserDb($username, $password) {
  $data = Modules::run('database/get', array(
  'table' => 'user u',
  'field'=> array('u.*', 'p.hak_akses', 'tr.foto as foto_taruna', 'tr.nama', 
  'tr.id as taruna_id', 'thw.taruna as taruna_id_wali', 'thw.nama as nama_wali', 
  'tr.no_hp', 'ttw.foto as foto_taruna_wali', 'ak.kompi as kompi_admin', 'tr.kompi as kompi_taruna', 
  'ttw.nama as nama_taruna_wali', 
  'ttw.kompi as kompi_wali'),
  'join' => array(
  array('priveledge p', 'u.priveledge = p.id'),
  array('taruna tr', 'u.id = tr.user and tr.deleted = 0', 'left'),
  array('taruna_has_wali_murid thw', 'thw.user = u.id', 'left'),
  array('taruna ttw', 'thw.taruna = ttw.id', 'left'),
  array('admin_kompi ak', 'u.id = ak.user', 'left'),
  ),
  'where' => array(
  'u.username' => $username,
  'u.password' => $password,
  'u.deleted' => 0
  )
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   if($result['foto_taruna'] != ''){
    $result['foto_taruna'] = str_replace(' ', '_', $result['foto_taruna']);
    $result['foto_taruna'] = str_replace('.png_', '_png_', $result['foto_taruna']);
    $result['foto_taruna'] = base_url(). 'files/berkas/taruna/'.$result['foto_taruna'];
   }else{
     if($result['foto_taruna_wali'] != ''){
      $result['foto_taruna_wali'] = str_replace(' ', '_', $result['foto_taruna_wali']);
      $result['foto_taruna_wali'] = str_replace('.png_', '_png_', $result['foto_taruna_wali']);
      $result['foto_taruna'] = base_url(). 'files/berkas/taruna/'.$result['foto_taruna_wali'];
     }
   }
  }
  
  return $result;
 }

 public function getDataPegawai($username, $password) {
  $data = Modules::run('database/get', array(
  'table' => 'user u',
  'field' => array('u.*', 'pv.priveledge as hak_akses'),
  'join' => array(
  array('priveledge pv', 'u.priveledge = pv.id')
  ),
  'where' => array(
  'u.username' => $username,
  'u.password' => $password,
  'u.is_active' => true
  )
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getDataUser($username, $password) {
  $data = $this->getDataUserDb($username, $password);
//  echo '<pre>';
//  print_r($data);
//  die;
  $result = array();
  if (!empty($data)) {
   $result = $data;
  }
//  else{
//   $data = $this->getDataPegawai($username, $password);
//   if(!empty($data)){
//    $result = $data;
//   }   
//  }

  return $result;
 }

 public function signIn() {
  $username = $this->input->post('username');
  // $username = "eko";
  $password = $this->input->post('password');
  // $password = "1234";

  $is_valid = false;
  $foto = '';
  $nama = "";
  $taruna = "";
  $user_id = "";
  $hak_akses = "";
  $no_hp = "";
  $kompi = "";
  $taruna_nama_wali = "";
  try {
   $data = $this->getDataUser($username, $password);
  //  echo '<pre>';
  //  print_r($data);die;
   if (!empty($data)) {
    $foto = $data['foto_taruna'];
    $nama = $data['nama'];
    $taruna = $data['taruna_id'];
    $no_hp = $data['no_hp'];
    $taruna_nama_wali = $data['nama_taruna_wali'];
    if($taruna == ''){
      $taruna = $data['taruna_id_wali'];
    }

    if($nama == ''){
      $nama = $data['nama_wali'];
    }
    
    if($data['nama'] == '' && $data['nama_wali'] == ''){
      $nama = $data['username'];
    }

    if($data['kompi_admin'] != ''){
      $kompi = $data['kompi_admin'];
    }
    
    if($data['kompi_taruna'] != ''){
      $kompi = $data['kompi_taruna'];
    }
    
    if($data['kompi_wali'] != ''){
      $kompi = $data['kompi_wali'];
    }

    $user_id = $data['id'];
    $hak_akses = $data['hak_akses'];
    $is_valid = true;
   }
  } catch (Exception $exc) {
   $is_valid = false;
  }
  
  echo json_encode(array('is_valid' => $is_valid, 
  'nama'=> $nama, 'foto'=> $foto, 'taruna'=> $taruna, 
  "user_id"=> $user_id, 'hak_akses'=> $hak_akses, 'no_hp'=> $no_hp, 
  'kompi'=> $kompi, 'taruna_nama_wali'=> $taruna_nama_wali));
 }

 public function setSessionData($data) {
  $session['user_id'] = $data['id'];
  $session['username'] = $data['username'];
  $session['hak_akses'] = $data['hak_akses'];
  $session['priveledge'] = $data['priveledge'];
  $this->session->set_userdata($session);
 }

 public function sign_out() {
  $this->session->sess_destroy();
  redirect(base_url());
 }

}
