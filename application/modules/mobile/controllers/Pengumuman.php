<?php

class Pengumuman extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'pengumuman';
 }

 public function getData() {
  $data = Modules::run('database/get', array(
              'table' => 'pengumuman',
              'where' => array('deleted' => 0)
  ));


  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  echo json_encode(array(
	  'data'=> $result
  ));
 }
}
