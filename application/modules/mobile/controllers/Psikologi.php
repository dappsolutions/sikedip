<?php

class Psikologi extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'psikologi';
  }

  public function getDataPsikologiTaruna()
  {
    $taruna = $this->input->post('taruna');
    // $taruna = '4';
    $data = Modules::run('database/get', array(
      'table' => 'psikologi ps',
      'field' => array(
        'ps.id', 't.nama as nama_taruna', 't.no_taruna',
        'ps.jam', 'ps.tanggal', 'ps.keterangan'
      ),
      'join' => array(
        array('taruna t', 'ps.taruna = t.id'),
      ),
      'where' => "ps.taruna = '" . $taruna . "' and (ps.tanggal >= '" . date('Y-m-d') . "') and ps.deleted = 0",
      'orderby' => "ps.id desc",
      'limit' => 5
    ));


    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
        array_push($result, $value);
      }
    }


    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getDataPsikologiTarunaList()
  {
    $taruna = $this->input->post('taruna');
    // $taruna = '4';
    $data = Modules::run('database/get', array(
      'table' => 'psikologi ps',
      'field' => array(
        'ps.id', 't.nama as nama_taruna', 't.no_taruna',
        'ps.jam', 'ps.tanggal', 'ps.keterangan'
      ),
      'join' => array(
        array('taruna t', 'ps.taruna = t.id'),
      ),
      'where' => "ps.taruna = '" . $taruna . "' and ps.deleted = 0",
      'orderby' => "ps.id desc",
      'limit' => 5
    ));


    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal"] = Modules::run("helper/getIndoDate", $value["tanggal"]);
        array_push($result, $value);
      }
    }


    $post['user'] = $this->input->post('user');
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = 'Psikologi';
    Modules::run("database/_insert", 'log_histori', $post);
    echo json_encode(array(
      'data' => $result
    ));
  }
}
