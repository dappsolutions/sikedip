<?php

class Token extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getModuleName()
    {
        return 'token';
    }

    public function setToken()
    {
        $data["token"] = $this->input->post("token");
        $user = $this->input->post("user");

        Modules::run("database/_update", "user", $data, array("id" => $user));
    }
}
