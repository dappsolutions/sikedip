<?php

class Izin extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'izin';
  }

  public function getDataIzinTaruna()
  {
    $taruna = $this->input->post('taruna');
    $data = Modules::run('database/get', array(
      'table' => 'jadwal_luar jl',
      'field' => array(
        'jl.id', 't.nama as nama_taruna', 'jl.semester',
        'jl.tanggal_awal', 'jl.tanggal_akhir', 'jl.jam_awal', 'jl.jam_akhir', 'jjl.jenis', 'jl.keterangan'
      ),
      'join' => array(
        array('taruna t', 'jl.taruna = t.id'),
        array('jenis_jadwal_luar jjl', 'jl.jenis_jadwal_luar = jjl.id'),
      ),
      'where' => array(
        'jl.taruna' => $taruna,
      ),
      'orderby' => "jl.id desc"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal_awal"] = Modules::run("helper/getIndoDate", $value["tanggal_awal"]);
        $value["tanggal_akhir"] = Modules::run("helper/getIndoDate", $value["tanggal_akhir"]);
        $value['tanggal'] = $value['tanggal_awal'] . ' - ' . $value['tanggal_akhir'];
        $value['jam'] = $value['jam_awal'] . ' - ' . $value['jam_akhir'];
        array_push($result, $value);
      }
    }

    $data = Modules::run('database/get', array(
      'table' => 'jadwal_luar_lain jl',
      'field' => array(
        'jl.id', 't.nama as nama_taruna', 'jl.semester',
        'jl.tanggal_awal', 'jl.tanggal_akhir', 'jl.jam_awal', 'jl.jam_akhir', 'jjl.jenis', 'jl.keterangan'
      ),
      'join' => array(
        array('jadwal_luar_has_taruna ths', 'jl.id = ths.jadwal_luar_lain'),
        array('taruna t', 'ths.taruna = t.id'),
        array('jenis_jadwal_luar jjl', 'jl.jenis_jadwal_luar = jjl.id'),
      ),
      'where' => "t.id = '" . $taruna . "' and jl.deleted = 0",
      'orderby' => "jl.id desc"
    ));
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value["tanggal_awal"] = Modules::run("helper/getIndoDate", $value["tanggal_awal"]);
        $value["tanggal_akhir"] = Modules::run("helper/getIndoDate", $value["tanggal_akhir"]);
        $value['tanggal'] = $value['tanggal_awal'] . ' - ' . $value['tanggal_akhir'];
        $value['jam'] = $value['jam_awal'] . ' - ' . $value['jam_akhir'];
        array_push($result, $value);
      }
    }


    $post['user'] = $this->input->post('user');
    $post['tanggal'] = date('Y-m-d');
    $post['jam'] = date('H:i:s');
    $post['aktifitas'] = 'Izin';
    Modules::run("database/_insert", 'log_histori', $post);
    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getIzinTarunaData()
  {
    $taruna = $this->input->post('taruna');
    // $taruna =  	141;

    $data = Modules::run('database/get', array(
      'table' => 'jadwal_luar jl',
      'field' => array(
        'jl.id', 't.nama as nama_taruna', 'jl.semester',
        'jl.tanggal_awal', 'jl.tanggal_akhir', 'jl.jam_awal', 'jl.jam_akhir', 'jjl.jenis', 'jl.keterangan'
      ),
      'join' => array(
        array('taruna t', 'jl.taruna = t.id'),
        array('jenis_jadwal_luar jjl', 'jl.jenis_jadwal_luar = jjl.id'),
      ),
      'where' => "jl.taruna = '" . $taruna . "' and (jl.tanggal_awal >= '" . date('Y-m-d') . "' or '" . date('Y-m-d') . "' <= jl.tanggal_akhir) and jl.deleted = 0",
      'orderby' => "jl.id desc"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['jam'] = $value['jam_awal'] . ' - ' . $value['jam_akhir'];
        array_push($result, $value);
      }
    }

    $data = Modules::run('database/get', array(
      'table' => 'jadwal_luar_lain jl',
      'field' => array(
        'jl.id', 't.nama as nama_taruna', 'jl.semester',
        'jl.tanggal_awal', 'jl.tanggal_akhir', 'jl.jam_awal', 'jl.jam_akhir', 'jjl.jenis', 'jl.keterangan'
      ),
      'join' => array(
        array('jadwal_luar_has_taruna ths', 'jl.id = ths.jadwal_luar_lain'),
        array('taruna t', 'ths.taruna = t.id'),
        array('jenis_jadwal_luar jjl', 'jl.jenis_jadwal_luar = jjl.id'),
      ),
      'where' => "t.id = '" . $taruna . "' and (jl.tanggal_awal >= '" . date('Y-m-d') . "' or '" . date('Y-m-d') . "' <= jl.tanggal_akhir) and jl.deleted = 0",
      'orderby' => "jl.id desc"
    ));
    if (!empty($data)) {      
      foreach ($data->result_array() as $value) {
        $value['jam'] = $value['jam_awal'] . ' - ' . $value['jam_akhir'];
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }
}
