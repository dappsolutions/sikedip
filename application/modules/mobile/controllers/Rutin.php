<?php

class Rutin extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'rutin';
 }

 public function getDataRutin() {
  $data = Modules::run('database/get', array(
              'table' => 'jadwal_rutin jr',
              'field' => ('jr.*'),
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  echo json_encode(array(
      'data' => $result
  ));
 }

}
