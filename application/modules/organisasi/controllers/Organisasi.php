<?php

class Organisasi extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'organisasi';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/organisasi.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'organisasi';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Organisasi";
  $data['title_content'] = 'Data Organisasi';
  $content = $this->getDataOrganisasi();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataOrganisasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.nama_Organisasi', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "u.deleted = 0"
  ));

  return $total;
 }

 public function getDataOrganisasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.nama_Organisasi', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "u.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataOrganisasi($keyword)
  );
 }

 public function getDetailDataOrganisasi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Organisasi";
  $data['title_content'] = 'Tambah Organisasi';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataOrganisasi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Organisasi";
  $data['title_content'] = 'Ubah Organisasi';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataOrganisasi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Organisasi";
  $data['title_content'] = 'Detail Organisasi';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama_organisasi'] = $value->nama_organisasi;
  $data['email'] = $value->email;
  $data['no_telp'] = $value->no_telp;
  $data['jalan'] = $value->jalan;
  $data['kelurahan'] = $value->kelurahan;
  $data['kota'] = $value->kota;
  $data['kecamatan'] = $value->kecamatan;
  $data['provinsi'] = $value->provinsi;
  $data['koordinat_map'] = $value->koordinat;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $Organisasi = $id;
  $this->db->trans_begin();
  try {
   $post_Organisasi = $this->getPostDataHeader($data);
   if ($id == '') {
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post_Organisasi['foto'] = $_FILES['file']['name'];
    }
    $Organisasi = Modules::run('database/_insert', $this->getTableName(), $post_Organisasi);
   } else {
    //update
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post_Organisasi['foto'] = $_FILES['file']['name'];
    }
    Modules::run('database/_update', $this->getTableName(), $post_Organisasi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'Organisasi' => $Organisasi));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/organisasi/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Organisasi";
  $data['title_content'] = 'Data Organisasi';
  $content = $this->getDataOrganisasi($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function simpanLokasi($id = '') {
  $lokasi = $this->input->post('lokasi');

  $is_valid = false;
  if ($id == '') {
   $id = $this->session->userdata('masjid_id');
  }
  $this->db->trans_begin();
  try {

   Modules::run('database/_update', $this->getTableName(), array('koordinat_map' => $lokasi), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function getKotaList($provinsi) {
  $data['list_kota'] = $this->getDataKota($provinsi);
  echo $this->load->view('list_kota', $data, true);
 }

 public function getKecamatan($kota) {
  $data['list_kec'] = $this->getDataKecamatan($kota);
  echo $this->load->view('list_kecamatan', $data, true);
 }

 public function getPropinsi() {
  $url = "http://dev.farizdotid.com/api/daerahindonesia/provinsi";
  $curl = curl_init();
  curl_setopt_array($curl, array(
//  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);


  $propinsi = json_decode($response);
  $data_propinsi = array();
  if (!empty($propinsi)) {
   $data_propinsi = $propinsi->semuaprovinsi;
  }

  return $data_propinsi;
 }

 public function getDataKota($provinsi) {
  $url = "http://dev.farizdotid.com/api/daerahindonesia/provinsi/" . $provinsi . "/kabupaten";
  $curl = curl_init();
  curl_setopt_array($curl, array(
//  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);


  $kota = json_decode($response);

  $data_kota = $kota->kabupatens;

  return $data_kota;
 }

 public function getDataKecamatan($kab) {
  $url = "http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/" . $kab . "/kecamatan";
  $curl = curl_init();
  curl_setopt_array($curl, array(
//  CURLOPT_URL => "http://api.rajaongkir.com/starter/province",
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);


  $kecamatan = json_decode($response);

  $data_kec = $kecamatan->kecamatans;

  return $data_kec;
 }

}
