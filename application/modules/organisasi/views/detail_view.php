<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box stat-widget">
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Nama Organisasi
      </div>
      <div class='col-md-4'>
       <?php echo $nama_organisasi ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Email
      </div>
      <div class='col-md-4'>
       <?php echo $email ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Telp / HP
      </div>
      <div class='col-md-4'>
       <?php echo $no_telp ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Jalan
      </div>
      <div class='col-md-4'>
       <?php echo $jalan ?>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kelurahan
      </div>
      <div class='col-md-4'>
       <?php echo $kelurahan ?>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kota
      </div>
      <div class='col-md-4'>
       <?php echo $kota ?>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kecamatan
      </div>
      <div class='col-md-4'>
       <?php echo $kecamatan ?>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Provinsi
      </div>
      <div class='col-md-4'>
       <?php echo $provinsi ?>
      </div>
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>       
       <?php if ($this->session->userdata('hak_akses') == 'Admin') { ?>
        <button id="" class="btn" onclick="Organisasi.back()">Kembali</button>
       <?php } ?>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
