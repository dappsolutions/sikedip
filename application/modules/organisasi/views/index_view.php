<div class='container-fluid'>

 <div class="white-box stat-widget">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-4'>
     <button id="" class="btn btn-warning" onclick="Organisasi.add()">Tambah</button>
    </div>
    <div class='col-md-8'>
     <input type='text' name='' id='' onkeyup="Organisasi.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <thead>
        <tr class="bg-success">
         <th class="">No</th>
         <th class="">Nama Organisasi</th>
         <th class="">Kecamatan</th>
         <th class="">Kelurahan</th>
         <th class="">Kota</th>
         <th class="">Jalan</th>
         <th class="">Provinsi</th>
         <th class="">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['nama_organisasi'] ?></td>
           <td><?php echo $value['kecamatan'] ?></td>
           <td><?php echo $value['kelurahan'] ?></td>
           <td><?php echo $value['kota'] ?></td>
           <td><?php echo $value['jalan'] ?></td>
           <td><?php echo $value['provinsi'] ?></td>
           <td class="text-center">
            <label id="" class="btn btn-warning font-14" 
                    onclick="Organisasi.ubah('<?php echo $value['id'] ?>')">Ubah</label>
            &nbsp;
            <label id="" class="btn btn-success font-14" 
                    onclick="Organisasi.detail('<?php echo $value['id'] ?>')">Detail</label>
            &nbsp;
            <label id="" class="btn btn-danger font-14" 
                    onclick="Organisasi.delete('<?php echo $value['id'] ?>')">Hapus</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="4">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!-- <div class="row">
    column 
   <div class="col-lg-12">
    <div class="card">
     <div class="card-block">
      <div class='row'>
       <div class='col-md-4'>
        <button id="" class="btn btn-warning" onclick="Organisasi.add()">Tambah</button>
       </div>
       <div class='col-md-8'>
        <input type='text' name='' id='' onkeyup="Organisasi.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
       </div>     
      </div>        
      <br/>
      <div class='row'>
       <div class='col-md-12'>
 <?php if (isset($keyword)) { ?>
  <?php if ($keyword != '') { ?>
                Cari Data : "<b><?php echo $keyword; ?></b>"
  <?php } ?>
 <?php } ?>
       </div>
      </div>
      <br/>
      <div class='row'>
       <div class='col-md-12'>
        <div class="table-responsive">
         <table class="table table-bordered table-striped">
          <thead>
           <tr>
            <th class="text-white bg-theme">No</th>
            <th class="text-white bg-theme">Nama UPT</th>
            <th class="text-white bg-theme">Alamat</th>
            <th class="text-white bg-theme">Action</th>
           </tr>
          </thead>
          <tbody>
 <?php if (!empty($content)) { ?>
  <?php $no = 1; ?>
  <?php foreach ($content as $value) { ?>
                   <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $value['upt'] ?></td>
                    <td><?php echo $value['alamat'] ?></td>
                    <td class="text-center">
                     <button id="" class="btn btn-warning font12" 
                             onclick="Organisasi.ubah('<?php echo $value['id'] ?>')">Ubah</button>
                     &nbsp;
                     <button id="" class="btn btn-success font12" 
                             onclick="Organisasi.detail('<?php echo $value['id'] ?>')">Detail</button>
                     &nbsp;
                     <button id="" class="btn btn-danger font12" 
                             onclick="Organisasi.delete('<?php echo $value['id'] ?>')">Hapus</button>
                     &nbsp;
                    </td>
                   </tr>
  <?php } ?>
 <?php } else { ?>
               <tr>
                <td class="text-center" colspan="4">Tidak Ada Data Ditemukan</td>
               </tr>
 <?php } ?>         
          </tbody>
         </table>
        </div>
       </div>
      </div>    
      <br/>
      <div class="row">
       <div class="col-md-12">
        <div class="pagination">
 <?php echo $pagination['links'] ?>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>-->
</div>