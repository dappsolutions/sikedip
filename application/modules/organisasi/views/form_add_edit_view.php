
<style>
 .pac-card {
  margin: 10px 10px 0 0;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  background-color: #fff;
  font-family: Roboto;
 }

 #pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
 }

 .pac-controls {
  display: inline-block;
  padding: 5px 11px;
 }

 .pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
 }

 #title {
  color: #fff;
  background-color: #4d90fe;
  font-size: 25px;
  font-weight: 500;
  padding: 6px 12px;
 }

 #description {
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
 }

 #infowindow-content .title {
  font-weight: bold;
 }

 #infowindow-content {
  display: none;
 }

 #map #infowindow-content {
  display: inline;
 }
</style>
<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box stat-widget">
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>

     <div class='row manual_add <?php echo!isset($foto) ? '' : 'display-none' ?>'>            
      <div class='col-md-3'>
       Foto
      </div>
      <div class='col-md-4'>
       <div class="fileinput fileinput-new input-group manual_upload" data-provides="fileinput">
        <div class="form-control" data-trigger="fileinput"> 
         <i class="glyphicon glyphicon-file fileinput-exists"></i> 
         <span class="fileinput-filename"></span>
        </div> 
        <span class="input-group-addon btn btn-default btn-file"> 
         <span class="fileinput-new" onclick="Organisasi.upload(this)">Select file</span> 
         <input type="file" style="display: none;" id="file" onchange="Organisasi.getFilename(this)"/>
        </span> 
       </div>
      </div>
     </div>
     <div class='row manual_detail <?php echo!isset($foto) ? 'display-none' : '' ?>'>
      <div message="Ganti Foto" class='col-md-3 '>
       <?php
       $img = base_url() . 'assets/images/no_available.png';
       if (isset($foto)) {
        if ($foto == '') {
         $img = base_url() . 'assets/images/no_available.png';
        } else {
         $img = base_url() . 'files/berkas/pengurus/' . $foto;
        }
       }
       ?>
       <img data-toggle="tooltip" title="Klik untuk Ganti File" data-placement="bottom" src="<?php echo $img ?>" height="180" width="180" class="hover" onclick="Organisasi.changeManual(this)"/>       
      </div>
      <div class='col-md-4'>
       <a href="#" onclick="Organisasi.showFoto(this, event)" class="label label-danger"><?php echo $foto ?></a>        
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nama Organisasi
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='nama_organisasi' class='form-control required' 
              value='<?php echo isset($nama_organisasi) ? $nama_organisasi : '' ?>' error="Nama Organisasi"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Email
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='email' class='form-control required' 
              value='<?php echo isset($email) ? $email : '' ?>' error="Email"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Telp / HP
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='no_telp' class='form-control required' 
              value='<?php echo isset($no_telp) ? $no_telp : '' ?>' error="No Telp / No HP"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Jalan
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='jalan' class='form-control required' 
              value='<?php echo isset($jalan) ? $jalan : '' ?>' error="Jalan"/>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Provinsi
      </div>
      <div class='col-md-4'>
       <select id="provinsi" error="Provinsi" class="form-control required" onchange="Masjid.getKota(this)">
        <option>Pilih Provinsi</option>
        <?php if (!empty($list_provinsi)) { ?>
         <?php foreach ($list_provinsi as $value) { ?>
          <?php $selected = ''; ?>
          <?php if (isset($provinsi)) { ?>
           <?php $selected = $value->nama == $provinsi ? 'selected' : ''; ?>
          <?php } ?>
          <option id="<?php echo $value->id ?>" <?php echo $selected ?> value="<?php echo $value->nama ?>"><?php echo $value->nama ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Kota
      </div>
      <div class='col-md-4' id="list_kota">
       <select id="kota" error="Kota" class="form-control required" onchange="Masjid.getKecamatan(this)">
        <option>Pilih Kota</option>
        <?php if (!empty($list_kota)) { ?>
         <?php foreach ($list_kota as $value) { ?>
          <?php $selected = ''; ?>
          <?php if (isset($kota)) { ?>
           <?php $selected = $value->nama == $kota ? 'selected' : ''; ?>
          <?php } ?>
          <option id="<?php echo $value->id ?>" <?php echo $selected ?> value="<?php echo $value->nama ?>"><?php echo $value->nama ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Kecamatan
      </div>
      <div class='col-md-4' id="list_kecamatan">
       <select id="kecamatan" error="Kota" class="form-control required">
        <option>Pilih Kecamatan</option>
        <?php if (!empty($list_kec)) { ?>
         <?php foreach ($list_kec as $value) { ?>
          <?php $selected = ''; ?>
          <?php if (isset($kecamatan)) { ?>
           <?php $selected = $value->nama == $kecamatan ? 'selected' : ''; ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value->nama ?>"><?php echo $value->nama ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <br/>   
     
     <div class="row">
      <div class='col-md-3'>
       Kelurahan
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='kelurahan' class='form-control required' 
              value='<?php echo isset($kelurahan) ? $kelurahan : '' ?>' error="Kelurahan"/>
      </div>
     </div>
     <br/>
     
     <?php if (isset($koordinat_map)) { ?>
      <?php if ($koordinat_map == '') { ?>
       <div class='row'>
        <div class='col-md-6'>
         <label class="badge badge-danger" onclick="Masjid.setLokasi(this)">Setting Lokasi</label>
        </div>
       </div>
      <?php } else { ?>
       <div class='row'>
        <div class='col-md-6'>
         <label class="badge badge-success" onclick="Masjid.setLokasi(this, '<?php echo 'with_map' ?>')">Lokasi Sudah Tersetting</label>
        </div>
       </div>
      <?php } ?>
     <?php } else { ?>
      <div class='row'>
       <div class='col-md-6'>
        <label class="badge badge-danger" onclick="Masjid.setLokasi(this)">Setting Lokasi</label>
       </div>
      </div>
     <?php } ?>
     <br/>
     
     <div class='row'>
      <div class='col-md-12'>
       <div class='card'>
        <div class='card-body'>
         <div class="pac-card" id="pac-card">
          <div>
           <div id="title">
            Setting Lokasi
           </div>
           <div id="type-selector" class="pac-controls">
            <input type="radio" name="type" id="changetype-all" checked="checked">
            <label for="changetype-all">All</label>

            <input type="radio" name="type" id="changetype-establishment">
            <label for="changetype-establishment">Establishments</label>

            <input type="radio" name="type" id="changetype-address">
            <label for="changetype-address">Addresses</label>

            <input type="radio" name="type" id="changetype-geocode">
            <label for="changetype-geocode">Geocodes</label>
           </div>
           <div id="strict-bounds-selector" class="pac-controls">
            <input type="checkbox" id="use-strict-bounds" value="">
            <label for="use-strict-bounds">Strict Bounds</label>
           </div>
          </div>
          <div id="pac-container">
           <input id="pac-input" type="text" class="form-control"
                  placeholder="Enter a location">
          </div>
         </div>
         <div id="map" style="height: 400px;"></div>
         <input type='hidden' name='' id='lokasi' class='form-control' value=''/>
         <div id="infowindow-content">
          <img src="" width="16" height="16" id="place-icon">
          <span id="place-name"  class="title"></span><br>
          <span id="place-address"></span>
         </div>
        </div>
       </div>
       <hr/>
       <div class='card'>
        <div class='card-body text-right'>
         <input type='hidden' name='' id='koordinat' class='form-control' value='<?php echo isset($koordinat) ? $koordinat : '' ?>'/>
         <input type='hidden' name='' id='latitude' class='form-control' value='<?php echo isset($latitude) ? $latitude : '' ?>'/>
         <input type='hidden' name='' id='longitude' class='form-control' value='<?php echo isset($longitude) ? $longitude : '' ?>'/>
         <!--<button id="" class="btn btn-success" onclick="Masjid.simpanLokasi()">Simpan Lokasi</button>-->
        </div>
       </div>
      </div>
     </div>
     
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Organisasi.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <?php if ($this->session->all_userdata('hak_akses') == 'Admin') { ?>
        <button id="" class="btn" onclick="Organisasi.back()">Kembali</button>
       <?php } ?>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
