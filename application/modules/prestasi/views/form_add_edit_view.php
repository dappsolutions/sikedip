<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-success">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title"><?php echo 'Prestasi' ?> <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Kategori</label>
           <div class="col-md-9">
            <select id='bidang' class="form-control required" error='Bidang'>
             <?php if (!empty($list_bidang)) { ?>
              <?php foreach ($list_bidang as $value) { ?>
               <?php $selected = '' ?>
               <?php if (isset($bidang_penghargaan)) { ?>
                <?php $selected = $value['id'] == $bidang_penghargaan ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['bidang_penghargaan'] ?></option>
              <?php } ?>
             <?php } else { ?>
              <option value="">Tidak Ada Data</option>  
             <?php } ?>
            </select>
           </div>
          </div>
         </div>
        </div>
        
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Prestasi</label>
           <div class="col-md-9">
            <input value="<?php echo isset($prestasi) ? $prestasi : '' ?>" type="text" id='prestasi' class="form-control required" error='Prestasi' placeholder="Prestasi"></div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Skor</label>
           <div class="col-md-9">
            <input value="<?php echo isset($skor) ? $skor : '' ?>" type="text" id='skor' class="form-control required" error='Skor' placeholder="Skor"></div>
          </div>
         </div>
        </div>        


        <div class="form-actions">
         <div class="row">
          <div class="col-md-12">
           <div class="row">
            <div class="col-md-offset-3 col-md-9 text-right">
             <button type="submit" class="btn btn-success" onclick="Prestasi.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
             <button type="button" class="btn btn-default" onclick="Prestasi.back()">Batal</button>
            </div>
           </div>
          </div>
          <div class="col-md-6"> </div>
         </div>
        </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
 <!--./row--> 
</div>
