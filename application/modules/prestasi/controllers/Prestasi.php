<?php

class Prestasi extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'prestasi';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/prestasi.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'prestasi';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Prestasi";
  $data['title_content'] = 'Data Prestasi';
  $content = $this->getDataPrestasi();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPrestasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.prestasi', $keyword),
       array('p.skor', $keyword),
       array('b.bidang_penghargaan', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'b.bidang_penghargaan as bidang'),
              'join' => array(
                  array('bidang_penghargaan b', 'b.id = p.bidang_penghargaan')
              ),
              'where' => "p.deleted = 0"
  ));

  return $total;
 }

 public function getDataPrestasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.prestasi', $keyword),
       array('p.skor', $keyword),
       array('b.bidang_penghargaan', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'b.bidang_penghargaan as bidang'),
              'join' => array(
                  array('bidang_penghargaan b', 'b.id = p.bidang_penghargaan')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPrestasi($keyword)
  );
 }

 public function getDetailDataPrestasi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'b.bidang_penghargaan as bidang'),
              'join' => array(
                  array('bidang_penghargaan b', 'b.id = t.bidang_penghargaan')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListBidang() {
  $data = Modules::run('database/get', array(
              'table' => 'bidang_penghargaan b',
              'field' => array('b.*'),
              'where' => "b.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Prestasi";
  $data['title_content'] = 'Tambah Prestasi';
  $data['list_bidang'] = $this->getListBidang();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPrestasi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Prestasi";
  $data['title_content'] = 'Ubah Prestasi';
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPrestasi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Prestasi";
  $data['title_content'] = "Detail Prestasi";
  echo Modules::run('template', $data);
 }

 public function getPostDataPrestasi($value) {
  $data['bidang_penghargaan'] = $value->bidang;
  $data['prestasi'] = $value->prestasi;
  $data['skor'] = $value->skor;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_prodi = $this->getPostDataPrestasi($data);

   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_prodi);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_prodi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Prestasi";
  $data['title_content'] = 'Data Prestasi';
  $content = $this->getDataPrestasi($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
